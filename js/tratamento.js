// $(document).ready(function() {
// 	buscaTratamento();
// });

// $(function () {
//   $("#example1").DataTable();
//   $('#example2').DataTable({
//     "paging": true,
//     "lengthChange": false,
//     "searching": false,
//     "ordering": true,
//     "info": true,
//     "autoWidth": false
//   });
// });

function buscaTratamento() {
	var baseURL = "<?php print base_url()?>";
	$.ajax({
		type: "POST",
		url: "index.php/admin/buscaTratamento",
		dataType: "json",
		success: function(data) {
			for (var i = 0; i < data.tratamento.length; i++) {
				var body = "<tr>";
				body += "<td>" + data.tratamento[i] + "</td>";
				body +=
					"<td id='tableTd'><button type='button' id='tableBtnEditar' class='btn btn-info btn-sm'><i class='fas fa-edit'></i></button> </td>";
				body +=
					"<td id='tableTd'><button type='button' id='tableBtnExcluir' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button> </td>";
				body += "</tr>";
				$("#example1 tbody").append(body);
			}
			$("#example1").DataTable();
			$("#example2").DataTable({
				paging: true,
				lengthChange: false,
				searching: false,
				ordering: true,
				info: true,
				autoWidth: false
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
}
