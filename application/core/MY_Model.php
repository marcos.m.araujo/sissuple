<?php
/**
 * Model Básico com funções de CRUD
 * relacionamentos
 * 
 * $this->user_model->where('email','email@email.com')->update($update_data);
 */
class MY_Model extends CI_Model
{
    protected $_table;
    public $_database;
    protected $primary_key;
   
    public function __construct()
    {
        parent::__construct();

    }

    protected function findTableName($name){
        switch($name){
            case 'GetPaciente':
                return 'viewPaciente';
                break;
            case 'SetPaciente':
                return 'tblPaciente';
                break;
            case 'Plano':
                return 'tblplanosaude';
                break;
            case 'PlanoContato':
                return 'tblPlanosSaudeContato';
                break;
            case 'Laudo':
                return 'tblLaudoElaboracao';
                break;
            case 'Cirurgia':
                return 'tblLaudoElaboracaoDadosCirurgia';
                break;      
            case 'Hospital':
                return 'tblHospitais';
                break;
            case 'HospitalContato':
                return 'tblHospitaisContato';
                break;
            case 'Tratamento':
                return 'tblTratamentos';
                break;
            case 'Sintomas':
                return 'tblSintomas';
                break;
            case 'SintomasLaudo':
                return 'tblLaudoElaboracaoPedidoSintomas';
                break;
            case 'DiagnosticoLaudo':
                return 'tblLaudoElaboracaoPedidoDiagnosticos';
                break;
            case 'Diagnostico':
                return 'tblDiagnosticos';
                break;
            case 'CID':
                return 'tblCID';
                break;
            case 'CIDLaudo':
                return 'tblLaudoElaboracaoPedidoCIDs';
                break;
            case 'Intervencao':
                return 'tblIntervencao';
                break;
            case 'IntervencaoLaudo':
                return 'tblLaudoElaboracaoPedidoIntervencoes';
                break;      
            case 'Procedimento':
                return 'tblProcedimentos';
                break;
            case 'ProcedimentoLaudo':
                return 'tblLaudoElaboracaoPedidoProcedimentos';
                break;
            case 'Objetivos':
                return 'tblAdminObjetivosTratmento';
                break;
            case 'ObjetivosLaudo':
                return 'tblLaudoElaboracaoPedidoObjtivos';
                break;
            case 'Sequelas':
                return 'tblsequelas';
                break;
            case 'SequelasLaudo':
                return 'tblLaudoElaboracaoPedidoSequelas';
                break;
            case 'Marcas':
                return 'tblLaudoMarca';
                break;
            case 'Fornecedores':
                return 'tblFornecedores';
                break;
            case 'Opme':
                return 'viewOpmes';
                break;
            case 'Laudos':
                return 'viewLaudoElaboracao';
                break;
            case 'Planos':
                return 'tblplanosaude';
                break;
            case 'Cirurgiao':
                return 'tblCirurgiao';
                break;
            case 'vPaciente':
                return 'viewPaciente';
                break;
            case 'Paciente':
                return 'tblPaciente';
                break;
            case 'Cirurgioes':
                return 'tblCirurgiao';
                break;
            case 'Compartilhamento':
                return 'tblCompartilhamentoLaudoModelo';
                break;
            case 'SetMaterial':
                return 'tblLaudoElaboracaoMaterial';
                break;  
        }
    }

   
    function verificaErroQuery($query)
    {
        if (!$query) {
            $db_error = $this->_database->error();
            throw new Exception('Erro ao tentar consultar o Banco de Dados. ' . $db_error['code'] . ': ' . $db_error['message']);
            return false; // unreachable retrun statement !!!
        } 
        
        // else if(empty($query->result())){
        //     throw new Exception('Erro ao tentar consultar o Banco de Dados. Dados Vazios');
        //     return false; 
        // }

        return true;
    }
}