<?php

defined('BASEPATH') or exit('No direct script access allowed');
/**
 * REST API
 * 
 * REpresetation State Transfer
 *
 * Padrao de arquitetura cliente-servidor 
 * para sistemas distribuídos de hipermedia
 * 
 * Baseado nos Verbos HTTP: GET, PUT, POST, DELETE
 * URIs: nome da funcionalidade
 * resposta HTTP: status, dados 
 * 
 * Funcionalidade: UF
 * Servico: informacões da UF
 * Representação: Nome, Sigla, 
 * 
 * @api DPP/API
 * @param HTTP
 * @return JSON
 * 
 * @author Roy Fielding, em sua tese de doutorado
 * @copyright 2000 © Roy Thomas Fielding, 2000. All rights reserved
 * @source https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm
 */
class API extends CI_Controller
{
    /**
     * Construtor
     * 
     * @uses CORE::database 
     * @uses API_Model
     */
    function __construct()
    { 
        parent::__construct();
        $this->load->database();
        $this->load->model('API_Model');
    }

    /**
     * Funcao global get
     * 
     * 
     * @param POST
     * @example HTTP/POST params{
     *   table: 'UF',
     *   column: 'Sigla',
     *   where: {Id: 4}
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: {0:{Sigla:'BA'}}
     * }
     * @uses API_Model get()
     */
    public function get()
    {       
        echo json_encode(
            $this->API_Model->get(
                $this->input->post(NULL, true)
            )
        );
    }

    /**
     * Funcao global insert
     * @api DPP/API/insert
     * 
     * 
     * @param POST
     * @example HTTP/POST params{
     *   table: 'UF',
     *   data: {
     *      Nome: 'Maranhão',
     *      Sigla: 'MA'
     *   }
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: 'dados inseridos com suceeso'
     * }
     * @uses API_Model insert()
     */
    public function insert(){
        
        $params = (array) json_decode($this->input->post('Dados', true));
        $params['data'] = (array) $params['data'];

        if(sizeof($_FILES) > 0){
            $this->uploadFiles($params);
        }

        echo json_encode($this->API_Model->insert($params));
        die;
    }

    
    /**
     * Funcao global update
     * @api APP/API/update
     * 
     * POST Request
     * @example HTTP/POST params{
     *   table: 'UF',
     *   data: {
     *      Nome: 'Maranhão',
     *      Sigla: 'MA'
     *   }
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: 'dados atualizados com suceeso'
     * }
     * @uses API_Model update()
     */
    public function update(){
        $params = (array) json_decode($this->input->post('Dados', true));
        $params['data'] = (array) $params['data'];
        $params['where'] = (array) $params['where'];

        if(sizeof($_FILES) > 0){
            $this->uploadFiles($params);
        }

        echo json_encode($this->API_Model->update($params));
        die;

        // echo json_encode(
        //     $this->API_Model->update($this->input->post(NULL, true)));
    }


    /**
     * Funcao global uploadFile
     * 
     * @param Array  by reference
     * 
     * @return void
     */
    public function uploadFiles(&$params){
        
        foreach($_FILES as $index => $value){

            if (!isset($params['data']['FilesRules'])) {
                echo json_encode([
                    'status' => false,
                    'result' => 'As regras de upload não foram definidas.'
                ]);
                die;
            }

            if (isset($_FILES[$index]['name'])) {

                $NomeArquivo = $_FILES[$index]['name'];
                $ext = @end(explode(".", $NomeArquivo));

                // verifica se o diretorio existe, se não ele cria
                if (!is_dir($params['data']['FilesRules']->upload_path)){
                    mkdir($params['data']['FilesRules']->upload_path, 0755, true);
                }
                $config['upload_path'] = $params['data']['FilesRules']->upload_path;
                $config['allowed_types'] = $params['data']['FilesRules']->allowed_types;

                $novoNome = random_bytes(10);
                $novoNomeRandom = bin2hex($novoNome) . "." . $ext;

                $pathFile = base_url($params['data']['FilesRules']->upload_path) . $novoNomeRandom;

                $config['file_name'] = $novoNomeRandom;

                $this->upload->initialize($config);
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload($index)) {
                    echo json_encode([
                        'status' => false,
                        'result' => $index . '->' . $this->upload->display_errors()
                    ]);
                    die;
                } else {
                    $params['data'][$index] = $pathFile;
                    unset($params['data']['FilesRules']);
                }
            }   
        }
    }

    public function deleteFile(){
        $path = $this->input->post('path', true);
        $path = str_replace(base_url(), "", $path);
        
        if( file_exists($path) ){ 
            // Remove file 
            unlink($path); 
      
            // Set status 
            echo json_encode(true); 
         }else{ 
            // Set status 
            echo json_encode(false); 
         } 
         die;
    }    



     /**
     * Funcao global update
     * @api APP/API/delele
     * 
     * POST Request
     * @example HTTP/POST params{
     *   table: 'UF',
     *   data: {
     *      Nome: 'Maranhão',
     *      Sigla: 'MA'
     *   }
     * }
     * 
     * @return JSON
     * @example {
     *    status: true,
     *    response: 'dados atualizados com suceeso'
     * }
     * @uses API_Model update()
     */
    public function delete(){
        $params = (array) json_decode($this->input->post('Dados', true));     
        $params['where'] = (array) $params['where'];

        if(sizeof($_FILES) > 0){
            $this->uploadFiles($params);
        }

        echo json_encode($this->API_Model->delete($params));
        die;

        // echo json_encode(
        //     $this->API_Model->update($this->input->post(NULL, true)));
    }
}