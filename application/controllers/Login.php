<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        // $this->load->database();
        $this->load->model('Login_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Cirurgiao_model');
        // $this->load->model('Clinica_model');
        $this->load->model('Termos_model');
        $this->load->library('phpmailer_lib');
    }

    public function index() {
        $_SESSION['cadastroRealizado'] = '';
        $this->load->view('login/index.php');
    }

    public function login() {
        $dados = $this->input->post(NULL, TRUE);
        $dados['usr'] = $this->Login_model->consultaLogin($dados);
        $retorno = array(
            "username" => $dados['usr']['nomeUsuario'],
            "email" => $dados['usr']['email'],
            "codigoPerfil" => $dados['usr']['codigoPerfil'],
            "ativo" => $dados['usr']['ativado'],
            'codigoUsuario' => $dados['usr']['codigoUsuario'],
            'logged_in' => TRUE
        );
        $this->session->set_userdata($retorno, 3);

        echo json_encode($retorno);
    }

    public function loginApp() {
        header("Access-Control-Allow-Origin: *");
        $data = json_decode(file_get_contents("php://input"));
        $dados['email'] = $data->email;
        $dados['senha'] = $data->senha;
        $result = $this->Login_model->consultaLogin($dados);

        echo json_encode($result);
    }

    public function destroy() {
        $this->session->sess_destroy();
        redirect('Login');
    }

    public function registro() {
        $dados = array(
            'conselhoProfissional' => $this->Cirurgiao_model->buscaConselhoProfissional(),
            'especialidades' => $this->Cirurgiao_model->buscaEspecilidade(),
            'dadosCirurgiao' => $this->Cirurgiao_model->getCirurgiao($this->session->codigoUsuario),
            'termo' => $this->Termos_model->get(),
            'estados' => $this->Cirurgiao_model->buscaUF(),
            'validacaoSenha' => ''
        );
        $this->load->view('login/registro.php', $dados);
    }

    public function bemVindo() {
        $dados = $this->input->post_get(NULL, TRUE);
        $_SESSION['codigoUsuario'] = $dados['codigoUsuario'];
        $_SESSION['email'] = $dados['email'];
        $_SESSION['nome'] = $dados['nome'];
        if ($this->enviarEmail($dados['email'], $dados['nome'], $dados['codigoUsuario'])) {
            $this->load->view('login/bemVindo');
        }
    }

    public function enviarEmail($email_, $nome, $codigoUsuario) {
        $numero_de_bytes = 3;
        $restultado_bytes1 = random_bytes($numero_de_bytes);
        $dados['token'] = bin2hex($restultado_bytes1);
        $dados['codigoUsuario'] = $codigoUsuario;
        $this->Cirurgiao_model->insertToken($dados);
        $email['email'] = $email_;
        $email['assunto'] = 'Bem vindo ao eBuco!!';
        $email['texto'] = '<div style="text-align: center"><h3>Olá ' . $nome . ', </h3>'
                . '<p>Para utilizar o servico do eBuco, é necessário que confirme o código de segurança<br>'
                . 'para garantir a integridade dos seus dados.</p>'
                . '<h4>Código de Ativação<br><b>' . $dados['token'] . '</b></h4></div>';
        $send = $this->phpmailer_lib->send($email);
        return $send;
    }

    public function reenviarEmail() {
        if ($this->enviarEmail($this->session->email, $this->session->nome, $this->session->codigoUsuario)) {
            $this->load->view('login/bemVindo');
        }
    }

    public function recuperaSenha() {
        $dados = $this->input->post(NULL, TRUE);
        $novaSenha = $this->Login_model->recuperaSenha($dados);
        if (!empty($novaSenha)) {
            echo '<style> '
            . 'body{ background-image: -webkit-linear-gradient( 136deg, rgb(116,235,213) 0%, rgb(63,43,150) 100%);  '
            . 'background-image: -ms-linear-gradient( 136deg, rgb(116,235,213) 0%, rgb(63,43,150) 100%);'
            . 'text-align: center; color: #fff}'
            . '</style>'
            . '<div><br><br><br>'
            . '<h1>Senha alterada com sucesso!</h1>'
            . '</div>'
            . '<h2>Enviamos um e-mail com uma nova senha provisória válida por 72 horas.</h2><br>'
            . '<a style="color: #fff" href="'.base_url('Login').'">Voltar ao Login</a>';
        } else {
            echo '<style> '
            . 'body{ background-image: -webkit-linear-gradient( 136deg, rgb(116,235,213) 0%, rgb(63,43,150) 100%);  '
            . 'background-image: -ms-linear-gradient( 136deg, rgb(116,235,213) 0%, rgb(63,43,150) 100%);'
            . 'text-align: center; color: #fff}'
            . '</style>'
            . '<div><br><br><br>'
            . '<img style="width:25%;" src="https://img.icons8.com/material-two-tone/1600/mail-error.png">'
            . '<h1 style="color: #8A0808">Ocorreu um erro ao tentar recuperar a senha, verifique o e-mail informado.</h1>'
            . '<p>suporte@ebuco.com.br</p>'
            . '</div>';
        }
        $email['email'] = $dados['email'];
        $email['assunto'] = 'Recuperacao de senha.';
        $email['texto'] = '<div style="text-align: center"><h3>' . $dados['email'] . ' </h3>'
                . '<p>Senha alterada com sucesso, sua nova senha provisória para acesso ao ebuco é : <b><br>' . $novaSenha . '</b>. <br> Válida por 72 horas, troque asssim que possível.</p></div>';
        $send = $this->phpmailer_lib->send($email);
    }

    public function alterarSenha(){
        $dados = $this->input->post(NULL,TRUE);

        if (empty($this->session->username) || (!array_key_exists('senha',$dados) || empty($dados['senha']))) {
            redirect('Login/destroy');
        }

        $dados['codigoUsuario'] = $this->session->codigoUsuario;
        $result = $this->Login_model->updateSenha($dados);
        die(json_encode(['result' => $result]));
    }

}
