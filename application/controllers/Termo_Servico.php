<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Termo_Servico extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Termos_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');
        //$this->load->helper('funcoes');
        //verificaLogin();
        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function index() {
         $dados['termo'] = $this->Termos_model->get();
   
        $dados['pagina'] = 'admin/termo_servico_view';
        $this->load->view('html/index_view', $dados);
    }
    
    public function save(){
        $dados['termo'] = $this->input->post('editor',null);      
        $result = $this->Termos_model->delete();
        if($result == true){
              $result = $this->Termos_model->create($dados);
        }
        $this->index();
    }

}
