<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();    
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');

        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function index()
    {
        if ($this->session->codigoPerfil == 1) {
            $dados['pagina'] = 'pacientes/index';
            $dados['sidemenu'] = 'pacientes/sidemenu';
            $this->load->view('app_template/index_view', $dados);
        }
    }
   
}
