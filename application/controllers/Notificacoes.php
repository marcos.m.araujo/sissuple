<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Notificacoes extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->library('phpmailer_lib');
        $this->load->model('EditorLaudo/LaudoElaboracaoModel', 'pedidoAutorizacao');
        if (empty($this->session->username)) {
            redirect('Login');
        }
    }
    public function prepararNotificacoes()
    {
        $post = $this->input->post(null, true);
        $viewLaudo = $this->pedidoAutorizacao->getModel('CodigoPlano, nomePaciente,tratamentos, email,hospital, estado', 'viewLaudoElaboracao', 'codigoLaudoElaboracaoCRIP',$post['codigoLaudo']);
        $nomePlano = $this->pedidoAutorizacao->getModel('nomePlano', 'tblplanosaude', 'CodigoPlano',  $viewLaudo[0]['CodigoPlano']);
        $emailPlano = $this->pedidoAutorizacao->getModel('email', 'tblPlanosSaudeContato', 'codigoPlano', $viewLaudo[0]['CodigoPlano']);
        $codigoHospital = $this->pedidoAutorizacao->getModel('codigoHospital', 'tblHospitais', 'nome', $viewLaudo[0]['hospital'], 'uf', $viewLaudo[0]['estado']);
        $emailHospital = $this->pedidoAutorizacao->getModel('email', 'tblHospitaisContato', 'codigoHospital', $codigoHospital[0]['codigoHospital']);

        if ($post['notificarPaciente']) {
            //notifica paciente
            $this->phpmailer_lib->notificarMovimentacao(
                $viewLaudo[0]['email'],
                $viewLaudo[0]['nomePaciente'],
                $viewLaudo[0]['nomePaciente'],
                $this->session->username,
                $viewLaudo[0]['tratamentos'],
                $nomePlano[0]['nomePlano'],
                md5($this->session->codigoLaudoElaboracao)
            );
        }
        if ($post['notificarHospital']) {
            //notifica hospital
            foreach ($emailHospital as $key => $value) {
                $this->phpmailer_lib->notificarMovimentacao(
                    $value['email'],
                    $value['email'],
                    $viewLaudo[0]['nomePaciente'],
                    $this->session->username,
                    $viewLaudo[0]['tratamentos'],
                    $nomePlano[0]['nomePlano'],
                    md5($this->session->codigoLaudoElaboracao)

                );
            }
        }

        //notifica cirgiao
        $this->phpmailer_lib->notificarMovimentacao(
            $this->session->email,
            $this->session->username,
            $viewLaudo[0]['nomePaciente'],
            $this->session->username,
            $viewLaudo[0]['tratamentos'],
            $nomePlano[0]['nomePlano'],
            md5($this->session->codigoLaudoElaboracao)
        );

        echo true;
    }
}
