<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Share extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('ShareReport/ShareModel', 'share');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('phpmailer_lib');
        $this->load->library('session');
        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function index()
    {
        $dados['pagina'] = 'compartilhado/index';
        $dados['sidemenu'] = 'compartilhado/sidemenu';
        $this->load->view('app_template/index_view', $dados);
    }

    public function sendEmailModeloLaudo()
    {
        $dados['email'] = $this->input->post("emails", true);
        $dados['codigoLaudoElaboracao'] = $this->input->post("codigo", true);
        $dados['codigoCirurgiao'] = $this->input->post("codigoCirurgiao", true);
        date_default_timezone_set("Brazil/East");
        $dados['dataCompartilhamento'] = date("Y-m-d H:i:s");
        if ($this->share->insertCompartilhamentoModelo($dados)) {
            $this->emailShare($dados['email']);
        }
        echo json_encode('ok');
    }

    public function getShereReportModel()
    {
        echo  json_encode($this->share->getShereReportModel());
    }

    function emailShare($param)
    {
        $email['email'] = $param;
        $email['assunto'] = 'Compartilhamento de laudo bucomaxilofacial';
        $email['texto'] = '<div 
            style="
                font-size:12pt;
                padding: 40px;
                text-align: justify; 
                width: 900px; 
                margin-left: 0%;
                color: #666;"            
                >
                Dr.(a) ' . $this->session->username . ' compartilhou um laudo com você.</h3>
                <img style="width: 20%; margin-left: -250px!important" src="https://ebuco.com.br/newtheme//img/ebuco.png">               
                    <p style="line-height: 1.2;">
                        Se ainda não tem cadastro no eBuco poderá cadastra-se ciclando <b><a href="https://ebuco.com.br/Site/cadastroCirurgiao">aqui</a></b>.<br> 
                        Se possui cadastro o laudo estará disponível em <b>Meus Laudos Compartilhados</b>.                     
                    </p>                                            
                    <div style="margin-left: 0%">
                        Atenciosamente<br><b>Atendimento Digital | eBuco</b> 
                    </div>             
                     <p>
                        <img style="width: 15px" src="https://logodownload.org/wp-content/uploads/2015/04/whatsapp-logo-1.png"> (61) 981831130<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Facebook_logo_%28square%29.png/600px-Facebook_logo_%28square%29.png"> Ebuco Maxilo<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/1024px-Instagram_logo_2016.svg.png"> ebuco_brasil<br>
                        <img style="width: 15px" src="https://www.ocomuniqueiro.com.br/wp-content/uploads/2018/01/Email-Logo.jpg"> laudo@ebuco.com.br
                    </p>
                   <a href="https://ebuco.com.br">ebuco.com.br</a>
            </div>';
        $send = $this->phpmailer_lib->sendLaudo($email);
        echo $send;
    }

    public function exluirLaudo()
    {
        $dados['codigoCompartilhamentoLaudoModelo'] = $this->input->post("codigo", true);
        echo $this->share->exluirModeloShare($dados);
    }
}
