<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Site extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Cirurgiao_model');
        $this->load->model('Termos_model');
        $this->load->helper('url');
    }

    public function index() {
        $this->load->view('site/home');
    }

    public function cadastroCirurgiao() {
        $dados = array(
            'conselhoProfissional' => $this->Cirurgiao_model->buscaConselhoProfissional(),
            'especialidades' => $this->Cirurgiao_model->buscaEspecilidade(),
            'dadosCirurgiao' => $this->Cirurgiao_model->getCirurgiao($this->session->codigoUsuario),
            'estados' => $this->Cirurgiao_model->buscaUF(),
            'validacaoSenha' => ''
        );
        $this->load->view('site/cadastroCirurgicao', $dados);
    }
    
    
    public function termosUso() {
        $dados['termo'] = $this->Termos_model->get();
        $this->load->view('site/termosUSO', $dados);
    }

}
