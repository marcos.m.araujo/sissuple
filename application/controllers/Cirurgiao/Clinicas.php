<?php

class Clinicas extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Clinica_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('my_helper');
        $this->load->library('session');

        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function loadNovaClinica() {
        if ($this->session->codigoCirurgiao === NULL) {
            echo "<script>location.reload()</script>";
        }

        $dados['estados'] = $this->Clinica_model->buscaUF();
        $this->load->view('cirurgiao/dados_clinica.php', $dados);
    }

    public function getClinicas()
    {
        $clinicas = $this->Clinica_model->getClinicas($this->session->codigoCirurgiao);
        $clinicaFavorita = $this->Clinica_model->getClinicaFavorita($this->session->codigoCirurgiao);
        die(json_encode([
            'clinicas' => $clinicas,
            'clinicaFavorita' => $clinicaFavorita
        ]));
    }

    public function getClinica() {
        $codigoClinica = $this->input->post('codigoClinica', true);
        $retorno = $this->Clinica_model->getClinica($codigoClinica);
        echo json_encode($retorno);
    }

    public function loadMinhasClinicas() {
        $clinicas = (object) array();
        if ($this->session->codigoCirurgiao != NULL) {
            $codigoCirurgiao = $this->session->codigoCirurgiao;
            $clinicas = $this->Clinica_model->getClinicas($codigoCirurgiao);
            foreach ($clinicas as &$clinica) {
                $clinica->cnpj = mask($clinica->cnpj, '##.###.###/####-##');
                $clinica->telefoneComercial = mask($clinica->telefoneComercial, '(##) # ####-####');
            }
        }

        $this->load->view('cirurgiao/minhas_clinicas.php', array('clinicas' => $clinicas));
    }

    public function loadUpdateClinica() {
        $codigoClinica = $this->input->post('codigoClinica', TRUE);
        $clinica = $this->Clinica_model->getClinica($codigoClinica);
        $this->load->view('cirurgiao/update_cirurgiao/dados_clinica.php', array(
            'clinica' => $clinica,
            'estados' => $this->Clinica_model->buscaUF()
        ));
    }

    public function createClinicas() {
        $dados = $this->input->post(NULL, TRUE);
        $dados['codigoCirurgiao'] = $this->session->codigoCirurgiao;
        $dados['logo'] = ifHasFileRenameFile('logo');
        $dados = $this->tratarCamposClinica($dados);
        
        $retorno = $this->Clinica_model->createClinicas($dados);

        if ($retorno) {
            ifHasFileSendFileToUpload('logo');

            die(json_encode([
                'message' => 'Cadastrado com sucesso',
                'clinica_id' => $this->Clinica_model->getInsertedClinica()
            ]));
        }

        error(500, 'Erro ao salvar os dados');
    }

    public function deleteClinicas() {
        $codigoClinica = $this->input->post('codigoClinica', true);
        $clinica = $this->Clinica_model->getClinica($codigoClinica);
        $clinica = $this->removeOldFle($clinica,'logo');
        unset($clinica);
        $retorno = $this->Clinica_model->deleteClinicas($codigoClinica);
        echo json_encode($retorno);
    }

    public function updateClinica() {
        $dados = $this->input->post(NULL, TRUE);
        $dados = $this->tratarCamposClinica($dados);
        $dados = $this->removeOldFle($dados,'logoAnterior');
        $dados['logo'] = ifHasFileRenameFile('logo');

        $retorno = $this->Clinica_model->updateClinica($dados);

        if ($retorno) {
            ifHasFileSendFileToUpload('logo');
            die(json_encode(array('message' => 'Alterado com sucesso')));
        }

        error(500, 'Erro ao salvar os dados');
    }

    public function saveClinicaFavorita()
    {
        $dados = (object) $this->input->post(null,true);
        $params = (object) [
            'codigoCirurgiao' => $this->session->codigoCirurgiao,
            'codigoClinica' => $dados->codigoClinica
        ];

        $retorno = false;
        
        if(filter_var($dados->isNewClinicaFavorita,FILTER_VALIDATE_BOOLEAN)){
            $retorno = $this->Clinica_model->createClinicaFavorita($params);
        }else{
            $retorno = $this->Clinica_model->updateClinicaFavorita($params);
        }
        
        die(json_encode(['result' => $retorno]));
    }

    private function removeOldFle($params, $field){
        $params =  is_object($params)? (array) $params : $params;
        if(array_key_exists($field, $params) && (!is_null($params[$field]) && !empty($params[$field]))) {
            if (file_exists('./uploads/logoClinica/' . $params[$field])) {
                unlink('./uploads/logoClinica/' . $params[$field]);
            }
            unset($params[$field]);
        }
        return $params;
    }

    private function tratarCamposClinica($params)
    {
        $params['cnpj'] = str_replace(array('.', '-', '/'), array('', '', ''), $params['cnpj']);
        $params['telefoneComercial'] = str_replace(array('(', ')', ' ', '-'), array('', '', '', ''), $params['telefoneComercial']);
        $params['cep'] = str_replace(array('.', '-'), array('', ''), $params['cep']);

        return $params;
    }

}
