<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cirurgiao extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Cirurgiao_model');
        $this->load->model('Clinica_model');
        $this->load->model('Termos_model');
        $this->load->model('Login_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('my_helper');
        $this->load->library('session');
        $this->load->library('phpmailer_lib');
    }

    public function index() {
        if (empty($this->session->username)) {
            redirect('Login');
        }
        $dados = array(
            'conselhoProfissional' => $this->Cirurgiao_model->buscaConselhoProfissional(),
            'dadosCirurgiao' => $this->Cirurgiao_model->getCirurgiao($this->session->codigoUsuario),
            'termo' => $this->Termos_model->get(),
            'estados' => $this->Cirurgiao_model->buscaUF()
        );
        $_SESSION['codigoCirurgiao'] = $dados['dadosCirurgiao'][0]['codigoCirurgiao'];
        $_SESSION['dadosCompletos'] = 1;
        if (empty($dados['dadosCirurgiao'])) {
            $dados['dadosCirurgiao'] = '';
            $_SESSION['dadosCompletos'] = '';
            $nome = $this->session->username;
            $primeiroNome_ = explode(" ", $nome);
            $sobreNome_ = explode(" ", $nome, 2);
            $dados['primeiroNome'] = $primeiroNome_[0];
            $dados['sobrenome'] = $sobreNome_[1];
        } else {
            $dados['primeiroNome'] = $dados['dadosCirurgiao'][0]['nomeCirurgiao'];
            $dados['sobrenome'] = $dados['dadosCirurgiao'][0]['sobrenomeCirurgiao'];
            $_SESSION['codigoCirurgiao'] = $dados['dadosCirurgiao'][0]['codigoCirurgiao'];
            $dados = array_merge($dados, ['clinicas' => $this->Clinica_model->getClinicas($dados['dadosCirurgiao'][0]['codigoCirurgiao'])]);
        }
        $dados['pagina'] = 'cirurgiao_old/home.php';
        $this->load->view('html/index_view', $dados);
    }

    public function listaLaudos() {
        $dados['pagina'] = 'laudos/laudo_list_view.php';
        $this->load->view('html/index_view', $dados);
    }

    public function cadastrarCirurgiao() {
        $especialidades = '';
        $dados = $this->input->post(NULL, TRUE);
        $numero_de_bytes = 2;
        $restultado_bytes1 = random_bytes($numero_de_bytes);
        $dados['senha'] = bin2hex($restultado_bytes1);

        array_walk_recursive($dados, function (&$value, $key) {
            if (in_array($key, array('cpf', 'telefoneCirurgiao'))) {
                $value = str_replace(array('.', '-', ' ', '(', ')'), array('', '', '', '', '', ''), $value);
            }
        });
        $verificaEmail = $this->Cirurgiao_model->getUsuarioEmail($dados);
        if ($verificaEmail === $dados['email']) {
            redirect('Site/cadastroCirurgiao');
            die;
        } else {
            $dados['codigoUsuario'] = $this->Cirurgiao_model->insertUsuario($dados);
        }
        $retorno = $this->Cirurgiao_model->createCirurgiao($dados);
        //$retorno = $this->Clinica_model->createClinicas($dados);
        if ($retorno) {
            $_SESSION['cadastroRealizado'] = 'Cadastrado com sucesso, enviamos para seu e-mail a senha de acesso!';
            $this->enviarEmail($dados['email'], $dados['nomeCirurgiao'], $dados['senha']);
            $this->load->view('login/index.php');
        }
    }

    public function enviarEmail($email_, $nome, $senha) {
        $email['email'] = $email_;
        $email['assunto'] = 'Bem vindo ao eBuco!!';   
        $email['texto'] = '<div 
            style="
                font-size:12pt;
                padding: 40px;
                text-align: justify; 
                width: 900px; 
                margin-left: 0%;
                color: #666;"            
                >
                <h3>Olá ' . $nome . ', Bem vindo ao eBuco!</h3>
                <img style="width: 20%; margin-left: -250px!important" src="https://ebuco.com.br/newtheme//img/ebuco.png">               
                    <p style="line-height: 1.2;">
                       Senha de acesso ao eBuco<br> 
                       <h4>Código de Acesso<br><b>' . $senha . '</b></h4>                    
                    </p>                                            
                    <div style="margin-left: 0%">
                        Atenciosamente<br><b>Atendimento Digital | eBuco</b> 
                    </div>             
                     <p>
                        <img style="width: 15px" src="https://logodownload.org/wp-content/uploads/2015/04/whatsapp-logo-1.png"> (61) 981831130<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Facebook_logo_%28square%29.png/600px-Facebook_logo_%28square%29.png"> Ebuco Maxilo<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/1024px-Instagram_logo_2016.svg.png"> ebuco_brasil<br>
                        <img style="width: 15px" src="https://www.ocomuniqueiro.com.br/wp-content/uploads/2018/01/Email-Logo.jpg"> laudo@ebuco.com.br
                    </p>
                   <a href="https://ebuco.com.br">ebuco.com.br</a>
            </div>';


        $send = $this->phpmailer_lib->send($email);
        return $send;
    }

    public function insertAceiteTermo() {
        $dados['codigoAceite'] = $this->input->post('codigoAceite', TRUE);
        $dados['codigoCirurgiao'] = $this->session->codigoCirurgiao;
        if (empty($dados['codigoAceite'])) {
            $dados['codigoAceite'] = 1;
        }
        $existeTermo = $this->Cirurgiao_model->getAceiteTermo($this->session->codigoCirurgiao);
        if (empty($existeTermo)) {
            $retorno = $this->Cirurgiao_model->insertAceiteTermo($dados);
        } else {
            $retorno = $this->Cirurgiao_model->updateAceiteTermo($dados);
        }
        if ($retorno == true) {
            redirect('Home');
        } else {
            $this->index();
        }
    }

    public function updateCirurgiao() {
        $dados = $this->input->post(NULL, TRUE);
        array_walk_recursive($dados, function (&$value, $key) {
            if (in_array($key, array('cpf', 'telefoneCirurgiao'))) {
                $value = str_replace(array('.', '-', ' ', '(', ')'), array('', '', '', '', '', ''), $value);
            }
        });

        if (!empty($_FILES['fotoPerfil']['tmp_name'])) {
            $ext = substr($_FILES['fotoPerfil']['name'], -4);
            $dados['fotoPerfil'] = $_FILES['fotoPerfil']['name'] = hash('md5', getdate()[0]) . $ext;
        }
        $dados['codigoCirurgiao'] = $this->session->codigoCirurgiao;
        $return = $this->Cirurgiao_model->updateCirurgiao($dados);

        if ($return) {
            if (!empty($_FILES['fotoPerfil']['tmp_name'])) {
                uploadFile($_FILES['fotoPerfil'], $_FILES['fotoPerfil']['name'], "./uploads/fotoPerfil/");
            }
        }

        $this->index();
    }

    public function downloadCertificados() {
        $file = $this->input->get('file', TRUE);
        $this->load->helper('download');
        force_download("./uploads/certificados/{$file}", NULL);
    }

    public function resetSenha() {
        $dados = $this->input->post(NULL, true);
        $usr = $this->Cirurgiao_model->getUsuario();
        if (md5($dados['senha_p']) != $usr) {
            echo (json_encode(0));
            die;
        }
        if ($dados['senha_s'] == $dados['senha']) {
            $retorno = $this->Cirurgiao_model->resetSenha($dados);
            if ($retorno == true) {
                die(json_encode(1));
            }
        } else {
            echo (json_encode(0));
        }
    }

    public function getUsuarioEmail() {
        $dados['email'] = $this->input->post('email', true);
        $retorno = $this->Cirurgiao_model->getUsuarioEmail($dados);
        if ($retorno === $dados['email']) {
            echo (json_encode(1));
        } else {
            echo (json_encode(0));
        }
    }

    public function ativarToken() {
        $dados = $this->input->post(NULL, true);
        $retorno = $this->Cirurgiao_model->ativarToken($dados);
        if ($retorno == 0) {
            $dados['confirmacao'] = 'Código inválido!';
            $this->load->view('login/bemVindo', $dados);
        } else {
            redirect('Login');
        }
    }

}
