<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('DetalheLaudo_Model', 'd');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');

        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function index()
    {
        if ($this->session->codigoPerfil == 1) {
            $_SESSION['codigoCirurgiao'] = $this->d->getCodigoCirurgiao($this->session->codigoUsuario);
            $dados['pagina'] = 'homeCirurgiao/index';
            $dados['sidemenu'] = 'homeCirurgiao/sidemenu';
            $this->load->view('app_template/index_view', $dados);
        }
    }
    public function detalheLaudo()
    {
        if ($this->session->codigoPerfil == 1) {
            $dados['codigoLaudo'] = $this->input->get("codigoLaudo", true);
            $dados['pagina'] = 'detalheLaudo/index';
            $dados['sidemenu'] = 'detalheLaudo/sidemenu';
            $this->load->view('app_template_box/index_view', $dados);
        }
    }

    public function getTimeLine()
    {
        $codigoLaudo = $this->input->get("codigoLaudo", true);
        $dados = $this->d->getTimeLine($codigoLaudo);
        foreach ($dados as $key => &$value) {
            if($value['TipoTime'] == 'Pedido de Autorização de Cirurgia'){
                $value['documentos'] =  $this->d->getDocumentos($codigoLaudo, $value['codigoTimeLine']);
                continue;
            }
            $value['documentos'] =  $this->d->getDocumentoTimeLine($value['codigoTimeLine']);

        }
        die(json_encode($dados));
    }

    public function meuPerfil()
    {
        if ($this->session->codigoPerfil == 1) {          
            $dados['pagina'] = 'perfil/index';
            $dados['sidemenu'] = 'perfil/sidemenu';
            $this->load->view('app_template/index_view', $dados);
        }
    }

    public function newTimeLine()
    {
        $dados = $this->input->post(null, true);
        $dados['codigoLaudoElaboracao'] = $this->d->getCodigoDoLaudo($dados['codigoLaudoElaboracao'])['codigoLaudoElaboracao'];
        $dados = array_merge($dados, [
            'nomeUsuario' => $this->session->username,
            'data' => date('y-m-d H:i:s'),
            'codigoPerfil' => $this->session->codigoPerfil,
        ]);
        $return = $this->d->setTimeLine($dados);
        $_SESSION['newTimeLine'] = $return;
        die(json_encode($return));
    }

    public function endTimeLine()
    {
        $dados = $this->input->post(null, true);
        if(isset($dados['endTime']) && $dados['endTime']){
            unset($_SESSION['newTimeLine']);
            die(json_encode(['result'=>true]));
        }
        die(json_encode(['result'=>false]));
    }

    public function openLaudo()
    {
        $codigoLaudo = $this->input->get('codigoLaudoElaboracao',true);

        if(empty($codigoLaudo)){
            redirect('/HomeCirurgiao/Home');
        }

        $file = $this->d->getDocumentos($codigoLaudo); 
        $file = array_filter($file,function($el){
            return strpos($el['tipoDocumento'], '1 - Laudo') !== false;
        });

        if(empty($file)){
            redirect('/HomeCirurgiao/Home');
        }

        $file = array_values($file)[0];
    
        $content = file_get_contents($file['arquivo']);

        header("Content-type:application/pdf");
        header('Content-Length: ' . strlen($content));
        header("Content-Disposition: inline; filename=\"{$file['nome']}\"");

        die($content);
    }
}
