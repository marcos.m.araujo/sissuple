<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Cirurgiao_model');
        $this->load->model('Clinica_model');
        $this->load->model('Paciente/Paciente_model', 'pacienteModel');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('session');
        //$this->load->helper('funcoes');
        //verificaLogin();
        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function index() {
        switch ($this->session->codigoPerfil) {
            case 1:
                $dados['dadosCirurgiao'] = $this->Cirurgiao_model->getCirurgiao($this->session->codigoUsuario);
                $_SESSION['codigoCirurgiao'] = $dados['dadosCirurgiao'][0]['codigoCirurgiao'];

                $_SESSION['fotoPerfil'] = $dados['dadosCirurgiao'][0]['fotoPerfil'];
                $dados['pacientes'] = $this->pacienteModel->getDadosPacienteFull();
                if (empty($dados['dadosCirurgiao'])) {
                    redirect('Cirurgiao/Cirurgiao');
                } else {
                    //               $clinica = $this->Clinica_model->getClinicas($dados['dadosCirurgiao'][0]['codigoCirurgiao']);
                    //               if (empty($clinica)) {
                    //                   redirect('Cirurgiao/Cirurgiao');
                    //                   die;
//                }
//                if ($dados['dadosCirurgiao'][0]['codigoAceite'] != 2) {
                    //                   redirect('Cirurgiao/Cirurgiao');
//                } else {
//                    $dados['username'] = $this->session->username;
//                    $dados['pagina'] = 'home/home_view';
//                    $this->load->view('html/index_view', $dados);
                    redirect('Laudos/Laudo_Pedido_Autorizacao_Lista');
                    //             }
                }
                break;
            case 2:
                redirect('Hospitais/Hospitais');
                break;
            case 3:
                redirect('Fornecedor/Fornecedor');
                break;
            case null:
                $dados['username'] = $this->session->username;
                $dados['pagina'] = 'home/home_view';
                $this->load->view('html/index_view', $dados);
                break;
        }
    }

    public function pacientes() {

        // $dados['query'] = $this->vmda_model->getAnualClassificada($dados);
    }

}
