<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laudo_Pedido_Autorizacao_PDFGuia extends CI_Controller {

    function __construct() {
        parent::__construct();
// $this->load->database();
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_model', 'pedidoAutorizacao');
        $this->load->model('Paciente/Paciente_model', 'paciente_model');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Sintoma_model', 'sintomas');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Diagnostico_model', 'diagnostico');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_CID_model', 'cid');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Intervencao_model', 'intervencao');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Procedimento_model', 'procedimento');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Objetivos_model', 'objetivo');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Sequela_model', 'sequela');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Material_model', 'pedidoMaterial');

        $this->load->model('Paciente/PlanoSaude_model', 'planos');
        // $this->load->model('Login_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('cookie');
//        if (empty($this->session->codigoLaudoElaboracao)) {
//            redirect('Login');
//        }
    }

    public function index() {
        if (!empty($this->input->post_get('codigoLaudoElaboracao'))) {
            $codigoLaudoElaboracao = $this->input->post_get('codigoLaudoElaboracao');
        } else {
            $codigoLaudoElaboracao = md5($this->session->codigoLaudoElaboracao);
        }
        $dadosElaboracaoLaudo = $this->pedidoAutorizacao->getLaudoElaboracaoCodigoElaboracao($codigoLaudoElaboracao);

    

        if ($dadosElaboracaoLaudo[0]['sexoPaciente'] == 'M') {
            $sexoPaciente = 'O';
        } else {
            $sexoPaciente = 'A';
        }

        if (!empty($dadosElaboracaoLaudo)) {
            $dados['codigoLaudoElaboracao'] = $dadosElaboracaoLaudo[0]['codigoLaudoElaboracao'];
            $mpdf = new \Mpdf\Mpdf(array(
                'mode' => 'utf-8',
                'format' => 'A4',
                'margin_top' => 10,
                'margin_left' => 10,
                'margin_bottom' => 10,
                'margin_right' => 0,
                'margin_header' => 5,
                'debug' => false,
                'allow_output_buffering' => false));
            $mpdf->writeHTML($this->css());
            $mpdf->writeHTML($this->dadosGuia($dadosElaboracaoLaudo, $dados['codigoLaudoElaboracao']));
     
            $mpdf->writeHTML($this->guia());
            $mpdf->Output('');
        } else {
            $this->load->view('html/404');
        }
    }

    public function css() {
        $html = "<style>
                    .img{
                      z-index:1;
                    }
                    .logoPlano{
                        margin-top: 80px;
                        position:absolute;              
                        z-index:3;                       
                        left: 80px!important;     
                        font-weight: bold;
                        width:100px!important;                     
                    }
                    .logo{
                        position:relative;             
                        width:110px!important;  
                        height: 30px!important;  
                    }
                    .clean{
                        margin-top: 60px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 82px!important;     
                        font-weight: bold;
                        width:120px!important;                        
                    }
                    .cleanNomePlano{
                        margin-top: 180px;
                        position:absolute;              
                        z-index:1;                   
                        left: 400px!important;     
                        font-weight: bold;
                        width:30px!important;                       
                    }
                    .nomePlano{
                        margin-top: 182px;
                        position:absolute;              
                        z-index:2;
                        font-size: 5pt;
                        left: 372px!important;     
                        font-weight: bold;
                        width:200px!important; 
                    }
                    .registroAns{
                       margin-top: 140px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 120px!important;     
                        font-weight: bold;
                        width:120px!important;                        
                    }
                    .clean2{
                       margin-top: 140px;
                        position:absolute;              
                        z-index:2;                      
                        left: 120px!important;     
                        font-weight: bold;
                        width:100px!important;  
                 
                    }
                    .ncarteira{
                        margin-top: 181px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 82px!important;     
                        font-weight: bold;
                        width:120px!important; 
                    }
                    .nomePaciente{
                        margin-top: 215px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 82px!important;     
                        font-weight: bold;
                    }
                    .nomeCirurgiao{
                        margin-top: 290px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 82px!important;     
                        font-weight: bold;
                    }
                   .cp{
                        margin-top: 290px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 405px!important;     
                        font-weight: bold;
                    }
                    .ncp{
                        margin-top: 290px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 515px!important;     
                        font-weight: bold;
                    } 
                    .ufcp{
                        margin-top: 290px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 620px!important;     
                        font-weight: bold;
                        width:20px;
                    }
                    .caraInt{
                        margin-top: 363px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 85px!important;     
                        font-weight: bold;
                        width:20px;
                    }
                    .diarias{
                        margin-top: 394px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 301px!important;     
                        font-weight: bold;
                        width:20px;
                    }
                    .diagnostivo{
                        margin-top: 432px;
                        position:absolute;              
                        z-index:2;
                        font-size: 7pt;
                        left: 85px!important;     
                        width:610px;
                    }
                    .cid{
                        margin-top: 530px;
                        position:absolute;              
                        z-index:1002;
                        font-size: 7pt;
                        left: 85px!important;     
                        width:120px!important; 
                        font-weight: bold;
                     }                     
                    .cid2{
                        margin-top: 530px;
                        position:absolute;              
                        z-index:1002;
                        font-size: 7pt;
                        left: 167px!important;     
                        width:120px!important;
                        font-weight: bold;
                     }                     
                     .cid3{
                        margin-top: 530px;
                        position:absolute;              
                        z-index:1002;
                        font-size: 7pt;
                        left: 247px!important;     
                        width:120px!important;                         
                        font-weight: bold;
                     }
                     .cid4{
                        margin-top: 530px;
                        position:absolute;              
                        z-index:1002;
                        font-size: 7pt;
                        left: 329px!important;     
                        width:120px!important; 
                        font-weight: bold;
                     }
                     .proc1{
                        margin-top: 572px;
                        position:absolute;              
                        z-index:1002;
                        font-size: 7pt;
                        left: 127px!important;     
                        width:120px!important; 
                        font-weight: bold;line-height: 1.8;
                     }                     
                    .procd1{
                        margin-top: 571px;
                        position:absolute;              
                        z-index:1002;
                        font-size: 7pt;
                        left: 280px!important;     
                        width:500px!important; 
                        line-height: 1.8;
                     }                     
                     .sps{
                        background-color: #fff!important;                        
                     }
                     .spss{
                        background-color:silver!important;                        
                     }
                     .divAssinatura{
                        margin-top: 956px;
                        position:absolute;              
                        z-index:1002;
                        font-size: 7pt;
                        left: -80px!important;     
                        width:500px!important; 
                        line-height: 1.1;
                        text-align: center;
                    }
                 </style>
                 ";
        return $html;
    }

    public function dadosGuia($dadosElaboracaoLaudo, $param) {
        $img = base_url('uploads/logoPlanos/');
        $logoPlano = $this->planos->consultaPlanosNome($dadosElaboracaoLaudo[0]['nomePlano']);

       
        $clean = base_url('uploads/logoPlanos/clean.png');
        $clean2 = base_url('uploads/logoPlanos/clean2.png');
        if (empty($logoPlano[0]['registroANS'])) {
            $registroANS = "";
        } else {
            $registroANS = $logoPlano[0]['registroANS'];
        }
         if (empty($logoPlano[0]['logo'])) {
             $logo = $clean;
        } else {
            $logo = $img . $logoPlano[0]['logo'];
        }

        $html = "<div class='clean'>
                    <img src='" . $clean . "'>                     
                </div>
                <div class='nomePlano'>
                      <span>" . $dadosElaboracaoLaudo[0]['nomePlano'] . "</span>                    
                </div>
                <div class='cleanNomePlano'>
                    <img src='" . $clean . "'>                     
                </div>
                <div class='logoPlano'>
                    <img class='logo' src='" . $logo . "'>                     
                </div>
                <div class='registroAns'>
                    <span>" . $registroANS . "</span>                     
                </div>
                <div class='clean2'>
                    <img src='" . $clean2 . "'>                    
                </div>
                 <div class='ncarteira'>
                    <span>" . $dadosElaboracaoLaudo[0]['numeroCarteirinha'] . "</span>                     
                </div>
                <div class='nomePaciente'>
                    <span>" . $dadosElaboracaoLaudo[0]['nomePaciente'] . "</span>                     
                </div>
                <div class='nomeCirurgiao'>
                    <span>" . $dadosElaboracaoLaudo[0]['nomeCirurgiao'] . " " . $dadosElaboracaoLaudo[0]['sobrenomeCirurgiao'] . "</span>
                </div>
                <div class='cp'>
                  <span>" . $dadosElaboracaoLaudo[0]['sigla'] . "</span>
                </div>
                <div class='ncp'>
                  <span>" . $dadosElaboracaoLaudo[0]['numeroConselhoProfissional'] . "</span>
                </div>
                <div class='ufcp'>
                  <span>" . $dadosElaboracaoLaudo[0]['ufcp'] . "</span>
                </div>
                <div class='caraInt'>
                  <span>E</span>
                </div>
                <div class='diarias'>
                  <span>" . $dadosElaboracaoLaudo[0]['diariasInternacao'] . "</span>
                </div>
               ";

        $dados['codigoLaudoElaboracao'] = $param;
        $result = $this->diagnostico->getDiagnostico($dados);
        $result = $this->diagnostico->putActionButtons($result, 'codigioDiagnostico');
        $html .= "<div class='diagnostivo'>";
        foreach ($result as $pac) {
            $html .= "<span>" . $pac['check'] . "</span>";
        }
        $html .= "</div>";

        $rcid = $this->cid->getCodCID($dados);
        $html .= "<div class='cid'>";
        $html .= "<span class='sps'>" . $rcid[0]['cid'] . "</span>";
        $html .= "</div>";
        if (count($rcid) > 1) {
            $html .= "<div class='cid2'>";
            $html .= "<span class='spss'>" . $rcid[1]['cid'] . "</span>";
            $html .= "</div>";
        }
        if (count($rcid) > 2) {
            $html .= "<div class='cid3'>";
            $html .= "<span class='spss'>" . $rcid[2]['cid'] . "</span>";
            $html .= "</div>";
        }
        if (count($rcid) > 3) {
            $html .= "<div class='cid4'>";
            $html .= "<span class='spss'>" . $rcid[3]['cid'] . "</span>";
            $html .= "</div>";
        }

        $rpro = $this->procedimento->getProcedimento($dados);
        $html .= "<div class='proc1'>";
        foreach ($rpro as $pac) {
            $html .= "<span class='sps'>" . $pac['codigoProcedimentoComplemento'] . "<br></span>";
        }
        $html .= "</div>";

        $html .= "<div class='procd1'>";


        foreach ($rpro as $pac) {
            $html .= "<span class='tuss'>" . $pac['procedimento'] . "<br></span>";
        }
        $html .= "</div>";

        $sexo = 'Dra.';
        if ($dadosElaboracaoLaudo[0]['sexo'] == 'M') {
            $sexo = 'Dr.';
        }
        $html .= "
            <div class='divAssinatura'>     
                <span class='nomeAssinatura'> " . $sexo . " " . $dadosElaboracaoLaudo[0]['nomeCirurgiao'] . " " . $dadosElaboracaoLaudo[0]['sobrenomeCirurgiao'] . "</span><br>
                <span class='registroAssinatura'> " . $dadosElaboracaoLaudo[0]['conselhoProfissional'] . "</span>
            </div>
                 ";


        return $html;
    }

    public function guia() {
        $h = base_url('newtheme/OutGuiaPDF/guia.jpg');
        $html = "
                 <div class='img'>
                   <img  src='{$h}'>
                 </div>
                 ";
        return $html;
    }

}
