<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();    
        $this->load->model('Login_model');
        $this->load->helper('form');
        $this->load->helper('url');
    
        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function getTratamento() {
        $dados['codigoLaudoElaboracao'] = $this->session->codigoLaudoElaboracao;
        $result = $this->pedidoAutorizacao->getTratamento($dados);
        $result = $this->pedidoAutorizacao->putActionButtons($result, 'codigoTratamento');
        echo json_encode($result);
    }
    



}
