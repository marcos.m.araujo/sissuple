<?php

use Dompdf\Dompdf;
use Dompdf\Options;

defined('BASEPATH') or exit('No direct script access allowed');

class Editor extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_model', 'detalhesLaudoAutorizacao');
        $this->load->model('EditorLaudo/LaudoElaboracaoModel', 'pedidoAutorizacao');
        $this->load->model('EditorLaudo/PacienteModel', 'paciente_model');
        $this->load->model('EditorLaudo/HospitalModel', 'hospital');
        $this->load->model('EditorLaudo/LaudoCacteristicasClinicasModel', 'caracteristicas');
        $this->load->model('DetalheLaudo_Model', 'detalheLaudo');
        $this->load->helper('form');
        $this->load->helper('my');
        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->load->library('session');
        $this->load->library('phpmailer_lib');
        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function index()
    {
        // $situacaoLaudo = $this->pedidoAutorizacao->getModel('codigoSituacaoLaudo', 'tbllaudoelaboracao', 'codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
        // if ($situacaoLaudo[0]['codigoSituacaoLaudo'] == 1 ) {         
        $dados['pagina'] = 'editorLaudo/index';
        $dados['sidemenu'] = 'editorLaudo/sidemenu';
        $this->load->view('app_template/index_view', $dados);
        // } else {
        //     redirect('HomeCirurgiao/home');
        // }
    }

    public function pull()
    {
        $dados = json_decode($this->input->post('Params', true), true);
        $dados = (array) $dados;
        switch ($dados[0]['tela']) {
            case 1:
                $codigoPaciente['codigoPaciente'] = $this->session->codigoPaciente;
                echo json_encode($this->pedidoAutorizacao->consultarPacienteID($codigoPaciente));
                break;
            case 2:
                $dados['dadosCirurgicos'] = $this->pedidoAutorizacao->getDadosCirurgico();
                echo json_encode($dados);
                break;
            case 3:
                echo json_encode($this->caracteristicas->getTratamento());
                break;
            case 4:
                echo json_encode($this->caracteristicas->getSintomas($dados[0]['codigoTratamento']));
                break;
            case 5:
                echo json_encode($this->caracteristicas->getDiagnisticos($dados[0]['codigoTratamento']));
                break;
            case 6:
                echo json_encode($this->caracteristicas->getCIDS($dados[0]['codigoTratamento']));
                break;
            case 7:
                echo json_encode($this->caracteristicas->getIntervencoes($dados[0]['codigoTratamento']));
                break;
            case 8:
                echo json_encode($this->caracteristicas->getProcedimentos($dados[0]['codigoTratamento']));
                break;
            case 9:
                echo json_encode($this->caracteristicas->getObjetivos($dados[0]['codigoTratamento']));
                break;
            case 10:
                echo json_encode($this->caracteristicas->getSequelas($dados[0]['codigoTratamento']));
                break;
        }
    }

    public function novoLaudo()
    {
        $codigoPacienteCrip['codigoPaciente'] = $this->input->get('codigo', true);
        $dadosPaciente = $this->paciente_model->consultarPacienteID($codigoPacienteCrip);
        date_default_timezone_set("Brazil/East");
        $dados['dataLaudo'] = date("Y-m-d H:i:s");
        $dados['codigoSituacaoLaudo'] = 1;
        $dados['codigoCirurgiao'] = $this->session->codigoCirurgiao;
        $dados['codigoPaciente'] = $dadosPaciente[0]['codigoPaciente'];
        $_SESSION['codigoPaciente'] =  $dados['codigoPaciente'];
        $dados['codigoSituacaoParecerEbuco'] = 1;
        $this->pedidoAutorizacao->cadastrarLaudoElaboracao($dados);
        $dadosLaudo = $this->pedidoAutorizacao->getLaudoElaboracaoCodigoLaudo($dados);
        $_SESSION['codigoLaudoElaboracao'] = $dadosLaudo[0]['codigoLaudoElaboracao'];
        $this->pedidoAutorizacao->setDadosCirurgicos('');
        redirect('EditorLaudo/Editor');
    }

    public function usarLaudoModelo()
    {

        $codigoLaudoAtual = $this->input->post_get("q", true);
        $codigoPaciente = $this->input->post_get("p", true);
        date_default_timezone_set("Brazil/East");
        $dados['dataLaudo'] = date("Y-m-d H:i:s");
        $dados['codigoSituacaoLaudo'] = 1;
        $dados['codigoCirurgiao'] = $this->session->codigoCirurgiao;
        $dados['codigoPaciente'] = $codigoPaciente;
        $_SESSION['codigoPaciente'] =  $codigoPaciente;
        $dados['codigoSituacaoParecerEbuco'] = 1;
        $result = $this->pedidoAutorizacao->cadastrarLaudoElaboracao($dados);
        $dadosLaudo = $this->pedidoAutorizacao->getLaudoElaboracaoCodigoLaudo($dados);
        $_SESSION['codigoLaudoElaboracao'] = $dadosLaudo[0]['codigoLaudoElaboracao'];
        $this->pedidoAutorizacao->clonarLaudoElaboracao($dadosLaudo[0]['codigoLaudoElaboracao'], $codigoLaudoAtual);
        if ($result)
            redirect('EditorLaudo/Editor');
    }

    public function excluirLaudo()
    {
        $codigo = $this->input->post('codigoElaboracao', true);
        echo json_encode($this->pedidoAutorizacao->excluirLaudo($codigo));
    }

    function getClassificacaoDocumento()
    {
        echo json_encode($this->pedidoAutorizacao->getClassificacaoDocumento());
    }
    function contatosHospital()
    {
        $codigo = $this->input->get("codigo", true);
        echo json_encode($this->pedidoAutorizacao->contatosHospital($codigo));
    }

    function getClassificacaoDocumentoTipoDocumento()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->pedidoAutorizacao->getClassificacaoDocumentoTipoDocumento($dados));
    }

    function getDocumentos()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->pedidoAutorizacao->getDocumentos($dados));
    }
    function getDocumentosDetalhesLaudo()
    {
        $dados = $this->input->get(null, true);
        echo json_encode($this->pedidoAutorizacao->getDocumentosDetalhesLaudo($dados));
    }

    function alterarDocumento()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->pedidoAutorizacao->alterarDocumento($dados));
    }
    function deletarMaterial()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->pedidoAutorizacao->deletarMaterial($dados));
    }
    function getMateriaisSalvos()
    {
        echo json_encode($this->pedidoAutorizacao->getMateriaisSalvos());
    }

    public function salvarDocumentos()
    {
        $dados = $this->input->post(null, true);
        if (isset($_FILES['Arquivo']['name'])) {
            $NomeArquivo = $_FILES['Arquivo']['name'];
            $novoNomeRandom = bin2hex(random_bytes(10)) . "." .  @end(explode(".", $NomeArquivo));
            $dados = $this->tratarDadosSalvarDocumentos($dados, $NomeArquivo, $novoNomeRandom);
            $return = $this->pedidoAutorizacao->salvarDocumentos($dados);
            if ($return) {
                if(isset($_SESSION['newTimeLine'])){
                    $this->pedidoAutorizacao->setDocsInTimeLine($_SESSION['newTimeLine'], $return);
                }
                $result['result'] = $this->uploadFile($novoNomeRandom);
            } else {
                $result['result'] = 'Erro ao salvar';
            }
            die(json_encode($result));
        }
    }

    public function atualizarLaudo()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->pedidoAutorizacao->updateLaudoElaboracaoMd5($dados['codigoLaudo'], $dados['situacao']));
    }

    public function finalizarLaudo()
    {
        date_default_timezone_set("Brazil/East");
        $dados['data'] = date("Y-m-d H:i:s");
        $dados['codigoLaudoElaboracao'] = $this->session->codigoLaudoElaboracao;
        $dados['nomeUsuario'] = $this->session->username;
        $dados['TipoTime'] = 'Pedido de Autorização de Cirurgia';
        $dados['codigoPerfil'] = 1;
        $update = $this->pedidoAutorizacao->updateLaudoElaboracao($this->session->codigoLaudoElaboracao, 2);
        echo json_encode($update);
        if ($update) {
            $this->generateLaudoGuiaPDF($this->session->codigoLaudoElaboracao);
          echo  $this->detalheLaudo->setTimeLine($dados);
            // $dadosLaudo = $this->pedidoAutorizacao->getLaudoElaboracaoNotificacao($this->session->codigoLaudoElaboracao);
            // $this->prepararNotificacoes($dadosLaudo[0]);
        }
    }

    public function prepararNotificacoes($dados)
    {
        $viewLaudo = $this->pedidoAutorizacao->getModel('CodigoPlano, nomePaciente,tratamentos, email,hospital, estado', 'viewLaudoElaboracao', 'codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
        $nomePlano = $this->pedidoAutorizacao->getModel('nomePlano', 'tblplanosaude', 'CodigoPlano',  $viewLaudo[0]['CodigoPlano']);
        $emailPlano = $this->pedidoAutorizacao->getModel('email', 'tblPlanosSaudeContato', 'codigoPlano', $viewLaudo[0]['CodigoPlano']);
        $codigoHospital = $this->pedidoAutorizacao->getModel('codigoHospital', 'tblHospitais', 'nome', $viewLaudo[0]['hospital'], 'uf', $viewLaudo[0]['estado']);
        $emailHospital = $this->pedidoAutorizacao->getModel('email', 'tblHospitaisContato', 'codigoHospital', $codigoHospital[0]['codigoHospital']);
        if ($dados['notificarPlano']) {
            // foreach ($emailPlano as $key => $value) {
            //     $this->phpmailer_lib->emailLaudoHospitalCompartilhado(
            //         $this->session->codigoLaudoElaboracao,
            //         $this->session->username,
            //         $nomePlano[0]['nomePlano'],
            //         $viewLaudo[0]['nomePaciente'],
            //         $value['email'],
            //         $this->session->email
            //     );
            // }
        }

        if ($dados['notificarPaciente']) {
            $this->phpmailer_lib->emailLaudoPaciente(
                $viewLaudo[0]['nomePaciente'],
                $viewLaudo[0]['email'],
                $this->session->username,
                $viewLaudo[0]['tratamentos'],
                $nomePlano[0]['nomePlano'],
                md5($this->session->codigoLaudoElaboracao)
            );
        }

        // if ($dados['notificarFornecedor']) {
        //     echo " notificarHospital";
        // }

        if ($dados['notificarHospital']) {
            foreach ($emailHospital as $key => $value) {
                $this->phpmailer_lib->emailLaudoHospitalCompartilhado(
                    $this->session->codigoLaudoElaboracao,
                    $this->session->username,
                    $nomePlano[0]['nomePlano'],
                    $viewLaudo[0]['nomePaciente'],
                    $value['email'],
                    $this->session->email
                );
            }
        }

        $this->phpmailer_lib->emailLaudoConcluido(
            $this->session->username,
            $this->session->email,
            $viewLaudo[0]['nomePaciente'],
            $viewLaudo[0]['tratamentos'],
            $nomePlano[0]['nomePlano'],
            md5($this->session->codigoLaudoElaboracao)
        );
    }

    public function generatePDF(){
        $pdfContent = $_POST['pdfContent'];
        $dados = $this->input->post('dados',TRUE);

        if(!isValidMd5($dados['codigoLaudoElaboracao'])) {
            $dados['codigoLaudoElaboracao'] = md5($dados['codigoLaudoElaboracao']);
        }
        $dadosElaboracaoLaudo = $this->detalhesLaudoAutorizacao->getLaudoElaboracaoCodigoElaboracao($dados['codigoLaudoElaboracao'])[0];
        $dadosElaboracaoLaudo['sexo'] =  ($dadosElaboracaoLaudo['sexo'] == 'M')? 'Dr.' : 'Dra.';
        $dados['codigoLaudoElaboracao'] = $dadosElaboracaoLaudo['codigoLaudoElaboracao'];

        $filename = bin2hex(random_bytes(10)).'.pdf';

        $dados = array_merge([
            'classificacaoDocumento' => 'Juntada de Documentos',
            'tipoDocumento' => 'Texto livre',
            'nome' => 'Texto livre',
            'arquivo' => base_url('uploadExames/').$filename
        ],$dados);
        
        $result = $this->pedidoAutorizacao->salvarDocumentos($dados);
        if(empty($result)){
            http_response_code(500);
            die(json_encode([
                'reuslt' => $result,
                'message' => 'Erro ao salvar Descrição da Cirurgia.'
            ]));
        }

        if(isset($_SESSION['newTimeLine'])){
            $this->pedidoAutorizacao->setDocsInTimeLine($_SESSION['newTimeLine'], $result);
        }

        ob_start();

        $this->load->view('comum/cabecalho_laudo_pdf',[
            'dadosElaboracaoLaudo' => $dadosElaboracaoLaudo,
            'pdfContent' => $pdfContent,
            'qrCode' => base64_encode(file_get_contents(generateQrCode($dadosElaboracaoLaudo['codigoLaudoElaboracaoCRIP'])))
        ]);

        $pdf = ob_get_clean();
         
        $mpdf =  $mpdf = new \Mpdf\Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'margin_top' => 30,
            'margin_left' => 30,
            'margin_bottom' => 50,
            'margin_right' => 20,
            'margin_header' => 2,
            'margin_footer' => 20,
            'debug' => false,
            'allow_output_buffering' => false
        ]);
        $mpdf->showImageErrors = true;
        $mpdf->WriteHTML($pdf);
        
        
        ob_start();
        $mpdf->Output();
        $output = ob_get_clean();
        file_put_contents('uploadExames/'.$filename,$output);
        die(json_encode(['result' => true]));
    }

    public function arquivarLaudo(){
        $justificativa = $this->input->post('Justificativa',TRUE);
        $codigoLaudo = $this->input->post('codigoLaudoElaboracao',TRUE);
        $codigoSituacaoLaudo = 3;

        $result = $this->pedidoAutorizacao->updateLaudoElaboracaoMd5($codigoLaudo,$codigoSituacaoLaudo,$justificativa);
        if(!$result){
            http_response_code(500);
        }
        die(json_encode(['result'=>$result]));
    }

    private function configGenerateGuiaLaudoPDF($laudo, $type)
    {
        $tipoDocumento = [
            'Laudo' => '1 - Laudo',
            'Guia' => '2 - Guia Cirurgíca',
        ];

        if(!array_key_exists($type,$tipoDocumento)){
            return false;
        }

        $dados['filename'] = bin2hex(random_bytes(20)).'.pdf';
        $dados['config'] = [
            'nome' => $type.date('dmYHis').bin2hex(random_bytes(10)).'pdf',
            'classificacaoDocumento' => 'Pedido de Autorização de Cirurgia',
            'tipoDocumento' => $tipoDocumento[$type],
            'codigoLaudoElaboracao' => $laudo,
            'arquivo' => base_url("uploadExames/{$dados['filename']}"),
            'privadoHospital' => 0, 
            'privadoPaciente' => 0, 
            'privadoPlano' => 0, 
            'privadoFornecedor' => 0
        ];

        return $dados;
    }

    private function generateLaudoGuiaPDF($codigoLaudo)
    {
        $laudoHash = md5($codigoLaudo);
        $laudo = $this->configGenerateGuiaLaudoPDF($codigoLaudo, 'Laudo');
        $guia = $this->configGenerateGuiaLaudoPDF($codigoLaudo, 'Guia');
        
        $saveLaudo = $this->pedidoAutorizacao->salvarDocumentos($laudo['config']);
        $saveGuia = $this->pedidoAutorizacao->salvarDocumentos($guia['config']);

        if($saveLaudo){
            $url = base_url('EditorLaudo/Laudo_Pedido_Autorizacao_PDF')."?codigoLaudoElaboracao={$laudoHash}";
            $file = file_get_contents($url);
            file_put_contents("uploadExames/{$laudo['filename']}",$file);
        }

        if($saveGuia){
            $url = base_url('EditorLaudo/Laudo_Pedido_Autorizacao_PDFGuia')."?codigoLaudoElaboracao={$laudoHash}";
            $file = file_get_contents($url);
            file_put_contents("uploadExames/{$guia['filename']}",$file);
        }
    }

    private function uploadFile($newName)
    {
        $config = [
            'upload_path' => 'uploadExames/',
            'allowed_types' =>'jpg|jpeg|png|gif|pdf|',
            'file_name' => $newName
        ];
        $this->upload->initialize($config);
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('Arquivo')) {
            return $this->upload->display_errors();
        } 
        return  'Salvo com sucesso!';
    }

    private function tratarDadosSalvarDocumentos($dados, $NomeArquivo, $novoNomeRandom)
    {
        $dados['codigoLaudoElaboracao'] = (!array_key_exists('codigoLaudoElaboracao',$dados))? 
        $this->session->codigoLaudoElaboracao : 
        $this->detalheLaudo->getCodigoDoLaudo($dados['codigoLaudoElaboracao'])['codigoLaudoElaboracao'];

        $dados = array_merge([
            'nome' => $NomeArquivo,
            'arquivo' => base_url('uploadExames/') . $novoNomeRandom,
            'privadoHospital' => 0, 
            'privadoPaciente' => 0,
            'privadoPlano' => 0,
            'privadoFornecedor' => 1
        ],$dados);

        return $dados;
    }

    private function newTimeLine($codigoLaudo)
    {
        if(!isset($_SESSION['newTimeLine'])) {
            $dados = $this->input->post(null, true);
            $dados['codigoLaudoElaboracao'] = $codigoLaudo;
            $dados = array_merge($dados, [
                'nomeUsuario' => $this->session->username,
                'data' => date('y-m-d H:i:s'),
                'codigoPerfil' => $this->session->codigoPerfil,
            ]);
            $return = $this->d->setTimeLine($dados);
            $_SESSION['newTimeLine'] = $return;
        }
        // die(json_encode($return));
    }
}
