<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Paciente extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('EditorLaudo/PacienteModel', 'paciente_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('cookie');
        $this->load->library('session');
        $this->load->library('phpmailer_lib');
        // if (empty($this->session->username)) {
        //     redirect('Login');
        // }
    }


    public function index()
    {
        $dados['pagina'] = 'editorLaudo/index';
        $dados['sidemenu'] = 'editorLaudo/sidemenu';
        $this->load->view('app_template/index_view', $dados);
    }

    public function getPaciente()    {

        $codigoPaciente['codigoPaciente'] = $this->session->codigoPaciente;
        echo json_encode($this->pedidoAutorizacao->consultarPacienteID($codigoPaciente));
    }
}
