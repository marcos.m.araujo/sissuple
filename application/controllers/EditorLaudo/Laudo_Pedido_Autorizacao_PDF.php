<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Laudo_Pedido_Autorizacao_PDF extends CI_Controller {

    function __construct() {
        parent::__construct();
        
// $this->load->database();
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_model', 'pedidoAutorizacao');
        $this->load->model('Paciente/Paciente_model', 'paciente_model');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Sintoma_model', 'sintomas');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Diagnostico_model', 'diagnostico');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_CID_model', 'cid');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Intervencao_model', 'intervencao');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Procedimento_model', 'procedimento');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Objetivos_model', 'objetivo');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Sequela_model', 'sequela');
        $this->load->model('Laudos/Laudo_Pedido_Autorizacao_Material_model', 'pedidoMaterial');
        $this->load->model('Login_model');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('cookie');
//        if (empty($this->session->codigoLaudoElaboracao)) {
//            redirect('Login');
//        }
    }

    public function index() {
        echo '<pre>';
      
        if (!empty($this->input->post_get('codigoLaudoElaboracao'))) {
            $codigoLaudoElaboracao = $this->input->post_get('codigoLaudoElaboracao');
        } else {
            $codigoLaudoElaboracao = md5($this->session->codigoLaudoElaboracao);
        }
        $dadosElaboracaoLaudo = $this->pedidoAutorizacao->getLaudoElaboracaoCodigoElaboracao($codigoLaudoElaboracao);

        if ($dadosElaboracaoLaudo[0]['sexoPaciente'] == 'M') {
            $sexoPaciente = 'O';
        } else {
            $sexoPaciente = 'A';
        }
        if (!empty($dadosElaboracaoLaudo)) {
            $dados['codigoLaudoElaboracao'] = $dadosElaboracaoLaudo[0]['codigoLaudoElaboracao'];
            $qtdMaterial = count($this->pedidoMaterial->consultaMeusPedidos($dados));
            // $examesAnexo = $this->exames->get($dados);
            $qr = $dados['codigoLaudoElaboracao'];
            $mpdf = new \Mpdf\Mpdf(array(
                'mode' => 'utf-8',
                'format' => 'A4',
                'margin_top' => 12,
                'margin_left' => 5,
                'margin_bottom' => 30,
                'margin_right' => 5,
                'margin_header' => 5,
                'debug' => false,
                'allow_output_buffering' => false));
            $css = $this->css();
            $cabecalho = $this->cabecalho($qr, $dadosElaboracaoLaudo);
            $dadosPaciente = $this->dadosPaciente($dadosElaboracaoLaudo);
            $dadosCirurgia = $this->dadosCirurgia($dadosElaboracaoLaudo);
            $dadosCaractClinicas = $this->cacteristicasClinicas($dados['codigoLaudoElaboracao'], $sexoPaciente);
            $rodape = $this->rodape($dadosElaboracaoLaudo);
            $nome = $dadosElaboracaoLaudo[0]['nomePaciente'];
            $mpdf->SetHeader('<h5 style="color:#084B8A">LAUDO BUCOMAXILOFACIAL N.º: ' . $dadosElaboracaoLaudo[0]['codigoLaudoElaboracao'] . '  - Paciente: ' . $nome . ' - ' . date("d/m/Y") . ' - Página: {PAGENO} </h5>');
            $mpdf->writeHTML($css);
            $mpdf->writeHTML($cabecalho);
            $mpdf->writeHTML($dadosPaciente);
            $mpdf->writeHTML($dadosCirurgia);
            $mpdf->writeHTML($dadosCaractClinicas);
            $mpdf->SetHTMLFooter($rodape);
            $mpdf->AddPage();
            if ($qtdMaterial >= 1) {
                $mpdf->writeHTML($this->materiais1($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 4) {
                $mpdf->writeHTML($this->materiais2($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 8) {
                $mpdf->writeHTML($this->materiais3($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 12) {
                $mpdf->writeHTML($this->materiais4($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 16) {
                $mpdf->writeHTML($this->materiais5($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 20) {
                $mpdf->writeHTML($this->materiais6($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 24) {
                $mpdf->writeHTML($this->materiais7($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 28) {
                $mpdf->writeHTML($this->materiais8($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 32) {
                $mpdf->writeHTML($this->materiais9($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 36) {
                $mpdf->writeHTML($this->materiais10($dados['codigoLaudoElaboracao']));
            }
            if ($qtdMaterial >= 40) {
                $mpdf->writeHTML($this->materiais11($dados['codigoLaudoElaboracao']));
            }
            $mpdf->SetHTMLFooter($rodape);
            $mpdf->Output('LAUDO BUCOMAXILOFACIAL - Paciente: ' . $nome . ' - ' . date("d/m/Y"), 'I');
        } else {
            $this->load->view('html/404');
        }
    }

    public function css() {
        $css = base_url('newtheme/css_PDF/bootstrap.min.css');
        $html = "<style>
                    @import url('{$css}');
                    .row div:nth-of-type(even) {
                        background-color: darkgray
                    }
                    .row div:nth-of-type(odd) {
                        background-color: lightgray
                    }
                    .quadrante{
                        border-left: 1px solid #084B8A;      
                        border-right: 1px solid #084B8A;    
                        border-bottom: 1px solid #084B8A;       
                    }
                    .quadranteGuia{                   
                        border: 1px solid #084B8A;       
                    }
                    .textoQuadrante{
                        background-color: #084B8A;
                        color: #fff;
                        font-size: 9pt;
                        text-align: center;
                        font-weight: bold;
                        width: 100%!important;
                    }
                    fieldset{
                        border: 0.5px solid #CEE3F6 !important;
                        margin-bottom: 5px!important;  
                        margin-top: 8px!important;  
                        border-radius:2px;
                        padding-left:10px!important;                        
                        text-align: justify;     
                        background-color: #EFF5FB;
                    }
                    fieldset legend{
                        color: #084B8A;                  
                        font-size: 7pt;
                    }
                    fieldset div{
                        padding-left:-5px!important;  
                        font-size: 8pt;
                        padding-top: 5px;
                        padding-bottom: 8px;
                    }
                    .md{
                        margin-left: -20px;
                    }
                    .mds{
                        margin-left: 5px;
                    }
                    .bord{
                      border-left: 1px solid #084B8A;      
                      border-right: 1px solid #084B8A;       
                    }
                    .nomeCirurgiao{
                      color:#084B8A;
                      font-size: 18pt;
                      font-weight: bold;
                    }
                    .dadosCabecalho{
                       color:#084B8A;  
                       font-size: 12pt;
                    }
                    .divAssinatura{
                        width:100%;
                        text-align: center;
                    }
                    .imgLaudo{
                        width: 100px;
                        height: 100px;
                        border-radius:25px !important;;
                        border: 0.5px solid #CEE3F6 !important;
                    }       
                    .desc{
                        font-weight: bold;
                        font-size: 9pt!important;
                    }
                    .descValue{
                        font-size: 9pt!important;
                    }
                    table td{
                        text-align: left;                      
                        padding-left: 3px; 
                    }
                    .td{
                      width: 150px;
                    }
                    .td1{
                      width: 100px;
                    }
                    .aplicacao{
                         margin-left: 8px!important;
                    }
                    .labelAplicacao{
                        font-weight: bold;
                        font-size: 7pt!important;
                        color:#084B8A;
                        width: 100%;
                    }
                    .valueAplicacao{
                        font-size: 7pt!important; 
                        text-align: justify;
                    }
                 </style>
                 ";
        return $html;
    }

    public function cabecalho($qr, $dadosElaboracaoLaudo) {
        $qr = $qr;
        $aux = base_url('newtheme/') . 'qr_img0.50j/php/qr_img.php?';
        $aux .= 'd=http://192.168.0.18:8080/sissuple/HomeCirurgiao/Home/openLaudo?codigoLaudoElaboracao=' . md5($qr) . '&';
        $aux .= 'e=H&';
        $aux .= 's=10&';
        $aux .= 't=J';
        $sexo = 'Dra.';
        if ($dadosElaboracaoLaudo[0]['sexo'] == 'M') {
            $sexo = 'Dr.';
        }
        $html = " 
            <table style='width:100%; margin-bottom: 3px'>
                <tr>
                    <td style='width:100%'>
                        <div>
                            <p class='nomeCirurgiao'>" . $sexo . " " . $dadosElaboracaoLaudo[0]['nomeCirurgiao'] . " " . $dadosElaboracaoLaudo[0]['sobrenomeCirurgiao'] . "</p>
                            <p class='dadosCabecalho'> Clínica: " . $dadosElaboracaoLaudo[0]['nomeClinica'] . "</p>
                            <p class='dadosCabecalho'> CNPJ: " . $dadosElaboracaoLaudo[0]['cnpj'] . " </p>
                            <p class='dadosCabecalho'> Endereço: " . $dadosElaboracaoLaudo[0]['endereco'] . " </p>
                            <p class='dadosCabecalho'> Contato: " . $dadosElaboracaoLaudo[0]['contatoClinica'] . "</p>
                        </div>
                    </td>
                    <td style='width:0%'>
                        <div style='width:100%' >
                            <img style='height: 150px; width: 150px; margin-left:70%' src=" . $aux . ">
                        </div>
                    </td>
                </tr>                
            </table>
                 ";
        return $html;
    }

    public function dadosPaciente($dadosDoPaciente) {
        $nomePaciente = '';
        $dataNascimento = '';
        $cpf = '';
        $numeroCarteirinha = '';
        $email = '';
        $nomePlano = '';
        foreach ($dadosDoPaciente as $pac) {
            $nomePaciente = $pac['nomePaciente'];
            $dataNascimento = date("d/m/Y", strtotime($pac['dataNascimento']));
            $cpf = $pac['CPF_F'];
            $nomePlano = $pac['nomePlano'];
            $numeroCarteirinha = $pac['numeroCarteirinha'];
            $email = $pac['email'];
        }
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>DADOS DO PACIENTE</div>   
                <div class='row'>                 
                    <div class='col-xs-5 mds'>
                        <fieldset>
                          <legend>NOME DO PACIENTE</legend>
                          <div>{$nomePaciente}</div>
                        </fieldset>  
                    </div>
                    <div class='col-xs-3 md'>
                        <fieldset>
                          <legend>DATA NASCIMENTO.</legend>
                          <div>{$dataNascimento}</div>
                        </fieldset>  
                    </div>
                    <div class='col-xs-3 md'>
                        <fieldset>
                          <legend>CPF</legend>
                           <div>{$cpf}</div>
                        </fieldset>  
                    </div>                                    
                </div>             
                 <div class='row'>                 
                    <div class='col-xs-5 mds'>
                        <fieldset>
                          <legend>PLANO DE SAÚDE</legend>
                          <div>{$nomePlano}</div>
                        </fieldset>  
                    </div>
                     <div class='col-xs-3 md'>
                        <fieldset>
                          <legend>N° Carteirinha</legend>
                           <div>{$numeroCarteirinha}</div>
                        </fieldset>  
                    </div> 
                    <div class='col-xs-3 md'>
                        <fieldset>
                          <legend>EMAIL</legend>
                          <div>{$email}</div>
                        </fieldset>  
                    </div>                                   
                </div>  
            </div>
                 ";
        return $html;
    }

    public function dadosCirurgia($dadosElaboracaoLaudo) {
        $diariasInternacao = '';
        $diariasUTI = '';
        $estado = '';
        $hospital = '';
        $tempoProcedimento = '';
        $tipoAnestesia = '';
        $equipe = '';
        $tipoHonorario = '';
        $dataCirurgia = '';
        foreach ($dadosElaboracaoLaudo as $pac) {
            $diariasInternacao = $pac['diariasInternacao'];
            $diariasUTI = $pac['diariasUTI'];
            $estado = $pac['estado'];
            $hospital = $pac['hospital'];
            $tempoProcedimento = $pac['tempoProcedimento'];
            $tipoAnestesia = $pac['tipoAnestesia'];
            $equipe = $pac['equipe'];
            $tipoHonorario = $pac['tipoHonorario'];
            $dataCirurgiaF = $pac['dataCirurgiaF'];
        }
        $html = "
             <div class='quadrante'> 
                <div class='textoQuadrante'>DADOS DA CIRURGIA</div>   
                    <div class='row'>                 
                        <div style='width: 479px' class='col-xs-12 mds'>
                            <fieldset>
                              <legend>HOSPITAL</legend>
                              <div>{$hospital} - {$estado}</div>
                            </fieldset>  
                        </div>                       
                        <div style='width: 120px' class='col-xs-2 md'>
                            <fieldset>
                              <legend>DATA CIRURGIA</legend>
                               <div>{$dataCirurgiaF}</div>
                            </fieldset>  
                        </div>
                        <div style='width: 120px' class='col-xs-2 md'>
                            <fieldset>
                              <legend>ANESTESIA</legend>
                               <div>{$tipoAnestesia}</div>
                            </fieldset>  
                        </div>
                        <div class='col-xs-3 mds'>
                            <fieldset>
                              <legend>EQUIPE CIRÚRGICA</legend>
                              <div>{$equipe}</div>
                            </fieldset>  
                        </div>
                        <div class='col-xs-2 md'>
                            <fieldset>
                              <legend>HONORÁRIO PROFISSIONAL</legend>
                               <div>{$tipoHonorario}</div>
                            </fieldset>  
                        </div>
                        <div class='col-xs-2 md'>
                            <fieldset>
                              <legend>TEMPO PROCEDIMENTO</legend>
                               <div>{$tempoProcedimento}</div>
                            </fieldset>  
                        </div>
                        <div style='width: 120px' class='col-xs-2 md'>
                            <fieldset>
                              <legend>DIÁRIA INTERNAÇÃO</legend>
                               <div>{$diariasInternacao}</div>
                            </fieldset>  
                        </div>
                        <div style='width: 120px' class='col-xs-2 md'>
                            <fieldset>
                              <legend>DIÁRIA UTI</legend>
                               <div>{$diariasUTI}</div>
                            </fieldset>  
                        </div>                             
                    </div>                 
                </div>
                 ";
        return $html;
    }

    public function cacteristicasClinicas($param, $sexoPaciente) {
        $html = "            
            <div class='quadrante'> 
            <div class='textoQuadrante'>CARACTERÍSTICAS CLÍNICAS</div> 
            <div class='row'>";
        $html .= $this->dadosSintomas($param, $sexoPaciente);
        $html .= $this->dadosDiagnostico($param);
        $html .= $this->dadosCID($param, $sexoPaciente);
        $html .= $this->dadosIntervencao($param, $sexoPaciente);
        $html .= $this->dadosProcedimento($param);
        $html .= $this->dadosObjetivo($param);
        $html .= $this->dadosSequela($param);
        // $html .= $this->dadosExames($examesAnexo);
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function dadosSintomas($param, $sexoPaciente) {
        $dados['codigoLaudoElaboracao'] = $param;
        $result = $this->sintomas->getSintomas($dados);
        $result = $this->sintomas->putActionButtons($result, 'codigoSintoma');
        $html = "           
            <div style='width: 738px' class='col-xs-12 mds'> 
               <fieldset style='height: 65px'>
               <legend>SINAIS, SINTOMAS E RELATOS</legend>";
        $p = "<div>" . $sexoPaciente . " paciente relata/reclama de ";
        foreach ($result as $pac) {
            $p .= " " . $pac['check'];
        }
        $html = $html . $p . "</div>";
        $html .= "</fieldset>";
        $html .= "</div>";
        return $html;
    }

    public function dadosDiagnostico($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $result = $this->diagnostico->getDiagnostico($dados);
        $result = $this->diagnostico->putActionButtons($result, 'codigioDiagnostico');
        $html = "
            <div style='width: 738px' class='col-xs-12 mds'>
                 <fieldset style='height: 65px'>
                <legend>DIAGNÓTICO</legend>";
        $p = "<div >Diante dos exames realizados foi possível verificar ";
        foreach ($result as $pac) {
            $p .= " " . $pac['check'];
        }
        $html = $html . $p . "</div>";
        $html .= "</fieldset></div>";
        return $html;
    }

    public function dadosCID($param, $sexoPaciente) {
        $dados['codigoLaudoElaboracao'] = $param;
        $result = $this->cid->getCID($dados);
        $result = $this->cid->putActionButtons($result, 'codigioCID');
        if ($sexoPaciente == 'A') {
            $portador = "portadora";
        }if ($sexoPaciente == 'O') {
            $portador = "portador";
        }
        $html = "
            <div style='width: 738px' class='col-xs-12 mds'>
                <fieldset style='height: 65px'>
                <legend>C.I.D.</legend>";
        $p = "<div>Assim, é possível diagnosticar " . strtolower($sexoPaciente) . " paciente é " . $portador . " de ";
        foreach ($result as $pac) {
            $p .= " " . $pac['check'];
        }
        $html = $html . $p . "</div>";
        $html .= "</fieldset></div>";
        return $html;
    }

    public function dadosIntervencao($param, $sexoPaciente) {
        $dados['codigoLaudoElaboracao'] = $param;
        $result = $this->intervencao->getIntervencao($dados);
        $result = $this->intervencao->putActionButtons($result, 'codigioIntervencao');
        foreach ($result as $pac) {
            $sintomas = $pac['check'];
        }
        $html = "
             <div style='width: 738px' class='col-xs-12 mds'>
                <fieldset style='height: 65px'>
                <legend>INTERVENÇÃO</legend>";
        $p = "<div>Como tratamento, " . strtolower($sexoPaciente) . " paciente deverá submeter-se a ";
        foreach ($result as $pac) {
            $p .= " " . $pac['check'];
        }
        $html = $html . $p . "</div>";
        $html .= "</fieldset></div>";
        return $html;
    }

    public function dadosProcedimento($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $result = $this->procedimento->getProcedimento($dados);
        $result = $this->procedimento->putActionButtons($result, 'codigioIntervencao');
        foreach ($result as $pac) {
            $sintomas = $pac['check'];
        }
        $html = "
             <div style='width: 738px' class='col-xs-12 mds'>
                <fieldset style='height: 65px'>
                <legend>PROCEDIMENTO</legend>";
        $p = "<div>O tratamento será realizado por meio dos procedimentos de ";
        foreach ($result as $pac) {
            $p .= " " . $pac['check'];
        }
        $html = $html . $p . "</div>";
        $html .= "</fieldset></div>";
        return $html;
    }

    public function dadosObjetivo($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $result = $this->objetivo->getObjetivos($dados);
        $result = $this->objetivo->putActionButtons($result, 'codigioIntervencao');
        foreach ($result as $pac) {
            $sintomas = $pac['check'];
        }
        $html = "
            <div style='width: 738px' class='col-xs-12 mds'>
                <fieldset style='height: 65px'>
                <legend>OBJETIVOS</legend>";
        $p = "<div>O tratamento proposto visa ";
        foreach ($result as $pac) {
            $p .= " " . $pac['check'];
        }
        $html = $html . $p . "</div>";
        $html .= "</fieldset></div>";
        return $html;
    }

    public function dadosSequela($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $result = $this->sequela->getSequela($dados);
        $result = $this->sequela->putActionButtons($result, 'codigioIntervencao');
        foreach ($result as $pac) {
            $sintomas = $pac['check'];
        }
        $html = "
             <div style='width: 738px' class='col-xs-12 mds'>
                <fieldset style='height: 65px'>
                <legend>SEQUELAS</legend>";
        $p = "<div>Caso não seja realizado o tratamento proposto, poderá ocorrer ";
        foreach ($result as $pac) {
            $p .= " " . $pac['check'];
        }
        $html = $html . $p . "</div>";
        $html .= "</fieldset></div>";
        return $html;
    }

    public function dadosExames($param) {
        $html = "
            <div style='width: 738px' class='col-xs-12 mds'>
                <fieldset>
                <legend>EXAMES EM ANEXO</legend>";
        $p = "<div>";
        if (!empty($param)) {
            foreach ($param as $indice => $pac) {
                if ($indice > 0) {
                    $p .= ", " . $pac['nomeExame'] . " ";
                } else {
                    $p .= "" . $pac['nomeExame'] . " ";
                }
            }
        } else {
            $p .= "Não há exames anexados.";
        }
        $html = $html . $p . "</div>";
        $html .= "</fieldset></div>";
        return $html;
    }

    public function materiais1($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'>                 
                    <div style='width: 738px' class='col-xs-12 mds'>
                        <fieldset>
                            <legend>MARCAS</legend>
                            <div style='font-weight: bold; padding: 2px'>Em atendimento ao Art. 5. da Resolução 115/2012 do CRO, ofereço o nome da(s) seguinte(s) marca(s) e fornecedor(s)  que possuem os materiais necessário. São elas: " . str_replace(",", ", ", $marcas[0]['marcas']) . " 
                            </div>
                        </fieldset>";
        $html .= "</div>";
        $i = 1;
        foreach ($result as $op) {
            if ($i <= 4) {
                $html .= "<div style='width: 738px; ' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";
                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais2($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 4 && $i <= 8) {
                $html .= "<div style='width: 738px;' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";
                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais3($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 8 && $i <= 12) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais4($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 12 && $i <= 16) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais5($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 16 && $i <= 20) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais6($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 20 && $i <= 24) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais7($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 24 && $i <= 28) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais8($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 28 && $i <= 32) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais9($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 32 && $i <= 36) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais10($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 36 && $i <= 30) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function materiais11($param) {
        $dados['codigoLaudoElaboracao'] = $param;
        $marcas = $this->pedidoMaterial->consultaMarcasConcatenadasPDF($dados['codigoLaudoElaboracao']);
        $result = $this->pedidoMaterial->consultaMeusPedidos($dados);
        $html = "
            <div class='quadrante'> 
            <div class='textoQuadrante'>MATERIAIS NECESSÁRIOS</div>   
                <div class='row'><br><br>";
        $i = 1;
        foreach ($result as $op) {
            if ($i > 40 && $i <= 44) {
                $html .= "<div style='width: 738px' class='col-xs-12 mds'>";
                $html .= "<fieldset style='height: 200px'>";
                $html .= "<legend>" . mb_strtoupper($op['nomenclatura'], 'UTF-8') . "</legend>";
                $html .= "<div >"
                        . "<div class='col-xs-2'>"
                        //.$this->returnImgTag($op['imagem'])
                        . "<img src=" . base_url('upload/') . $op['imagem'] . "  class='imgLaudo' />"
                        . "</div>"
                        . "<div  class='col-xs-9'>"
                        . "<table>"
                        . "<tr>"
                        . "<td class='td11'><span class='desc'>Quantidade:</span></td><td class='td'><span class='descValue'> {$op['qtdMaterial']}</span></td> "
                        . "<td ><span class='desc'>Nomenclatura:</span></td><td class='td'><span class='descValue'>{$op['nomenclatura']} </span></td>"
                        . "<td><span class='desc'>Tipo:</span></td><td class='td'> <span class='descValue'>{$op['tipo']}</span></td>"
                        . "</tr>"
                        . "<tr><td><span class='desc' style='color:#084B8A; '>Dimensão</span></td></tr> "
                        . "<tr>";
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sistema:</span></td><td  class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['sistema'] != '') {
                    $html .= "<td><span class='desc'>Sitema:</span></td><td class='td'> <span class='descValue'>{$op['sistema']}</span></td>";
                }
                if ($op['comprimento'] != '') {
                    $html .= "<td><span class='desc'>Comprimento:</span></td><td class='td'> <span class='descValue'>{$op['comprimento']}</span></td>";
                }
                if ($op['espessura'] != '') {
                    $html .= "<td><span class='desc'>Espessura:</span></td><td class='td'> <span class='descValue'>{$op['espessura']}</span></td>";
                }
                if ($op['pontaAtiva'] != '') {
                    $html .= "<td><span class='desc'>Ponta Ativa:</span></td><td class='td'> <span class='descValue'>{$op['pontaAtiva']}</span></td>";
                }
                if ($op['haste'] != '') {
                    $html .= "<td><span class='desc'>Haste:</span></td><td class='td'> <span class='descValue'>{$op['haste']}</span></td>";
                }
                if ($op['encaixe'] != '') {
                    $html .= "<td><span class='desc'>Encaixe:</span></td><td class='td'> <span class='descValue'>{$op['encaixe']}</span></td> ";
                }
                $html .= "<tr></tr>";
                if ($op['diametro'] != '') {
                    $html .= "<tr><td><span class='desc'>Diâmetro:</span></td><td class='td'> <span class='descValue'>{$op['diametro']}</span></td>";
                }
                $html .= "</tr>"
                        . "<tr>"
                        . "<td class='td1'><span class='desc'>Materia Prima:</span></td><td class='td'><span class='descValue'> {$op['materiaPrima']}</span></td> "
                        . "<td><span class='desc'>Marca:</span></td><td class='td'><span class='descValue'> {$op['marca']}</span></td>"
                        . "</tr>"
                        . "</table>"
                        . "<div class='aplicacao'>"
                        . "<span class='labelAplicacao'>Aplicação Anatômica</span><br>"
                        . "<span class='valueAplicacao'>".substr($op['aplicacao'],0,200)."</span>"
                        . "</div>"
                        . "</div>";

                if ($op['especificacaoFabricante'] === "Sim") {
                    $html .= "<span class='labelAplicacao'>Especificação do fabricante e outros</span><br>"
                            . "<span class='valueAplicacao'>".substr($op['obs'],0,647)."</span>";
                }
                $html .= "</div>";
                $html .= "</fieldset>";
                $html .= "</div>";
            }
            $i++;
        }
        $html .= "</div>";
        $html .= "</div>";
        return $html;
    }

    public function rodape($dadosElaboracaoLaudo) {
        $sexo = 'Dra.';
        if ($dadosElaboracaoLaudo[0]['sexo'] == 'M') {
            $sexo = 'Dr.';
        }
        $html = "
            <div class='divAssinatura'>
                <p class='localAssinatura'>__________________________________</p>
                <p class='nomeAssinatura'> " . $sexo . " " . $dadosElaboracaoLaudo[0]['nomeCirurgiao'] . " " . $dadosElaboracaoLaudo[0]['sobrenomeCirurgiao'] . "</p>
                <p class='registroAssinatura'> " . $dadosElaboracaoLaudo[0]['conselhoProfissional'] . "</p>
            </div>
                 ";
        return $html;
    }

    private function returnImgTag($arquivo)
    {
        $mimeTypeImg = get_headers(base_url('upload/') . $arquivo,1)['Content-Type'];
        $img64 = base64_encode(file_get_contents(base_url('upload/') . $arquivo));
        return "<img src='data:{$mimeTypeImg};base64, $img64' class='imgLaudo' />"; 
    }
}
