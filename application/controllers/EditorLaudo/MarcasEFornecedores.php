<?php

defined('BASEPATH') or exit('No direct script access allowed');

class MarcasEFornecedores extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('EditorLaudo/MarcasEForneceoresModel', 'pedidoAutorizacao');
        $this->load->helper('form');
        $this->load->helper('url');

        if (empty($this->session->username)) {
            redirect('Login');
        }
    }

    public function consultaFornecedores()
    {
        
        $dados = $this->input->post(null, true);
        $dados['codigoLaudoElaboracao'] = $this->session->codigoLaudoElaboracao;
        $getFornecedor = $this->pedidoAutorizacao->consultarFonecedores($dados);
        $arrayFornecedor = array();
        foreach ($getFornecedor as $v) {          
            array_push($arrayFornecedor, [
                "codigoFornecedor" => $v['codigoFornecedor'],
                "fornecedor" => $v['fornecedor'],
                "municipio" => $v['municipio'],
                "estado" => $v['estado'],
                "marcas" => explode(',', $v['marcas']),        
                "codigoMarcaPrimeiraMarca" => $v['codigoPrimeiraMarca'],
                "primeiraMarca" => $v['primeiraMarca']
            ]);
        }

        echo json_encode($arrayFornecedor);
    }

    public function consultaMunicipioFornecedores()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->pedidoAutorizacao->consultarMunicipioFornecedor($dados));
    }

    function removeFornecedor()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->pedidoAutorizacao->removeFornecedor($dados));
    }

    function preferenciaFornecedor()
    {
        $dados = $this->input->post(null, true);
        echo json_encode($this->pedidoAutorizacao->preferenciaFornecedor($dados));
    }

    function selecionaMateriais()
    {
        $dados = json_decode($this->input->post("filtros", true));
        $dado = (array) $dados;
        echo json_encode($this->pedidoAutorizacao->selecionaMateriais($dado));
    }

    function incluirMaterial()
    {
        $dados = json_decode($this->input->post("dados", true));
        $dado = (array) $dados;
        $dado['codigoLaudoElaboracao'] = $this->session->codigoLaudoElaboracao;
        echo json_encode($this->pedidoAutorizacao->incluirMaterial($dado));
    }

    public function selecionaFornecedor()
    {
        $dados = $this->input->post(null, true);
        $dados['codigoLaudoElaboracao'] = $this->session->codigoLaudoElaboracao;
        echo json_encode($this->pedidoAutorizacao->selecionaFornecedor($dados));
    }

    public function incluirRascunhoFinalidade()
    {
        $dados = $this->input->post(null, true);
        $dados['CodigoCirurgiao'] = $this->session->codigoCirurgiao;
        echo json_encode($this->pedidoAutorizacao->incluirRascunhoFinalidade($dados));
        die;
    }

    public function readRascunhoFinalidade()
    {
        $dados = $this->input->post(null, true);
        $dados['CodigoCirurgiao'] = $this->session->codigoCirurgiao;
        
        die(json_encode($this->pedidoAutorizacao->readRascunhoFinalidade($dados)));
    }
}
