<?php

class Clinica_model extends CI_Model {

    public function buscaUF() {
        $SQL = " SELECT * FROM tblEstados";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function getClinicas($codigoCirurgiao) {
        $this->db->select('*');
        $this->db->from('tblClinicas');
        $this->db->where('codigoCirurgiao', $codigoCirurgiao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function createClinicas($dados) {
        $this->db->set('nomeClinica', $dados['nomeClinica']);
        $this->db->set('cnpj', $dados['cnpj']);
        $this->db->set('cep', $dados['cep']);
        $this->db->set('uf', $dados['uf']);
        $this->db->set('municipio', $dados['municipio']);
        $this->db->set('endereco', $dados['endereco']);
        $this->db->set('complemento', $dados['complemento']);
        $this->db->set('telefoneComercial', $dados['telefoneComercial']);
        $this->db->set('email', $dados['email']);
        $this->db->set('codigoCirurgiao', $dados['codigoCirurgiao']);
        $this->db->set('bairro', $dados['bairro']);
        $this->db->set('logo',$dados['logo']);

        if ($this->db->insert('tblClinicas', $dados)) {
            return true;
        }

        return false;
    }

    public function deleteClinicas($codigoClinica) {
        $this->db->where('codigoClinica', $codigoClinica);
        if (!$this->db->delete('tblClinicas')) {
            return false;
        }
        return true;
    }

    public function updateClinica($dados) {
        $codigoClinica = $dados['codigoClinica'];
        unset($dados['codigoClinica']);

        $this->db->where('codigoClinica', $codigoClinica);
        if ($this->db->update('tblClinicas', $dados)) {
            return true;
        }

        return false;
    }

    public function getClinica($codigoClinica) {
        $this->db->select('*');
        $this->db->from('tblClinicas');
        $this->db->where('codigoClinica', $codigoClinica);
        $query = $this->db->get();
        return $query->row();
    }

    public function getClinicaFavorita($codigoCirurgiao)
    {
        $this->db->select('codigoClinica');
        $this->db->from('tblClinicaFavorita');
        $this->db->where('codigoCirurgiao', $codigoCirurgiao);
        $query = $this->db->get();
        $query = $query->row();

        if(empty($query)){
            return ['codigoClinica' => ''];
        }

        return $query;
    }

    public function getInsertedClinica()
    {
        return $this->db->insert_id();
    }

    public function createClinicaFavorita($params)
    {
        return $this->db->insert('tblClinicaFavorita', $params);
    }

    public function updateClinicaFavorita($params)
    {
        $this->db->set('codigoClinica',$params->codigoClinica);
        $this->db->where('codigoCirurgiao',$params->codigoCirurgiao);
        return $this->db->update('tblClinicaFavorita');
    }

}
