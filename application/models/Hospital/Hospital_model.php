<?php

class Hospital_model extends CI_Model {

    public function consultaSolicitacaoAcesso() {
        $SQL = "
        SELECT 
            a.codigoUsuario,
            a.nomeUsuario, 
            a.email, 
            a.telefone, 
            h.nome AS hospital, 
            h.uf, 
            h.municipio 
        FROM usuarios AS a
        LEFT JOIN tblhospitais AS h
        ON a.codigoPerfilUsuario = h.codigoHospital
        WHERE a.codigoPerfil = 2
        AND a.ativado = 0
        ";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function excluirSolicitacao($dados) {
        $this->db->where('codigoUsuario', $dados['codigoUsuario']);
        $this->db->delete('usuarios');
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function aprovarSolicitacao($dados) {
        $this->db->where('codigoUsuario', $dados['codigoUsuario']);
        $this->db->set('ativado', 1);
        $this->db->set('senha', md5($dados['senha']));
        return $this->db->update('usuarios');
    }

    public function getUsuarioEmail($dados) {
        $this->db->select('email');
        $this->db->from('usuarios');
        $this->db->where('codigoUsuario', $dados['codigoUsuario']);
        $query = $this->db->get();
        $result = $query->result_array();
        if (empty($result)) {
            return 0;
        }
        return $result[0]['email'];
    }

    public function getUsuarioNome($dados) {
        $this->db->select('nomeUSuario');
        $this->db->from('usuarios');
        $this->db->where('codigoUsuario', $dados['codigoUsuario']);
        $query = $this->db->get();
        $result = $query->result_array();
        if (empty($result)) {
            return 0;
        }
        return $result[0]['nomeUSuario'];
    }

    public function consultaAplicacaoAnatomica($dados) {
        $this->db->select('*');
        $this->db->from('viewAnatomiaAplicacao_consulta');
        if (!empty($dados['codigoAnatomia'])) {
            $this->db->where('codigoAnatomia', $dados['codigoAnatomia']);
        }
        if (!empty($dados['codigoAnatomiaAplicacaoFamilia'])) {
            $this->db->where('codigoAnatomiaAplicacaoFamilia', $dados['codigoAnatomiaAplicacaoFamilia']);
        }
        $this->db->order_by("cadastroCirurgiao", "desc");
        $this->db->order_by("nome", "asc");

        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultaAplicacaoAcaoRelMaterial($dados) {
        $this->db->select('codigoOpmeCategoria');
        $this->db->from('tblAnatomiaAcaoRel');
        $this->db->where('codigoAnatomiaAcao', $dados['codigoAnatomiaAcao']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultaAplicacaoAnatomicaNome($dados) {
        $this->db->select('codigoAnatomiaAplicacaoAnatomica');
        $this->db->from('tblAnatomiaAplicacaoAnatomica');
        $this->db->where('nome', $dados['nome']);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['codigoAnatomiaAplicacaoAnatomica'];
    }

    public function putActionButtonsTable($arrDados, $id) {
        foreach ($arrDados as $index => $value) {

            $arrDados[$index]['editar'] = '<button type="button" id="tableBtnEditar" '
                    . 'onclick="modalEditar(' . $arrDados[$index]['codigoAnatomia'] . ',\'' . $arrDados[$index]['anatomia'] . '\',' . $arrDados[$index]['codigoAnatomiaAplicacaoAnatomica'] . ',\'' . $arrDados[$index]['nome'] . '\',' . $arrDados[$index]['codigoAnatomiaAplicacao'] . ',\'' . $arrDados[$index]['nomeLado'] . '\')" class="btn btn-info btn-sm"><i class="fas fa-edit"></i></button>';
            $arrDados[$index]['excluir'] = "<button type='button' id='tableBtnExcluir' onclick='modalExcluir(" . $arrDados[$index]['codigoAnatomiaAplicacaoAnatomica'] . ")' class='btn btn-danger btn-sm'><i class='fas fa-trash-alt'></i></button>";
        }
        return $arrDados;
    }

    public function consultaAnatomia() {
        $this->db->select('*');
        $this->db->from('tblAnatomia');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultaAnatomiaLado() {
        $this->db->select('*');
        $this->db->from('tblAnatomiaAplicacao');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function inserirAcoes($dados) {
        $this->db->set('anatomiaAcao', $dados['acao']);
        return $this->db->insert('tblAnatomiaAcao');
    }

    public function inserirAnatomiaAplicacao($dados) {
        return $this->db->insert('tblAnatomiaAplicacaoAnatomica', $dados);
    }

    public function updateAplicacao($dados) {
        $this->db->where('codigoAnatomiaAplicacaoAnatomica', $dados['codigoAnatomiaAplicacaoAnatomica']);
        $this->db->set('codigoAnatomiaAplicacao', $dados['codigoAnatomiaAplicacao']);
        $this->db->set('codigoAnatomia', $dados['codigoAnatomia']);
        $this->db->set('nome', $dados['nome']);
        return $this->db->update('tblAnatomiaAplicacaoAnatomica', $dados);
    }

    public function updateAcoesCategoria($dados) {
        $this->db->set('codigoAnatomiaAcao', $dados['codigoAcao']);
        $this->db->set('codigoOpmeCategoria', $dados['codigoCategoria']);
        return $this->db->insert('tblAnatomiaAcaoRel');
    }

    public function excluirAcao($dados) {
        if ($this->deletarAcoesCategoria($dados)) {
            $this->db->where('codigoAnatomiaAcao', $dados['codigoAcao']);
            $this->db->delete('tblAnatomiaAcao');
            $this->db->trans_complete();
            if ($this->db->trans_status() === true) {
                $this->db->trans_rollback();
                return true;
            } else {
                $this->db->trans_rollback();
                return false;
            }
        }
    }

    public function deletarAcoesCategoria($dados) {
        $this->db->where('codigoAnatomiaAcao', $dados['codigoAcao']);
        $this->db->delete('tblAnatomiaAcaoRel');
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

}
