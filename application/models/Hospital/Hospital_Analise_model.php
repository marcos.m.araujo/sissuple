<?php

class Hospital_Analise_model extends CI_Model
{



    public function compartilharLaudo($dados)
    {
        $this->db->insert('tblLaudoHospital', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function getLaudoHospital($email, $where)
    {
        $this->db->select('Hosp.codigoLaudoHospital');
        $this->db->select('Hosp.codigoLaudoElaboracao');
        $this->db->select('md5(Hosp.codigoLaudoElaboracao) as codigoLaudoElaboracao_crip');
        $this->db->select('Hosp.nomeHospital');
        $this->db->select('Hosp.emailHospital');
        $this->db->select('Hosp.codigoAviso');
        $this->db->select('Hosp.dataEntradaPedido');
        $this->db->select("DATE_FORMAT(Hosp.dataEntradaPedido, '%d/%m/%Y %H:%i:%s') AS dataEntradaPedido_f");
        $this->db->select("DATE_FORMAT(Hosp.dataAceitePedido, '%d/%m/%Y %H:%i:%s') AS dataAceitePedido_f");
        $this->db->select('Hosp.codigoSituacaoLaudo');
        $this->db->select('Hosp.obs');
        $this->db->select('Laudo.codigoPaciente');
        $this->db->select('Pac.nomePaciente');
        $this->db->select('Pac.convenio');
        $this->db->select('SI.nome as situacaoLaudo');
        $this->db->select("concat(CI.nomeCirurgiao,' ', CI.sobrenomeCirurgiao) as nomeCirurgiao");
        $this->db->select('CI.CodigoCirurgiao');
        $this->db->select("GROUP_CONCAT(concat(TUSS.codigoProcedimentoComplemento, ' - ', TUSS.procedimento))as TUSS");
        $this->db->from('tblLaudoHospital as Hosp');
        $this->db->join('tblLaudoElaboracao as Laudo', 'Hosp.codigoLaudoElaboracao = Laudo.codigoLaudoElaboracao', 'left');
        $this->db->join('tblPaciente as Pac', 'Laudo.codigoPaciente = Pac.codigoPaciente', 'left');
        $this->db->join('tblLaudoElaboracaoPedidoProcedimentos as CP', 'Laudo.codigoLaudoElaboracao = CP.codigoLaudoElaboracao', 'left');
        $this->db->join('tblProcedimentos as TUSS', 'CP.codigoProcedimento = TUSS.codigoProcedimento', 'left');
        $this->db->join('tblCirurgiao as CI', 'Laudo.CodigoCirurgiao = CI.CodigoCirurgiao', 'left');
        $this->db->join('tbllaudosituacao as SI', 'Hosp.codigoSituacaoLaudo = SI.codigoSituacaoLaudo', 'left');
        $this->db->where($where);
        $this->db->where('Hosp.emailHospital', $email);

        $this->db->group_by('
            Hosp.codigoLaudoHospital,
            Hosp.codigoLaudoElaboracao,
            Hosp.nomeHospital,
            Hosp.emailHospital,
            Hosp.codigoAviso,
            Hosp.dataEntradaPedido,
            Hosp.codigoSituacaoLaudo,
            Hosp.obs,
            Laudo.codigoPaciente,
            Pac.nomePaciente,
            Pac.convenio
        ');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getCirurgiao($codigo)
    {
        $this->db->select('nomeCompleto, email');
        $this->db->from('veiwCirurgiao');
        $this->db->where('codigoCirurgiao', $codigo);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getLaudoElaboracao($codigo)
    {
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracao');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigo);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function setRecebeLaudo($dados)
    {
        unset($dados['nomePaciente']);
        unset($dados['codigoCirurgiao']);
        $this->db->where('codigoLaudoHospital', $dados['codigoLaudoHospital']);
        unset($dados['codigoLaudoHospital']);

        $this->db->update('tblLaudoHospital', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }
}
