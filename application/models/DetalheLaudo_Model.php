<?php

class DetalheLaudo_Model extends CI_Model
{
    public function getTimeLine($codigoLaudo)
    {
        $this->db->select('*');
        $this->db->from('tblTimeLine');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudo);
        $this->db->order_by('data desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDocumentos($codigoLaudo, $timeline = '')
    {
        $this->db->select('nome, tipoDocumento, arquivo, codigoLaudoElaboracaoExame, descricaoTextoLivre');
        $this->db->from('tblLaudoElaboracaoDocumento');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudo);

        if(!empty($timeline)){
            $where = " codigoLaudoElaboracaoExame NOT IN(
                            SELECT doc.codigoLaudoElaboracaoExame 
                            FROM  tblDocumtoTimeLine as doc
                            LEFT JOIN tblTimeLine as time
                            ON doc.codigoTimeLine = time.codigoTimeLine
                            WHERE md5(time.codigoLaudoElaboracao) = '{$codigoLaudo}')
            ";
            $this->db->where($where, null, false);
        }

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDocumentoTimeLine($codigoTimeLine){
        $this->db->select('doc.nome, doc.tipoDocumento, doc.arquivo, doc.codigoLaudoElaboracaoExame, doc.descricaoTextoLivre');
        $this->db->from('tblLaudoElaboracaoDocumento as doc');
        $this->db->join('tblTimeLine as time', 'time.codigoLaudoElaboracao = doc.codigoLaudoElaboracao','left');
        $this->db->join('tblDocumtoTimeLine as doctime', 'doctime.codigoTimeLine = time.codigoTimeLine and doctime.codigoLaudoElaboracaoExame = doc.codigoLaudoElaboracaoExame', 'left');
        $this->db->where('doctime.codigoTimeLine',$codigoTimeLine);
        $query = $this->db->get();
        return $query->result_array();
    }



    public function getCodigoCirurgiao($codigoLaudo)
    {
        $this->db->select('codigoCirurgiao');
        $this->db->from('tblCirurgiao');
        $this->db->where('codigoUsuario', $codigoLaudo);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['codigoCirurgiao'];
    }

    public function getCodigoDoLaudo($codigoLaudo)
    {
        $this->db->select('codigoLaudoElaboracao');
        $this->db->from('tblLaudoElaboracao');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudo);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function setTimeLine($dados)
    {
        $this->db->insert('tblTimeLine', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return $this->db->insert_id();
        }
    }
}
