<?php

class ApiModel extends CI_Model {
    /* Aplicativo */

    public function getDadosPacienteFullAPP($codigoCirurgiao) {
        $this->db->select('*');
        $this->db->where('codigoCirurgiao', $codigoCirurgiao);
        $this->db->from('viewPaciente');
        $this->db->order_by("nomePaciente asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTratamentos() {
        $this->db->select('*');
        $this->db->from('tblTratamentos');
        $this->db->order_by("codigoTratamento asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getTratamentosPrevitos($codigoPaciente) {
        $this->db->select('*');
        $this->db->where('codigoPaciente', $codigoPaciente);
        $this->db->from('tblPacienteLaudoPrevisto');
        $this->db->order_by("codigoTratamento asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDadosPacienteIDAPP($codigoPaciente) {
        $this->db->select('*');
        $this->db->where('codigoPaciente', $codigoPaciente);
        $this->db->from('viewPaciente');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDadosPacienteIDLikeAPP($nomePaciente, $codigoCirurgiao) {
        $this->db->select('*');
        $this->db->where('codigoCirurgiao', $codigoCirurgiao);
        $this->db->like('nomePaciente', $nomePaciente);
        $this->db->from('viewPaciente');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultarPacientesCPFAPP($CPF) {
        $CPF = $this->limpaDocumentos($CPF);
        $this->db->select('*');
        $this->db->from('viewPaciente');
        $this->db->where('CPF_', $CPF);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function editarPacienteAPP($dados) {
        $CPF = $this->limpaDocumentos($dados->CPF);
        $this->db->where('codigoPaciente', $dados->codigoPaciente);
        $this->db->set('nomePaciente', $dados->nomePaciente);
        $this->db->set('nomeMaePaciente', $dados->nomeMaePaciente);
        $this->db->set('dataNascimento', $dados->dataNascimento);
        $this->db->set('convenio', $dados->convenio);
        $this->db->set('CPF', $CPF);
        $this->db->set('numeroCarteirinha', $dados->numeroCarteirinha);
        $this->db->set('dataInicioContratoConvenio', $dados->dataInicioContratoConvenio);
        $this->db->set('email', $dados->email);
        $this->db->set('telefone', $dados->telefone);
        $this->db->set('codigoCirurgiao', $dados->codigoCirurgiao);
        $this->db->set('nomeOrtodontista', $dados->nomeOrtodontista);
        $this->db->set('telefoneOrtodontista', $dados->telefoneOrtodontista);
        $this->db->update('tblPaciente');

        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function cadastrarPacienteAPP($dados) {
        $CPF = $this->limpaDocumentos($dados->CPF);
        $this->db->set('nomePaciente', $dados->nomePaciente);
        $this->db->set('nomeMaePaciente', $dados->nomeMaePaciente);
        $this->db->set('dataNascimento', $dados->dataNascimento);
        $this->db->set('convenio', $dados->convenio);
        $this->db->set('CPF', $CPF);
        $this->db->set('numeroCarteirinha', $dados->numeroCarteirinha);
        $this->db->set('dataInicioContratoConvenio', $dados->dataInicioContratoConvenio);
        $this->db->set('email', $dados->email);
        $this->db->set('telefone', $dados->telefone);
        $this->db->set('codigoCirurgiao', $dados->codigoCirurgiao);
        $this->db->set('nomeOrtodontista', $dados->nomeOrtodontista);
        $this->db->set('telefoneOrtodontista', $dados->telefoneOrtodontista);
        $this->db->insert('tblPaciente');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function excluirPacienteAPP($dados) {
        $this->db->where('codigoPaciente', $dados);
        return $this->db->delete("tblPaciente");
    }

    public function getElaboracaoPacienteAPP($codigoPaciente) {
        $this->db->select('codigoLaudoElaboracao');
        $this->db->where('codigoPaciente', $codigoPaciente);
        $this->db->from('viewLaudoElaboracao');
        $query = $this->db->get();
        return $query->result_array();
    }

    function limpaDocumentos($valor) {
        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);
        return $valor;
    }

    public function insertTratamentoPrevisto($dados) {
        if (!$this->db->insert('tblPacienteLaudoPrevisto', $dados)) {
            return false;
        }
        return true;
    }

    public function deleteTratamentoPrevisto($codigoPaciente) {
        $this->db->where('codigoPaciente', $codigoPaciente);
        if (!$this->db->delete('tblPacienteLaudoPrevisto')) {
            return false;
        }
        return true;
    }

}
