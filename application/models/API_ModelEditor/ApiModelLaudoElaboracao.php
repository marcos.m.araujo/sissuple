<?php

class ApiModelLaudoElaboracao extends CI_Model {
    /* Aplicativo */

    public function getHospital($nomeHospital) {
        $this->db->select('codigoHospital, nome, uf, municipio, endereco');
        $this->db->like('busca', $nomeHospital);
        $this->db->from('viewHospitais');
        $this->db->limit(20);
        $this->db->order_by("nome asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSintomas($codigoTratamento) {
        $this->db->select('*');
        $this->db->from('tblSintomas');
        $this->db->where('codigoTratamento', $codigoTratamento);
        $this->db->order_by("sintomas asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDiagnostico($codigoTratamento) {
        $this->db->select('*');
        $this->db->from('tblDiagnosticos');
        $this->db->where('codigoTratamento', $codigoTratamento);
        $this->db->order_by("diagnostico asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCID($codigoTratamento) {
        $this->db->select('*');
        $this->db->from('tblCID');
        $this->db->where('codigoTratamento', $codigoTratamento);
        $this->db->order_by("CID asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getIntervencao($codigoTratamento) {
        $this->db->select('*');
        $this->db->from('tblIntervencao');
        $this->db->where('codigoTratamento', $codigoTratamento);
        $this->db->order_by("intervencao asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getProcedimentos($codigoTratamento) {
        $this->db->select('*');
        $this->db->from('tblProcedimentos');
        $this->db->where('codigoTratamento', $codigoTratamento);
        $this->db->order_by("procedimento asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getObjetivos($codigoTratamento) {
        $this->db->select('*');
        $this->db->from('tblAdminObjetivosTratmento');
        $this->db->where('codigoTratamento', $codigoTratamento);
        $this->db->order_by("objetivo asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getSequelas($codigoTratamento) {
        $this->db->select('*');
        $this->db->from('tblSequelas');
        $this->db->where('codigoTratamento', $codigoTratamento);
        $this->db->order_by("sequela asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMarcas() {
        $SQL = "SELECT 
                distinct codigoMarca, marca
                FROM viewOpmes
                where codigoMarca is not null
                and codigoMarca != 0 and codigoOpmeCategoria = 3";
        $SQL .= "    order by marca asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function setLaudoElaboracao($dados) {
     
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->set('codigoPaciente', $dados->codigoPaciente);
        $this->db->set('codigoCirurgiao', $dados->codigoCirurgiao);
        $this->db->set('dataLaudo', $data);
        $this->db->set('codigoSituacaoLaudo', 1);
        $this->db->insert('tblLaudoElaboracao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            $codigoLaudoElaboracao = $this->getCodigoLaudoElaboracao($dados->codigoCirurgiao, $dados->codigoPaciente, $data);
            return $this->setDadosCirurgicos($dados, $codigoLaudoElaboracao);
        }
    }

    public function getCodigoLaudoElaboracao($param, $param2, $param3) {
        $this->db->select('codigoLaudoElaboracao');
        $this->db->from('tblLaudoElaboracao');
        $this->db->where('codigoCirurgiao', $param);
        $this->db->where('codigoPaciente', $param2);
        $this->db->where('dataLaudo', $param3);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result[0]['codigoLaudoElaboracao'];
    }

    public function setDadosCirurgicos($dados, $codigoLaudoElaboracao) {
        $this->db->set('codigoLaudoElaboracao', $codigoLaudoElaboracao);
        $this->db->set('diariasInternacao', $dados->diariasInternacao);
        $this->db->set('diariasUTI', $dados->diariasUTI);
        $this->db->set('tempoProcedimento', $dados->tempoProcedimento);
        $this->db->set('tipoAnestesia', $dados->tipoAnestesia);
        $this->db->set('estado', $dados->estado);
        $this->db->set('hospital', $dados->hospital);
        $this->db->set('codigoEquipeCirurgica', $dados->codigoEquipeCirurgica);
        $this->db->set('tipoHonorario', $dados->tipoHonorario);
        $this->db->set('dataCirurgia', $dados->dataCirurgia);
        $this->db->insert('tblLaudoElaboracaoDadosCirurgia');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

}
