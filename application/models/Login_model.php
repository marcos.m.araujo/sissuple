
<?php

class Login_model extends CI_Model {

    public function consultaLogin($dados) {
        $this->db->select('ativado');
        $this->db->select('codigoPerfil');
        $this->db->select('codigoUsuario');
        $this->db->select('email');
        $this->db->select('nomeUsuario');
        $this->db->select('token');
        $this->db->select('telefone');
        $this->db->select('codigoPerfilUsuario');
        $this->db->from('usuarios');
        $this->db->where('email', $dados['email']);
        $this->db->where('senha', md5($dados["senha"]));
        $query = $this->db->get();
        return $query->row_array();
    }

    public function cadastraUsuario($dados) {
        $this->db->set('nomeUsuario', $dados["nomeCompleto"]);
        $this->db->set('email', $dados["email"]);
        $this->db->set('senha', md5($dados["senha"]));
        $this->db->insert('usuarios');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function recuperaSenha($dados) {
        $this->db->select('email');
        $this->db->from('usuarios');
        $this->db->where('email', $dados['email']);
        $query = $this->db->get();
        $result = $query->result_array();
        if (!empty($result)) {
            $emailBanco = $result[0]['email'];
            if ($emailBanco == $dados['email']) {
                $numero_de_bytes = 4;
                $restultado_bytes1 = random_bytes($numero_de_bytes);
                $dados['senha'] = bin2hex($restultado_bytes1);
                $this->db->where('email', $dados["email"]);
                $this->db->set('senha', md5($dados["senha"]));
                $this->db->update('usuarios');
                $this->db->trans_complete();
                if ($this->db->trans_status() === false) {
                    $this->db->trans_rollback();
                    return false;
                } else {
                    $this->db->trans_commit();
                    return $dados['senha'];
                }
            }
        } else {
            return false;
        }
    }

    public function updateSenha($dados){

        $this->db->set('senha',md5($dados['senha']));
        $this->db->where('codigoUsuario', $dados['codigoUsuario']);
        if(!$this->db->update('usuarios')){
            return false;
        }
        return true;
    }

}
