<?php

class Laudo_Pedido_Autorizacao_model extends CI_Model
{

    public function excluirLaudo($dados)
    {

 
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('timeLine');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoHospital');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoDadosCirurgia');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoMaterial');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoPedidoCIDs');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoPedidoDiagnosticos');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoPedidoIntervencoes');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoPedidoIntervencoes');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoPedidoObjtivos');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoPedidoProcedimentos');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoPedidoSequelas');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracaoPedidoSintomas');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoMarca');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoMarcaOpmeUtilizada');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->delete('tblLaudoElaboracao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function consultaMarcasLaudosInsumos()
    {
        $SQL = "SELECT 
                distinct codigoMarca, marca
                FROM viewOpmes
                where codigoMarca is not null
                and codigoMarca != 0 ";
        $SQL .= "    order by marca asc";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function salvarMarcas($dados)
    {
        $this->db->trans_begin();
        foreach ($dados as $dado) {
            $this->db->insert('tblLaudoMarca', $dado);
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function deletarMarcas($dados, $codigoLaudoElaboracao)
    {
        foreach ($dados as $d) {
            $this->db->where('codigoMarca', $d);
            $this->db->where('codigoLaudoElaboracao', $codigoLaudoElaboracao);
            $this->db->delete("tblLaudoMarca");
        }
    }

    public function consultaProdudosSelecionados($dados)
    {
        $this->db->where_in('codigoMarca', $dados);
        $this->db->where('codigoOpmeMaterial', $dados['codigoMaterial']);
        $this->db->order_by("codigoMarca");
        $query = $this->db->get('viewOpmes');
        return $query->result();
    }

    public function clonarLaudoElaboracao($novoCodigo, $codigoAntigo)
    {
        $query = $this->db->query("call SetModeloLaudo({$novoCodigo}, {$codigoAntigo})");
    }

    public function getLaudoElaboracaoCodigoLaudo($dados)
    {
        $this->db->select('md5(codigoLaudoElaboracao) as codigoLaudoElaboracaoCRIP, codigoLaudoElaboracao, codigoSituacaoLaudo, codigoPaciente, (select codigoTratamento from tblLaudoElaboracaoPedidoSintomas as sint where sint.codigoLaudoElaboracao = laudo.codigoLaudoElaboracao limit 1 ) as tratamento');
        $this->db->from('tblLaudoElaboracao laudo');
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $this->db->where('codigoPaciente', $dados['codigoPaciente']);
        $this->db->order_by("codigoLaudoElaboracao desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLaudoElaboracaoCodigoLaudoCRIP($dados)
    {
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracao');
        $this->db->where("codigoLaudoElaboracaoCRIP", $dados);
        $this->db->limit(1);
        $this->db->order_by("dataLaudoR desc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLaudoElaboracao($dados)
    {
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracao');
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $query = $this->db->get();
        return $query->result_array();
    }
 

    public function getDadosCirurgiaoCodigoUsuario($dados)
    {
        $this->db->select('*');
        $this->db->from('tblCirurgiao');
        $this->db->where('codigoUsuario', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLaudoElaboracaoPendente($dados)
    {
        $codigos = array('1', '2');
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracao');
        $this->db->where('codigoCirurgiao', $dados['codigoCirurgiao']);
        $this->db->where_in('codigoSituacaoLaudo', $codigos);
        if (!empty($dados['codigoLaudoElaboracao']))
            $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->order_by("dataLaudo", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLaudoElaboracaoArquivados($dados)
    {
        $codigos = array('8', '9');
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracao');
        $this->db->where('codigoCirurgiao', $dados['codigoCirurgiao']);
        $this->db->where_in('codigoSituacaoLaudo', $codigos);
        if (!empty($dados['codigoLaudoElaboracao']))
            $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->order_by("dataLaudo", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAlertasVencimentosLaudos()
    {
        $SQL = "SELECT  (30 - (TO_DAYS(CURDATE()) - TO_DAYS(laudo.DataProtocoloPedido))) AS diasRestantes,
                count(*) as total
                FROM tblLaudoElaboracao laudo
                where (30 - (TO_DAYS(CURDATE()) - TO_DAYS(laudo.DataProtocoloPedido))) <=10
                and codigoCirurgiao = {$this->session->codigoCirurgiao} and codigoSituacaoLaudo = 3
                group by  (30 - (TO_DAYS(CURDATE()) - TO_DAYS(laudo.DataProtocoloPedido))) ";

        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function getAlertasLaudosCompartilhados()
    {
        $SQL = "SELECT
                DATE_FORMAT(dataCompartilhamento, '%d/%m/%Y %H:%i') AS dataLaudo
                FROM tblCompartilhamentoLaudoModelo
                where email = '{$this->session->email}'
                and (TO_DAYS(CURDATE()) - TO_DAYS(dataCompartilhamento)) <= 5";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function getDadosCirurgiaPaciente($dados)
    {
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracao');
        $this->db->where('codigoCirurgiao', $dados['codigoCirurgiao']);
        if (!empty($dados['codigoLaudoElaboracao']))
            $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLaudoElaboracaoEmAnalise($dados)
    {
        $codigos = array('3', '4');
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracao');
        $this->db->where('codigoCirurgiao', $dados['codigoCirurgiao']);
        $this->db->where_in('codigoSituacaoLaudo', $codigos);
        if (!empty($dados['codigoLaudoElaboracao']))
            $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->order_by("dataLaudo", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLaudoElaboracaoConcluido($dados)
    {
        $codigos = array('5', '6', '7');
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracao');
        $this->db->where('codigoCirurgiao', $dados['codigoCirurgiao']);
        $this->db->where_in('codigoSituacaoLaudo', $codigos);
        if (!empty($dados['codigoLaudoElaboracao']))
            $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        if (!empty($dados['codigoPaciente']))
            $this->db->where('codigoPaciente', $dados['codigoPaciente']);
        $this->db->order_by("dataLaudo", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getLaudoSituacao()
    {
        $this->db->select('*');
        $this->db->from('tblLaudoSituacao');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function putActionButtonsGetLaudoElaboracaoList($arrDados, $id)
    {
        foreach ($arrDados as $index => $value) {
            $arrDados[$index]['lupa'] = '<a href=' . base_url() . 'Laudos/Laudo_Pedido_Autorizacao/?q=' . md5($arrDados[$index]['codigoPaciente']) . ' class="text-muted"><i class="fa fa-folder-open"></i></a>';
            $arrDados[$index][''] = '<a  class="text-muted"><i class="fa fa-folder-open"></i></a>';
            if ($arrDados[$index]['codigoSituacaoConfeccao'] == 0) {
                $arrDados[$index]['situacaoLaudo'] = '<span style="padding: 5px" class="badge bg-warning">Laudo em confecção</span>';
            } else {
                $arrDados[$index]['situacaoLaudo'] = '<span style="padding: 5px" class="badge bg-success">Laudo finalizado</span>';
            }
        }
        return $arrDados;
    }

    //funcao não está legal, vou mudar para abaixo buscando pelo codigo do laudo elaboracao
    public function getLaudoElaboracaoCodigoElaboracao($dados)
    {
        $SQL = "SELECT LaudoElaboracao.*,
                DadosCirurgia.codigoLaudoElaboracaoDadosCirurgia,
                DadosCirurgia.codigoLaudoElaboracaoDadosCirurgia,
		        DadosCirurgia.diariasInternacao,
                DadosCirurgia.diariasUTI,
                DadosCirurgia.tempoProcedimento,
                DadosCirurgia.tipoAnestesia,
                DadosCirurgia.estado,
                DadosCirurgia.hospital,
                DadosCirurgia.codigoEquipeCirurgica,
                DadosCirurgia.tipoHonorario,
                DadosCirurgia.dataCirurgia,
                DadosCirurgia.dataCirurgiaF,
                DadosCirurgia.equipe,
                pl.nomePlano,
                Cirurgiao.nomeCirurgiao,
                Cirurgiao.sobrenomeCirurgiao,
                Cirurgiao.sexo,                
                CP.sigla as sigla,
                UF.uf as ufcp,
                Cirurgiao.numeroConselhoProfissional  as numeroConselhoProfissional,
                concat(CP.sigla, '/' , UF.uf, ' ', Cirurgiao.numeroConselhoProfissional) as conselhoProfissional,
                Clinicas.nomeClinica,
                Clinicas.cnpj, 
                concat(Clinicas.endereco, ' ' ,Clinicas.complemento, ' ' ,Clinicas.municipio, ' - ' ,ClinicaUf.uf) as endereco,
                concat(Clinicas.email, ' / ' ,Clinicas.telefoneComercial) as contatoClinica
                FROM viewLaudoElaboracao as LaudoElaboracao
                left join viewLaudoElaboracaoDadosCirurgia as DadosCirurgia
                on LaudoElaboracao.codigoLaudoElaboracao = DadosCirurgia.codigoLaudoElaboracao 
                left join tblplanosaude as pl
                on LaudoElaboracao.CodigoPlano = pl.CodigoPlano 
                left join tblCirurgiao as Cirurgiao
                on LaudoElaboracao.codigoCirurgiao = Cirurgiao.codigoCirurgiao
                left join tblClinicaFavorita as clinicaF
                on clinicaF.codigoCirurgiao = LaudoElaboracao.codigoCirurgiao
                left join tblClinicas as Clinicas               
                on  clinicaF.codigoClinica = Clinicas.codigoClinica
		        left join tblEstados as UF
		        on Cirurgiao.codigoEstado = UF.codigoEstado
                left join tblEstados as ClinicaUf
		        on ClinicaUf.codigoEstado = Clinicas.uf
                left join tblConselhoProfissional as CP
                on Cirurgiao.codigoConselhoProfissional = CP.codigoConselhoProfissional ";
        $SQL .= "where LaudoElaboracao.codigoLaudoElaboracaoCRIP = '{$dados}' limit 1 ";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }


    public function cadastrarLaudoElaboracao($dados)
    {
        $this->db->insert('tblLaudoElaboracao', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function editarLaudoElaboracao($dados)
    {
        $this->db->where('codigoLaudoElaboracao', $dados['codigoLaudoElaboracao']);
        $this->db->set('codigoSituacaoLaudo', $dados['codigoSituacaoLaudo']);
        $this->db->update('tblLaudoElaboracao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function getListPacientes()
    {
        $this->db->select('*');
        $this->db->from('viewPaciente');
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function putActionButtonsgetListPacientes($arrDados, $id)
    {
        foreach ($arrDados as $index => $value) {
            $arrDados[$index]['nome'] = '<a href= ' . base_url() . 'Laudos/Laudo_Pedido_Autorizacao/?q=' . md5($arrDados[$index]['codigoPaciente']) . '>' . $arrDados[$index]['nomePaciente'] . '</a>';
        }
        return $arrDados;
    }

    public function updateLaudoElaboracao($dados, $id)
    {
        $this->db->trans_begin();
        $this->db->where('codigoLaudoElaboracao', $id);
        $this->db->update('tblLaudoElaboracao', $dados);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return true;
    }

    public function updateLaudoElaboracaoAnalisePlano($dados, $id)
    {
        date_default_timezone_set("Brazil/East");
        $data = date("Y-m-d H:i:s");
        $this->db->trans_begin();
        $this->db->set('dataUltimaSituacaoLaudo', $data);
        $this->db->where('codigoLaudoElaboracao', $id);
        $this->db->update('tblLaudoElaboracao', $dados);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return true;
    }

    public function insertMarcasAutorizada($marcas, $codigoLaudoElaboracao)
    {
        $this->db->trans_begin();
        foreach ($marcas as $marca) {
            $dados = [
                'codigoMarca' => $marca,
                'codigoLaudoElaboracao' => $codigoLaudoElaboracao
            ];
            $this->db->replace('tblLaudoMarcaOpmeUtilizada', $dados);
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return true;
    }

    public function getMarcaOpmeAutorizada($dados = [], $select = ['*'])
    {
        $this->db->select($select);
        $this->db->from('tblLaudoMarcaOpmeUtilizada as opme');
        $this->db->join('tblMarcas as marca', 'opme.codigoMarca = marca.codigoMarca', 'left');
        $this->db->where(array_filter($dados));
        $query = $this->db->get();
        return $query->result_array();
    }
}
