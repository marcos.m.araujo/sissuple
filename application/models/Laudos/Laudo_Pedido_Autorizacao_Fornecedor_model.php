<?php

class Laudo_Pedido_Autorizacao_Fornecedor_model extends CI_Model {

    public function buscaFornecedores($dados) {
        $SQL = "SELECT 
                distinct codigoFornecedor
                FROM tblFornecedoresMarcas 
                where codigoMarca = {$dados['codigoMarca']}";

        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function buscaMarcasDosFornecedores($dados) {      
       $SQL = "SELECT distinct codigoMarca FROM tblFornecedoresMarcas where codigoFornecedor in ({$dados})";
       $query = $this->db->query($SQL);
       return $query->result();
  
    }

}
