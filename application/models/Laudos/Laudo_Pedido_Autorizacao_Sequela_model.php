<?php

class Laudo_Pedido_Autorizacao_Sequela_model extends CI_Model {

    public function salvar($dados) {
        $this->db->insert('tblLaudoElaboracaoPedidoSequelas', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function deletar($dados) {
        $this->db->where('codigoSequela', $dados['codigoSequela']);
        $this->db->delete("tblLaudoElaboracaoPedidoSequelas");
    }

    public function getSequela($dados) {
        $SQL = "SELECT 
                cods.codigoSequela,
                dia.sequela
                FROM tblLaudoElaboracaoPedidoSequelas as cods
                left join tblSequelas as dia
                on cods.codigoSequela = dia.codigoSequelas
            where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    //verifica qual o ultimo e penultimo registro para acrescentar o E
    public function putActionButtons($arrDados, $id) {
        $max = count($arrDados) - 1;
        $penultimo = count($arrDados) - 2;
        if (count($arrDados) == 1) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["sequela"] . '.';
            }
        } if (count($arrDados) == 2) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["sequela"] . '';
                $arrDados[$max]['check'] = ' e ' . $arrDados[$index]["sequela"] . '. ';
            }
        } if (count($arrDados) > 2) {
            foreach ($arrDados as $index => $value) {
                if ($index < $penultimo) {
                    $arrDados[$index]['check'] = '' . $arrDados[$index]["sequela"] . ', ';
                }
                $arrDados[$penultimo]['check'] = '' . $arrDados[$penultimo]["sequela"] . ' ';
                $arrDados[$max]['check'] = 'e ' . $arrDados[$index]["sequela"] . '. ';
            }
        }
        return $arrDados;
    }

}
