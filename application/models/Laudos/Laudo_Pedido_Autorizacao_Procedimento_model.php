<?php

class Laudo_Pedido_Autorizacao_Procedimento_model extends CI_Model {

    public function salvar($dados) {
        $this->db->insert('tblLaudoElaboracaoPedidoProcedimentos', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function deletar($dados) {
        $this->db->where('codigoProcedimento', $dados['codigoProcedimento']);
        $this->db->delete("tblLaudoElaboracaoPedidoProcedimentos");
    }

    public function getProcedimento($dados) {
        $SQL = "SELECT 
                cods.codigoProcedimento,
                concat(dia.procedimento, ' (', dia.codigoProcedimentoComplemento, ' - TUSS)') as procedimento,
                dia.codigoProcedimentoComplemento
                FROM tblLaudoElaboracaoPedidoProcedimentos as cods
                left join tblProcedimentos as dia
                on cods.codigoProcedimento = dia.codigoProcedimento
            where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    //verifica qual o ultimo e penultimo registro para acrescentar o E
    public function putActionButtons($arrDados, $id) {
        $max = count($arrDados) - 1;
        $penultimo = count($arrDados) - 2;
        if (count($arrDados) == 1) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["procedimento"] . '.';
            }
        } if (count($arrDados) == 2) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["procedimento"] .'';
                $arrDados[$max]['check'] = ' e ' . $arrDados[$index]["procedimento"] . '. ';
            }
        } if (count($arrDados) > 2) {
            foreach ($arrDados as $index => $value) {
                if ($index < $penultimo) {
                    $arrDados[$index]['check'] = '' . $arrDados[$index]["procedimento"] .', ';
                }
                $arrDados[$penultimo]['check'] = '' . $arrDados[$penultimo]["procedimento"] . ' ';
                $arrDados[$max]['check'] = 'e ' . $arrDados[$index]["procedimento"] . '. ';
            }
        }
        return $arrDados;
    }

}
