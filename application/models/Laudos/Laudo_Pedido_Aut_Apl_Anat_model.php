<?php

class Laudo_Pedido_Aut_Apl_Anat_model extends CI_Model {

    public function consultaAnatomia() {
        $SQL = "SELECT * FROM tblAnatomia";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function consultaAnatomiaAcao($dados) {
        $SQL = "SELECT * FROM viewAnatomiaAcaoRelacionamento where codigoOpmeCategoria = {$dados['codigoCategoria']} order by anatomiaAcao ";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function consultaAnatomiaTermosPontoA($dados) {
        $SQL = "SELECT * FROM viewAnatomiaAplicacao_consulta  where codigoAnatomiaAplicacao = 1 and codigoAnatomia = {$dados['codigoAnatomia']} order by nome ";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function consultaAnatomiaTermosPontoB($dados) {
        $SQL = "SELECT * FROM viewAnatomiaAplicacao_consulta  where codigoAnatomiaAplicacao = 2 and codigoAnatomia = {$dados['codigoAnatomia']} order by nome ";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function consultaAnatomiaRegiaoA($dados) {
        $SQL = "SELECT *FROM viewAnatomiaAplicacao_consulta  where codigoAnatomiaAplicacao = 3 and codigoAnatomia = {$dados['codigoAnatomia']} order by nome ";
        $query = $this->db->query($SQL);
        return $query->result();
    }
//
//    public function consultaAnatomiaRegiaoB($dados) {
//        $SQL = "SELECT * FROM viewAnatomiaRegiaoRelacionamento where lado = 2 and codigoAnatomia = {$dados['codigoAnatomia']}";
//        $query = $this->db->query($SQL);
//        return $query->result();
//    }

    public function consultaAnatomiaSitioA($dados) {
        $SQL = "SELECT * FROM viewAnatomiaAplicacao_consulta  where codigoAnatomiaAplicacao = 5 and codigoAnatomia = {$dados['codigoAnatomia']} order by nome ";
        $query = $this->db->query($SQL);
        return $query->result();
    }

//    public function consultaAnatomiaSitioB($dados) {
//        $SQL = "SELECT * FROM viewAnatomiaSitioAnatomicoRelacionamento where lado = 2 and tblanatomia_codigoAnatomia = {$dados['codigoAnatomia']}";
//        $query = $this->db->query($SQL);
//        return $query->result();
//    }

}
