<?php

class Laudo_Pedido_Autorizacao_Diagnostico_model extends CI_Model {

    public function salvarDiagnosticos($dados) {
        $this->db->insert('tblLaudoElaboracaoPedidoDiagnosticos', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function getDiagnostico($dados) {
        $SQL = "SELECT 
                cods.codigoDiagnostico,
                dia.diagnostico
                FROM tblLaudoElaboracaoPedidoDiagnosticos as cods
                left join tblDiagnosticos as dia
                on cods.codigoDiagnostico = dia.codigoDiagnostico
            where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function deletar($dados) {
        $this->db->where('codigoDiagnostico', $dados['codigoDiagnostico']);
        $this->db->delete("tblLaudoElaboracaoPedidoDiagnosticos");
    }

    //verifica qual o ultimo e penultimo registro para acrescentar o E
    public function putActionButtons($arrDados, $id) {
        $max = count($arrDados) - 1;
        $penultimo = count($arrDados) - 2;
        if (count($arrDados) == 1) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["diagnostico"] . '.';
            }
        } if (count($arrDados) == 2) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["diagnostico"] . '';
                $arrDados[$max]['check'] = ' e ' . $arrDados[$index]["diagnostico"] . '. ';
            }
        } if (count($arrDados) > 2) {
            foreach ($arrDados as $index => $value) {
                if ($index < $penultimo) {
                    $arrDados[$index]['check'] = '' . $arrDados[$index]["diagnostico"] . ', ';
                }
                $arrDados[$penultimo]['check'] = '' . $arrDados[$penultimo]["diagnostico"] . ' ';
                $arrDados[$max]['check'] = 'e ' . $arrDados[$index]["diagnostico"] . '. ';
            }
        }
        return $arrDados;
    }

}
