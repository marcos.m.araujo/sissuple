<?php

class Laudo_Pedido_Autorizacao_Sintoma_model extends CI_Model {

    public function salvarSintomas($dados) {
        $this->db->replace('tblLaudoElaboracaoPedidoSintomas', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function deletar($dados) {
        $this->db->where('codigoSintoma', $dados['codigoSintoma']);
        $this->db->delete("tblLaudoElaboracaoPedidoSintomas");
    }

    public function getSintomas($dados) {
        $SQL = "select cods.codigoSintoma,
            sint.sintomas
            FROM tblLaudoElaboracaoPedidoSintomas as cods
            left join tblSintomas as sint
            on cods.codigoSintoma = sint.codigoSintoma 
            where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    //verifica qual o ultimo e penultimo registro para acrescentar o E
    public function putActionButtons($arrDados, $id) {
        $max = count($arrDados) - 1;
        $penultimo = count($arrDados) - 2;
        if (count($arrDados) == 1) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["sintomas"] . '.';
            }
        } if (count($arrDados) == 2) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["sintomas"] . '';
                $arrDados[$max]['check'] = ' e ' . $arrDados[$index]["sintomas"] . '. ';
            }
        } if (count($arrDados) > 2) {
            foreach ($arrDados as $index => $value) {
                if ($index < $penultimo) {
                    $arrDados[$index]['check'] = '' . $arrDados[$index]["sintomas"] . ', ';
                }
                $arrDados[$penultimo]['check'] = '' . $arrDados[$penultimo]["sintomas"] . ' ';
                $arrDados[$max]['check'] = 'e ' . $arrDados[$index]["sintomas"] . '. ';
            }
        }
        return $arrDados;
    }

}
