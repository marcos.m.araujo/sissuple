<?php

class Laudo_Pedido_Autorizacao_DadosCirurgicos_model extends CI_Model {

    public function getDadosCirurgicos($dados) {
        $this->db->select('*');
        $this->db->from('viewLaudoElaboracaoDadosCirurgia');
        $this->db->where('codigoLaudoElaboracao', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getEquipeCirurgica() {
        $this->db->select('*');
        $this->db->from('tblEquipeCirurgica');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function setDadosCirurgicos($dados) {
        $this->db->insert('tblLaudoElaboracaoDadosCirurgia', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function setUpdateDadosCirurgicos($dados) {
        $this->db->where('codigoLaudoElaboracaoDadosCirurgia', $dados['codigoLaudoElaboracaoDadosCirurgia']);
        $this->db->update('tblLaudoElaboracaoDadosCirurgia', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

}
