<?php

class Laudo_Pedido_Autorizacao_Intervencao_model extends CI_Model {

    public function salvarIntervencao($dados) {
        $this->db->insert('tblLaudoElaboracaoPedidoIntervencoes', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function deletar($dados) {
        $this->db->where('codigoIntervencao', $dados['codigoIntervencao']);
        $this->db->delete("tblLaudoElaboracaoPedidoIntervencoes");
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function getIntervencao($dados) {

        $SQL = "SELECT 
                cods.codigoIntervencao,
                dia.intervencao
                FROM tblLaudoElaboracaoPedidoIntervencoes as cods
                left join tblIntervencao as dia
                on cods.codigoIntervencao = dia.codigoIntervencao
                where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    //verifica qual o ultimo e penultimo registro para acrescentar o E
    public function putActionButtons($arrDados, $id) {
        $max = count($arrDados) - 1;
        $penultimo = count($arrDados) - 2;
        if (count($arrDados) == 1) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["intervencao"] . '.';
            }
        } if (count($arrDados) == 2) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["intervencao"] . '';
                $arrDados[$max]['check'] = ' e ' . $arrDados[$index]["intervencao"] . '. ';
            }
        } if (count($arrDados) > 2) {
            foreach ($arrDados as $index => $value) {
                if ($index < $penultimo) {
                    $arrDados[$index]['check'] = '' . $arrDados[$index]["intervencao"] . ', ';
                }
                $arrDados[$penultimo]['check'] = '' . $arrDados[$penultimo]["intervencao"] . ' ';
                $arrDados[$max]['check'] = 'e ' . $arrDados[$index]["intervencao"] . '. ';
            }
        }
        return $arrDados;
    }

}
