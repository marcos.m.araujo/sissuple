<?php

class Laudo_Pedido_Autorizacao_Objetivos_model extends CI_Model {

    public function salvar($dados) {
        $this->db->insert('tblLaudoElaboracaoPedidoObjtivos', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function deletar($dados) {
        $this->db->where('codigoObjetivo', $dados['codigoObjetivo']);
        $this->db->delete("tblLaudoElaboracaoPedidoObjtivos");
    }

    public function getObjetivos($dados) {
        $SQL = "SELECT 
                cods.codigoObjetivo,
                dia.objetivo
                FROM tblLaudoElaboracaoPedidoObjtivos as cods
                left join tblAdminObjetivosTratmento as dia
                on cods.codigoObjetivo = dia.codigoObjetivoTratamento
            where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function putActionButtons($arrDados, $id) {
        $max = count($arrDados) - 1;
        $penultimo = count($arrDados) - 2;
        if (count($arrDados) == 1) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["objetivo"] . '.';
            }
        } if (count($arrDados) == 2) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["objetivo"] . '';
                $arrDados[$max]['check'] = ' e ' . $arrDados[$index]["objetivo"] . '. ';
            }
        } if (count($arrDados) > 2) {
            foreach ($arrDados as $index => $value) {
                if ($index < $penultimo) {
                    $arrDados[$index]['check'] = '' . $arrDados[$index]["objetivo"] . ', ';
                }
                $arrDados[$penultimo]['check'] = '' . $arrDados[$penultimo]["objetivo"] . ' ';
                $arrDados[$max]['check'] = 'e ' . $arrDados[$index]["objetivo"] . '. ';
            }
        }
        return $arrDados;
    }

}
