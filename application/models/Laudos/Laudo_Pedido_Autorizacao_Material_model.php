<?php

class Laudo_Pedido_Autorizacao_Material_model extends CI_Model
{

    //consulta fornecedores para laudo web
    public function consultarFonecedores($dados)
    {
        $this->db->select('f.codigoFornecedor');
        $this->db->select('f.fornecedor');
        $this->db->select('f.nomeRepresentante');
        $this->db->select('f.contato');
        $this->db->select('f.endereco');
        $this->db->select('f.municipio');
        $this->db->select('f.estado');
        $this->db->select('(select GROUP_CONCAT(mc.marca) 
                            from tblFornecedoresMarcas as m   
                            left join tblMarcas as mc
                            on m.codigoMarca = mc.codigoMarca
                            where m.codigoFornecedor = f.codigoFornecedor 
                            order by mc.marca asc
                            ) as marcas');

        $this->db->from('tblFornecedores f');


        $this->db->where('f.estado', $dados['estado']);

        if (!empty($dados['municipio'])) {
            $this->db->where('f.municipio', $dados['municipio']);
        }
        $this->db->order_by("f.fornecedor", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    //consulta municipios dos fornecedores para laudo web
    public function consultarMunicipioFonecedores($dados)
    {
        $this->db->select('municipio');
        $this->db->from('tblFornecedores ');
        $this->db->where('estado', $dados['estado']);
        $this->db->order_by("municipio", "asc");
        $this->db->distinct("municipio");
        $this->db->group_by("municipio");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultaOpmesLista($marca)
    {
        $SQL = "SELECT   
		distinct
		opmes.codigoMarca,
                opmes.codigoOpmeMateriais AS codigoOpmeMaterial,
                opmes.codigoOpmeTipo AS codigoOpmeTipo,
                material.material AS material,        
                tipo.tipo AS tipo,     
                opmes.nomenclatura AS nomenclatura ,
                marcas.marca
                FROM
                tblopmes opmes       
                LEFT JOIN tblopmesmaterial material ON opmes.codigoOpmeMateriais = material.codigoOpmeMaterial
                LEFT JOIN tblmarcas marcas ON opmes.codigoMarca = marcas.codigoMarca
                LEFT JOIN tblopmestipo tipo ON opmes.codigoOpmeTipo = tipo.codigoOpmeTipo  where opmes.codigoMarca = {$marca} ";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function consultaMarcasConcatenadasPDF($codigoLaudoElaboracao)
    {
        $SQL = " SELECT 
        GROUP_CONCAT((SELECT 
                    marcas.marca
                FROM
                    tblFornecedoresMarcas forMarcas
                        LEFT JOIN
                    tblMarcas AS marcas ON forMarcas.codigoMarca = marcas.codigoMarca
                WHERE
                    forMarcas.codigoFornecedor = MarcaLaudo.codigoFornecedor
                LIMIT 1),' (' ,f.fornecedor, ')') AS marcas
                
            FROM
                tblLaudoMarca AS MarcaLaudo
                left join tblFornecedores as f
                on MarcaLaudo.codigoFornecedor = f.codigoFornecedor
            WHERE
                MarcaLaudo.codigoLaudoElaboracao = {$codigoLaudoElaboracao} ";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    // seleciona marcas de acordo como primeiro fornecedor selecionado, nome do campo é primeiraMarcaSelecioanada
    public function consultaCategoriaMarcas($dados)
    {
        $SQL = "SELECT distinct codigoOpmeCategoria, categoria FROM tblFornecedorCategoriasOpmes
                where codigoFornecedor =    
                (
                   select codigoMarcaPrimeiraMarca from tblLaudoMarca 
                   where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']} limit 1                     
                ) ";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function consultaCategoriaMaterial($dados)
    {
        $SQL = "SELECT distinct  codigoOpmeMaterial, material FROM tblFornecedorMaterialOpmes
                where codigoFornecedor =    
                (
                    select codigoMarcaPrimeiraMarca from tblLaudoMarca 
                    where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']} limit 1
                     
                ) and codigoOpmeCategoria = {$dados['codigoOpmeCategoria']} ";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    //consulta o tipo de material
    public function consultaCategoriaMaterialTipo($dados)
    {
        $SQL = "SELECT distinct codigoOpmeTipo, tipo FROM tblFornecedorTipoOpmes
                where codigoFornecedor = 
                (
                    select codigoMarcaPrimeiraMarca from tblLaudoMarca 
                    where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']} limit 1
                     
                ) and codigoOpmeMaterial = {$dados['codigoOpmeMaterial']} and tipo is not null ";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    //consulta material de acordo com a marca selecionada
    public function consultaMaterial($dados)
    {
        $SQL = "SELECT * FROM viewOpmes
                where codigoMarca in    
                (
                    SELECT codigoMarca FROM tblFornecedoresMarcas
                    where codigoFornecedor = 
                    (
                        select codigoMarcaPrimeiraMarca from tblLaudoMarca 
                        where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']} limit 1
                     )
                ) and codigoOpmeTipo = {$dados['codigoOpmeTipo']} ";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function deletar($dados)
    {
        $this->db->where('codigoMarca', $dados['codigoMarca']);
        $this->db->delete("tblLaudoMarca");
    }

    public function consultaFornecedorSalvo($dados)
    {
        $SQL = "SELECT
                mLaudo.codigoMarca,            
                m.fornecedor,
                mLaudo.utilizacaoOpme,
                estado as ufFornecedor,
                municipio
                FROM tblLaudoMarca as mLaudo
                left join tblFornecedores as m
                on mLaudo.codigoMarca = m.codigoFornecedor
                where mLaudo.codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']};";

        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function consultaMeusPedidos($dados)
    {
        $SQL = "SELECT * FROM viewMeusPedidosMateriais              
                where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']};";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function consultaMinhasAplicacoes($dados, $like, $codigoOpmeMaterial)
    {
        $SQL = "SELECT 
                Material.codigoLaudoMaterial,
                Material.aplicacao,
                Opmes.material,
                Opmes.tipo,
                Opmes.nomenclatura,
                Opmes.marca
                FROM tblLaudoElaboracaoMaterial Material
                left join tblLaudoElaboracao Laudo
                on Material.codigoLaudoElaboracao = Laudo.codigoLaudoElaboracao
                left join viewOpmes Opmes
                on Material.codigoMaterial = Opmes.codigoOpme
                where Laudo.codigoCirurgiao = {$dados}";

        if (!empty($like)) {
            $SQL .= " and Material.aplicacao like '%{$like}%'";
        }
        $SQL .= " and Opmes.codigoOpmeMaterial = '{$codigoOpmeMaterial}'";
        $SQL .= " limit 20";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    //consulta os materiais por marcas e por material
    public function consultaProdudosSelecionados($dados)
    {
        $this->db->where_in('codigoMarca', $dados);
        $this->db->where('codigoOpmeMaterial', $dados['codigoMaterial']);
        $this->db->order_by("codigoMarca");
        $query = $this->db->get('viewOpmes');
        return $query->result();
    }

    public function consultaProdudosID($dados)
    {
        $this->db->select('*');
        $this->db->from('viewOpmes');
        $this->db->where('codigoOpme', $dados['codigoOpme']);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function salvar($dados)
    {
        $this->db->insert('tblLaudoElaboracaoMaterial', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function updateLaudoMaterialPedido($codigoLaudoMaterial, $dados)
    {
        $this->db->trans_begin();
        $this->db->where('codigoLaudoMaterial', $codigoLaudoMaterial);
        $this->db->update('tblLaudoElaboracaoMaterial', $dados);
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function consultaOpmesSalvos($dados)
    {
        $SQL = "SELECT mat.* ,
                op.imagem
                FROM tblLaudoElaboracaoMaterial as mat
                left join tblOpmes as op
                on mat.codigoMaterial = op.codigoOpme where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function deletarMaterialLaudo($dados)
    {
        $this->db->where('codigoLaudoMaterial', $dados['codigoLaudoMaterial']);
        $this->db->delete('tblLaudoElaboracaoMaterial');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function materialPedido($dados)
    {
        $this->db->select('*');
        $this->db->from('viewLaudosMaterialPedido');
        $this->db->where($dados);
        $query = $this->db->get();
        return $query->result_array();
    }
}
