<?php

class Laudo_Pedido_Autorizacao_CID_model extends CI_Model {

    public function salvarCID($dados) {
        $this->db->insert('tblLaudoElaboracaoPedidoCIDs', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function deletar($dados) {
        $this->db->where('codigoCID', $dados['codigoCID']);
        $this->db->delete("tblLaudoElaboracaoPedidoCIDs");
    }

    public function getCID($dados) {
        $SQL = "SELECT 
                cods.codigoCID,
                concat(dia.cid, ' (',dia.codigoCIDComplemento, ')') as cid
                FROM tblLaudoElaboracaoPedidoCIDs as cods
                left join tblCID as dia
                on cods.codigoCID = dia.codigoCID
            where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getCodCID($dados) {
        $SQL = "SELECT     
                cods.codigoCID,
                dia.codigoCIDComplemento as cid
                FROM tblLaudoElaboracaoPedidoCIDs as cods
                left join tblCID as dia
                on cods.codigoCID = dia.codigoCID
            where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    //verifica qual o ultimo e penultimo registro para acrescentar o E
    public function putActionButtons($arrDados, $id) {
        $max = count($arrDados) - 1;
        $penultimo = count($arrDados) - 2;
        if (count($arrDados) == 1) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["cid"] . '.';
            }
        } if (count($arrDados) == 2) {
            foreach ($arrDados as $index => $value) {
                $arrDados[$index]['check'] = '' . $arrDados[$index]["cid"] . '';
                $arrDados[$max]['check'] = ' e ' . $arrDados[$index]["cid"] . '. ';
            }
        } if (count($arrDados) > 2) {
            foreach ($arrDados as $index => $value) {
                if ($index < $penultimo) {
                    $arrDados[$index]['check'] = '' . $arrDados[$index]["cid"] . ', ';
                }
                $arrDados[$penultimo]['check'] = '' . $arrDados[$penultimo]["cid"] . ' ';
                $arrDados[$max]['check'] = 'e ' . $arrDados[$index]["cid"] . '. ';
            }
        }
        return $arrDados;
    }

}
