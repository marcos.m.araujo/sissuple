<?php

class Laudo_Pedido_Autorizacao_Tratamento_model extends CI_Model {

    public function salvarSintomas($dados) {
        $this->db->insert('tblLaudoElaboracaoPedidoSintomas', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function getTratamento($dados) {
        $SQL = " SELECT 
                distinct Sintomas.codigoTratamento, Tratamento.tratamento
                FROM tblLaudoElaboracaoPedidoSintomas as Sintomas
                left join tblTratamentos as Tratamento
                on Sintomas.codigoTratamento = Tratamento.codigoTratamento
                where codigoLaudoElaboracao = {$dados['codigoLaudoElaboracao']}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function putActionButtons($arrDados, $id) {
        foreach ($arrDados as $index => $value) {
            $arrDados[$index]['check'] = '<li>' . $arrDados[$index]["tratamento"] . '</li>';
        }
        return $arrDados;
    }



}
