<?php

class MarcasEForneceoresModel extends CI_Model
{
    public function consultarFonecedores($dados)
    {
        $this->db->select('f.codigoFornecedor');
        $this->db->select('f.fornecedor');
        $this->db->select('f.nomeRepresentante');
        $this->db->select('f.contato');
        $this->db->select('f.endereco');
        $this->db->select('f.municipio');
        $this->db->select('f.estado');
        $this->db->select('(select GROUP_CONCAT(distinct mc.marca) 
                            from tblFornecedoresMarcas as m   
                            left join tblMarcas as mc
                            on m.codigoMarca = mc.codigoMarca
                            where m.codigoFornecedor = f.codigoFornecedor 
                            order by mc.marca asc
                            ) as marcas');
        $this->db->select('(SELECT 
                                m.codigoMarca
                            FROM
                                tblFornecedoresMarcas AS m
                            WHERE
                                m.codigoFornecedor = f.codigoFornecedor 
                            ORDER BY m.codigoFornecedoresMarcas ASC
                            LIMIT 1
                            ) as codigoPrimeiraMarca');
        $this->db->select('(SELECT 
                                mc.marca
                            FROM
                                tblFornecedoresMarcas AS m
                                left join tblMarcas as mc
                                on m.codigoMarca = mc.codigoMarca
                            WHERE
                                m.codigoFornecedor = f.codigoFornecedor 
                            ORDER BY m.codigoFornecedoresMarcas ASC
                            LIMIT 1
                            ) as primeiraMarca');

        $this->db->from('tblFornecedores f');


        $this->db->where('f.estado', $dados['estado']);
        $this->db->where('f.ativo', 1);

        if (!empty($dados['municipio'])) {
            $this->db->where('f.municipio', $dados['municipio']);
        }
        $this->db->order_by("f.fornecedor", "asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultaFornecedorSalvo()
    {
        $this->db->select('codigoFornecedor, codigoLaudoMarca');
        $this->db->from('tblLaudoMarca');
        $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function selecionaMateriais($dados)
    {
        if (empty($dados['marca'])) {
            //seleciona lista de fornecedores
            $this->db->select('codigoFornecedor');
            $this->db->from('tblLaudoMarca');
            $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
            $f = $this->db->get();            
            $fornecedores = $f->result_array();
        
         
            $marcasArr = array();
        
            if (!empty($fornecedores[0]['codigoFornecedor'])) {
                //selecina marcas dos fornecedores
                $this->db->select('distinct codigoMarca', false);
                $this->db->from('tblFornecedoresMarcas');
                $this->db->where('codigoFornecedor', $fornecedores[0]['codigoFornecedor']);
                $f = $this->db->get();
                $marcas = $f->result_array();
                foreach ($marcas as $key => $value) {
                    array_push($marcasArr, $value['codigoMarca']);
                }
            }
        }

        // $order_byRand = false;
        $this->db->select('*');
        $this->db->from('viewOpmes');

        // $order_byRand = true; //orderm aleatoria

        if (!empty($dados['avancoRecuo']))
            $this->db->where('avancoRecuo', $dados['avancoRecuo']);
        if (!empty($dados['comprimento']))
            $this->db->where('comprimento', $dados['comprimento']);
        if (!empty($dados['diametro']))
            $this->db->where('diametro', $dados['diametro']);
        if (!empty($dados['espessura']))
            $this->db->where('espessura', $dados['espessura']);
        if (!empty($dados['nomenclatura']))
            $this->db->where('nomenclatura', $dados['nomenclatura']);
        if (!empty($dados['qtdFuros']))
            $this->db->where('qtdFuros', $dados['qtdFuros']);
        if (!empty($dados['sistema']))
            $this->db->where('sistema', $dados['sistema']);
        if (!empty($dados['tamanho']))
            $this->db->where('tamanho', $dados['tamanho']);
        if (!empty($dados['volume']))
            $this->db->where('volume', $dados['volume']);
        if (!empty($dados['categoria']))
            $this->db->where('categoria', $dados['categoria']);
        if (!empty($dados['material']))
            $this->db->where('material', $dados['material']);
        if (!empty($dados['tipo']))
            $this->db->where('tipo', $dados['tipo']);
        if (!empty($dados['marca'])) {
            $this->db->where('marca', $dados['marca']);
        } else {
            if (!empty($marcasArr))
                $this->db->where_in('codigoMarca', $marcasArr);
            else {
                $this->db->order_by('RAND()');
                $this->db->limit(1000);
            }
        }

        $query = $this->db->get();
        return $query->result_array();
    }


    function removeFornecedor($dados)
    {
        $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
        $this->db->where('codigoFornecedor', $dados['codigoFornecedor']);
        $this->db->delete('tblLaudoMarca', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function preferenciaFornecedor($dados)
    {
        $this->db->set('fornecedorPreferencia', $dados['fornecedorPreferencia']);
        $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
        $this->db->where('codigoFornecedor', $dados['codigoFornecedor']);
        $this->db->update('tblLaudoMarca', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }


    public function consultarMunicipioFornecedor($dados)
    {
        $this->db->select('distinct municipio', false);
        $this->db->from('tblFornecedores');
        $this->db->where('estado', $dados['estado']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function selecionaFornecedor($dados)
    {
        $this->db->insert('tblLaudoMarca', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }
    public function incluirMaterial($dados)
    {
        $this->db->insert('tblLaudoElaboracaoMaterial', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function editarMaterial($dados)
    {
        $this->db->where($dados['where']);
        $this->db->update('tblLaudoElaboracaoMaterial', $dados['dados']);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function incluirRascunhoFinalidade($dados)
    {   
        $this->db->trans_start();
        $this->db->insert('tblFinalidadeAnatomica', $dados);
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return ['result' => false];
        } else {
            $this->db->trans_commit();
            return ['result' => true];
        }
    }

    public function readRascunhoFinalidade($dados)
    {   
        $this->db->select(['*']);
        $this->db->from('tblFinalidadeAnatomica');
        $this->db->where(array_filter($dados));
        $query = $this->db->get();
        return $query->result_array();
    }

}
