<?php

class PacienteModel extends CI_Model
{

    public function consultarPacientes($dados)
    {
        $this->db->select('pa.codigoPaciente');
        $this->db->select('pa.nomePaciente');
        $this->db->select('pa.nomeMaePaciente');
        $this->db->select("DATE_FORMAT(pa.dataNascimento, '%d/%m/%Y') as dataNascimento");
        $this->db->select('pa.CPF');
        $this->db->select('pa.convenio');
        $this->db->select('pl.nomePlano');
        $this->db->select('pa.numeroCarteirinha');
        $this->db->select("DATE_FORMAT(pa.dataInicioContratoConvenio, '%d/%m/%Y') as dataInicioContratoConvenio");
        $this->db->select('pa.email');
        $this->db->from('tblPaciente pa');
        $this->db->join('tblplanosaude pl', 'pa.convenio = pl.CodigoPlano', 'left');
        if (!empty($dados["codigoPaciente"])) {
            $this->db->where("codigoPaciente", $dados["codigoPaciente"]);
        }
        if (!empty($dados["codigoConvenio"])) {
            $this->db->where("convenio", $dados["codigoConvenio"]);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function editarPaciente($dados)
    {
        unset($dados['codigoTratamento']);
        $this->db->where('codigoPaciente', $dados['codigoPaciente']);
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        unset($dados['codigoPaciente']);
        unset($dados['codigoCirurgiao']);
        unset($dados['codigoAgenda']);
        $this->db->update('tblPaciente', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function insertTratamentoPrevisto($dados)
    {
        if (!$this->db->insert('tblPacienteLaudoPrevisto', $dados)) {
            return false;
        }
        return true;
    }

    public function deleteTratamentoPrevisto($codigoPaciente)
    {
        $this->db->where('codigoPaciente', $codigoPaciente);
        if (!$this->db->delete('tblPacienteLaudoPrevisto')) {
            return false;
        }
        return true;
    }

    public function cadastrarPaciente($dados)
    {
        unset($dados['codigoTratamento']);
        $dados['codigoCirurgiao'] = $this->session->codigoCirurgiao;
        unset($dados['codigoAgenda']);
        $this->db->insert('tblPaciente', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getElaboracaoPaciente($codigoPaciente)
    {
        $this->db->select('codigoLaudoElaboracao');
        $this->db->where('codigoPaciente', $codigoPaciente);
        $this->db->from('viewLaudoElaboracao');
        $query = $this->db->get();
        return $query->result_array();
    }

    //consulta md5 para o laudo
    public function consultarPacienteID($dados)
    {
        $SQL = "SELECT * from viewPaciente where md5(codigoPaciente) = '{$dados['codigoPaciente']}'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getDadosPacienteID($dados)
    {
        $this->db->select('*');
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $this->db->where('codigoPaciente', $dados['codigoPaciente']);
        $this->db->from('viewPaciente');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function buscaNomePaciente($dados)
    {
        $this->db->select('*');
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $this->db->like('nomePaciente', $dados['nome']);
        $this->db->from('viewPaciente');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDadosPacienteFull()
    {
        $this->db->select('*');
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $this->db->from('viewPaciente');
        $this->db->order_by("nomePaciente asc");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDadosPacienteCPF($dados)
    {
        $this->db->select('*');
        $this->db->where('CPF', $dados['cpf']);
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $this->db->from('viewPaciente');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPacienteDataNascimento($dados)
    {
        $this->db->select('*');
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $this->db->where('dataNascimento', $dados['dataNascimento']);
        $this->db->from('viewPaciente');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getPacienteDataNascimentoECPF($dados)
    {
        $this->db->select('*');
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $this->db->where('dataNascimento', $dados['dataNascimento']);
        $this->db->where('CPF', $dados['CPF']);
        $this->db->from('viewPaciente');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultarPacientesCPF($CPF)
    {
        $this->db->select('*');
        $this->db->from('viewPaciente');
        $this->db->where('CPF_', $CPF);
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function excluirPaciente($dados)
    {
        $this->db->where('codigoPaciente', $dados);
        return $this->db->delete("tblPaciente");
    }
}
