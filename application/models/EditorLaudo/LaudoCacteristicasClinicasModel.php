<?php

class LaudoCacteristicasClinicasModel extends CI_Model
{


    public function getTratamento()
    {
        $SQL = " SELECT 
                    Tratamento.codigoTratamento,
                    Tratamento.tratamento,
                    (SELECT DISTINCT
                            codigoTratamento
                        FROM
                            tblLaudoElaboracaoPedidoSintomas AS Sintomas
                        WHERE
                            Tratamento.codigoTratamento = Sintomas.codigoTratamento
                                AND Sintomas.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                    ) AS tratamentoUsado
                FROM
                    tblTratamentos AS Tratamento ";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getSintomas($codigoTratamento)
    {
        $SQL = " SELECT 
                Sintomas.codigoSintoma,
                Sintomas.codigoTratamento,
                Sintomas.sintomas,  
                (SELECT 
                        codigoSintoma
                    FROM
                        tblLaudoElaboracaoPedidoSintomas AS Uso
                    WHERE
                        Sintomas.codigoSintoma = Uso.codigoSintoma
                            AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS sintomaUsado
            FROM
                tblSintomas AS Sintomas
                where Sintomas.codigoTratamento = {$codigoTratamento}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function consultarPacienteID($dados)
    {
        $SQL = "SELECT * from viewPaciente where codigoPaciente = '{$dados['codigoPaciente']}'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }


    public function likeSintomas($dados)
    {
        $like = $dados['param'];
        $codigoTratamento = $dados['codigoTratamento'];
        $SQL = " SELECT 
                Sintomas.codigoSintoma,
                Sintomas.codigoTratamento,
                Sintomas.sintomas,  
                (SELECT 
                        codigoLaudoSintoma
                    FROM
                        tblLaudoElaboracaoPedidoSintomas AS Uso
                    WHERE
                        Sintomas.codigoSintoma = Uso.codigoSintoma
                            AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS sintomaUsado
            FROM
                tblSintomas AS Sintomas
                where Sintomas.codigoTratamento = {$codigoTratamento}   and Sintomas.sintomas like '%{$like}%'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getDiagnisticos($codigoTratamento)
    {
        $SQL = " SELECT 
                Diagnostico.codigoDiagnostico,
                Diagnostico.codigoTratamento,
                Diagnostico.diagnostico,
                (SELECT 
                        codigoDiagnostico
                    FROM
                        tblLaudoElaboracaoPedidoDiagnosticos AS Uso
                    WHERE
                            Diagnostico.codigoDiagnostico = Uso.codigoDiagnostico
                            AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblDiagnosticos AS Diagnostico
                where Diagnostico.codigoTratamento = {$codigoTratamento}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function likeDiagnisticos($dados)
    {
        $like = $dados['param'];
        $codigoTratamento = $dados['codigoTratamento'];
        $SQL = " SELECT 
                Diagnostico.codigoDiagnostico,
                Diagnostico.codigoTratamento,
                Diagnostico.diagnostico,
                (SELECT 
                        codigoDiagnostico
                    FROM
                        tblLaudoElaboracaoPedidoDiagnosticos AS Uso
                    WHERE
                            Diagnostico.codigoDiagnostico = Uso.codigoDiagnostico
                            AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblDiagnosticos AS Diagnostico
                where Diagnostico.codigoTratamento = {$codigoTratamento} and Diagnostico.diagnostico like '%{$like}%'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getCIDS($codigoTratamento)
    {
        $SQL = " SELECT 
                CID.codigoCID,
                CID.codigoTratamento,
                CID.CID,
                CID.codigoCIDComplemento,
                (SELECT 
                        codigoCID
                    FROM
                        tblLaudoElaboracaoPedidoCIDs AS Uso
                    WHERE
                        CID.codigoCID = Uso.codigoCID
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblCID AS CID
                where CID.codigoTratamento = {$codigoTratamento}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function likeCIDS($dados)
    {
        $like = $dados['param'];
        $codigoTratamento = $dados['codigoTratamento'];
        $SQL = " SELECT 
                CID.codigoCID,
                CID.codigoTratamento,
                CID.CID,
                CID.codigoCIDComplemento,
                (SELECT 
                        codigoCID
                    FROM
                        tblLaudoElaboracaoPedidoCIDs AS Uso
                    WHERE
                        CID.codigoCID = Uso.codigoCID
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblCID AS CID
                where CID.codigoTratamento = {$codigoTratamento} and CID.CID like '%{$like}%'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getIntervencoes($codigoTratamento)
    {
        $SQL = " SELECT 
                CARACT.*,           
                (SELECT 
                        codigoIntervencao
                    FROM
                        tblLaudoElaboracaoPedidoIntervencoes AS Uso
                    WHERE
                        CARACT.codigoIntervencao = Uso.codigoIntervencao
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblIntervencao AS CARACT
                where CARACT.codigoTratamento = {$codigoTratamento}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function likeIntervencoes($dados)
    {
        $like = $dados['param'];
        $codigoTratamento = $dados['codigoTratamento'];
        $SQL = " SELECT 
               CARACT.*,           
                (SELECT 
                        codigoIntervencao
                    FROM
                    tblLaudoElaboracaoPedidoIntervencoes AS Uso
                    WHERE
                        CARACT.codigoIntervencao = Uso.codigoIntervencao
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblIntervencao AS CARACT
                where CARACT.codigoTratamento = {$codigoTratamento} and CARACT.intervencao like '%{$like}%'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getProcedimentos($codigoTratamento)
    {
        $SQL = " SELECT 
                CARACT.*,           
                (SELECT 
                        codigoProcedimento
                    FROM
                        tblLaudoElaboracaoPedidoProcedimentos AS Uso
                    WHERE
                        CARACT.codigoProcedimento = Uso.codigoProcedimento
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblProcedimentos AS CARACT
                where CARACT.codigoTratamento = {$codigoTratamento}";

               
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function likeProcedimentos($dados)
    {
        $like = $dados['param'];
        $codigoTratamento = $dados['codigoTratamento'];
        $SQL = " SELECT 
               CARACT.*,           
                (SELECT 
                    codigoProcedimento
                    FROM
                        tblLaudoElaboracaoPedidoProcedimentos AS Uso
                    WHERE
                        CARACT.codigoProcedimento = Uso.codigoProcedimento
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblprocedimentos AS CARACT
                where CARACT.codigoTratamento = {$codigoTratamento} and CARACT.procedimento like '%{$like}%'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getObjetivos($codigoTratamento)
    {
        $SQL = " SELECT 
                CARACT.*,           
                (SELECT 
                        codigoObjetivoTratamento
                    FROM
                        tblLaudoElaboracaoPedidoObjtivos AS Uso
                    WHERE
                        CARACT.codigoObjetivoTratamento = Uso.codigoObjetivo
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblAdminObjetivosTratmento AS CARACT
                where CARACT.codigoTratamento = {$codigoTratamento}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function likeObjetivos($dados)
    {
        $like = $dados['param'];
        $codigoTratamento = $dados['codigoTratamento'];
        $SQL = " SELECT 
               CARACT.*,           
                (SELECT 
                    codigoObjetivoTratamento
                    FROM
                        tblLaudoElaboracaoPedidoObjtivos AS Uso
                    WHERE
                        CARACT.codigoObjetivoTratamento = Uso.codigoObjetivo
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblAdminObjetivosTratmento AS CARACT
                where CARACT.codigoTratamento = {$codigoTratamento} and CARACT.objetivo like '%{$like}%'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function getSequelas($codigoTratamento)
    {
        $SQL = " SELECT 
                CARACT.*,           
                (SELECT 
                        codigoSequelas
                    FROM
                        tblLaudoElaboracaoPedidoSequelas AS Uso
                    WHERE
                        CARACT.codigoSequelas = Uso.codigoSequela
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblSequelas AS CARACT
                where CARACT.codigoTratamento = {$codigoTratamento}";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function likeSequelas($dados)
    {
        $like = $dados['param'];
        $codigoTratamento = $dados['codigoTratamento'];
        $SQL = " SELECT 
                CARACT.*,           
                (SELECT 
                        codigoSequelas
                    FROM
                        tblLaudoElaboracaoPedidoSequelas AS Uso
                    WHERE
                        CARACT.codigoSequelas = Uso.codigoSequela
                        AND Uso.codigoLaudoElaboracao = {$this->session->codigoLaudoElaboracao}
                ) AS usado
            FROM
                tblSequelas AS CARACT
                where CARACT.codigoTratamento = {$codigoTratamento} and CARACT.sequela like '%{$like}%'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }
}
