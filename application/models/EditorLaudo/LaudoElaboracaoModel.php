<?php

class LaudoElaboracaoModel extends CI_Model
{

    public function excluirLaudo($codigoLaudoElaboracao)
    {
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblTimeLine');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoDadosCirurgia');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoMaterial');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoPedidoCIDs');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoPedidoDiagnosticos');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoPedidoIntervencoes');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoPedidoObjtivos');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoPedidoProcedimentos');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoPedidoSequelas');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoPedidoSintomas');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoMarca');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracaoDocumento');
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudoElaboracao);
        $this->db->delete('tblLaudoElaboracao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function consultarPacienteID($dados)
    {
        $SQL = "SELECT * from viewPaciente where codigoPaciente = '{$dados['codigoPaciente']}'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function novoItem($dados)
    {
        $this->db->insert($dados['table'], $dados['dados']);
        return $this->db->insert_id();
    }

    public function push($dados)
    {
        $this->db->set($dados['dados']);
        $this->db->insert($dados['table']);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function pull($dados)
    {
        if (!empty($dados['table'])) {
            $this->db->select('*');
            if ($dados['where'])
                $this->db->where($dados['dados']);
            $query = $this->db->get($dados['table']);
            return $query->result_array();
        }
    }

    public function rmContato($dados)
    {
        if (!empty($dados['table'])) {
            $this->db->where($dados['coluna'], $dados['codigo']);
            if ($dados['table'] != 'tblContatosEmailHospital')
                $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
            $this->db->delete($dados['table']);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return FALSE;
            } else {
                $this->db->trans_commit();
                return TRUE;
            }
        }
    }

    public function rm($dados)
    {
        if (!empty($dados['table'])) {
            $this->db->where($dados['coluna']);
            $this->db->delete($dados['table']);
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return FALSE;
            } else {
                $this->db->trans_commit();
                return TRUE;
            }
        }
    }

    public function cadastrarLaudoElaboracao($dados)
    {
        $this->db->insert('tblLaudoElaboracao', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function getLaudoElaboracaoCodigoLaudo($dados)
    {
        $this->db->select('md5(codigoLaudoElaboracao) as codigoLaudoElaboracaoCRIP, codigoLaudoElaboracao, codigoSituacaoLaudo, codigoPaciente, (select codigoTratamento from tblLaudoElaboracaoPedidoSintomas as sint where sint.codigoLaudoElaboracao = laudo.codigoLaudoElaboracao limit 1 ) as tratamento');
        $this->db->from('tblLaudoElaboracao laudo');
        $this->db->where('codigoCirurgiao', $this->session->codigoCirurgiao);
        $this->db->where('codigoPaciente', $dados['codigoPaciente']);
        $this->db->order_by("codigoLaudoElaboracao desc");
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getClassificacaoDocumento()
    {
        $this->db->select('distinct cabeca', false);
        $this->db->from('tblTipoDocumento ');
        $this->db->where('codigoPerfil', 1);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function contatosHospital($codigo)
    {
        $this->db->select('*');
        $this->db->from('tblContatosEmailHospital');
        $this->db->where('codigoHospital',  $codigo);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getClassificacaoDocumentoTipoDocumento($dados)
    {
        $this->db->select('distinct tipoDocumento', false);
        $this->db->from('tblTipoDocumento ');
        $this->db->where('codigoPerfil', 1);
        $this->db->where('cabeca', $dados['classificacao']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getDocumentos($dados)
    {
        $this->db->select('*');
        $this->db->from('tblLaudoElaboracaoDocumento ');
        $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getDocumentosDetalhesLaudo($dados)
    {
        $this->db->select('*');
        $this->db->from('tblLaudoElaboracaoDocumento as doc');
        $this->db->join('tblDocumtoTimeLine as doctime','doc.codigoLaudoElaboracaoExame = doctime.codigoLaudoElaboracaoExame', 'left');
        $this->db->where('doctime.codigoTimeLine', $dados['codigoTimeLine']);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getMateriaisSalvos()
    {
        $this->db->select('*');
        $this->db->from('tblLaudoElaboracaoMaterial');
        $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
        $this->db->join('viewOpmes as opme', 'codigoMaterial = codigoOpme', 'left');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function salvarDocumentos($dados)
    {
        $this->db->insert('tblLaudoElaboracaoDocumento',$dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return $this->db->insert_id();
        }
    }

    public function setDocsInTimeLine($timeLine, $document)
    {
        if($this->db->insert('tblDocumtoTimeLine',[
            'codigoTimeLine' => $timeLine,
            'codigoLaudoElaboracaoExame' => $document
        ])){
            return true;
        }

        return false;
    }

    public function setDadosCirurgicos($dados)
    {
        if (!empty($dados)) {
            $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
            $this->db->update('tblLaudoElaboracaoDadosCirurgia', $dados);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return FALSE;
            } else {
                $this->db->trans_commit();
                return TRUE;
            }
        } else {
            date_default_timezone_set("Brazil/East");
            $data = date("Y-m-d");
         
            $dataC = date('Y-m-d', strtotime("+30 days",strtotime($data)));
            $this->db->set('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
            $this->db->set('diariasInternacao', 1);
            $this->db->set('diariasUTI', 0);
            $this->db->set('tempoProcedimento', '4 horas');
            $this->db->set('tipoAnestesia', 'Geral');
            $this->db->set('codigoEquipeCirurgica', 1);
            $this->db->set('dataCirurgia', $dataC);
            $this->db->set('tipoHonorario', 'Particular');
            $this->db->insert('tblLaudoElaboracaoDadosCirurgia');
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return FALSE;
            } else {
                $this->db->trans_commit();
                return TRUE;
            }
        }
    }
    public function getDadosCirurgico()
    {
        $this->db->select('*');
        $this->db->from('tblLaudoElaboracaoDadosCirurgia');
        $this->db->where('codigoLaudoElaboracao', $this->session->codigoLaudoElaboracao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function alterarDocumento($dados)
    {
        if ($dados['tipoAlteracao'] == 'deletar') {
            $this->db->where('codigoLaudoElaboracaoExame', $dados['codigoLaudoElaboracaoExame']);
            $this->db->delete('tblLaudoElaboracaoDocumento');
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return FALSE;
            } else {
                $this->db->trans_commit();
                return TRUE;
            }
        } else {

            $this->db->select($dados['tipoAlteracao']);
            $this->db->from('tblLaudoElaboracaoDocumento ');
            $this->db->where('codigoLaudoElaboracaoExame', $dados['codigoLaudoElaboracaoExame']);
            $query = $this->db->get();
            $privado =  $query->result_array();
            $privado[0][$dados['tipoAlteracao']] == '1' ? $priv = 0 : $priv = 1;
            $this->db->set($dados['tipoAlteracao'], $priv);
            $this->db->where('codigoLaudoElaboracaoExame', $dados['codigoLaudoElaboracaoExame']);
            $this->db->update('tblLaudoElaboracaoDocumento');
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                return FALSE;
            } else {
                $this->db->trans_commit();
                return TRUE;
            }
        }
    }

    public function deletarMaterial($dados)
    {
        $this->db->where('codigoLaudoMaterial', $dados['codigo']);
        $this->db->delete('tblLaudoElaboracaoMaterial');
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function getLaudoElaboracaoNotificacao($dados)
    {
        $this->db->select('notificarPlano,notificarPaciente,notificarFornecedor,notificarHospital');
        $this->db->from('tblLaudoElaboracao');
        $this->db->where('codigoLaudoElaboracao', $dados);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function getModel($colum, $table, $where, $value, $and = '', $value2 = '')
    {
        $this->db->select($colum);
        $this->db->from($table);
        $this->db->where($where, $value);
        if (!empty($and))
            $this->db->where($and, $value2);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function updateLaudoElaboracao($codigoLaudo, $codigoSituacao)
    {
        $this->db->trans_begin();
        $this->db->where('codigoLaudoElaboracao', $codigoLaudo);
        $this->db->set('codigoSituacaoLaudo', $codigoSituacao);
        $this->db->update('tblLaudoElaboracao');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return true;
    }
    public function updateLaudoElaboracaoMd5($codigoLaudo, $codigoSituacao, $justificativa = "")
    {
        $this->db->trans_begin();
        $this->db->where('md5(codigoLaudoElaboracao)', $codigoLaudo);
        $this->db->set('codigoSituacaoLaudo', $codigoSituacao);
        if($justificativa !== ""){
            $this->db->set('Justificativa', $justificativa);
        }
        $this->db->update('tblLaudoElaboracao');
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        }
        $this->db->trans_commit();
        return true;
    }

    public function clonarLaudoElaboracao($novoCodigo, $codigoAntigo)
    {
        $query = $this->db->query("call SetModeloLaudo({$novoCodigo}, {$codigoAntigo})");
    }
}
