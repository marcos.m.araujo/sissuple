<?php

/**
 * Model para API
 * 
 * @uses core/MY_Model
 */
class API_Model extends MY_Model
{
    /**
     * Construtor
     * 
     * @uses CORE::database
     */
    public function __construct()
    {
        parent::__construct();
        $this->_database = $this->load->database('default', true);

    }

    /**
     * Funcao global GET 
     * para selecionar dados das funcionalidaes
     * 
     * @param Array $params 
     * @example [
     *   'table' => 'UF',
     *   'columns' => ['Sigla'],
     *   'where' => ['Id' => 4]
     * ]
     * 
     * @return Array
     * @example [
     *    'status' => true,
     *    'response' => [0=>['Sigla'=>'BA']]
     * }
     * @uses API_Model get()
     */
    public function get($params)
    {
        try{
            $this->_database->trans_start(FALSE);
        
            /**
             * Representa o nome das colunas da tabela
             * @example [
             *   'table' => 'UF',
             *   'columns' => ['NomeCompleto','Sigla'],
             * ]
             */
            if(isset($params['columns'])){
                $this->_database->select($params['columns']);
            } 
            
            
            /**
             * Representa o nome da tabela 
             * @example [
             *   'table' => 'UF',
             *   'columns' => ['NomeCompleto','Sigla'],
             * ]
             * @uses core/MY_Model::findTableName()
             */
            $this->_database->from($this->findTableName($params['table']));

            /**
             * Represesnta relacao entre tabelas
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   join => [
             *       table => 'TipoBilioteca',
             *       on => 'CodigoTipoBiblioteca'
             *   ],
             * ]
             */
            if(isset($params['join'])){
                $table1 = $this->findTableName($params['table']);
                $table2 = $this->findTableName($params['join']['table']);
                
                $field1 = $table1 . '.' .$params['join']['on'];
                $field2 = $table2 . '.' . $params['join']['on'];
                $this->_database->join($table2, $field1 . ' = ' . $field2);
            }

            /**
             * Representa clausula de seleção WHERE
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   where => [
             *       RecordID => 4
             *   ],
             * ]
             */
            if( isset($params['where']) && $params['where'] != []) {
                $this->_database->where($params['where']);
            }     

            /**
             * Representa clausula de seleção WHERE IN
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   whereIn => [
             *       RecordID => [4,5,6]
             *   ],
             * ]
             * 
             * opcao para CAST
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   whereIn => [
             *      isCAST => 'CAST(Tags as varchar(255))',
             *      value => ['Ambiental', 'Desapropriação']
             *   ],
             * ] 
             * 
             */
            if(isset($params['whereIn'])){
                if(isset($params['whereIn']['isCAST'])){
                    $this->_database->where_in($params['whereIn']['isCAST'], $params['whereIn']['value']);
                } else {
                    foreach($params['whereIn'] as $index => $value){

                        $this->_database->where_in($index, $value);
                    }
                }
            }  

            /**
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   like => [
             *      Tag => ['Ambiental', 'Desapropriação']
             *   ],
             * ] 
             * 
             *
             * @todo é preciso testar ainda
             */
            if(isset($params['like'])){
                foreach($params['like'] as $index => $value){
                    $this->_database->like($index, $value);
                }
            } 

            
            /**
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   likeMany => [
             *      Tag => ['Ambiental', 'Desapropriação']
             *   ],
             * ] 
             */
            if(isset($params['likeMany'])){
                foreach($params['likeMany'] as $index => $value){
                    $items = json_decode($value);
                    foreach($items as $item){
                        $this->_database->like($index,$item);
                    }
                }
            } 

            /**
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   orderBy => ['Nome', 'Idade DESC']
             *   ],
             * ] 
             */
            if(isset($params['orderBy'])){
                foreach($params['orderBy'] as $item){
                    $this->_database->order_by($item);
                }
            }
            

            /**
             * opcao para debug do SQL
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   debug => 'safely_debug'
             * ] 
             */
            if(ENVIRONMENT != 'production'){
                if(isset($params['debug']) && $params['debug'] == 'safely_debug'){
                    print_r($params);
                    echo $this->_database->get_compiled_select();
                    die;
                }
            }

            $query = $this->_database->get();

            if($this->verificaErroQuery($query)){
                
                $result = $query->result_array();
                
                $this->_database->trans_complete();

                return [
                    'status' => true,
                    'result' => $result
                ];
            }

            $this->_database->trans_rollback();

        } catch(Exception $e){
                  
            return [
                'status' => false,
                'result' => $e->getMessage()
            ];
        }
    }

    /**
     * Funcao global Insert 
     * 
     * @param Array $params 
     * @example [
     *   'table' => 'UF',
     *   'columns' => ['Sigla'],
     *   'where' => ['Id' => 4]
     * ]
     * 
     * @return Array
     * @example [
     *    'status' => true,
     *    'response' => 'Dados Inseridos com sucesso'
     * }
     * @uses API_Model get()
     */
    public function insert($params)
    {
        try{
            $this->_database->trans_start(FALSE);
            $this->_database->insert(
                $this->findTableName($params['table']), 
                $params['data']
            );
            $this->_database->trans_complete();
            
            
            if ($this->_database->trans_status() === FALSE){
                $this->_database->trans_rollback();
                throw new Exception("Erro ao tentar inserir os dados.\n" . $this->_database->get_compiled_insert());
            } else  {
                $this->_database->trans_commit();     
                return [
                    'status' => true,
                    'result' => 'Dados inseridos com sucesso.'
                ];
            }
        
        } catch(Exception $e){
                  
            return [
                'status' => false,
                'result' => $e->getMessage()
            ];
        }
    }

    
    /**
     * Funcao global UPDATE
     * 
     * @param Array $params 
     * @example [
     *   'table' => 'UF',
     *   'where' => ['Id' => 4]
     *   'set' => [
     *       'Coordenadas' => '0x8912e91a0'
     *   ]
     * ]
     * 
     * @return Array
     * @example [
     *    'status' => true,
     *    'response' => 'Dados alterados com sucesso.'
     * }
     * @uses API_Model get()
     */
    public function update($params)
    {
        try{
            $this->_database->trans_start(FALSE);

            /**
             * Representa campos a serem alterados
             *    'set' => [
             *       'Coordenadas' => '0x8912e91a0'
             *   ]
             */
            if(isset($params['data']) && $params['data'] != []) {
                $this->_database->set($params['data']);                   
            } 
            
            /**
             * Representa clausula de seleção WHERE
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   where => [
             *       RecordID => 4
             *   ],
             * ]
             */
            if( isset($params['where']) && $params['where'] != []) {
                $this->_database->where($params['where']);
            }     

             

            /**
             * opcao para debug do SQL
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   debug => 'safely_debug'
             * ] 
             */
            if(isset($params['debug']) && $params['debug'] == 'safely_debug'){
                print_r($params);
                echo $this->_database->get_compiled_update($this->findTableName($params['table']));
                die;
            }
            
            $this->_database->update($this->findTableName($params['table']));
            $this->_database->trans_complete();
            
            if ($this->_database->trans_status() === FALSE){
                $this->_database->trans_rollback();
                throw new Exception("Erro ao tentar alterar os dados.\n" . $this->_database->get_compiled_update());
            } else  {
                $this->_database->trans_commit();     
                return [
                    'status' => true,
                    'result' => 'Dados alterados com sucesso.'
                ];
            }
        
        } catch(Exception $e){
                  
            return [
                'status' => false,
                'result' => $e->getMessage()
            ];
        }
    }


      
    /**
     * Funcao global DELETE
     * 
     * @param Array $params 
     * @example [
     *   'table' => 'UF',
     *   'where' => ['Id' => 4]
     * ]
     * 
     * @return Array
     * @example [
     *    'status' => true,
     *    'response' => 'Dados alterados com sucesso.'
     * }
     * @uses API_Model get()
     */
    public function delete($params)
    {
        try{
            $this->_database->trans_start(FALSE);
       
            /**
             * Representa clausula de seleção WHERE
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   where => [
             *       RecordID => 4
             *   ],
             * ]
             */
            if( isset($params['where']) && $params['where'] != []) {
                $this->_database->where($params['where']);
            }     
             

            /**
             * opcao para debug do SQL
             * 
             * @example [
             *   table => 'BibliotecaEdicoes',
             *   debug => 'safely_debug'
             * ] 
             */
            if(isset($params['debug']) && $params['debug'] == 'safely_debug'){
                print_r($params);
                echo $this->_database->get_compiled_update($this->findTableName($params['table']));
                die;
            }
            
            $this->_database->delete($this->findTableName($params['table']));
            $this->_database->trans_complete();
            
            if ($this->_database->trans_status() === FALSE){
                $this->_database->trans_rollback();
                throw new Exception("Erro ao tentar deletar os dados.\n" . $this->_database->get_compiled_update());
            } else  {
                $this->_database->trans_commit();     
                return [
                    'status' => true,
                    'result' => 'Dado deletado com sucesso.'
                ];
            }
        
        } catch(Exception $e){
                  
            return [
                'status' => false,
                'result' => $e->getMessage()
            ];
        }
    }
}