<?php

class Termos_model extends CI_Model {

    public function buscaUF() {
        $SQL = " SELECT * FROM tblEstados";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function get() {
        $this->db->select('termo');
        $this->db->from('tblTermoServico');     
        $query = $this->db->get();
        return $query->result_array();
    }



    public function create($dados) {
        if ($this->db->insert('tblTermoServico', $dados)) {
            return true;
        }

        return false;
    }

    public function delete() {
        $this->db->where('codigoTermoServico > 0');
        if (!$this->db->delete('tblTermoServico')) {
            return false;
        }
        return true;
    }

    public function updateClinica($dados) {
        $codigoClinica = $dados['codigoClinica'];
        unset($dados['codigoClinica']);

        $this->db->where('codigoClinica', $codigoClinica);
        if ($this->db->update('tblClinicas', $dados)) {
            return true;
        }

        return false;
    }

    public function getClinica($codigoClinica) {
        $this->db->select('*');
        $this->db->from('tblClinicas');
        $this->db->where('codigoClinica', $codigoClinica);
        $query = $this->db->get();
        return $query->row();
    }

}
