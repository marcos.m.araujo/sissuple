<?php

class Cirurgiao_model extends CI_Model {

    public function buscaConselhoProfissional() {
        $SQL = " SELECT * FROM tblConselhoProfissional";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function buscaEspecilidade() {
        $SQL = " SELECT * FROM tblEspecialidades";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function buscaUF() {
        $SQL = " SELECT * FROM tblEstados";
        $query = $this->db->query($SQL);
        return $query->result();
    }

    public function getCirurgiao($codigoUsuario) {
        $this->db->select('*');
        $this->db->from('veiwCirurgiao');
        $this->db->where('codigoUsuario', $codigoUsuario);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCertificadosCirurgiao($codigoCirurgiao) {
        $this->db->select('*');
        $this->db->from('tblCertificados');
        $this->db->where('codigoCirurgiao', $codigoCirurgiao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getEspecialidadeCirurgiao($codigoCirurgiao) {
        $this->db->select('codigoEspecialidade');
        $this->db->from('tblCirurgiaoEspecialidade');
        $this->db->where('codigoCirurgiao', $codigoCirurgiao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getAceiteTermo($codigoCirurgiao) {
        $this->db->select('*');
        $this->db->from('tblCirurgiaoAceiteTermo');
        $this->db->where('codigoCirurgiao', $codigoCirurgiao);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function insertAceiteTermo($dados) {
        $result = $this->db->insert('tblCirurgiaoAceiteTermo', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function updateAceiteTermo($dados) {
        $this->db->set('codigoAceite', $dados['codigoAceite']);
        $this->db->where('codigoCirurgiao', $dados['codigoCirurgiao']);
        if (!$this->db->update('tblCirurgiaoAceiteTermo')) {
            return false;
        }
        return true;
    }

    public function createCirurgiao($dados) {
        
        date_default_timezone_set("Brazil/East");
        $dataCadastro = date("Y-m-d H:i:s");
        if (empty($dados['avaliadorS']))
            $dados['avaliadorS'] = 1;
        if (empty($dados['desempatadorS']))
            $dados['desempatadorS'] = 1;
        $this->db->set('nomeCirurgiao', $dados['nomeCirurgiao']);
        $this->db->set('sobrenomeCirurgiao', $dados['sobrenomeCirurgiao']);
        $this->db->set('cpf', $dados['cpf']);
        $this->db->set('sexo', $dados['sexo']);
        $this->db->set('email', $dados['email']);
        $this->db->set('codigoUsuario', $dados['codigoUsuario']);
        $this->db->set('telefoneCirurgiao', $dados['telefoneCirurgiao']);
        $this->db->set('codigoConselhoProfissional', $dados['codigoConselhoProfissional']);
        $this->db->set('numeroConselhoProfissional', $dados['numeroConselhoProfissional']);
        $this->db->set('codigoEstado', $dados['codigoEstado']);
         $this->db->set('dataCadastro', $dataCadastro);
        $this->db->set('avaliadorS', $dados['avaliadorS']);
        $this->db->set('desempatadorS', $dados['desempatadorS']);
        if (!$this->db->insert('tblCirurgiao')) {
            return false;
        }
        
        return true;
    }

    public function insertCertificados($dados) {
        if (!$this->db->insert('tblCertificados', $dados)) {
            return false;
        }
        return true;
    }

    public function insertEspecialiadades($dados) {
        if (!$this->db->insert('tblCirurgiaoEspecialidade', $dados)) {
            return false;
        }
        return true;
    }

    public function returnCodigoCirurgiao($dados) {
        $this->db->select('codigoCirurgiao');
        $this->db->from('tblCirurgiao');
        $this->db->where('codigoUsuario', $dados['codigoUsuario']);
        $query = $this->db->get();
        $result = $query->result_array();
        if (empty($result)) {
            return 0;
        }
        return $result[0]['codigoCirurgiao'];
    }

    public function updateUsuario($codigoCirurgiao, $codigoUsuario) {
        $this->db->set('codigoCirurgiao', $codigoCirurgiao);
        $this->db->where('codigoUsuario', $codigoUsuario);
        if (!$this->db->update('usuarios')) {
            return false;
        }
        return true;
    }

    public function deleteCertificados($codigoAnexo) {
        $this->db->where('codigoAnexos', $codigoAnexo);
        if (!$this->db->delete('tblCertificados')) {
            return false;
        }
        return true;
    }

    public function updateCirurgiao($dados) {
        $this->db->where('codigoCirurgiao', $dados['codigoCirurgiao']);
        unset($dados['codigoCirurgiao']);
        $this->db->update('tblCirurgiao', $dados);
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function deleteEspecialidades($codigoCirurgiao) {
        $this->db->where('codigoCirurgiao', $codigoCirurgiao);
        if (!$this->db->delete('tblCirurgiaoEspecialidade')) {
            return false;
        }
        return true;
    }

    public function getCertificado($codigoAnexo) {
        $this->db->select('nomeArquivo');
        $this->db->from('tblCertificados');
        $this->db->where('codigoAnexos', $codigoAnexo);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function getUsuario() {
        $this->db->select('senha');
        $this->db->from('usuarios');
        $this->db->where('codigoUsuario', $this->session->codigoUsuario);
        $query = $this->db->get();
        $result = $query->result_array();
        if (empty($result)) {
            return 0;
        }
        return $result[0]['senha'];
    }

    public function resetSenha($senha) {
        $this->db->set('senha', md5($senha['senha']));
        $this->db->where('codigoUsuario', $this->session->codigoUsuario);
        if (!$this->db->update('usuarios')) {
            return false;
        }
        return true;
    }

    public function getUsuarioEmail($dados) {
        $this->db->select('email');
        $this->db->from('usuarios');
        $this->db->where('email', $dados['email']);
        $query = $this->db->get();
        $result = $query->result_array();
        if (empty($result)) {
            return 0;
        }
        return $result[0]['email'];
    }

    public function insertUsuario($dados) {
        $nome = $dados['nomeCirurgiao'] . ' ' . $dados['sobrenomeCirurgiao'];
        $this->db->set('nomeUsuario', $nome);
        $this->db->set('email', $dados['email']);
        $this->db->set('codigoPerfil', 1);
        $this->db->set('ativado', 1);
        $this->db->set('senha', md5($dados['senha']));
        $this->db->set('telefone', $dados['telefoneCirurgiao']);
        if (!$this->db->insert('usuarios')) {
            return false;
        }
        $this->db->select('codigoUsuario');
        $this->db->from('usuarios');
        $this->db->where('email', $dados['email']);
        $query = $this->db->get();
        $result = $query->result_array();
        if (empty($result)) {
            return 0;
        }
        return $result[0]['codigoUsuario'];
        return true;
    }

    public function insertToken($dados) {
        $this->db->set('token', $dados['token']);
        $this->db->where('codigoUsuario', $dados['codigoUsuario']);
        if (!$this->db->update('usuarios')) {
            return false;
        }
        return true;
    }

    public function ativarToken($dados) {
        $this->db->select('codigoUsuario');
        $this->db->from('usuarios');
        $this->db->where('token', $dados['token']);
        $query = $this->db->get();
        $result = $query->result_array();
        if (empty($result)) {
            return 0;
        }
        if ($result[0]['codigoUsuario'] == $dados['codigoUsuario']) {
            $this->db->set('ativado', 1);
            $this->db->where('codigoUsuario', $dados['codigoUsuario']);
            if (!$this->db->update('usuarios')) {
                return false;
            }
            return true;
        }
        return false;
    }

}
