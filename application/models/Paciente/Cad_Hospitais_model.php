<?php

class Cad_Hospitais_model extends CI_Model {

    public function consultaHospital($dados) {
        $this->db->select('*');
        $this->db->from('tblHospitais');
        if (!empty($dados["codigoHospital"])) {
            $this->db->where("codigoHospital", $dados["codigoHospital"]);
        }
        if (!empty($dados["nome"])) {
            $this->db->like("nome", $dados["nome"]);
        }
        if (!empty($dados["municipio"])) {
            $this->db->like("municipio", $dados["municipio"]);
        }
        if (!empty($dados["uf"])) {
            $this->db->where("uf", $dados["uf"]);
        } else {
            $this->db->limit(100);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function consultaMunicipio($dados) {
        $this->db->select('distinct(municipio)');
        $this->db->from('tblHospitais');
        if (!empty($dados["codigoHospital"])) {
            $this->db->where("codigoHospital", $dados["codigoHospital"]);
        }
        if (!empty($dados["nome"])) {
            $this->db->like("nome", $dados["nome"]);
        }
        if (!empty($dados["uf"])) {
            $this->db->where("uf", $dados["uf"]);
        } else {
            $this->db->limit(100);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function consultaHospitalTop10($dados) {
        $this->db->select('*');
        $this->db->from('tblHospitais');
        if (!empty($dados["nome"])) {
            $this->db->like("nome", $dados["nome"]);
        }
        if (!empty($dados["uf"])) {
            $this->db->where("uf", $dados["uf"]);
        }
        $this->db->limit(10);
        $query = $this->db->get();
        return $query->result();
    }

    public function excluirHospital($dados) {
        $this->db->where('codigoHospital', $dados['codigoHospital']);
        return $this->db->delete("tblHospitais");
    }

    public function salvarHospital($dados) {

        $this->db->trans_begin();

        $this->db->set('nome', $dados['nomeCompleto']);
        $this->db->set('uf', $dados['uf']);
        $this->db->set('cnpj', $dados['cnpj']);
        $this->db->set('endereco', $dados['endereco']);
        $this->db->set('telefoneFixo', $dados['telefoneFixo']);
        $this->db->set('telefoneCelular', $dados['telefoneCelular']);
        $this->db->set('email', $dados['email']);
        $this->db->set('contatoCotacao', $dados['contatoCotacao']);

        $this->db->insert('tblHospitais');

        //make transaction complete
        $this->db->trans_complete();

        //check if transaction status TRUE or FALSE
        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //if everything went right, commit the data to the database
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function editarHospital($dados) {

        $this->db->trans_begin();

        $this->db->where('codigoHospital', $dados['codigoHospital']);
        $this->db->set('nome', $dados['nomeCompleto']);
        $this->db->set('uf', $dados['uf']);
        $this->db->set('cnpj', $dados['cnpj']);
        $this->db->set('endereco', $dados['endereco']);
        $this->db->set('telefoneFixo', $dados['telefoneFixo']);
        $this->db->set('telefoneCelular', $dados['telefoneCelular']);
        $this->db->set('email', $dados['email']);
        $this->db->set('contatoCotacao', $dados['contatoCotacao']);
        $this->db->update('tblHospitais');

        //make transaction complete
        $this->db->trans_complete();

        //check if transaction status TRUE or FALSE
        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //if everything went right, commit the data to the database
            $this->db->trans_commit();
            return TRUE;
        }
    }

}
