<?php

class PlanoSaude_model extends CI_Model {

    public function consultaPlanos($dados) {
        $this->db->select('*');
        $this->db->from('tblplanosaude');
        if (!empty($dados['codigoPlano'])) {
            $this->db->where("codigoPlano", $dados["codigoPlano"]);
        }
        $query = $this->db->get();
        return $query->result_array();
    }

    public function consultaPlanosNome($dados) {
        $this->db->select('*');
        $this->db->from('tblplanosaude');
        $this->db->where("nomePlano", $dados);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function editarPaciente($dados) {
        $this->db->where('codigoPaciente', $dados['codigoPaciente']);
        $this->db->set('nomePaciente ', $dados['nomeCompleto']);
        $this->db->set('nomeMaePaciente ', $dados['nomeMae']);
        $this->db->set('dataNacimento ', $dados['dataNascimento']);
        $this->db->set('CPF ', $dados['CPF']);
        $this->db->set('convenio ', $dados['convenio']);
        $this->db->set('numeroCarteirinha ', $dados['nCarteirinha']);
        $this->db->set('dataInicioContratoConvenio ', $dados['dataContrato']);
        $this->db->set('email ', $dados['email']);
        $this->db->set('situacaoPaciente ', 0);

        $this->db->update('tblpaciente');
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function cadastrarFornecedor($dados) {
        $this->db->set('codigoFabricante', $dados['fabricante']);
        $this->db->set('fornecedor', $dados['fornecedor']);
        $this->db->set('nomeRepresentante', $dados['nomeRepresentante']);
        $this->db->set('contato', $dados['contatoRepresentante']);
        $this->db->set('estado', $dados['estado']);
        $this->db->set('endereco', $dados['endereco']);


        $this->db->insert('tblFornecedores');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function excluirPlano($dados) {
        $this->db->where('codigoPlano', $dados['codigoPlano']);
        return $this->db->delete("tblplanosaude");
    }

    public function salvarPlano($dados) {

        $this->db->trans_begin();

        $this->db->set('nomePlano', $dados['nomePlano']);
        $this->db->set('cnpj', $dados['cnpjPlano']);
        $this->db->set('endereco', $dados['enderecoPlano']);
        $this->db->set('telefoneFixo', $dados['telefoneFixo']);
        $this->db->set('telefoneCelular', $dados['telefoneCelular']);
        $this->db->set('email', $dados['emailPlano']);
        $this->db->set('logo', $dados['logo']);
        $this->db->set('razaoSocial ', $dados['razaoSocial']);
        $this->db->set('registroANS ', $dados['registroANS']);
        $this->db->insert('tblplanosaude');

        //make transaction complete
        $this->db->trans_complete();

        //check if transaction status TRUE or FALSE
        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //if everything went right, commit the data to the database
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function editarPlano($dados) {

        $this->db->trans_begin();

        $this->db->where('CodigoPlano', $dados['CodigoPlano']);
        $this->db->set('nomePlano', $dados['nomePlano']);
        $this->db->set('cnpj', $dados['cnpjPlano']);
        $this->db->set('endereco', $dados['enderecoPlano']);
        $this->db->set('telefoneFixo', $dados['telefoneFixo']);
        $this->db->set('telefoneCelular', $dados['telefoneCelular']);
        $this->db->set('email', $dados['emailPlano']);
        $this->db->set('logo', $dados['logo']);
        $this->db->set('razaoSocial ', $dados['razaoSocial']);
        $this->db->set('registroANS ', $dados['registroANS']);
        $this->db->update('tblplanosaude');

        //make transaction complete
        $this->db->trans_complete();

        //check if transaction status TRUE or FALSE
        if ($this->db->trans_status() === FALSE) {
            //if something went wrong, rollback everything
            $this->db->trans_rollback();
            return FALSE;
        } else {
            //if everything went right, commit the data to the database
            $this->db->trans_commit();
            return TRUE;
        }
    }

}
