<?php

class Cad_Cirurgiao_model extends CI_Model {

    public function consultaCirurgiao($dados) {
        $this->db->select('ci.codigoCirurgiao');
        $this->db->select('ci.nomeCirurgiao');
        $this->db->select('ci.codigoConselhoProfissional');
        $this->db->select('co.sigla');
        $this->db->select('co.conselhoProfissional');
        $this->db->select('ci.numeroConselhoProfissional');
        $this->db->select('ci.codigoEspecialidade');
        $this->db->select('es.especialidade');
        $this->db->select('ci.CPF');
        $this->db->select('ci.nomeClinica');
        $this->db->select('ci.enderecoProfissional');
        $this->db->select('ci.email');
        $this->db->from('tblCirurgiao ci');
        $this->db->join('tblEspecialidades es', 'ci.codigoEspecialidade = es.codigoEspecialidade', 'left');
        $this->db->join('tblConselhoProfissional co', 'ci.codigoConselhoProfissional = co.codigoConselhoProfissional', 'left');
        if (!empty($dados["codigoCirurgiao"])) {
            $this->db->where("ci.codigoCirurgiao", $dados["codigoCirurgiao"]);
        }
        if (!empty($dados["codigoEspecialidade"])) {
            $this->db->where("ci.codigoEspecialidade", $dados["codigoEspecialidade"]);
        }
        if (!empty($dados["codigoConselho"])) {
            $this->db->where("ci.codigoConselho", $dados["codigoConselho"]);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function editarPaciente($dados) {
        $this->db->where('codigoPaciente', $dados['codigoPaciente']);
        $this->db->set('nomePaciente ', $dados['nomeCompleto']);
        $this->db->set('nomeMaePaciente ', $dados['nomeMae']);
        $this->db->set('dataNascimento ', $dados['dataNascimento']);
        $this->db->set('CPF ', $dados['CPF']);
        $this->db->set('convenio ', $dados['convenio']);
        $this->db->set('numeroCarteirinha ', $dados['nCarteirinha']);
        $this->db->set('dataInicioContratoConvenio ', $dados['dataContrato']);
        $this->db->set('email ', $dados['email']);
        $this->db->set('situacaoPaciente ', 0);

        $this->db->update('tblPaciente');
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function cadastraCirurgiao($dados) {
        $this->db->set('nomeCirurgiao', $dados["nomeCompleto"]);
        $this->db->set('codigoConselhoProfissional', $dados["conselhoProfissional"]);
        $this->db->set('numeroConselhoProfissional', $dados["numeroConselhoProfissional"]);
        $this->db->set('codigoEspecialidade', $dados["especialidadeCirurgiao"]);
        $this->db->set('CPF', $dados["cpfCirurgiao"]);
        $this->db->set('nomeClinica', $dados["clinicaCirurgiao"]);
        $this->db->set('enderecoProfissional', $dados["enderecoProfissional"]);
        $this->db->set('email', $dados["email"]);
        $this->db->set('situacaoCirurgiao', 0);
        $this->db->insert('tblCirurgiao');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function excluirCirurgiao($dados) {
        $this->db->where('codigoCirurgiao', $dados['codigo']);
        return $this->db->delete("tblCirurgiao");
    }

    public function populaConselho() {
        $this->db->select("*");
        $this->db->from("tblConselhoProfissional");
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

    public function populaEspecialidade() {
        $this->db->select("*");
        $this->db->from("tblEspecialidades");
        $consulta = $this->db->get();
        $resultado = $consulta->result();
        return $resultado;
    }

}
