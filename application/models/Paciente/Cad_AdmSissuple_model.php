<?php

class Cad_AdmSissuple_model extends CI_Model {

    public function consultaADM($dados) {
        $this->db->select('*');
        $this->db->from('tbladmsissuple');
        if (!empty($dados["codigoAdmSissuple"])) {
            $this->db->where("codigoAdmSissuple", $dados["codigoAdmSissuple"]);
        }
        $query = $this->db->get();
        return $query->result();
    }

    public function editarADM($dados) {
        $this->db->where('codigoAdmSissuple', $dados['codigoAdmSissuple']);
        $this->db->set('nome', $dados["nomeCompleto"]);
        $this->db->set('cpf', $dados["cpf"]);
        $this->db->set('email', $dados["email"]);
        $this->db->set('telefone', $dados["telefoneCelular"]);

        $this->db->update('tbladmsissuple');
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function cadastrarADM($dados) {
        $this->db->set('nome', $dados["nomeCompleto"]);
        $this->db->set('cpf', $dados["cpf"]);
        $this->db->set('email', $dados["email"]);
        $this->db->set('telefone', $dados["telefoneCelular"]);
        $this->db->insert('tbladmsissuple');
        $this->db->trans_complete();
        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function excluirADM($dados) {
        $this->db->where('codigoAdmSissuple', $dados['codigo']);
        return $this->db->delete("tbladmsissuple");
    }

}
