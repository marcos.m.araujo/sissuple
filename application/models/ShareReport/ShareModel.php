<?php

class ShareModel extends CI_Model {

    public function getShereReportModel() {
        $SQL = "SELECT 
                mo.codigoCompartilhamentoLaudoModelo,
                mo.codigoLaudoElaboracao,
                mo.email,
                mo.codigoCirurgiao,
                mo.dataCompartilhamento,
                la.tratamentos,
                la.intervencao,
                la.marca,
                la.dataLaudo,
                concat(' Dr.(a) ', ci.nomeCirurgiao, ' ',  ci.sobrenomeCirurgiao) as nomeCirurgiao,
                ci.email as emailCirurgiao   
            FROM tblCompartilhamentoLaudoModelo mo
            left join viewLaudoElaboracao as la
            on mo.codigoLaudoElaboracao = la.codigoLaudoElaboracao
            left join tblCirurgiao as ci
            on mo.codigoCirurgiao = ci.codigoCirurgiao where mo.email = '{$this->session->email}'";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    public function insertCompartilhamentoModelo($dados) {
        $this->db->insert('tblCompartilhamentoLaudoModelo', $dados);
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

    public function updateAplicacao($dados) {
        $this->db->where('codigoAnatomiaAplicacaoAnatomica', $dados['codigoAnatomiaAplicacaoAnatomica']);
        $this->db->set('codigoAnatomiaAplicacao', $dados['codigoAnatomiaAplicacao']);
        $this->db->set('codigoAnatomia', $dados['codigoAnatomia']);
        $this->db->set('nome', $dados['nome']);
        return $this->db->update('tblAnatomiaAplicacaoAnatomica', $dados);
    }

    public function exluirModeloShare($dados) {
        $this->db->where('codigoCompartilhamentoLaudoModelo', $dados['codigoCompartilhamentoLaudoModelo']);
        $this->db->delete('tblCompartilhamentoLaudoModelo');
        $this->db->trans_complete();
        if ($this->db->trans_status() === true) {
            $this->db->trans_rollback();
            return true;
        } else {
            $this->db->trans_rollback();
            return false;
        }
    }

}
