<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class PHPMailer_Lib {

    public function __construct() {
        $this->CI = &get_instance();
        log_message('Debug', 'PHPMailer class is loaded.');
    }

    public function send($dados) {
        require_once APPPATH . 'third_party/PHPMailer/Exception.php';
        require_once APPPATH . 'third_party/PHPMailer/PHPMailer.php';
        require_once APPPATH . 'third_party/PHPMailer/SMTP.php';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = 'email-ssl.com.br';
        $mail->SMTPAuth = true;
        $mail->Username = 'suporte@ebuco.com.br';
        $mail->Password = '!@Uc14417655';
        $mail->SMTPSecure = 'ssl';
        $mail->CharSet = 'UTF-8';
        $mail->Port = 465;
        $mail->setFrom('suporte@ebuco.com.br', 'Suporte eBuco');
        $mail->addAddress($dados['email']);
        $mail->Subject = $dados['assunto'];
        $mail->isHTML(true);
        $mailContent = $dados['texto'];
        $mail->Body = $mailContent;
        if (!$mail->send()) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
    }
    
     public function sendLaudo($dados) {
        require_once APPPATH . 'third_party/PHPMailer/Exception.php';
        require_once APPPATH . 'third_party/PHPMailer/PHPMailer.php';
        require_once APPPATH . 'third_party/PHPMailer/SMTP.php';
        $mail = new PHPMailer;
        $mail->isSMTP();
        $mail->Host = 'email-ssl.com.br';
        $mail->SMTPAuth = true;
        $mail->Username = 'laudo@ebuco.com.br';
        $mail->Password = '!@Uc14417655';
        $mail->SMTPSecure = 'ssl';
        $mail->CharSet = 'UTF-8';
        $mail->Port = 465;
        $mail->setFrom('laudo@ebuco.com.br', 'Laudo eBuco');
        $mail->addAddress($dados['email']);
        $mail->Subject = $dados['assunto'];
        $mail->isHTML(true);
        $mailContent = $dados['texto'];
        $mail->Body = $mailContent;
        if (!$mail->send()) {
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }
    }


    public function emailLaudoHospitalCompartilhado($numeroLaudo, $nomeCirurgiao, $plano, $paciente, $email_, $emailCirurgiao)
    {
        $email['email'] = $email_;
        $email['assunto'] = 'Novo Laudo eBuco Numero: ' . $numeroLaudo . '';
        $email['texto'] = '<div 
        style="
            font-size:12pt;
            padding: 40px;
            text-align: justify; 
            width: 600px; 
            margin-left: 0%;
            color: #666";               
            font-family: "Open Sans",Calibri,Verdana,sans-serif;                
            >
            <img style="width: 30%; margin-left: 30%" src="https://ebuco.com.br/newtheme//img/ebuco.png"><br>
            Olá ' . $email_ . '  ,
           
                <p style="line-height: 1.2;">
                    Dr.(a) <b>' . $nomeCirurgiao . '</b> compartihou um laudo bucomaxilofacial com você.
                </p>
                <p> <b>Nome do paciente: </b> ' . $paciente . '<br> 
                <b> Plano de saúde: </b>   ' . $plano . '<br>
                <b> E-mail cirurgião: </b>   ' . $emailCirurgiao . '
                </p>          
                <p style="line-height: 1.2; text-align: justify">
                    Caso necessite de auxílio para análise de autorizações parciais, composição de juntas médicas ou de negativas, 
                    nossos técnicos estão à disposição para te ajudar.
                    Também estamos nas redes sociais, local em que poderemos responder questões em tempo real.                       
                </p>
                <br>
                           
                <a style="background-image: linear-gradient(to right, #acb6e5, #86fde8);
                          color: #051937;
                          padding:10px;
                          border-radius: 15px;
                          margin-left:25%;                        
                          text-decoration:none; 
                          -webkit-box-shadow: 0px 10px 22px -13px rgba(0,0,0,0.75);
                          -moz-box-shadow: 0px 10px 22px -13px rgba(0,0,0,0.75);
                           box-shadow: 0px 10px 22px -13px rgba(0,0,0,0.75);
                "                      
                href="https://ebuco.com.br/Novo_Parceiro"><b>Clique aqui para realizar cadastro</b></a>
                <br>
                 <div style="margin-left: 0%; margin-top: 20px">
                        Atenciosamente<br><b>Atendimento Digital | eBuco</b> 
                 </div>
                <div style="margin-left: 0%">
                <br>
                 <p>
                    <img style="width: 15px" src="https://logodownload.org/wp-content/uploads/2015/04/whatsapp-logo-1.png"> (61) 981831130<br>
                    <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Facebook_logo_%28square%29.png/600px-Facebook_logo_%28square%29.png"> Ebuco Maxilo<br>
                    <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/1024px-Instagram_logo_2016.svg.png"> ebuco_brasil<br>
                    <img style="width: 15px" src="https://www.ocomuniqueiro.com.br/wp-content/uploads/2018/01/Email-Logo.jpg"> laudo@ebuco.com.br
                </p>
              <a href="https://ebuco.com.br/">https://ebuco.com.br/</a>
                </div> 
        </div>';
       $this->sendLaudo($email);
    }

    function emailLaudoConcluido($nomeCirurgiao, $email_, $nomePaciente, $tratamentos, $plano,$codigoLaudo  )
    {
        $email['email'] = $email_;
        $email['assunto'] = 'Novo Laudo eBuco';
        $email['texto'] = '<div 
            style="
                font-size:12pt;
                padding: 40px;
                text-align: justify; 
                width: 900px; 
                margin-left: 0%;
                color: #666";               
                font-family: "Open Sans",Calibri,Verdana,sans-serif;
                >
                Olá Dr.(a) ' . $nomeCirurgiao . ',</h3>
                <img style="width: 40%; margin-left: 40px" src="https://ebuco.com.br/newtheme//img/ebuco.png">
                    <p style="line-height: 1.2;">
                        Nós do eBuco ficamos felizes que você tenha uma nova cirurgia.
                        Por isso, estamos te enviando a cópia em PDF do laudo confeccionado.
                        Agora você poderá registrar a movimentação do pedido e acompanhar a contagem de prazos legais para a autorização. 
                    </p>
                    <p> <b>Nome do paciente: </b>' . $nomePaciente. '<br> <b>Tratamento proposto: </b>' . $tratamentos . '<br>
                    <b> Plano de saúde: </b>' . $plano . '</p> 
                                  
                        
                    <a download="paciente_fulano" href="' . base_url() . 'EditorLaudo/Laudo_Pedido_Autorizacao_PDF?codigoLaudoElaboracao=' . $codigoLaudo . '">Baixar Laudo.pdf</a>
                    <p style="line-height: 1.2;">
                        Se você já deu entrada no pedido, a tendencia é que o plano de saúde analise o laudo e os exames e adote uma das seguintes condutas:<br><br>
                        <b>1 - Autorizar o pedido integralmente (Estamos torcendo para isso);</b><br>
                        <b>2 - Autorizar o pedido parcialmente (Sem composição de junta médica, isso não pode);</b><br>
                        <b>3 - Abrir uma divergência técnica e sugerir a composição de uma junta médica (Acontece muito, viu?!?!);</b><br>
                        <b>4 - Negar sumariamente o pedido.</b>
                    </p>
                    <p style="line-height: 1.2;">
                        Caso necessite de auxílio para análise de autorizações parciais, composição de juntas médicas ou de negativas, nossos técnicos estão à disposição para te ajudar por meio de pareceres técnicos sobre o processo de autorização.
                        Também estamos nas redes sociais, local em que poderemos responder questões em tempo real.                       
                    </p>
                     <div style="margin-left: 0%">
                            Atenciosamente<br><b>Atendimento Digital | eBuco</b> 
                     </div>
                    <div style="margin-left: 0%">
                    <br>
                     <p>
                        <img style="width: 15px" src="https://logodownload.org/wp-content/uploads/2015/04/whatsapp-logo-1.png"> (61) 981831130<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Facebook_logo_%28square%29.png/600px-Facebook_logo_%28square%29.png"> Ebuco Maxilo<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/1024px-Instagram_logo_2016.svg.png"> ebuco_brasil<br>
                        <img style="width: 15px" src="https://www.ocomuniqueiro.com.br/wp-content/uploads/2018/01/Email-Logo.jpg"> laudo@ebuco.com.br
                    </p>

                    </div>
 
            </div>';
        $this->send($email);
        
    }

    function emailLaudoPaciente($nomePaciente, $emailPaciente, $nomeCirurgicao, $tratamentos, $plano,$codigoLaudo  )
    {
        $email['email'] = $emailPaciente;
        $email['assunto'] = 'Movimentação do laudo eBuco';
        $email['texto'] = '<div 
            style="
                font-size:12pt;
                padding: 40px;
                text-align: justify; 
                width: 900px; 
                margin-left: 0%;
                color: #666";               
                font-family: "Open Sans",Calibri,Verdana,sans-serif;
                >
                Olá ' . $nomePaciente . ',</h3>
                <img style="width: 40%; margin-left: 40px" src="https://ebuco.com.br/newtheme//img/ebuco.png">
                    <p style="line-height: 1.2;">
                        Existe uma nova movimentação referente ao seu tratamento.
                    </p>
                    <p> <b>Nome do paciente: </b>' . $nomePaciente. '<br> <b>Tratamento proposto: </b>' . $tratamentos . '<br>
                    <b> Plano de saúde: </b>' . $plano . '</p> <br>  
                    <b> Cirurgião (ã): </b>   ' . $nomeCirurgicao . '<br>                     
                        
                    <a download="paciente_fulano" href="' . base_url() . 'EditorLaudo/Laudo_Pedido_Autorizacao_PDF?codigoLaudoElaboracao=' . $codigoLaudo . '">Baixar Laudo.pdf</a>
                 
                
                     <div style="margin-left: 0%">
                            Atenciosamente<br><b>Atendimento Digital | eBuco</b> 
                     </div>
                    <div style="margin-left: 0%">
                    <br>
                     <p>
                        <img style="width: 15px" src="https://logodownload.org/wp-content/uploads/2015/04/whatsapp-logo-1.png"> (61) 981831130<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Facebook_logo_%28square%29.png/600px-Facebook_logo_%28square%29.png"> Ebuco Maxilo<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/1024px-Instagram_logo_2016.svg.png"> ebuco_brasil<br>
                        <img style="width: 15px" src="https://www.ocomuniqueiro.com.br/wp-content/uploads/2018/01/Email-Logo.jpg"> laudo@ebuco.com.br
                    </p>

                    </div>
 
            </div>';
      return  $this->send($email);
        
    }

    function notificarMovimentacao($emailDestinatario, $nomeDestinatario, $nomePaciente, $nomeCirurgicao, $tratamentos, $plano,$codigoLaudo  )
    {
        $email['email'] = $emailDestinatario;
        $email['assunto'] = 'Movimentacao de laudo eBuco';
        $email['texto'] = '<div 
            style="
                font-size:12pt;
                padding: 40px;
                text-align: justify; 
                width: 900px; 
                margin-left: 0%;
                color: #666";               
                font-family: "Open Sans",Calibri,Verdana,sans-serif;
                >
                Olá ' . $nomeDestinatario . ',</h3>
                <img style="width: 40%; margin-left: 40px" src="https://ebuco.com.br/newtheme//img/ebuco.png">
                    <p style="line-height: 1.2;">
                        Existe uma nova movimentação referente ao laudo no eBuco.
                    </p>
                    <p> <b>Nome do paciente: </b>' . $nomePaciente. '<br> <b>Tratamento proposto: </b>' . $tratamentos . '<br>
                    <b> Plano de saúde: </b>' . $plano . '</p> <br>  
                    <b> Cirurgião (ã): </b>   ' . $nomeCirurgicao . '<br>                     
                        
                    <a download="paciente_fulano" href="' . base_url() . 'EditorLaudo/Laudo_Pedido_Autorizacao_PDF?codigoLaudoElaboracao=' . $codigoLaudo . '">Baixar Laudo.pdf</a>
                
                
                     <div style="margin-left: 0%">
                            Atenciosamente<br><b>Atendimento Digital | eBuco</b> 
                     </div>
                    <div style="margin-left: 0%">
                    <br>
                     <p>
                        <img style="width: 15px" src="https://logodownload.org/wp-content/uploads/2015/04/whatsapp-logo-1.png"> (61) 981831130<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Facebook_logo_%28square%29.png/600px-Facebook_logo_%28square%29.png"> Ebuco Maxilo<br>
                        <img style="width: 15px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/1024px-Instagram_logo_2016.svg.png"> ebuco_brasil<br>
                        <img style="width: 15px" src="https://www.ocomuniqueiro.com.br/wp-content/uploads/2018/01/Email-Logo.jpg"> laudo@ebuco.com.br
                    </p>

                    </div>
 
            </div>';
      return  $this->send($email);
        
    }
}
