<!doctype html>
<html lang="pt-br">

<head>
  <?php $this->load->view('app_template/head') ?>
</head>

<link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&display=swap" rel="stylesheet">
<style>
  h1, h2, h2, span, p{
    font-family: 'Open Sans Condensed', sans-serif;
  }
</style>

<body class="theme-orange font-montserrat light_version h-menu">
  <div class="page-loader-wrapper">
    <div class="loader">
      <div class="bar1"></div>
      <div class="bar2"></div>
      <div class="bar3"></div>
      <div class="bar4"></div>
      <div class="bar5"></div>
    </div>
  </div>
  <div class="overlay"></div>
  <div id="wrapper">
    <nav class="navbar top-navbar">
    
      <div style="margin-top: -100px!important" class="progress-container">
        <div class="progress-bar" id="myBar"></div>
      </div>
    </nav>
    <div class="search_div">
      <?php//$this->load->view('app_template_box/search_div') ?>
    </div>
    <div id="megamenu" class="megamenu particles_js">
      <?php $this->load->view('app_template_box/megamenu') ?>
      <div id="particles-js"></div>
    </div>
    <div id="rightbar" class="rightbar">
      <?php $this->load->view('app_template_box/rightbar') ?>
    </div>
  
    <div id="main-content">
      <?php $this->load->view($pagina) ?>
    </div>
  </div>
  <?php $this->load->view('app_template/script') ?>