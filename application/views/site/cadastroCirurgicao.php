<html lang="pt-BR">

<head>
    <title>eBuco - HOME</title>
    <link rel="icon" type="image/png" href="<?= base_url('newtheme/') ?>login/images/icons/imgEbuco.ico" />
    <link type="text/css" rel="stylesheet" href="<?= base_url('newtheme/') ?>site/home.css">
    <link type="text/css" rel="stylesheet" href="<?= base_url('newtheme/') ?>site/criadordesites.css">
    <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/flaticon.css" />
    <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/owl.carousel.css" />
    <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/style.css" />
    <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/animate.css" />
</head>

<body>
    <section class="contact-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-text">
                        <h3>Cadastrar</h3>
                        <div class="contact-info">
                            <div class="ci-image set-bg" data-setbg="<?= base_url('newtheme/') ?>site/img/doctors/1.jpg"></div>
                            <div class="ci-content">
                                <h4>Dados pessoais do cirurgião</h4>
                                <p>Após a validação do seu cadastro poderá cadastrar suas clínicas em seu pefil no eBuco.</p><br>
                                <div id="validacaoEmail"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <form class="contact-form" method="POST" action="<?= base_url() ?>Cirurgiao/Cirurgiao/cadastrarCirurgiao">
                        <input type="text" name="nomeCirurgiao" placeholder="Nome" required="true">
                        <input type="text" name="sobrenomeCirurgiao" placeholder="Sobrenome" required="true">
                        <input type="text" placeholder="CPF" id="cpf" name="cpf" placeholder="CPF" required onchange="validaCPF()" maxlength="14" onkeypress="formatar(this, '000.000.000-00')">
                        <input type="text" name="telefoneCirurgiao" id="telefone" placeholder="Telefone celular" required="true" minlength="9" maxlength="14" onkeypress="formatar(this, '00 00000-0000')">
                        <select name="sexo" type="text" placeholder="Sexo" required="true">
                            <option value="">Sexo</option>
                            <option value="M">M</option>
                            <option value="F">F</option>
                        </select>
                        <select name="codigoConselhoProfissional" type="text" placeholder="Sexo" required="true">
                            <?php if (isset($conselhoProfissional) && !empty($conselhoProfissional)) : ?>
                                <?php foreach ($conselhoProfissional as $value) : ?>
                                    <option value="<?= $value->codigoConselhoProfissional ?>"><?= $value->conselhoProfissional; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        <input type="text" name="numeroConselhoProfissional" placeholder="N° Conselho Profissional" required="true">
                        <select name="codigoEstado" id="estado_up" name="estado_up" title="UF Conselho Profissional" style="width: 100%;" required="true">
                            <option value="">Estado</option>
                            <option value="1">Acre</option>
                            <option value="2">Alagoas</option>
                            <option value="3">Amapá</option>
                            <option value="4">Amazonas</option>
                            <option value="5">Bahia</option>
                            <option value="6">Ceará</option>
                            <option value="7">Distrito Federal</option>
                            <option value="8">Espirito Santo</option>
                            <option value="9">Goiás</option>
                            <option value="10">Maranhão</option>
                            <option value="11">Mato Grosso do Sul</option>
                            <option value="12">Mato Grosso</option>
                            <option value="13">Minas Gerais</option>
                            <option value="14">Pará</option>
                            <option value="15">Paraíba</option>
                            <option value="16">Paraná</option>
                            <option value="17">Pernambuco</option>
                            <option value="18">Piauí</option>
                            <option value="19">Rio de Janeiro</option>
                            <option value="20">Rio Grande do Norte</option>
                            <option value="21">Rio Grande do Sul</option>
                            <option value="22">Rondônia</option>
                            <option value="23">Roraima</option>
                            <option value="24">Santa Catarina</option>
                            <option value="25">São Paulo</option>
                            <option value="26">Sergipe</option>
                            <option value="27">Tocantins</option>
                        </select>
                        <input type="email" id="email" onblur="emailDuplicado()" name="email" placeholder="E-mail" required="true">
                        <input onclick="aceite()" type="checkbox"> Declaro que li os termos de uso. <a target="blanck" href="<?= base_url() ?>Site/termosUSO">Termos de uso</a><br>
                        <button id="btnSubmit" class="site-btn">Cadastrar</button>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <script src="<?= base_url('newtheme/') ?>site/js/jquery-3.2.1.min.js"></script>
    <script>
        var baseURL = "<?php print base_url() ?>"
        $(document).ready(function() {
            $("#btnSubmit").css("display", "none");
        })

        function aceite() {
            $("#btnSubmit").toggle("slow");
        }

        function validaCPF() {
            var cpf = $("#cpf").val();
            cpf = cpf.replace("-", "");
            cpf = cpf.replace(".", "");
            cpf = cpf.replace(".", "");
            resultCPF = TestaCPF(cpf);
            if (resultCPF === false) {
                $("#cpf").css('border-color', 'red');
                $("#cpf").after('<span style="font-size:9pt; color: red">CPF não é válido</span>');
                return false;
            } else {
                $("#cpf").css('border-color', '#71cd29');
                $("#cpf").next('span').remove();
            }
        }

        function emailDuplicado() {
            var em = $('#email').val()
            console.log(em)
            $.ajax({
                type: 'POST',
                url: baseURL + 'Cirurgiao/Cirurgiao/getUsuarioEmail',
                data: {
                    email: em
                },
                dataType: "json",
                success: function(data) {
                    if (data == 1) {
                        $("#email").after('<span style="font-size:9pt; color: red">e-mail já encontra-se em nossa base de dados.</span>');
                        $("#email").css('border-color', 'red');
                    } else {
                        $("#email").css('border-color', '#71cd29');
                        $("#email").next('span').remove();
                    }
                },
                error: function(data) {
                    console.log(data);
                },
            })
        }
    </script>

    <script src="<?= base_url('newtheme/') ?>site/js/bootstrap.min.js"></script>
    <script src="<?= base_url('newtheme/') ?>site/js/owl.carousel.min.js"></script>
    <script src="<?= base_url('newtheme/') ?>site/js/circle-progress.min.js"></script>
    <script src="<?= base_url('newtheme/') ?>site/js/main.js"></script>
    <?php $this->load->view('comum/comum_js'); ?>
</body>

</html>