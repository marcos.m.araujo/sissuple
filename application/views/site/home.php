<html lang="pt-BR">
    <head>
        <title>eBuco - HOME</title>
        <link rel="icon" type="image/png" href="<?= base_url('newtheme/') ?>login/images/icons/imgEbuco.ico"/>
        <link type="text/css" rel="stylesheet" href="<?= base_url('newtheme/') ?>site/home.css">
        <link type="text/css" rel="stylesheet" href="<?= base_url('newtheme/') ?>site/criadordesites.css">
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/flaticon.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/owl.carousel.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/style.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/animate.css" />
    </head>
    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>

        <!-- Header section -->
        <header class="header-section">
            <div class="container">
                <!-- Site Logo -->
                <a href="<?= base_url() ?>" class="site-logo">
                    <img class="logo" src="<?= base_url('newtheme/') ?>site/img/logo.png" alt="">
                </a>
                <!-- responsive -->
                <div class="nav-switch">
                    <i class="fa fa-bars"></i>
                </div>
                <!-- Main Menu -->
                <ul class="main-menu">
                    <li class="active"><a href="<?= base_url() ?>">Home</a></li>        
                    <li><a href="#servicos">Serviços</a></li>
                    <li><a href="#contato">Contato</a></li>                    
                    <li><a href="<?= base_url() ?>Novo_Parceiro">Seja um parceiro</a></li>
                    <li><a href="<?= base_url() ?>Login">Entrar</a></li>

                </ul>
            </div>
            <div class="header-info">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 hi-item">
                            <div class="hs-icon">
                                <img src="<?= base_url('newtheme/') ?>site/img/icons/medical-report.png" alt="">
                            </div>
                            <div class="hi-content">
                                <h6>Controle de pedidos</h6>
                                <p style="font-size: 10pt;  margin-left: 25px">Tenha um maior controle sobre seus laudos.</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 hi-item">
                            <div class="hs-icon">
                                <img src="<?= base_url('newtheme/') ?>site/img/icons/placa-de-dardo.png" alt="">
                            </div>
                            <div class="hi-content">
                                <h6>Características clínicas</h6>
                                <p style="font-size: 10pt;  margin-left: 25px">Sintomas, diagnósticos, tratamentos, CID´s, códigos TUSS e
                                    outros de forma simples e objetiva.</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 hi-item">
                            <div class="hs-icon">
                                <img src="<?= base_url('newtheme/') ?>site/img/icons/dental.png" alt="">
                            </div>
                            <div class="hi-content">
                                <h6>Seleção de OPME's</h6>
                                <p style="font-size: 10pt;  margin-left: 25px">Acesse um catálogo com milhares de OPME's.</p>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 hi-item">
                            <div class="hs-icon">
                                <img src="<?= base_url('newtheme/') ?>site/img/icons/servico-ao-cliente.png" alt="">
                            </div>
                            <div class="hi-content">
                                <h6>Parecer técnico</h6>
                                <p style="font-size: 10pt;  margin-left: 25px">Técnicos especializados.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Header section end -->


        <!-- Hero section -->
        <section class="hero-section">
            <div class="hero-slider owl-carousel">
                <!-- item -->
                <div class="hs-item set-bg text-white" data-setbg="<?= base_url('newtheme/') ?>site/img/slide-1.jpg">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-7">
                                <h3>Laudos e pareceres digitais em Cirurgia e Traumatologia Bucomaxilofacial</h3>
                                <p>O eBuco permite que o cirurgião selecione as OPME's necessárias para a realização da cirurgia conforme
                                    tipo, dimensões e matéria-prima.</p>
                                <a href="<?= base_url() ?>Site/cadastroCirurgiao" class="site-btn">Saiba mais</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- item -->
                <div class="hs-item set-bg text-white" data-setbg="<?= base_url('newtheme/') ?>site/img/slide-1.jpg">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-7">
                                <h3>Seleção de OPME's</h3>
                                <p>O eBuco permite que o cirurgião selecione as OPME's necessárias para a realização da cirurgia conforme
                                    tipo, dimensões e matéria-prima.</p>
                                <a href="<?= base_url() ?>Site/cadastroCirurgiao" class="site-btn">Saiba mais</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Hero section end -->


        <!-- Banner section -->
        <section class="banner-section">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 banner-text text-white">
                        <h4>Cadastre-se</h4>
                        <p>Cirurgião</p>
                    </div>
                    <div class="col-lg-5 text-lg-right">
                        <a href="<?= base_url() ?>Site/cadastroCirurgiao" class="site-btn sb-light">Sou cirurgião</a>
                    </div>
                </div>   
            </div>
        </section>

        <section class="about-section spad">
            <div  style="width: 100%; align-items: center; text-align: center" >
                <iframe src="https://www.youtube.com/embed/QJPrsV2Szjk" width="50%" height="400px">
                </iframe>
            </div>
        </section>

        <section class="facts-section set-bg" data-setbg="<?= base_url('newtheme/') ?>site/img/facts-bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 fact">
                        <img style="width: 100px;" src="<?= base_url('newtheme/') ?>site/img/icons/electric-toothbrush.png" alt="">
                        <h2>Mais de 5.000</h2>
                        <p>OPME'S cadastrados</p>
                    </div>
                    <div class="col-md-3 col-sm-6 fact">
                        <img style="width: 100px;" src="<?= base_url('newtheme/') ?>site/img/icons/loop.png" alt="">
                        <h2>Reutilização</h2>
                        <p>de Laudos</p>
                    </div>
                    <div class="col-md-3 col-sm-6 fact">
                        <img style="width: 100px;" src="<?= base_url('newtheme/') ?>site/img/icons/escudo.png" alt="">
                        <h2>Dados seguros</h2>
                        <p>Segurança em primeiro lugar</p>
                    </div>
                    <div class="col-md-3 col-sm-6 fact">
                        <img style="width: 100px;" src="<?= base_url('newtheme/') ?>site/img/icons/smiley.png" alt="">
                        <h2>Facilidade</h2>
                        <p>deixa o eBuco facilitar seu trabalho</p>
                    </div>
                </div>
            </div>
        </section>


        <div class="gallery-section">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-sm-6 p-0">
                        <img src="<?= base_url('newtheme/') ?>site/img/gallery/1.jpg" alt="">
                    </div>
                    <div class="col-lg-3 col-sm-6 p-0">
                        <img src="<?= base_url('newtheme/') ?>site/img/gallery/2.jpg" alt="">
                    </div>
                    <div class="col-lg-3 col-sm-6 p-0">
                        <img src="<?= base_url('newtheme/') ?>site/img/gallery/3.jpg" alt="">
                    </div>
                    <div class="col-lg-3 col-sm-6 p-0">
                        <img src="<?= base_url('newtheme/') ?>site/img/gallery/4.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>

        <section id="servicos" class="page-info-section set-bg" data-setbg="img/page-info-bg/2.jpg">
            <div class="container">
                <h2>Serviços</h2>
            </div>
        </section>

        <section class="services-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 service">
                        <div class="service-icon">
                            <img src="<?= base_url('newtheme/') ?>site/img/icons/medical-report.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Controle de Pedidos</h4>
                            <p>O eBuco permite que o cirurgião confeccione o laudo e o posicione no sistema conforme o prazo e a análise
                                do plano de saúde.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 service">
                        <div class="service-icon">
                            <img src="<?= base_url('newtheme/') ?>site/img/icons/dental.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Seleção de OPME's</h4>
                            <p>O selecione as OPME's necessárias para a realização da cirurgia conforme tipo, dimensão e matéria-prima.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-4 service">
                        <div class="service-icon">
                            <img src="<?= base_url('newtheme/') ?>site/img/icons/servico-ao-cliente.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Parecer Técnico</h4>
                            <p>Técnicos especializados na análise de auditorias, divergências, juntas médicas e odontológicas de planos
                                de saúde, além de assistências técnicas e periciais.</p>
                        </div>
                    </div>
                    <div class="col-lg-4 service">
                        <div class="service-icon">
                            <img src="<?= base_url('newtheme/') ?>site/img/icons/placa-de-dardo.png" alt="">
                        </div>
                        <div class="service-content">
                            <h4>Características Clínicas</h4>
                            <p>Oferecemos uma lista com centenas de hipóteses de diagnósticos, tratamentos, CID's, código TUSS,
                                objetivos de tratamentos, riscos em caso de não realização do tratamento proposto.</p>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section id="contato" class="contact-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="contact-text">
                            <h3>Contato</h3>
                            <div class="contact-info">
                                <div class="ci-image set-bg" data-setbg="<?= base_url('newtheme/') ?>site/img/contato.png"></div>
                                <div class="ci-content">
                                    <p>Tecnologia da informação em medicina e odontologia</p>
                                    <p><img width="40px" src="<?= base_url('newtheme/') ?>site/img//f5d7a93e49fe403597399af84fe5146a.png">(61) 98183-1130</p>
                                    <p>suporte@ebuco.com.br</p>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6">
                        <form class="contact-form">
                            <input type="text" placeholder="Nome">
                            <input type="text" placeholder="E-mail">
                            <input type="text" placeholder="Assunto">
                            <textarea placeholder="Mensagem"></textarea>
                            <button class="site-btn">Enviar</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>

        <section class="footer-top-section set-bg" data-setbg="<?= base_url('newtheme/') ?>site/img/footer-bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-widget">
                            <div class="row">

                                <div id="f2148d14-b6f3-4044-b102-9cc18eecc763" class="col-lg-2">
                                    <a href="https://www.instagram.com/ebuco_brasil/?hl=pt-br" target="_blank">
                                        <img class="cs-chosen-image" src="<?= base_url('newtheme/') ?>site/img//54f9cfc519a74eb2a86fddee6372a3cc.png" title="instagram-png-instagram-png-logo-1455.png" alt="instagram-png-instagram-png-logo-1455.png">
                                    </a>
                                </div>
                                <div id="379fb6a7-76c1-4217-8b3e-b3d36333b1b6" class="col-lg-2">
                                    <a href="https://api.whatsapp.com/send?phone=5561981831130" target="_blank">
                                        <img class="cs-chosen-image" src="<?= base_url('newtheme/') ?>site/img//f5d7a93e49fe403597399af84fe5146a.png" title="md_5b321ca97f12d.png" alt="md_5b321ca97f12d.png">
                                    </a>
                                </div>
                                <div id="df489ebc-84e7-4658-85eb-86216100c4ee" class="col-lg-2">
                                    <a href="https://www.facebook.com/ebuco.maxilo" target="_blank">
                                        <img class="cs-chosen-image" src="<?= base_url('newtheme/') ?>site/img//bd61e20585d542aea2e779b8705833fc.png" title="Facebook" alt="Icon Facebook">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer top section end -->

        <!-- Footer section -->
        <footer class="footer-section">
            <div class="container">

                <div class="copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;
                    <script>document.write(new Date().getFullYear());</script> Todos os direitos reservados - Tecnologia da informação em medicina e odontologia
                </div>
            </div>
        </footer>
        <!-- Footer top section end -->
        <!--====== Javascripts & Jquery ======-->
        <script src="<?= base_url('newtheme/') ?>site/js/jquery-3.2.1.min.js"></script>
        <script src="<?= base_url('newtheme/') ?>site/js/bootstrap.min.js"></script>
        <script src="<?= base_url('newtheme/') ?>site/js/owl.carousel.min.js"></script>
        <script src="<?= base_url('newtheme/') ?>site/js/circle-progress.min.js"></script>
        <script src="<?= base_url('newtheme/') ?>site/js/main.js"></script>

    </body>

</html>
