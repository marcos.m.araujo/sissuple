<html lang="pt-BR">
    <head>
        <title>eBuco - HOME</title>
        <link rel="icon" type="image/png" href="<?= base_url('newtheme/') ?>login/images/icons/imgEbuco.ico"/>
        <link type="text/css" rel="stylesheet" href="<?= base_url('newtheme/') ?>site/home.css">
        <link type="text/css" rel="stylesheet" href="<?= base_url('newtheme/') ?>site/criadordesites.css">
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/font-awesome.min.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/flaticon.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/owl.carousel.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/style.css" />
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>site/css/animate.css" />
    </head>
    <body>

        <section class="contact-section spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="termoServico" style="text-align: justify">
                            <?php if (!empty($termo[0]['termo'])) { ?>
                                <?= $termo[0]['termo'] ?>
                            <?php } ?>
                        </div>
                    </div>



                </div>
            </div>
        </section>

        <section class="footer-top-section set-bg" data-setbg="<?= base_url('newtheme/') ?>site/img/footer-bg.jpg">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-widget">
                            <div class="row">

                                <div id="f2148d14-b6f3-4044-b102-9cc18eecc763" class="col-lg-2">
                                    <a href="https://www.instagram.com/ebuco_brasil/?hl=pt-br" target="_blank">
                                        <img class="cs-chosen-image" src="<?= base_url('newtheme/') ?>site/img//54f9cfc519a74eb2a86fddee6372a3cc.png" title="instagram-png-instagram-png-logo-1455.png" alt="instagram-png-instagram-png-logo-1455.png">
                                    </a>
                                </div>
                                <div id="379fb6a7-76c1-4217-8b3e-b3d36333b1b6" class="col-lg-2">
                                    <a href="https://api.whatsapp.com/send?phone=5561981831130" target="_blank">
                                        <img class="cs-chosen-image" src="<?= base_url('newtheme/') ?>site/img//f5d7a93e49fe403597399af84fe5146a.png" title="md_5b321ca97f12d.png" alt="md_5b321ca97f12d.png">
                                    </a>
                                </div>
                                <div id="df489ebc-84e7-4658-85eb-86216100c4ee" class="col-lg-2">
                                    <a href="https://www.facebook.com/ebuco.maxilo" target="_blank">
                                        <img class="cs-chosen-image" src="<?= base_url('newtheme/') ?>site/img//bd61e20585d542aea2e779b8705833fc.png" title="Facebook" alt="Icon Facebook">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer top section end -->

        <!-- Footer section -->
        <footer class="footer-section">
            <div class="container">

                <div class="copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;
                    <script>document.write(new Date().getFullYear());</script> Todos os direitos reservados - Tecnologia da informação em medicina e odontologia
                </div>
            </div>
        </footer>
        <!-- Footer top section end -->
        <!--====== Javascripts & Jquery ======-->
        <script src="<?= base_url('newtheme/') ?>site/js/jquery-3.2.1.min.js"></script>
        <script src="<?= base_url('newtheme/') ?>site/js/bootstrap.min.js"></script>
        <script src="<?= base_url('newtheme/') ?>site/js/owl.carousel.min.js"></script>
        <script src="<?= base_url('newtheme/') ?>site/js/circle-progress.min.js"></script>
        <script src="<?= base_url('newtheme/') ?>site/js/main.js"></script>

    </body>

</html>
