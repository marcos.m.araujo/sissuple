<?php $this->load->view('app_template/script_interno') ?>
<?php $this->load->view('comum/busca_cep_js') ?>
<div id="perfil" class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-4 col-sm-12">
                <h2>Perfil</h2>
            </div>
            <div class="col-md-8 col-sm-12 text-left hidden-xs">
                <div class="row">
                    <div class="col-md-9 text-left">
                        <h2>Clínicas</h2>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-success btn-sm btn-block btn-round" @click="isNovaClinica = !isNovaClinica">Adicionar clínica</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-4">
            <form id="basic-form" method="post" v-on:submit.prevent="confirmAlteracaoDeSenha">
                <div class="card">
                    <div class="form-group">
                        <label>Nome</label>
                        <input required v-model="meusDados.nomeCirurgiao" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Sobrenome</label>
                        <input required v-model="meusDados.sobrenomeCirurgiao" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>E-mail</label>
                        <input required v-model="meusDados.email" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>CPF</label>
                        <input required v-model="meusDados.cpf" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Sexo</label>
                        <select required v-model="meusDados.sexo" class="form-control">
                            <option value="M">M</option>
                            <option value="F">F</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Telefone</label>
                        <input required v-model="meusDados.telefoneCirurgiao" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Estado</label>
                        <select required v-model="meusDados.codigoEstado" class="form-control">
                            <option value="1">Acre</option>
                            <option value="2"> Alagoas</option>
                            <option value="3">Amapá</option>
                            <option value="4"> Amazonas</option>
                            <option value="5">Bahia</option>
                            <option value="6"> Ceará</option>
                            <option value="7"> Distrito Federal</option>
                            <option value="8"> Espírito Santo</option>
                            <option value="9"> Goiás</option>
                            <option value="10"> Maranhão</option>
                            <option value="11"> Mato Grosso</option>
                            <option value="12"> Mato Grosso do Sul</option>
                            <option value="13"> Minas Gerais</option>
                            <option value="14"> Pará</option>
                            <option value="15"> Paraíba</option>
                            <option value="16"> Paraná</option>
                            <option value="17"> Pernambuco</option>
                            <option value="18"> Piauí</option>
                            <option value="19"> Rio de Janeiro</option>
                            <option value="20"> Rio Grande do Norte</option>
                            <option value="21"> Rio Grande do Sul</option>
                            <option value="22"> Rondônia</option>
                            <option value="23"> Roraima</option>
                            <option value="24"> Santa Catarina</option>
                            <option value="25"> São Paulo</option>
                            <option value="26"> Sergipe</option>
                            <option value="27"> Tocantins</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Conselho Profissional</label>
                        <select required v-model="meusDados.codigoConselhoProfissional" class="form-control">
                            <option value="1"> CRM Conselho Regional de Medicina</option>
                            <option value="2"> CRO Conselho Regional de Odontologia</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Número Conselho Profissional</label>
                        <input required v-model="meusDados.numeroConselhoProfissional" type="text" class="form-control">
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Alterar senha</label>
                        <input type="password" v-model="senha" class="form-control">
                        <div class="invalid-feedback">
                            Looks good!
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Cofirmar senha</label>
                        <input type="password" class="form-control" :class="(isSenhaValid)? '' : 'is-invalid'" v-model="confirmeSenha" :disabled="isDisabled" :required="isRequired">
                        <div class="invalid-feedback">
                            Senhas não conferem.
                        </div>
                    </div>

                    <button class="btn btn-info btn-block btn-round" type="submit">Atualizar dados</button>
                </div>
            </form>
        </div>
        <div class="col-md-8">
            <form @submit.prevent="createClinica" v-if="isNovaClinica">
                <div class="card">
                    <div class="form-group">
                        <label>Nome da Clínica:</label>
                        <input required v-model="clinica.nomeClinica" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>CNPJ</label>
                        <input required v-model="clinica.cnpj" @keyup="formataCnpj($event.target.value)" type="text" class="form-control" :class="{'is-invalid': !isCnpjValid}" maxlength="18">
                        <div class="invalid-feedback" v-if="!isCnpjValid">CNPJ informado é inválido.</div>
                    </div>
                    <div class="form-group">
                        <label>E-mail:</label>
                        <input required v-model="clinica.email" type="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Telefone:</label>
                        <input required v-model="clinica.telefoneComercial" @keyup="formataTelefone($event.target.value)" type="text" class="form-control" maxlength="15">
                    </div>
                    <div class="form-group">
                        <label>CEP:</label>
                        <input required v-model="clinica.cep" type="text" class="form-control" @keyup="buscaCep($event.target.value); formataCep($event.target.value);" maxlength="9">
                        <span class="text-warning" v-if="isSearchingCep"><i class="fas fa-spinner fa-pulse"></i> Buscando CEP...<span>
                    </div>
                    <div class="form-group">
                        <label>Endereço:</label>
                        <input required v-model="clinica.endereco" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Bairro:</label>
                        <input required v-model="clinica.bairro" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Complemento:</label>
                        <input v-model="clinica.complemento" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>UF:</label>
                        <select required v-model="clinica.uf" class="form-control">
                            <option value="1">AC</option>
                            <option value="2">AL</option>
                            <option value="3">AP</option>
                            <option value="4">AM</option>
                            <option value="5">BA</option>
                            <option value="6">CE</option>
                            <option value="7">DF</option>
                            <option value="8">ES</option>
                            <option value="9">GO</option>
                            <option value="10">MA</option>
                            <option value="11">MT</option>
                            <option value="12">MS</option>
                            <option value="13">MG</option>
                            <option value="14">PA</option>
                            <option value="15">PB</option>
                            <option value="16">PR</option>
                            <option value="17">PE</option>
                            <option value="18">PI</option>
                            <option value="19">RJ</option>
                            <option value="20">RN</option>
                            <option value="21">RS</option>
                            <option value="22">RO</option>
                            <option value="23">RR</option>
                            <option value="24">SC</option>
                            <option value="25">SP</option>
                            <option value="26">SE</option>
                            <option value="27">TO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Município:</label>
                        <input required v-model="clinica.municipio" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="customFile">Logo:</label>
                        <div class="custom-file">
                            <input id="fileLogo" v-model="documento" type="file" class="custom-file-input" ref="logoClinica" accept=".jpg, .png">
                            <label class="custom-file-label" for="customFile">{{documento}}</label>
                        </div>
                    </div>
                    <button class="btn btn-success btn-block btn-round" type="submit">Salvar</button>
                </div>
            </form>
            <div class="body">
                <table class="table table-sm display compact" style="width: 100%;" id="tblMinhasClinicas">
                    <thead>
                        <th>Clínica</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Ações</th>
                    </thead>
                    <tbody>
                        <tr v-for="clinica in minhasClinicas">
                            <td>{{clinica.nomeClinica}}</td>
                            <td>{{clinica.email}}</td>
                            <td>{{mascaraTelefone(clinica.telefoneComercial)}}</td>
                            <td>
                                <button @click="favoritarClinica(clinica.codigoClinica)" type="button" class="btn btn-sm btn-default m-1" title="Favorita" data-toggle="tooltip" data-placement="top" data-original-title="Editar"><i class="fas fa-star" :class="{'text-warning':(clinicaFavorita.codigoClinica == clinica.codigoClinica)}"></i></button>
                                <button @click="editClinica(clinica)" type="button" class="btn btn-sm btn-default m-1" title="Editar Clínica" data-toggle="tooltip" data-placement="top" data-original-title="Editar"><i class="text-warning fas fa-hospital"></i></button>
                                <button @click="deleteClinica(clinica.codigoClinica)" type="button" class="btn btn-sm btn-default m-1" title="Remover Clínica" data-toggle="tooltip" data-placement="top" data-original-title="Remover"><i class="text-danger icon-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    var site_url = '<?= base_url() ?>';
    var perfil = new Vue({
        el: "#perfil",
        data() {
            return {
                isSearchingCep: false,
                isCnpjValid: true,
                isNovaClinica: false,
                senha: '',
                confirmeSenha: '',
                isRequired: false,
                isDisabled: true,
                isLoaded: false,
                isSenhaValid: true,
                meusDados: {},
                documento: '',
                clinica: {
                    codigoClinica: '',
                    nomeClinica: '',
                    cnpj: '',
                    email: '',
                    telefoneComercial: '',
                    cep: '',
                    endereco: '',
                    bairro: '',
                    complemento: '',
                    uf: '',
                    municipio: '',
                },
                minhasClinicas: [],
                clinicaFavorita: {
                    codigoClinicaFavorita: ''
                }
            }
        },
        watch: {
            'clinica.cnpj': function(val) {
                if (val.replace(/\D/g, '').length == 14) {
                    this.isCnpjValid = validarCNPJ(val);
                    this.clinica.cnpj = (this.isCnpjValid) ? this.clinica.cnpj : '';
                }
            },
            isNovaClinica: function(val) {
                if (!val) {
                    this.clinica = Object.assign({}, this._clinica);
                }
            },
            senha: function(val) {
                if (val.length != 0 && !this.isLoaded) {
                    this.senha = '',
                        this.isLoaded = true;
                } else if (val.length != 0) {
                    this.isRequired = true,
                        this.isDisabled = false
                } else {
                    this.isRequired = false;
                    this.isDisabled = true;
                    this.confirmeSenha = '';
                }
            },
            confirmeSenha: function(val) {
                if (this.senha !== this.confirmeSenha) {
                    this.isSenhaValid = false;
                } else {
                    this.isSenhaValid = true;
                }
            }
        },
        created() {
            if (!this.hasOwnProperty('_clinica')) {
                this._clinica = Object.assign({}, this.clinica);
            }
        },
        async mounted() {
            await this.get()
            await this.getClinicas();
        },
        methods: {
            async favoritarClinica(clinica, isNewClinicaFavorita = false) {
                await axios.post(site_url + "Cirurgiao/Clinicas/saveClinicaFavorita", $.param({
                        codigoClinica: clinica,
                        isNewClinicaFavorita: isNewClinicaFavorita
                    }))
                    .then(async response => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Salvo'
                        });
                        await this.getClinicas();
                    })
            },
            editClinica(clinica) {
                var thisClinica = Object.assign({}, clinica);
                delete thisClinica.codigoCirurgiao;
                this.clinica = thisClinica;
                this.isNovaClinica = true;
            },
            async deleteClinica(codigoClinica) {
                await axios.post(site_url + "Cirurgiao/Clinicas/deleteClinicas", $.param({
                        codigoClinica: codigoClinica
                    }))
                    .then(async response => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Salvo',
                            text: 'Clinica removida com sucesso'
                        });

                        await this.getClinicas();
                    })
                    .catch(error => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Erro',
                            text: 'Não foi possível remover a clínica.'
                        });
                    });
            },
            async getClinicas() {
                this.minhasClinicas = [];
                vmGlobal.montaDatatable('#tblMinhasClinicas');
                var data = await axios.post(site_url + "Cirurgiao/Clinicas/getClinicas")
                    .then(response => {
                        return response.data;
                    });
                this.minhasClinicas = data.clinicas;
                this.clinicaFavorita = data.clinicaFavorita
                vmGlobal.montaDatatable('#tblMinhasClinicas');
            },
            async buscaCep(cep) {
                cep = cep.replace(/\D/g, '');

                var validacep = /^[0-9]{8}$/;

                if (validacep.test(cep)) {
                    this.isSearchingCep = true;

                    await axios.get("https://viacep.com.br/ws/" + cep + "/json/unicode")
                        .then(response => {
                            this.isSearchingCep = false;

                            if (response.data.hasOwnProperty('erro')) {
                                Swal.fire({
                                    icon: 'error',
                                    text: 'Cep nao encontrado',
                                });

                                return;
                            }

                            this.clinica.endereco = response.data.logradouro;
                            this.clinica.bairro = response.data.bairro;
                            this.clinica.municipio = response.data.localidade;
                            this.clinica.complemento = response.data.complemento;
                            this.clinica.uf = swap(ufs)[response.data.uf];
                            this.clinica.cep = response.data.cep;
                        });
                }
            },
            formataTelefone(telefone) {
                this.clinica.telefoneComercial = this.mascaraTelefone(telefone);
            },
            formataCep(cep) {
                this.clinica.cep = this.marcaraCep(cep);
            },
            formataCnpj(cnpj) {
                this.clinica.cnpj = this.mascaraCnpj(cnpj);
            },
            marcaraCep(cep) {
                return cep.replace(/\D/g, '').replace(/^(\d{5})?(\d{3})?/, "$1-$2");
            },
            mascaraCnpj(cnpj) {
                return cnpj.replace(/\D/g, '').replace(/^(\d{2})(\d{3})?(\d{3})?(\d{4})?(\d{2})?/, "$1.$2.$3/$4-$5");
            },
            mascaraTelefone(telefone) {
                telefone = telefone.replace(/\D/g, ""); //Remove tudo o que não é dígito
                telefone = telefone.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
                telefone = telefone.replace(/(\d)(\d{4})$/, "$1-$2"); //Coloca hífen entre o quarto e o quinto dígitos
                return telefone;
            },
            async createClinica() {
                var url = (this.clinica.codigoClinica.length == 0) ? 'createClinicas' : 'updateClinica';
                if (this.clinica.codigoClinica.length == 0) {
                    delete this.clinica.codigoClinica
                }

                var formData = new FormData();

                $.each(this.clinica, function(key, val) {
                    if (key == 'logo' && (val !== null && val.length != 0)) {
                        formData.append('logoAnterior', val);
                    }

                    formData.append(key, val);
                });

                if (this.$refs.logoClinica.files.length != 0) {
                    formData.append('logo', this.$refs.logoClinica.files[0]);
                }

                await axios.post(site_url + "Cirurgiao/Clinicas/" + url, formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                    .then(async response => {
                        Swal.fire({
                            icon: 'success',
                            title: 'Salvo',
                            text: response.data.message
                        });

                        await this.getClinicas();

                        if (this.clinicaFavorita.codigoClinica.length == 0) {
                            await this.favoritarClinica(response.data.clinica_id, true);
                        }
                    })
                    .catch(error => {
                        Swal.fire({
                            icon: 'error',
                            title: 'Erro',
                            text: error.response.data.message
                        });
                    });

                this.clinica = Object.assign({}, this._clinica);
                this.isNovaClinica = false;
                this.isSearchingCep = false;
                this.isCnpjValid = true;
                this.isNovaClinica = false;
                this.documento = '';
            },
            async get() {
                var param = {
                    table: 'Cirurgioes',
                    where: {
                        codigoCirurgiao: '<?= $this->session->codigoCirurgiao ?>'
                    }
                }
                var dados = await vmGlobal.getFromAPI(param)
                this.meusDados = dados[0]
            },
            async update() {
                var param = {
                    table: 'Cirurgioes',
                    data: this.meusDados,
                    where: {
                        codigoCirurgiao: '<?= $this->session->codigoCirurgiao ?>'
                    }
                }
                await vmGlobal.updateFromAPI(param)
                window.location.reload();
            },
            confirmAlteracaoDeSenha() {
                if (this.senha.length !== 0 && this.isSenhaValid) {
                    Swal.fire({
                        title: 'Alterar a senha?',
                        text: "Realmente deseja alterar a senha?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Alterar'
                    }).then((result) => {
                        if (result.value) {
                            this.updateSenha();
                            this.update()
                        }
                    });
                } else {
                    this.update()
                }
            },
            async updateSenha() {
                try {
                    await axios.post(site_url + 'Login/alterarSenha', $.param({
                            senha: this.senha
                        }))
                        .then(response => {
                            Swal.fire({
                                icon: 'success',
                                text: 'Senha alterada com sucesso!',
                            })
                        });
                } catch (error) {
                    Swal.fire({
                        icon: 'error',
                        text: 'Não foi possível alterar a senha!',
                    })
                }
            }
        }
    })
</script>