<script src="<?= base_url('newtheme/ckeditor/ckeditor.js') ?>"></script>
<script>
    baseurl = '<?= base_url(); ?>';
    var documentos = new Vue({
        el: "#documentos",
        data() {
            return {
                classificacaoDocumento: '',
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                listClassificacaoDocumento: [],
                tipoDocumento: '',
                listTipoDocumentos: [],
                documento: '',
                listDocumentos: [],
                observacoesSigilo: {
                    privadoHospital: 0,
                    privadoPaciente: 0,
                    privadoPlano: 0,
                    privadoFornecedor: 1
                },
                descricaoTextoLivre: ''
            }
        },
        watch: {
            classificacaoDocumento() {
                this.getClassificacaoDocumentoTipoDocumento()
            },
        },
        mounted() {
            CKEDITOR.replace('observacoesFinais', {
                htmlEncodeOutput: true,
                removeButtons: 'Image,Source',
                extraPlugins: 'base64image',
                width: '100%',
                height: 500
            });
        },
        methods: {
            async getDocumentos() {
                await axios.post('<?= base_url('EditorLaudo/Editor/getDocumentos') ?>').then((resp) => {
                    this.listDocumentos = resp.data
                }).catch((e) => {}).finally(() => {

                })
            },
            salvarDocumentos() {
                var doc = document.getElementById("FileArquivo").files[0];
                if (doc != '') {
                    var formData = new FormData();
                    formData.append('Arquivo', doc);
                    formData.append('classificacaoDocumento', this.classificacaoDocumento);
                    formData.append('tipoDocumento', this.tipoDocumento);
                    $.ajax({
                        url: '<?= base_url('EditorLaudo/Editor/salvarDocumentos') ?>',
                        method: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            documentos.listTipoDocumentos = []
                            documentos.classificacaoDocumento = ''
                            documentos.tipoDocumento = ''
                            documentos.documento = ''
                            documentos.getDocumentos()

                        },
                        error: function(error) {
                            console.log(error);
                        }
                    })
                }
            },
            async getClassificacaoDocumento() {
                await axios.post('<?= base_url('EditorLaudo/Editor/getClassificacaoDocumento') ?>').then((resp) => {
                    this.listClassificacaoDocumento = resp.data
                }).catch((e) => {}).finally(() => {

                })
            },
            getClassificacaoDocumentoTipoDocumento() {
                var formData = new FormData();
                formData.append("classificacao", this.classificacaoDocumento);
                axios.post('<?= base_url('EditorLaudo/Editor/getClassificacaoDocumentoTipoDocumento') ?>', formData).then((resp) => {
                    this.listTipoDocumentos = resp.data
                }).catch((e) => {}).finally(() => {

                })
            },
            alterarDocumento(codigo, tipo) {
                var formData = new FormData();
                formData.append("codigoLaudoElaboracaoExame", codigo);
                formData.append("tipoAlteracao", tipo);
                axios.post('<?= base_url('EditorLaudo/Editor/alterarDocumento') ?>', formData).then((resp) => {
                    this.getDocumentos()
                }).catch((e) => {}).finally(() => {

                })
            },
            deletarLaudo() {
                Swal.fire({
                    title: 'Deletar?',
                    text: "Será excluído tudo referente ao laudo!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não',
                }).then((result) => {
                    if (result.value) {
                        window.location = '<?= base_url("HomeCirurgiao/Home") ?>';
                    }
                })
            },
            async geneatePDF() {
                if (CKEDITOR.instances.observacoesFinais.getData().length != 0) {
                    var pdfContent = CKEDITOR.instances.observacoesFinais.getData();
                    var dados = {
                        codigoLaudoElaboracao: dadospaciente.codigoLaudoElaboracao,
                        descricaoTextoLivre: this.descricaoTextoLivre
                    };
                    dados = Object.assign(dados, this.observacoesSigilo);
                    try {
                        await axios.post(baseurl + 'EditorLaudo/Editor/generatePDF', $.param({
                                pdfContent: pdfContent,
                                dados: dados
                            }))
                            .then(response => {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Salvo!',
                                    text: 'Descrição da cirurgia salvo com sucesso.',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                this.listTipoDocumentos = []
                                this.classificacaoDocumento = ''
                                this.tipoDocumento = ''
                                this.documento = ''
                                this.getDocumentos()
                                CKEDITOR.instances.observacoesFinais.setData('')
                            });
                    } catch (error) {
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Oops...',
                            text: error.response.data.message,
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                }else{
                    this.listTipoDocumentos = []
                    this.classificacaoDocumento = '';
                    this.tipoDocumento = '';
                    this.documento = '';
                    CKEDITOR.instances.observacoesFinais.setData('')
                    this.getDocumentos();                    
                }
            },
            showDescricaoTextoLivre(doc){
                console.log(doc)
                if(doc.descricaoTextoLivre != null){
                    return doc.descricaoTextoLivre;
                }

                return doc.nome;
            }
        },
    })
</script>