<div id="documentos">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Documentos, exames e outros</h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 12" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
                <a href="javascript:void(0);" @click="sidemenu.tela = 14" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Anexar</h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div class="form-group">
                                <label>Classificação do documento</label>
                                <select required v-model="classificacaoDocumento" class="form-control">
                                    <option :value="i.cabeca" v-for="i in listClassificacaoDocumento">{{i.cabeca}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <div v-show="classificacaoDocumento != ''" class="form-group">
                                <label>Tipo Documento</label>
                                <select required v-model="tipoDocumento" class="form-control">
                                    <option :value="i.tipoDocumento" v-for="i in listTipoDocumentos">{{i.tipoDocumento}}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div v-show="tipoDocumento != '' && !tipoDocumento.toLowerCase().includes('texto livre')" class="form-group">
                                <label for="customFile">Arquivo</label>
                                <div required class="custom-file">
                                    <input id="FileArquivo" v-model="documento" type="file" class="custom-file-input">
                                    <label class="custom-file-label" for="customFile">{{documento}}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-12">
                            <div v-show="documento != ''" class="form-group">
                                <button @click='salvarDocumentos' style="margin-top: 32px;" type="button" class="btn btn-primary">Anexar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" v-show="tipoDocumento != '' && tipoDocumento.toLowerCase().includes('texto livre')">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Descrição da Cirurgia</h2>
                        </div>
                        <div class="body">
                            <div class="row mb-2">
                                <div class="col-md-6">
                                    <div class="project-actions">
                                        Sigiloso para :
                                        <a target="_blank" href="javascript:void(0)" class="btn btn-default btn-sm" v-for="(val,name) in observacoesSigilo" @click="observacoesSigilo[name] = (val == 0)? 1 : 0">
                                            <i :style="{color: (observacoesSigilo[name] == 0)? 'green' : 'red'}" class="fas" :class="[(observacoesSigilo[name] == 0)? 'fa-lock-open' : 'fa-lock']"></i>
                                            {{name.replace('privado','')}}
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group row">
                                                <label for="" class="col-sm-3 col-form-label">Título do Documento: </label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control form-control-sm" v-model="descricaoTextoLivre">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <button @click='geneatePDF()' type="button" class="btn btn-primary">Salvar</button>
                                        </div>                                    
                                    </div>
                                </div>
                            </div>
                            <textarea id="observacoesFinais" cols="30" rows="10" class="form-control">

                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Documentos salvos</h2>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <table class="table table-striped projects">
                            <tbody>
                                <tr v-for="i in listDocumentos">
                                    <td>
                                        {{i.classificacaoDocumento}}
                                    </td>
                                    <td class="project-state">
                                        {{i.tipoDocumento}}
                                    </td>
                                    <td class="project-state">
                                        <a :href="i.arquivo" target="_blank"> {{showDescricaoTextoLivre(i)}}</a>
                                    </td>
                                    <td class="project-actions text-right">
                                        Sigiloso para :
                                        <a v-if="i.privadoHospital == 0" class="btn btn-default btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoHospital')">
                                            <i style="color: green" class="fas fa-lock-open"></i>
                                            Hospital
                                        </a>
                                        <a v-if="i.privadoHospital == 1" class="btn btn-default btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoHospital')">
                                            <i style="color: red" class="fas fa-lock"></i>
                                            Hospital
                                        </a>
                                        <a v-if="i.privadoPaciente == 0" class="btn btn-default btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoPaciente')">
                                            <i style="color: green" class="fas fa-lock-open"></i>
                                            Paciente
                                        </a>
                                        <a v-if="i.privadoPaciente == 1" class="btn btn-default btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoPaciente')">
                                            <i style="color: red" class="fas fa-lock"></i>
                                            Paciente
                                        </a>
                                        <a v-if="i.privadoPlano == 0" class="btn btn-default btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoPlano')">
                                            <i style="color: green" class="fas fa-lock-open"></i>
                                            Plano
                                        </a>
                                        <a v-if="i.privadoPlano == 1" class="btn btn-default btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoPlano')">
                                            <i style="color: red" class="fas fa-lock"></i>
                                            Plano
                                        </a>

                                        <a v-if="i.privadoFornecedor == 0" class="btn btn-default btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoFornecedor')">
                                            <i style="color: green" class="fas fa-lock-open"></i>
                                            Fornecedor
                                        </a>
                                        <a v-if="i.privadoFornecedor == 1" class="btn btn-default btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoFornecedor')">
                                            <i style="color: red" class="fas fa-lock"></i>
                                            Fornecedor
                                        </a>
                                        <a class="btn btn-danger btn-sm" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'deletar')">
                                            <i class="fas fa-trash">
                                            </i>
                                            Deletar
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- <div class="col-md-12">
    <button @click="finalizarLaudo" type="button" class="btn btn-success col-md-4"><i class="fa fa-check-circle"></i> <span>FINALIZAR LAUDO</span></button>
</div> -->
</div>


<?php $this->load->view('editorLaudo/documentos/script') ?>