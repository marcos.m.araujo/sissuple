<script>
    var intervencoes = new Vue({
        el: "#intervencoes",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                cadastroCirurgiao: <?= $this->session->codigoCirurgiao ?>,
                lista: [],
                cadastro: false,
                novo: '',
                intervencaoUsado: [],
                intervencaoNaoUsado: [],
            }
        },
        methods: {
            search(target) {
                this.intervencaoNaoUsado = this.lista.filter(el => {
                    return el.usado === null || el.codigoIntervencao != el.usado;
                });

                var value = target.value;
                value = value.toLowerCase();
                value = clearStr(value);
                if (value.length >= 3) {
                    this.intervencaoNaoUsado = this.intervencaoNaoUsado.filter(el => {
                        var busca = el.intervencao.toLowerCase();
                        busca = clearStr(busca);
                        return busca.includes(value);
                    });
                }
            },
            async get() {
                this.lista = [];
                // vmGlobal.montaDatatable('#tblIntervencoes');
                this.lista = await vmGlobal.getAxios(sidemenu.tela, tratamento.codigoTratamento);
                // vmGlobal.montaDatatable('#tblIntervencoes', [1, 'asc']);

                this.intervencaoNaoUsado = this.lista.filter(el => {
                    return el.usado === null || el.codigoIntervencao != el.usado;
                });

                this.intervencaoUsado = this.lista.filter(el => {
                    return el.usado !== null && el.codigoIntervencao == el.usado;
                });

            },

            async insert() {
                var param = {
                    table: 'Intervencao',
                    data: {
                        codigoTratamento: tratamento.codigoTratamento,
                        intervencao: this.novo,
                        cadastroCirurgiao: this.cadastroCirurgiao
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get();
            },

            async deletar(codigo) {
                var param = {
                    table: 'IntervencaoLaudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoIntervencao: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(param, null, false);
                await this.get()
            },
            async selecionar(codigo) {
                var param = {
                    table: 'IntervencaoLaudo',
                    data: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoIntervencao: codigo,
                        codigoTratamento: tratamento.codigoTratamento,
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get()
            }


        }
    })
</script>