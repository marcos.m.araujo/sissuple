<script>
    var cid = new Vue({
        el: "#cid",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                cadastroCirurgiao: <?= $this->session->codigoCirurgiao ?>,
                lista: [],
                cadastro: false,
                novo: '',
                isSelected: false,
                cidUsado: [],
                cidNaoUsado: [],
            }
        },
        methods: {
            search(target) {
                this.cidNaoUsado = this.lista.filter(el => {
                    return el.usado === null || el.codigoCID != el.usado;
                });

                var value = target.value;
                value = value.toLowerCase();
                value = clearStr(value);
                if (value.length >= 3) {
                    this.cidNaoUsado = this.cidNaoUsado.filter(el => {
                        var busca = el.codigoCIDComplemento + ' - ' + el.CID;
                        busca = busca.toLowerCase();
                        busca = clearStr(busca);
                        return busca.includes(value);
                    });
                }
            },
            async get() {
                this.lista = [];
                // vmGlobal.montaDatatable('#tblCID');
                this.lista = await vmGlobal.getAxios(sidemenu.tela, tratamento.codigoTratamento)
                // vmGlobal.montaDatatable('#tblCID', [1,'asc']);
                this.cidNaoUsado = this.lista.filter(el => {
                    return el.usado === null || el.codigoCID != el.usado
                });

                this.cidUsado = this.lista.filter(el => {
                    return el.usado !== null && el.codigoCID == el.usado
                });

            },
            async insert() {
                var param = {
                    table: 'CID',
                    data: {
                        codigoTratamento: tratamento.codigoTratamento,
                        CID: this.novo,
                        cadastroCirurgiao: this.cadastroCirurgiao
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get()
            },

            async deletar(codigo) {
                var param = {
                    table: 'CIDLaudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoCID: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(param, null, false);
                await this.get()
            },
            async selecionar(codigo) {
                var param = {
                    table: 'CIDLaudo',
                    data: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoCID: codigo,
                        codigoTratamento: tratamento.codigoTratamento,
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get()
            },
            checkIfUsado(cid, usado) {
                if (usado !== null && cid == usado) {
                    this.isSelected = true;
                    return true;
                }
                return false;
            }
        }
    })
</script>