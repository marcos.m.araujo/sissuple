<div id="sintomas">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Sinais, sintomas e/ou relatos </h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 3" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
                <a href="javascript:void(0);" @click="sidemenu.tela = 5" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="header">
                        <h4 class="text-info">O(a) paciente reclama de:</h4>
                    </div>
                </div>
            </div>
            <div class="row clearfix mb-3 mt-2">
                <div class="col-lg-10">
                    <div class="input-group" v-if="cadastro">
                        <input v-model="sintomas" type="text" class="form-control" placeholder="Digite aqui um novo item para cadastro" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button v-if="sintomas !=''" @click="insert" class="btn btn-outline-success" type="button">Adicionar</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 d-flex justify-content-center">
                    <a href="javascript:void(0);" @click="cadastro = true" class="btn btn-sm btn-success btn-round" title="">Novo</a>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-6">
                    <input type="text" class="form-control form-control-sm mb-2" placeholder="Digite para filtrar ..." @keyup="search($event.target)">
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-6">
                    <label for="">Selecione um sintoma:</label>
                    <div class="list-group">
                        <a v-for="i in sintomasNaoUsados" @click="selecionar(i.codigoSintoma)" href="javascript:void(0);" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action">
                            {{i.sintomas.toUpperCase()}}
                            <span class="ml-3"><i class="fas fa-chevron-right"></i></span>
                        </a>
                        <a v-if="sintomasNaoUsados.length == 0" href="javascript:void(0);" class="list-group-item d-flex justify-content-center align-items-center">
                            Nenhum sintoma encontrado.
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label for="">Sintomas selecionados:</label>
                    <div class="list-group">
                        <a v-for="i in sintomasUsados" href="javascript:void(0);" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" @click="deletar(i.codigoSintoma)">
                            <span class="mr-3"><i class="fas fa-chevron-left"></i></span>
                            {{i.sintomas.toUpperCase()}}
                        </a>
                        <a v-if="sintomasUsados.length == 0" href="javascript:void(0);" class="list-group-item d-flex justify-content-center align-items-center">
                            Nenhum sintoma selecionado.
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <div class="body">
                <div class="table-responsive">
                    <div class="header">
                        <h4 class="text-white">O(a) paciente reclama de:</h4>
                    </div>
                    <table id="tblSintomas" class="table   dataTable table-custom ">
                        <thead>
                            <tr>
                                <th>
                                    <div v-if="cadastro" class="input-group mb-3">
                                        <input v-model="sintomas" type="text" class="form-control" placeholder="Digite aqui um novo item para cadastro" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button v-if="sintomas !=''" @click="insert" class="btn btn-outline-success" type="button">Adicionar</button>
                                        </div>
                                    </div>
                                </th>
                                <th><a href="javascript:void(0);" @click="cadastro = true" class="btn btn-sm btn-success btn-round" title="">Novo</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in lista">
                                <td style="width: 90%;" class="text-white">{{i.sintomas.toUpperCase()}}</td>
                                <td v-if="checkIfUsado(i.sintomaUsado, i.codigoSintoma)"><a href="javascript:void(0);" @click="deletar(i.codigoSintoma)" class="btn btn-sm btn-success btn-round" title="">Selecionado</a></td>
                                <td v-else><a href="javascript:void(0);" @click="selecionar(i.codigoSintoma)" class="btn btn-sm btn-info btn-round" title="">Selecionar</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div> -->
<?php $this->load->view('editorLaudo/sintomas/script') ?>