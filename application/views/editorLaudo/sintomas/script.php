<script>
    var sintomas = new Vue({
        el: "#sintomas",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                cadastroCirurgiao: <?= $this->session->codigoCirurgiao ?>,
                lista: [],
                cadastro: false,
                sintomas: '',
                sintomasUsados: [],
                sintomasNaoUsados: []
            }
        },
        methods: {
            search(target) {
                this.sintomasNaoUsados = this.lista.filter(el => {
                    return el.sintomaUsado === null || el.codigoSintoma != el.sintomaUsado
                });

                var value = target.value;
                value = value.toLowerCase();
                value = clearStr(value);
                if (value.length >= 3) {
                    this.sintomasNaoUsados = this.sintomasNaoUsados.filter(el => {
                        var busca = el.sintomas.toLowerCase();
                        busca = clearStr(busca)
                        return busca.includes(value);
                    });
                }
            },
            async get() {
                this.lista = []

                this.lista = await vmGlobal.getAxios(sidemenu.tela, tratamento.codigoTratamento)

                this.sintomasNaoUsados = this.lista.filter(el => {
                    return el.sintomaUsado === null || el.codigoSintoma != el.sintomaUsado
                });

                this.sintomasUsados = this.lista.filter(el => {
                    return el.sintomaUsado !== null && el.codigoSintoma == el.sintomaUsado
                });
                // vmGlobal.montaDatatable('#tblSintomas', [1, 'asc']);
            },

            async insert() {
                var param = {
                    table: 'Sintomas',
                    data: {
                        codigoTratamento: tratamento.codigoTratamento,
                        sintomas: this.sintomas,
                        cadastroCirurgiao: this.cadastroCirurgiao
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get()
            },

            async deletar(codigoSintoma) {
                var param = {
                    table: 'SintomasLaudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoSintoma: codigoSintoma
                    }
                }
                await vmGlobal.deleteFromAPI(param, null, false);
                await this.get()
            },
            async selecionar(codigoSintoma) {
                var param = {
                    table: 'SintomasLaudo',
                    data: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoSintoma: codigoSintoma,
                        codigoTratamento: tratamento.codigoTratamento,
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false)
                await this.get()
            },
            checkIfUsado(sintoma, usado) {
                if (usado !== null && sintoma == usado) {
                    return true;
                }
                return false;
            }
        }
    })
</script>