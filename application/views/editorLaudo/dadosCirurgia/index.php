<div id="cirurgia">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Dados da cirurgia</h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 2" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
                <a href="javascript:void(0);" @click="update" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">

        <div class="col-md-2"></div>
        <div class="col-xl-8 col-lg-8 col-md-7">
            <div class="card">
                <div class="header">
                    <h2>Editar dados da cirurgia</h2>
                    <ul class="header-dropdown dropdown">
                        <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    </ul>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="form-group col-md-4">
                            <label class="text-info">Data prevista da cirurgia</label>
                            <input v-model="dadosHospital.dataCirurgia" name="dataCirurgia" type="date" class="form-control" required>
                        </div>

                        <div class="form-group col-lg-4 col-md-12">
                            <label class="text-info">Diárias de internação</label>
                            <select v-model="dadosHospital.diariasInternacao" class="form-control">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-12">
                            <label class="text-info">Diárias de UTI</label>
                            <select v-model="dadosHospital.diariasUTI" class="form-control">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-12">
                            <label class="text-info">Tempo de procedimento</label>
                            <select v-model="dadosHospital.tempoProcedimento" class="form-control" required>
                                <option value="1 hora">1 hora</option>
                                <option value="2 horas">2 horas</option>
                                <option value="3 horas">3 horas</option>
                                <option selected value="4 horas">4 horas</option>
                                <option value="5 horas">5 horas</option>
                                <option value="Acima de 5 horas">Acima de 5 horas</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-12">
                            <label class="text-info">Anestesia</label>
                            <select v-model="dadosHospital.tipoAnestesia" class="form-control" required>
                                <option value=""></option>
                                <option value="Sedação">Sedação</option>
                                <option selected value="Geral">Geral</option>
                                <option value="Raqui">Raqui</option>
                                <option value="Local">Local</option>
                                <option value="Piriodoral">Piriodoral</option>
                            </select>
                        </div>

                        <div class="form-group col-lg-4 col-md-12">
                            <label class="text-info">Equipe cirúrgica formada por</label>
                            <select id="equipes" v-model="dadosHospital.codigoEquipeCirurgica" class="form-control" required>
                                <option value="1">Cirurgião e 1º Aux.</option>
                                <option value="2">Cirurgião, 1º e 2º Aux.</option>
                                <option value="3">Cirurgião, 1º, 2º e 3º Aux.</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-12">
                            <label class="text-info">Honorário profissional</label>
                            <select v-model="dadosHospital.tipoHonorario" class="form-control" required>
                                <option></option>
                                <option selected value="Particular">Particular</option>
                                <option value="Pelo plano">Pelo plano</option>
                                <option value="Pelo hospital">Pelo hospital</option>
                            </select>
                        </div>
                        <div class="form-group col-lg-4 col-md-12">
                            <label class="text-info">UF local cirurgia <small v-if="estadoObrigadotio" class="text-danger">*Obrigatório</small></label>
                            <select @change="getHospital"  v-model="dadosHospital.estado" id="estado" class="form-control select2" style="width: 100%;" required>
                                <option value=""></option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AP">AP</option>
                                <option value="AM">AM</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="MG">MG</option>
                                <option value="PA">PR</option>
                                <option value="PB">PB</option>
                                <option value="PR">PR</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RS">RS</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="SC">SC</option>
                                <option value="SP">SP</option>
                                <option value="SE">SE</option>
                                <option value="TO">TO</option>
                            </select>
                        </div>

                        <div v-show="dadosHospital.estado!= ''" class="col-lg-12 col-md-12">
                            <label class="text-info">Hospital <small v-if="hospitalObrigatorio" class="text-danger">*Obrigatório</small></label>
                            <input @change="getCodigoHospital" v-model="dadosHospital.hospital" ref="hospital" list="browsers" class="form-control" name="browsers" placeholder="Selecione o hospital">
                            <datalist id="browsers">
                                <option @blur="setCodigoHospital(i.codigoHospital)" v-for="i in hospitais" :value="i.nome">
                            </datalist>
                        </div>
                        <div class="col-lg-12 col-md-12 text-right" style="padding: 10px">
                            <button @click="update" type="button" class="btn  btn-success">Atualizar dados da cirurgia</button>
                        </div>
                    

                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('editorLaudo/dadosCirurgia/script') ?>