<script>
    var cirurgia = new Vue({
        el: "#cirurgia",
        data() {
            return {
                dadosHospital: {},
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                hospitais: [],
                hospitalObrigatorio: false,
                estadoObrigadotio: false
            }
        },
        watch: {

        },
        async mounted() {},
        methods: {
            async getCirurgia() {
                dadospaciente.checkPaciente();
                var param = {
                    table: 'Cirurgia',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao
                    }
                }
                var dados = await vmGlobal.getFromAPI(param)
                if (dados != '')
                    this.dadosHospital = dados[0]


            },

            async update() {
                if (this.dadosHospital.estado != '' || this.dadosHospital.hospital != '') {
                    if (this.dadosHospital.codigoLaudoElaboracaoDadosCirurgia != undefined) {
                        var param = {
                            table: 'Cirurgia',
                            where: {
                                codigoLaudoElaboracaoDadosCirurgia: this.dadosHospital.codigoLaudoElaboracaoDadosCirurgia
                            },
                            data: {
                                diariasInternacao: this.dadosHospital.diariasInternacao,
                                diariasUTI: this.dadosHospital.diariasUTI,
                                tempoProcedimento: this.dadosHospital.tempoProcedimento,
                                tipoAnestesia: this.dadosHospital.tipoAnestesia,
                                codigoEquipeCirurgica: this.dadosHospital.codigoEquipeCirurgica,
                                tipoHonorario: this.dadosHospital.tipoHonorario,
                                estado: this.dadosHospital.estado,
                                hospital: this.dadosHospital.hospital,
                                codigoHospital: this.dadosHospital.codigoHospital,
                                dataCirurgia: this.dadosHospital.dataCirurgia
                            }
                        }
                        await vmGlobal.updateFromAPI(param)
                    } else {
                        var param = {
                            table: 'Cirurgia',
                            data: {
                                codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                                diariasInternacao: this.dadosHospital.diariasInternacao,
                                diariasUTI: this.dadosHospital.diariasUTI,
                                tempoProcedimento: this.dadosHospital.tempoProcedimento,
                                tipoAnestesia: this.dadosHospital.tipoAnestesia,
                                codigoEquipeCirurgica: this.dadosHospital.codigoEquipeCirurgica,
                                tipoHonorario: this.dadosHospital.tipoHonorario,
                                estado: this.dadosHospital.estado,
                                hospital: this.dadosHospital.hospital,

                            }
                        }
                        await vmGlobal.insertFromAPI(param)
                    }
                    await this.getCirurgia;
                    this.hospitalObrigatorio = false;
                    this.estadoObrigadotio = false;
                    sidemenu.tela = 3
                }else{
                    this.hospitalObrigatorio = true;
                    this.estadoObrigadotio = true;
                }
            },
            async getHospital() {
                this.dadosHospital.hospital = ''
                this.hospitais = []
                var param = {
                    table: 'Hospital',
                    where: {
                        uf: this.dadosHospital.estado
                    }
                }
                var dados = await vmGlobal.getFromAPI(param)
                if (dados != '')
                    this.hospitais = dados

            },

            async getCodigoHospital() {
                var param = {
                    table: 'Hospital',
                    where: {
                        nome: this.dadosHospital.hospital,
                        uf: this.dadosHospital.estado
                    }
                }
                var dados = await vmGlobal.getFromAPI(param)
                this.dadosHospital.codigoHospital = dados[0].codigoHospital
                await this.update();
            },



        }
    })
</script>