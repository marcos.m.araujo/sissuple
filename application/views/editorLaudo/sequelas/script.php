<script>
    var sequelas = new Vue({
        el: "#sequelas",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                cadastroCirurgiao: <?= $this->session->codigoCirurgiao ?>,
                lista: [],
                cadastro: false,
                novo: '',
                sequelasUsadas: [],
                sequelasNaoUsadas: []
            }
        },
        methods: {
            search(target) {
                this.sequelasNaoUsadas = this.lista.filter(el => {
                    return el.usado === null || el.codigoSequelas != el.usado
                });

                var value = target.value;
                value = value.toLowerCase();
                value = clearStr(value);

                if (value.length >= 3) {
                    this.sequelasNaoUsadas = this.sequelasNaoUsadas.filter(el => {
                        var busca = el.sequela.toLowerCase();
                        var busca = clearStr(busca);
                        return busca.includes(value);
                    });
                }
            },
            async get() {
                this.lista = [];
                this.lista = await vmGlobal.getAxios(sidemenu.tela, tratamento.codigoTratamento);

                this.sequelasNaoUsadas = this.lista.filter(el => {
                    return el.usado === null || el.codigoSequelas != el.usado
                });

                this.sequelasUsadas = this.lista.filter(el => {
                    return el.usado !== null && el.codigoSequelas == el.usado
                });

                // vmGlobal.montaDatatable('#tblSequelas', [1, 'asc']);
            },

            async insert() {
                var param = {
                    table: 'Sequelas',
                    data: {
                        codigoTratamento: tratamento.codigoTratamento,
                        sequela: this.novo,
                        cadastroCirurgiao: this.cadastroCirurgiao
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get()
            },

            async deletar(codigo) {
                var param = {
                    table: 'SequelasLaudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoSequela: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(param, null, false);
                await this.get();
            },
            async selecionar(codigo) {
                var param = {
                    table: 'SequelasLaudo',
                    data: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoSequela: codigo,
                        codigoTratamento: tratamento.codigoTratamento,
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get();
            }


        }
    })
</script>