<style>
    .load {
        width: 500px;
        z-index: 1000;
        background-color: #fff;
        border-radius: 10px;
        position: fixed;
        top: 30%;
        left: 35%;
        border: 2px solid #17A2B8;
        -webkit-box-shadow: 3px 6px 59px -8px rgba(255, 255, 255, 1);
        -moz-box-shadow: 3px 6px 59px -8px rgba(255, 255, 255, 1);
        box-shadow: 3px 6px 59px -8px rgba(255, 255, 255, 1);
        display: none;
    }
</style>
<img class="load" src="<?= base_url('newtheme/img/spinner3.gif') ?>">
<div id="compartilhamentos">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Compartilhamentos</h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 13" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2>Notificações</h2>
                </div>
                <div class="body">

                    <div class="col-md-12">
                        <ul class="list-group mb-3 tp-setting">
                            <li class="list-group-item">
                                Notificar o paciente dos andamentos do laudo
                                <div class="float-right">
                                    <label class="switch">
                                        <input type="checkbox" v-model="notificaPaciente">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>
                <div class="body">
                    <div class="col-md-12">
                        <ul class="list-group mb-3 tp-setting">
                            <li class="list-group-item">
                                Notificar o hospital dos andamentos do laudo
                                <div class="float-right">
                                    <label class="switch">
                                        <input v-model="notificaHospital" type="checkbox">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div v-show="notificaHospital" class="col-12 tagscard">
                        <input placeholder="Digite os e-mail's" ref="emailsHospital" type="text" class="form-control" data-role="tagsinput">
                    </div>
                    <div v-if="notificaHospital" class="col-12 text-right">
                        <br>
                        <button @click="setEmailHospital" type="button" class="btn btn-success" data-dismiss="modal">Adicionar E-mail's</button>
                    </div>

                    <table v-show="notificaHospital" class="table table-hover table-custom spacing5">
                        <thead>
                            <tr>
                                <th class="text-info" colspan="2">Contatos do hospital que serão notificados</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in emailsHospital ">
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ml-3">
                                            <a href="page-invoices-detail.html" title="">E-mail</a>
                                            <p class="mb-0">{{i.email}}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button @click="deletarContatoHospital(i.codigoHospitalContato)" type="button" class="btn btn-sm btn-default bg-danger" title="" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="icon-trash"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="body">
                    <div class="col-md-12">
                        <ul class="list-group mb-3 tp-setting">
                            <li class="list-group-item">
                                Notificar o plano de saúde sobre o andamentos do laudo
                                <div class="float-right">
                                    <label class="switch">
                                        <input type="checkbox" v-model="notificaPlano">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div v-show="notificaPlano" class="col-12 tagscard">
                        <input placeholder="Digite os e-mail's" ref="emails" type="text" class="form-control" data-role="tagsinput">
                    </div>
                    <div v-if="notificaPlano" class="col-12 text-right">
                        <br>
                        <button @click="setEmail" type="button" class="btn btn-success" data-dismiss="modal">Adicionar E-mail's</button>
                    </div>

                    <table v-if="notificaPlano" class="table table-hover table-custom spacing5">
                        <thead>
                            <tr>
                                <th class="text-info" colspan="2">Contatos do planos de saúde que serão notificados</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in emails">
                                <td>
                                    <div class="d-flex align-items-center">
                                        <div class="ml-3">
                                            <a href="page-invoices-detail.html" title="">E-mail</a>
                                            <p class="mb-0">{{i.email}}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <button @click="deletar(i.codigoPlanosSaudeContato)" type="button" class="btn btn-sm btn-default bg-danger" title="" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="icon-trash"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <button @click="finalizarLaudo" type="button" class="btn btn-success col-md-4"><i class="fa fa-check-circle"></i> <span>FINALIZAR LAUDO</span></button>
    </div>
</div>
<?php $this->load->view('editorLaudo/compartilhamentos/script') ?>