<script>
    const compartilhamentos = new Vue({
        el: '#compartilhamentos',
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                emails: [],
                notificaPaciente: false,
                notificaPlano: false,
                notificaHospital: false,
                emailsHospital: [],
                dadosHospital: {},
            }
        },
        watch: {
            notificaPlano() {
                this.getEmails()
                this.updateNotificacao()
            },
            notificaPaciente() {
                this.updateNotificacao()
            },
            notificaHospital() {
                this.updateNotificacaoHospital()
            }
        },
        mounted() {
            this.getDadosHospital();
            this.getNofificacoes();
            this.getEmails()
        },
        created() {
            this.dadosHospital = cirurgia.dadosHospital;
        },
        methods: {
            finalizarLaudo() {
                Swal.fire({
                    position: 'top-end',
                    icon: 'success',
                    title: 'Aguarde.....',
                    showConfirmButton: false,
                    timer: 2500
                })
                $(".load").css("display", 'block')
                axios.post('<?= base_url('EditorLaudo/Editor/finalizarLaudo') ?>').then((resp) => {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Salvo!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    window.location.href = "<?= base_url('HomeCirurgiao/home') ?> ";
                }).catch((e) => {}).finally(() => {     $(".load").css("display", 'none')})
            },
            deletarLaudo() {
                Swal.fire({
                    title: 'Deletar?',
                    text: "Será excluído tudo referente ao laudo!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não',
                }).then((result) => {
                    if (result.value) {
                        window.location = '<?= base_url("HomeCirurgiao/Home") ?>';
                    }
                })
            },
            async setEmail() {
                var emails = this.$refs.emails.value
                var arr = emails.split(',')
                for (var i = 0; i < arr.length; i++) {
                    var param = {
                        table: 'PlanoContato',
                        data: {
                            codigoPlano: dadospaciente.dadosPaciente.convenio,
                            email: arr[i]
                        }
                    };
                    await vmGlobal.insertFromAPI(param)
                }
                await this.getEmails()
            },
            async getEmails() {
                var param = {
                    table: 'PlanoContato',
                    where: {
                        CodigoPlano: dadospaciente.dadosPaciente.convenio
                    }
                }
                this.emails = await vmGlobal.getFromAPI(param)
            },
            async getNofificacoes() {
                var param = {
                    table: 'Laudo',
                    where: {
                        codigoLaudoElaboracao: dadospaciente.codigoLaudoElaboracao
                    }
                }
                var dados = await vmGlobal.getFromAPI(param)
                if (dados[0].notificarPaciente == 1)
                    this.notificaPaciente = true
                else
                    this.notificaPaciente = false
                if (dados[0].notificarPlano == 1)
                    this.notificaPlano = true
                else
                    this.notificaPlano = false
            },
            async updateNotificacao() {
                var param = {
                    table: 'Laudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao
                    },
                    data: {
                        notificarPaciente: this.notificaPaciente,
                        notificarPlano: this.notificaPlano,
                    },
                }
                await vmGlobal.updateFromAPI(param)
            },
            async deletar(valor) {
                var param = {
                    table: 'PlanoContato',
                    where: {
                        codigoPlanosSaudeContato: valor
                    }
                }
                vmGlobal.deleteFromAPI(param)
                await this.getEmails()
            },
            // notificação hospital
            async getDadosHospital() {
               var param = {
                   table: 'Cirurgia',
                   where: {
                       codigoLaudoElaboracao: this.codigoLaudoElaboracao
                   }
               }
               var dados = await vmGlobal.getFromAPI(param)
              this.dadosHospital = dados[0]      
              this.getEmailsHospital()
           },

           async getNofificacoesHospital() {
               var param = {
                   table: 'Laudo',
                   where: {
                       codigoLaudoElaboracao: this.codigoLaudoElaboracao
                   }
               }
               var dados = await vmGlobal.getFromAPI(param)
               if (dados[0].notificarHospital == 1) {
                   this.notificaHospital = true
                   await this.getEmailsHospital()
               } else {
                   this.notificaHospital = false
               }

           },
           async updateNotificacaoHospital() {
               var param = {
                   table: 'Laudo',
                   where: {
                       codigoLaudoElaboracao: this.codigoLaudoElaboracao
                   },
                   data: {
                       notificarHospital: this.notificaHospital,

                   },
               }
               await vmGlobal.updateFromAPI(param)
               await this.getEmailsHospital()
           },

           async setEmailHospital() {
               var emailsHospital = this.$refs.emailsHospital.value
               var arr = emailsHospital.split(',')
               console.log(arr)
               for (var i = 0; i < arr.length; i++) {
                   var param = {
                       table: 'HospitalContato',
                       data: {
                           codigoHospital: this.dadosHospital.codigoHospital,
                           email: arr[i]
                       }
                   };
                   await vmGlobal.insertFromAPI(param)
               }
               await this.getEmailsHospital()
           },
           async getEmailsHospital() {
               var param = {
                   table: 'HospitalContato',
                   where: {
                       codigoHospital: this.dadosHospital.codigoHospital
                   }
               }
               this.emailsHospital = await vmGlobal.getFromAPI(param)
           },
           async deletarContatoHospital(valor) {
               var param = {
                   table: 'HospitalContato',
                   where: {
                       codigoHospitalContato: valor
                   }
               }
               await vmGlobal.deleteFromAPI(param)
               await this.getEmailsHospital()
           },
        },
    });
</script>