<style>
    .metismenu li {
        border-bottom: 2px solid #F1F4F6;
        cursor: pointer;
    }

    .metismenu span {

        font-size: 9pt !important;
    }
</style>
<div class="navbar-brand">
    <a href="<?= base_url('HomeCirurgiao/Home') ?>">
        <img width="100" src="<?= base_url('newtheme/img/logo.png') ?>">

    </a>
    <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
</div>
<div id="sid" class="sidebar-scroll">
    <div class="user-account">
        <div class="user_div">
            <img src="" class="user-photo">
        </div>
        <div class="dropdown">
            <span>Bem vindo,</span>
            <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong><?= $this->session->username ?></strong></a>
            <ul style="right: -70px!important" class="dropdown-menu dropdown-menu-right account vivify flipInY">
                <li><a href="page-profile.html"><i class="icon-user"></i>Meu perfil</a></li>
                <li><a href="app-inbox.html"><i class="icon-envelope-open"></i>Home</a></li>
                <li class="divider"></li>
                <li><a href="page-login.html"><i class="icon-power"></i>Sair</a></li>
            </ul>
        </div>
    </div>
    <nav id="left-sidebar-nav" class="sidebar-nav ">
        <ul id="main-menu" class="metismenu">

            <li @click="tela = 1" :class="[tela == 1 ? 'nav-item active' : 'nav-item']"><a class="nav-link active show"><i class="icon-user"></i><span>Dados do paciente</span></a></li>
            <li @click="tela = 2" :class="[tela == 2 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-calendar"></i><span>Dados da cirurgia</span></a></li>
            <li @click="tela = 3" :class="[tela == 3 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Tratamentos</span></a></li>
            <li @click="tela = 4" :class="[tela == 4 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Sinais e sintomas</span></a></li>
            <li @click="tela = 5" :class="[tela == 5 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Diagnóstico</span></a></li>
            <li @click="tela = 6" :class="[tela == 6 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>C.I.D</span></a></li>
            <li @click="tela = 7" :class="[tela == 7 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Intervenções</span></a></li>
            <li @click="tela = 8" :class="[tela == 8 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Procedimentos</span></a></li>
            <li @click="tela = 9" :class="[tela == 9 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Objetivos</span></a></li>
            <li @click="tela = 10" :class="[tela == 10 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Sequelas</span></a></li>
            <li @click="tela = 11" :class="[tela == 11 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Marcas de material</span></a></li>
            <li @click="tela = 12" :class="[tela == 12 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Materiais</span></a></li>
            <li @click="tela = 13" :class="[tela == 13 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Documentos Complementares</span></a></li>
            <li @click="tela = 14" :class="[tela == 14 ? 'nav-item active' : 'nav-item']"><a class="nav-link"><i class="icon-check"></i><span>Compartilhamentos</span></a></li>
        </ul>
    </nav>
</div>

<script>
    var sidemenu = new Vue({
        el: "#sid",
        data() {
            return {
                tela: 1
            }
        },
        watch: {
            tela() {
                mudaTela()
            },
        },
        mounted() {

        },
    })
</script>