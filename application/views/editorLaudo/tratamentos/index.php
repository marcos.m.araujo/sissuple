<div id="tratamento">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Tratamentos </h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 2" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
                <a href="javascript:void(0);" @click="sidemenu.tela = 4" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>
    <div class="header">
            <h4 class="text-info">Escolha um tratamento por vez.</h4>
        </div>
    <div class="row clearfix">
        
        <div v-for="i in dadosTratamentos" class="col-lg-3 col-md-6 col-sm-12">
            <div class="card w_card3">
                <div style="height: 190px;" class="body">
                    <div class="text-center"><i class="text-warning fas fa-bullseye"></i>
                        <h5 class="m-t-20 mb-0">{{i.tratamento}}</h5>
                        <br>
                        <a href="javascript:void(0);" @click="selecionaTratamento(i.codigoTratamento)" :class="[i.codigoTratamento == codigoTratamento? 'btn-success': 'btn-info' ]" class="btn  btn-round">Selecionar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('editorLaudo/tratamentos/script') ?>