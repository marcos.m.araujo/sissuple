<script>
    var tratamento = new Vue({
        el: "#tratamento",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                dadosTratamentos:[],
                codigoTratamento: 0
            }
        },
        methods: {
            async getTratamento() {
                var param = {
                    table: 'Tratamento',  
                    orderBy: {tratamento: 'tratamento asc'}               
                }
                this.dadosTratamentos = await vmGlobal.getFromAPI(param)   
                this.getTratamentoSalvo()
            },
            async getTratamentoSalvo() {
                var param = {
                    table: 'SintomasLaudo',  
                    where:{
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao
                    }          
                }
                var tratamentosSalvo = await vmGlobal.getFromAPI(param)   
               this.codigoTratamento = tratamentosSalvo[0].codigoTratamento    
            },
            selecionaTratamento(param){
                this.codigoTratamento = param
                sidemenu.tela = 4
            }
        }
    })
</script>