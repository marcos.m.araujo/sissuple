<script>
    var objetivos = new Vue({
        el: "#objetivos",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                cadastroCirurgiao: <?= $this->session->codigoCirurgiao ?>,
                lista: [],
                cadastro: false,
                novo: '',
                objetivosNaoUsados: [],
                objetivosUsados: []
            }
        },
        methods: {
            search(target) {
                this.objetivosNaoUsados = this.lista.filter(el => {
                    return el.usado === null || el.codigoObjetivoTratamento != el.usado
                });

                var value = target.value;
                value = value.toLowerCase();
                value = clearStr(value);

                if (value.length >= 3) {
                    this.objetivosNaoUsados = this.objetivosNaoUsados.filter(el => {
                        var busca = el.objetivo.toLowerCase();
                        busca = clearStr(busca)
                        return busca.includes(value);
                    });
                }
            },
            async get() {
                this.lista = [];
                this.lista = await vmGlobal.getAxios(sidemenu.tela, tratamento.codigoTratamento);

                this.objetivosNaoUsados = this.lista.filter(el => {
                    return el.usado === null || el.codigoObjetivoTratamento != el.usado
                });

                this.objetivosUsados = this.lista.filter(el => {
                    return el.usado !== null && el.codigoObjetivoTratamento == el.usado
                });
                // vmGlobal.montaDatatable('#tblObjetivos', [1, 'asc']);
            },

            async insert() {
                var param = {
                    table: 'Objetivos',
                    data: {
                        codigoTratamento: tratamento.codigoTratamento,
                        objetivo: this.novo,
                        cadastroCirurgiao: this.cadastroCirurgiao
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false)
                await this.get()
            },

            async deletar(codigo) {
                var param = {
                    table: 'ObjetivosLaudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoObjetivo: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(param, null, false);
                await this.get()
            },
            async selecionar(codigo) {
                var param = {
                    table: 'ObjetivosLaudo',
                    data: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoObjetivo: codigo,
                        codigoTratamento: tratamento.codigoTratamento,
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get();
            }


        }
    })
</script>