<div id="objetivos">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Objetivos </h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 8" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
                <a href="javascript:void(0);" @click="sidemenu.tela = 10" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="header">
                        <h4 class="text-info">O tratamento proposto visa:</h4>
                    </div>
                </div>
            </div>
            <div class="row clearfix mb-3 mt-2">
                <div class="col-lg-10">
                    <div class="input-group" v-if="cadastro">
                        <input v-model="novo" type="text" class="form-control" placeholder="Digite aqui um novo item para cadastro" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button v-if="novo !=''" @click="insert" class="btn btn-outline-success" type="button">Adicionar</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 d-flex justify-content-center">
                    <a href="javascript:void(0);" @click="cadastro = true" class="btn btn-sm btn-success btn-round" title="">Novo</a>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-lg-6">
                    <input type="text" class="form-control form-control-sm mb-2" placeholder="Digite para filtrar ..." @keyup="search($event.target)">
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-6">
                    <label for="">Selecione um objetivo:</label>
                    <div class="list-group">
                        <a v-for="i in objetivosNaoUsados" @click="selecionar(i.codigoObjetivoTratamento)" href="javascript:void(0);" class=" list-group-item d-flex justify-content-between align-items-center list-group-item-action">
                            {{i.objetivo.toUpperCase()}}
                            <span class="ml-3"><i class="fas fa-chevron-right"></i></span>
                        </a>
                        <a v-if="objetivosNaoUsados.length == 0" href="javascript:void(0);" class=" list-group-item d-flex justify-content-center align-items-center">
                            Nenhum objetivo encontrado.
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label for="">Objetivos selecionados:</label>
                    <div class="list-group">
                        <a v-for="i in objetivosUsados" href="javascript:void(0);" class=" list-group-item d-flex justify-content-between align-items-center list-group-item-action" @click="deletar(i.codigoObjetivoTratamento)">
                            <span class="mr-3"><i class="fas fa-chevron-left"></i></span>
                            {{i.objetivo.toUpperCase()}}
                        </a>
                        <a v-if="objetivosUsados.length == 0" href="javascript:void(0);" class=" list-group-item d-flex justify-content-center align-items-center">
                            Nenhum objetivo selecionado.
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="row clearfix">
        <div class="col-lg-12">
            <div class="body">
                <div class="table-responsive">
                    <div class="header">
                        <h4 class="text-info">O tratamento proposto visa:</h4>
                    </div>
                    <table id="tblObjetivos" class="table   dataTable table-custom ">
                        <thead>
                            <tr>
                                <th>
                                    <div v-if="cadastro" class="input-group mb-3">
                                        <input v-model="novo" type="text" class="form-control" placeholder="Digite aqui um novo item para cadastro" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button v-if="novo !=''" @click="insert" class="btn btn-outline-success" type="button">Adicionar</button>
                                        </div>
                                    </div>
                                </th>
                                <th><a href="javascript:void(0);" @click="cadastro = true" class="btn btn-sm btn-success btn-round" title="">Novo</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in lista">
                                <td style="width: 90%;" class="text-info">{{i.objetivo.toUpperCase()}}</td>
                                <td v-if="i.codigoObjetivoTratamento == i.usado"><a href="javascript:void(0);" @click="deletar(i.codigoObjetivoTratamento)" class="btn btn-sm btn-success btn-round" title="">Selecionado</a></td>
                                <td v-else><a href="javascript:void(0);" @click="selecionar(i.codigoObjetivoTratamento)" class="btn btn-sm btn-info btn-round" title="">Selecionar</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> -->
</div>
<?php $this->load->view('editorLaudo/objetivos/script') ?>