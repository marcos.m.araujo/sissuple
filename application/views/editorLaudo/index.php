<style>
    .list-group-item{
        color: #ffff;
    }
    .list-group-item:hover{
        color: #3C89DA;
    }
    .list-group-item{
        color: black!important;
    }
</style>
<?php $this->load->view('app_template/script_interno') ?>
<div style="width: 99.5%;">
    <div id="telaPaciente" style="display: none;">
        <?php $this->load->view('editorLaudo/paciente/index') ?>
    </div>
    <div id="telaDadosCirurgia" style="display: none;">
        <?php $this->load->view('editorLaudo/dadosCirurgia/index') ?>
    </div>
    <div id="telaDadosTratamento" style="display: none;">
        <?php $this->load->view('editorLaudo/tratamentos/index') ?>
    </div>
    <div id="telaDadosSintomas" style="display: none;">
        <?php $this->load->view('editorLaudo/sintomas/index') ?>
    </div>
    <div id="telaDadosDiagnostico" style="display: none;">
        <?php $this->load->view('editorLaudo/diagnosticos/index') ?>
    </div>
    <div id="telaDadosCID" style="display: none;">
        <?php $this->load->view('editorLaudo/cid/index') ?>
    </div>
    <div id="telaDadosIntervencoes" style="display: none;">
        <?php $this->load->view('editorLaudo/intervencoes/index') ?>
    </div>
    <div id="telaDadosProcedimentos" style="display: none;">
        <?php $this->load->view('editorLaudo/procedimentos/index') ?>
    </div>
    <div id="telaDadosObjetivos" style="display: none;">
        <?php $this->load->view('editorLaudo/objetivos/index') ?>
    </div>
    <div id="telaDadosSequelas" style="display: none;">
        <?php $this->load->view('editorLaudo/sequelas/index') ?>
    </div>
    <div id="telaDadosMarcas" style="display: none;">
        <?php $this->load->view('editorLaudo/marcasEFornecedores/index') ?>
    </div>
    <div id="telaDadosMateriais" style="display: none;">
        <?php $this->load->view('editorLaudo/materiais/index') ?>
    </div>
    <div id="telaDadosDocumentos" style="display: none;">
        <?php $this->load->view('editorLaudo/documentos/index') ?>
    </div>
    <div id="telaDadosCompartilhamentos" style="display: none;">
        <?php $this->load->view('editorLaudo/compartilhamentos/index') ?>
    </div>

</div>
<script>
    const clearStr = function(str) {
        str = str.replace(/[àáâãäå]/g, "a");
        str = str.replace(/[eéèëê]/g, "e");
        str = str.replace(/[iíìïî]/g, "i");
        str = str.replace(/[oóòõöô]/g, "o");
        str = str.replace(/[uúùüû]/g, "u");
        str = str.replace(/[ç]/g, "c");

        return str;
    }

    $(document).ready(() => {
        mudaTela()
    })

    function previewPDF() {
        document.getElementById('pdfPrev').src = '<?= base_url('EditorLaudo/Laudo_Pedido_Autorizacao_PDF?codigoLaudoElaboracao=' . md5($this->session->codigoLaudoElaboracao)) ?>';
    }

    function deletarLaudo() {
        documentos.deletarLaudo()
    }

    function mudaTela() {
        if (tratamento.codigoTratamento == 0 && sidemenu.tela > 3) {
            sidemenu.tela == 4
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Selecione um tratamento para continuar! :)',

            })
            return false;
        }
        if (sidemenu.tela == 1) {
            $('#telaPaciente').css("display", "block")
            dadospaciente.getPaciente()
        } else {
            $('#telaPaciente').css("display", "none")
        }
        if (sidemenu.tela == 2) {
            cirurgia.getCirurgia()
            $('#telaDadosCirurgia').css("display", "block")
        } else {
            $('#telaDadosCirurgia').css("display", "none")
        }
        if (sidemenu.tela == 3) {
            tratamento.getTratamento()
            $('#telaDadosTratamento').css("display", "block")
        } else {
            $('#telaDadosTratamento').css("display", "none")
        }

        if (sidemenu.tela == 4) {
            sintomas.get()
            $('#telaDadosSintomas').css("display", "block")
        } else {
            $('#telaDadosSintomas').css("display", "none")
        }
        if (sidemenu.tela == 5) {
            diagnosticos.get()
            $('#telaDadosDiagnostico').css("display", "block")
        } else {
            $('#telaDadosDiagnostico').css("display", "none")
        }
        if (sidemenu.tela == 6) {
            cid.get()
            $('#telaDadosCID').css("display", "block")
        } else {
            $('#telaDadosCID').css("display", "none")
        }
        if (sidemenu.tela == 7) {
            intervencoes.get()
            $('#telaDadosIntervencoes').css("display", "block")
        } else {
            $('#telaDadosIntervencoes').css("display", "none")
        }
        if (sidemenu.tela == 8) {
            procedimentos.get()
            $('#telaDadosProcedimentos').css("display", "block")
        } else {
            $('#telaDadosProcedimentos').css("display", "none")
        }
        if (sidemenu.tela == 9) {
            objetivos.get()
            $('#telaDadosObjetivos').css("display", "block")
        } else {
            $('#telaDadosObjetivos').css("display", "none")
        }
        if (sidemenu.tela == 10) {
            sequelas.get()
            $('#telaDadosSequelas').css("display", "block")
        } else {
            $('#telaDadosSequelas').css("display", "none")
        }
        if (sidemenu.tela == 11) {
            $('#telaDadosMarcas').css("display", "block")
        } else {
            $('#telaDadosMarcas').css("display", "none")
        }
        if (sidemenu.tela == 12) {
            materiais.selecionaMateriais('')
            $('#telaDadosMateriais').css("display", "block")
        } else {
            $('#telaDadosMateriais').css("display", "none")
        }
        if (sidemenu.tela == 13) {
            documentos.getDocumentos()
            documentos.getClassificacaoDocumento()
            $('#telaDadosDocumentos').css("display", "block")
        } else {
            $('#telaDadosDocumentos').css("display", "none")
        }
        if (sidemenu.tela == 14) {
            compartilhamentos.getEmails();
            compartilhamentos.getDadosHospital()
            compartilhamentos.getNofificacoesHospital()
            $('#telaDadosCompartilhamentos').css("display", "block")
        } else {
            $('#telaDadosCompartilhamentos').css("display", "none")
        }


    }
</script>