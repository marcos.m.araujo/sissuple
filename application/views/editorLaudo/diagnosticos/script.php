<script>
    var diagnosticos = new Vue({
        el: "#diagnosticos",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                cadastroCirurgiao: <?= $this->session->codigoCirurgiao ?>,
                lista: [],
                cadastro: false,
                novo: '',
                diagnosticosUsados: [],
                diagnosticosNaoUsados: []
            }
        },
        methods: {
            search(target) {
                this.diagnosticosNaoUsados = this.lista.filter(el => {
                    return el.usado === null || el.codigoDiagnostico != el.usado;
                });

                var value = target.value;
                value = value.toLowerCase();
                value = clearStr(value);
                if (value.length >= 3) {
                    this.diagnosticosNaoUsados = this.diagnosticosNaoUsados.filter(el => {
                        var busca = el.diagnostico.toLowerCase();
                        busca = clearStr(busca);
                        console.log(busca);
                        return busca.includes(value);
                    });
                }
            },
            async get() {
                this.lista = []

                this.lista = await vmGlobal.getAxios(sidemenu.tela, tratamento.codigoTratamento)

                this.diagnosticosNaoUsados = this.lista.filter(el => {
                    return el.usado === null || el.codigoDiagnostico != el.usado;
                });

                this.diagnosticosUsados = this.lista.filter(el => {
                    return el.usado !== null && el.codigoDiagnostico == el.usado;
                });
                // vmGlobal.montaDatatable('#tblDiagnostico', [1, 'asc']);
            },

            async insert() {
                var param = {
                    table: 'Diagnostico',
                    data: {
                        codigoTratamento: tratamento.codigoTratamento,
                        diagnostico: this.novo,
                        cadastroCirurgiao: this.cadastroCirurgiao
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get()
            },

            async deletar(codigo) {
                var param = {
                    table: 'DiagnosticoLaudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoDiagnostico: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(param, null, false);
                await this.get()
            },
            async selecionar(codigo) {
                var param = {
                    table: 'DiagnosticoLaudo',
                    data: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoDiagnostico: codigo,
                        codigoTratamento: tratamento.codigoTratamento,
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false);
                await this.get()
            },
            checkIfUsado(diagnostico, usado) {
                if (usado !== null && diagnostico == usado) {
                    return true;
                }
                return false;
            }
        }
    })
</script>