<div id="procedimentos">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Procedimentos </h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 7" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
                <a href="javascript:void(0);" @click="sidemenu.tela = 9" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="header">
                        <h4 class="text-info">O tratamento será realizado por meio dos procedimentos de:</h4>
                    </div>
                </div>
            </div>
            <div class="row clearfix mt-2">
                <div class="col-lg-6">
                    <input type="text" class="form-control form-control-sm mb-2" placeholder="Digite para filtrar ..." @keyup="search($event.target)">
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-6">
                    <label for="">Selecione um procedimento:</label>
                    <div class="list-group">
                        <a v-for="i in procedimentoNaoUsado" @click="selecionar(i.codigoProcedimento)" href="javascript:void(0);" class=" list-group-item d-flex justify-content-between align-items-center list-group-item-action">
                            {{i.codigoProcedimentoComplemento}} - {{i.procedimento.toUpperCase()}}
                            <span class="ml-3"><i class="fas fa-chevron-right"></i></span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label for="">Procedimentos selecionados:</label>
                    <div class="list-group">
                        <a v-for="i in procedimentoUsado" href="javascript:void(0);" class=" list-group-item d-flex justify-content-between align-items-center list-group-item-action" @click="deletar(i.codigoProcedimento)">
                            <span class="mr-3"><i class="fas fa-chevron-left"></i></span>
                            {{i.codigoProcedimentoComplemento}} - {{i.procedimento.toUpperCase()}}
                        </a>
                        <a v-if="procedimentoUsado.length == 0" href="javascript:void(0);" class=" list-group-item d-flex justify-content-center align-items-center">
                            Nenhum procedimento selecionado.
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="row clearfix">
        <div class="col-lg-12">
            <div class="body">
                <div class="table-responsive">
                    <div class="header">
                        <h4 class="text-info">O tratamento será realizado por meio dos procedimentos de:</h4>
                    </div>
                    <table id="tblProcedimento" class="table   dataTable table-custom ">
                        <thead>
                            <tr>
                                <th>

                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in lista">
                                <td style="width: 90%;" class="text-info">{{i.codigoProcedimentoComplemento}} - {{i.procedimento.toUpperCase()}}</td>
                                <td v-if="i.codigoProcedimento == i.usado"><a href="javascript:void(0);" @click="deletar(i.codigoProcedimento)" class="btn btn-sm btn-success btn-round" title="">Selecionado</a></td>
                                <td v-else><a href="javascript:void(0);" @click="selecionar(i.codigoProcedimento)" class="btn btn-sm btn-info btn-round" title="">Selecionar</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div> -->
</div>
<?php $this->load->view('editorLaudo/procedimentos/script') ?>