<script>
    var procedimentos = new Vue({
        el: "#procedimentos",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                cadastroCirurgiao: <?= $this->session->codigoCirurgiao ?>,
                lista: [],
                cadastro: false,
                novo: '',
                procedimentoUsado: [],
                procedimentoNaoUsado: []
            }
        },
        methods: {
            search(target) {
                this.procedimentoNaoUsado = this.lista.filter(el => {
                    return el.usado === null || el.codigoProcedimento != el.usado
                });

                var value = target.value;
                value = value.toLowerCase();
                value = clearStr(value);
                if (value.length >= 3) {
                    this.procedimentoNaoUsado = this.procedimentoNaoUsado.filter(el => {
                        var busca = el.codigoProcedimentoComplemento + ' - ' + el.procedimento;
                        busca = busca.toLocaleLowerCase();
                        busca = clearStr(busca);
                        return busca.includes(value);
                    });
                }
            },
            async get() {
                this.lista = [];
                this.lista = await vmGlobal.getAxios(sidemenu.tela, tratamento.codigoTratamento);

                this.procedimentoNaoUsado = this.lista.filter(el => {
                    return el.usado === null || el.codigoProcedimento != el.usado
                });

                this.procedimentoUsado = this.lista.filter(el => {
                    return el.usado !== null && el.codigoProcedimento == el.usado
                });

                // vmGlobal.montaDatatable('#tblProcedimento', [1, 'asc']);
            },

            async insert() {
                var param = {
                    table: 'Procedimento',
                    data: {
                        codigoTratamento: tratamento.codigoTratamento,
                        procedimento: this.novo,
                        cadastroCirurgiao: this.cadastroCirurgiao
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false)
                await this.get()
            },

            async deletar(codigo) {
                var param = {
                    table: 'ProcedimentoLaudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoProcedimento: codigo
                    }
                }
                await vmGlobal.deleteFromAPI(param, null, false)
                await this.get()
            },
            async selecionar(codigo) {
                var param = {
                    table: 'ProcedimentoLaudo',
                    data: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao,
                        codigoProcedimento: codigo,
                        codigoTratamento: tratamento.codigoTratamento,
                    }
                }
                await vmGlobal.insertFromAPI(param, null, false)
                await this.get()
            }


        }
    })
</script>