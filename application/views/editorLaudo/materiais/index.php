<style>
    .load {
        width: 500px;
        z-index: 1000;
        background-color: #fff;
        border-radius: 10px;
        position: fixed;
        top: 30%;
        left: 35%;
        border: 2px solid #17A2B8;
        -webkit-box-shadow: 3px 6px 59px -8px rgba(255, 255, 255, 1);
        -moz-box-shadow: 3px 6px 59px -8px rgba(255, 255, 255, 1);
        box-shadow: 3px 6px 59px -8px rgba(255, 255, 255, 1);
        display: none;
    }



    .autocomplete {
        /*the container must be positioned relative:*/
        position: relative;
        display: inline-block;
        margin-left: 5px;
        font-size: 11pt;
    }

    input {
        border: 1px solid transparent;
        background-color: #fff;
        padding: 10px;
        font-size: 16px;
    }

    input[type=text] {
        background-color: #fff;
        width: 100%;
    }

    input[type=submit] {
        background-color: DodgerBlue;
        color: #fff;
    }

    .autocomplete-items {
        position: absolute;
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        /*position the autocomplete items to be the same width as the container:*/
        top: 100%;
        left: 0;
        right: 0;
    }

    .autocomplete-items div {
        padding: 10px;
        cursor: pointer;
        background-color: #fff;
        border-bottom: 1px solid #d4d4d4;
       
    }

    .autocomplete-items div:hover {
        /*when hovering an item:*/
        background-color: #e9e9e9;
    }

    .autocomplete-active {
        /*when navigating through the items using the arrow keys:*/
        background-color: DodgerBlue !important;
        color: #ffffff;
    }
</style>
<img class="load" src="<?= base_url('newtheme/img/spinner3.gif') ?>">
<div id="materiais">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Materiais</h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 11" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
                <a href="javascript:void(0);" @click="sidemenu.tela = 13" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>



    <div class="row clearfix">
        <div class="col-lg-3 col-md-6">
            <form autocomplete="off">
                <div class="autocomplete" style="width:300px;">
                    <input id="myInput" type="text" name="nomenclatura" placeholder="Busca por nomenclatura">
                </div>
                <input @click="buscaPorNomenclatura" value="Buscar" type="button">
            </form>
            <?php $this->load->view('editorLaudo/materiais/filtros') ?>
        </div>
        <div class="col-md-9">
            <div class="body">
                <ul class="nav nav-tabs3">
                    <li class="nav-item"><a class="nav-link show active bg-light" id="opmestab" data-toggle="tab" href="#Home-new2">Opmes</a></li>
                    <li class="nav-item"><a class="nav-link bg-light" data-toggle="tab" @click="getMateriaisSalvos" id="tabMateriaisSalvos" href="#Profile-new2">Opmes salvo</a></li>
                    <li v-show="telaOpmeSelecao" class="nav-item bg-light"><a class="nav-link" data-toggle="tab" id="selecaoOpme" href="#Profile-new3">Opme Selecionado</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane show active" id="Home-new2">
                        <h6 v-if="listaOPME.length > 1">{{listaOPME.length}} Opmes encontrados </h6>
                        <h6 v-else>{{listaOPME.length}} Opme encontrado </h6>
                        <div class="row clearfix">
                            <div v-for="i in listaOPME.slice(0, limite)" class="col-lg-4 col-md-6">
                                <div class="card">
                                    <img width="100" height="300" class="card-img-top" :src="enderecoURLImg(i.imagem)" alt="">
                                    <div class="body text-center">
                                        <h6 class="mt-3 mb-0">{{i.nomenclatura}}</h6>
                                        <div v-if="i.marca != ''"><span>Marca: {{i.marca}}</span></div>
                                        <div v-if="i.tipo != ''"><span>Tipo: {{i.tipo}}</span></div>
                                        <div v-if="i.sistema != ''"><span>Sistema: {{i.sistema}}</span></div>
                                        <div v-if="i.qtdFuros != '' && i.qtdFuros != 0"><span>Furos: {{i.qtdFuros}}</span></div>
                                        <div v-if="i.espessura != ''"><span>Espessura: {{i.espessura}}</span></div>
                                        <div v-if="i.diametro != ''"><span>Diâmetro: {{i.diametro}}</span></div>
                                        <div v-if="i.comprimento != ''"><span>Comprimento: {{i.comprimento}}</span></div>
                                        <div v-if="i.tamanho != ''"><span>Tamanho: {{i.tamanho}}</span></div>
                                        <div v-if="i.volume != ''"><span>Volume: {{i.volume}}</span></div>
                                        <div v-if="i.avancoRecuo != ''"><span>Avanço/Recuo:: {{i.avancoRecuo}}</span></div>
                                        <br>
                                        <button @click="getOpme(i.codigoOpme)" class="btn btn-success btn-sm">Selecionar</button>
                                    </div>
                                </div>
                            </div>
                            <button v-if="listaOPME.length > 50" style="margin-bottom: 100px;" @click="limitesImg" class="btn btn-outline-warning btn-block mb-10" type="button">Ver mais +50 imagens</button>
                        </div>
                    </div>
                    <div class="tab-pane" id="Profile-new2">
                        <h6>Meus materiais</h6>
                        <div class="row">
                            <div v-for="i in listMateriais" class="col-xl-4 col-lg-4 col-md-5">
                                <div class="card">
                                    <img width="100" height="300" class="card-img-top" :src="enderecoURLImg(i.imagem)" alt="Card image cap">
                                    <div class="body">
                                        <h4 class="card-title"> {{i.qtdMaterial}} - {{i.nomenclatura}}</h4>
                                        <div class="card-subtitle">
                                            <span v-if="i.marca != ''"><span>Marca: {{i.marca}}</span></span>
                                            <span v-if="i.tipo != ''"><span>Tipo: {{i.tipo}}</span></span>
                                            <span v-if="i.sistema != ''"><span>Sistema: {{i.sistema}}</span></span>
                                            <span v-if="i.qtdFuros != '' && i.qtdFuros != 0"><span>Furos: {{i.qtdFuros}}</span></span>
                                            <span v-if="i.espessura != ''"><span>Espessura: {{i.espessura}}</span></span>
                                            <span v-if="i.diametro != ''"><span>Diâmetro: {{i.diametro}}</span></span>
                                            <span v-if="i.comprimento != ''"><span>Comprimento: {{i.comprimento}}</span></span>
                                            <span v-if="i.tamanho != ''"><span>Tamanho: {{i.tamanho}}</span></span>
                                            <span v-if="i.volume != ''"><span>Volume: {{i.volume}}</span></span>
                                            <span v-if="i.avancoRecuo != ''"><span>Avanço/Recuo:: {{i.avancoRecuo}}</span></span>
                                        </div>
                                        <small class="text-muted">Aplicação: </small>
                                        <p class="card-text text-info"> {{i.aplicacao}}</p>
                                        <a href="javascript:void(0)" @click="deletarMaterial(i.codigoLaudoMaterial)" class="btn btn-danger">Excluir</a>
                                        <a href="javascript:void(0)" @click="editarMaterial(i.codigoLaudoMaterial)" class="btn btn-success">Editar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div v-show="telaOpmeSelecao" class="tab-pane" id="Profile-new3">
                        <h6>Escolha material</h6>
                        <div class="row">
                            <div class="col-xl-5 col-lg-5 col-md-5">
                                <div class="card">
                                    <div v-for="i in detalheMaterial" class="body">
                                        <small class="text-muted">Detalhe: </small>
                                        <a class="light-link" target="_blank" :href="enderecoURLImg(i.imagem)"><img class="card-img-top" :src="enderecoURLImg(i.imagem)" alt=""></a>
                                        <hr>
                                        <small class="text-muted">Especificações </small>
                                        <div v-if="i.marca != ''"><span>Marca: {{i.marca}}</span></div>
                                        <div v-if="i.tipo != ''"><span>Tipo: {{i.tipo}}</span></div>
                                        <div v-if="i.sistema != ''"><span>Sistema: {{i.sistema}}</span></div>
                                        <div v-if="i.qtdFuros != '' && i.qtdFuros != 0"><span>Furos: {{i.qtdFuros}}</span></div>
                                        <div v-if="i.espessura != ''"><span>Espessura: {{i.espessura}}</span></div>
                                        <div v-if="i.diametro != ''"><span>Diâmetro: {{i.diametro}}</span></div>
                                        <div v-if="i.comprimento != ''"><span>Comprimento: {{i.comprimento}}</span></div>
                                        <div v-if="i.tamanho != ''"><span>Tamanho: {{i.tamanho}}</span></div>
                                        <div v-if="i.volume != ''"><span>Volume: {{i.volume}}</span></div>
                                        <div v-if="i.avancoRecuo != ''"><span>Avanço/Recuo:: {{i.avancoRecuo}}</span></div>
                                        <hr>
                                        <small class="text-muted">Especificação do fabricante: </small>
                                        <p style="text-align: justify;">{{i.obs}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-7 col-md-5 ">
                                <div class="card ">
                                    <form v-on:submit.prevent="incluirMaterial">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="qtdMat" class="col-form-label">Quantidade</label>
                                                <input required v-model="materialEscolha.qtdMaterial" min="1" type="number" class="form-control" id="qtdMat">
                                            </div>
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Inserir as especificações do fabricante no laudo?</label>
                                                <select required v-model="materialEscolha.especificacaoFabricante" class="form-control">
                                                    <option selected value="Sim">Sim</option>
                                                    <option value="Não">Não</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Informe o local e qual a finalidade do material</label>
                                                <textarea required v-model="materialEscolha.aplicacao" maxlength="200" rows="5" class="form-control" id="message-text"></textarea>
                                                {{200 - materialEscolha.aplicacao.length}}
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" @click="closeSelecMaterial" class="btn btn-info">Fechar</button>
                                            <button type="submit" class="btn btn-success">Salvar</button>
                                        </div>
                                    </form>
                                    <table id="tblFinalidadeMaterial" class="table table-sm" style="width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>Aplicações anatômicas já utilizadas</th>
                                                <th>-</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{finalidades.length}}
                                            <tr v-if="finalidades.length > 0" v-for="fin in finalidades">
                                                <td>
                                                    {{fin.Finalidade.substring(0,30)}}<span v-if="fin.Finalidade.length > 30">...</span>
                                                </td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-default m-1" @click="copyFinalidade(fin)">
                                                        <i class="text-info far fa-copy"></i>
                                                    </a>|
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-default m-1" @click="eraseFinalidade(fin)">
                                                        <i class="text-danger fas fa-eraser"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" class="btn btn-sm btn-default m-1" @click="viewFinalidade(fin)">
                                                        <i class="text-primary far fa-eye"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modalEditMaterial" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form v-on:submit.prevent="updateMaterial">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title">Editar Material</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Quantidade</label>
                            <input required v-model="editMaterial.qtdMaterial" min="1" type="number" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">Inserir as especificações do fabricante no laudo?</label>
                            <select required v-model="editMaterial.especificacaoFabricante" class="form-control">
                                <option selected value="Sim">Sim</option>
                                <option value="Não">Não</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="col-form-label">Informe o local e qual a finalidade do material</label>
                            <textarea required v-model="editMaterial.aplicacao" maxlength="200" rows="5" class="form-control" id="message-text"></textarea>
                            {{200 - editMaterial.aplicacao.length}}
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" @click="cancelEdit()">Fechar</button>
                        <button type="submit" class="btn btn-success">Salvar mudanças</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal" id="modalFinalidade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title">Finalidade</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <textarea maxlength="200" rows="5" class="form-control" id="message-text" disabled v-model="finalidadeModal.Finalidade">
                    </textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-success" data-dismiss="modal" @click="copyFinalidade(finalidadeModal)">Selecionar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('editorLaudo/materiais/script') ?>