<script>
    var searchNomenclatura = [];
    var materiais = new Vue({
        el: '#materiais',
        data() {
            return {
                sistemaTotal: [],
                marcasTotal: [],
                qtdFurosTotal: [],
                espessuraTotal: [],
                diametroTotal: [],
                comprimentoTotal: [],
                tamanhoTotal: [],
                nomenclatura: [],
                volumeTotal: [],
                avancoRecuo: [],
                arrsistema: [],
                arrMarcas: [],
                arrqtdFuros: [],
                arrespessura: [],
                arrdiametro: [],
                arrcomprimento: [],
                arrtamanho: [],
                arrvolume: [],
                arrvavancoRecuo: [],
                arrvnomenclatura: [],
                categoriaTotal: [],
                materialTotal: [],
                tipoTotal: [],
                arrcategoriaTotal: [],
                arrmaterialTotal: [],
                arrtipoTotal: [],
                filtrosMaterias: {
                    nomenclatura: '',
                    marca: '',
                    sistema: '',
                    qtdFuros: '',
                    espessura: '',
                    diametro: '',
                    comprimento: '',
                    tamanho: '',
                    volume: '',
                    avancoRecuo: '',
                    categoria: '',
                    material: '',
                    tipo: '',
                },
                categoria: '',
                material: '',
                tipo: '',
                material: '',
                marca: '',
                sistema: '',
                furos: '',
                espessura: '',
                diametro: '',
                comprimento: '',
                tamanho: '',
                volume: '',
                avanco: '',
                nomencla: '',
                obs: '',
                enderecoOPME: '',
                listaOPME: null,
                telaOpmeSelecao: false,
                detalheMaterial: {},
                materialEscolha: {
                    aplicacao: '',
                    especificacaoFabricante: 'Sim',
                    codigoMaterial: '',
                    qtdMaterial: ''
                },
                editMaterial: {
                    aplicacao: '',
                    especificacaoFabricante: 'Sim',
                    codigoMaterial: '',
                    qtdMaterial: 1
                },
                listMateriais: [],
                limite: 50,
                finalidades: [],
                finalidadesSelected: [],
                finalidadeModal: {
                    Finalidade: '',
                },
                searchNomenclaturaModel: ''
            }
        },
        watch: {
            filtrosMaterias: {
                deep: true,
                handler() {
                    this.selecionaMateriais('filtro')
                }
            },
            telaOpmeSelecao(a) {
                if (a)
                    $("#selecaoOpme").click()
                else
                    $("#tabMateriaisSalvos").click()
            }
        },
        methods: {
            async getOpme(codigo) {
                this.telaOpmeSelecao = true
                var param = {
                    table: 'Opme',
                    where: {
                        codigoOpme: codigo
                    }
                }
                var dados = await vmGlobal.getFromAPI(param)
                this.detalheMaterial = dados

                if (!this.hasOwnProperty('_materialEscolha')) {
                    this._materialEscolha = Object.assign({}, this.materialEscolha);
                }

                this.finalidadesSelected = [];
                
                this.readRascunhosFinalidade(this.detalheMaterial[0].codigoOpmeTipo);
            },
            async selecionaMateriais(param) {
                $(".load").css("display", 'block')
                var formData = new FormData();
                var url;
                // if (param == 'filtro')
                //     url = '<?= base_url('EditorLaudo/MarcasEFornecedores/selecionaMateriaisFiltros') ?>'
                // else
                url = '<?= base_url('EditorLaudo/MarcasEFornecedores/selecionaMateriais') ?>'

                formData.append("filtros", JSON.stringify(this.filtrosMaterias));
                await axios.post(url, formData).then((resp) => {
                        this.listaOPME = resp.data
                        this.enderecoOPME = this.listaOPME[0].imagem
                        this.material = this.listaOPME[0].material
                        this.marca = this.listaOPME[0].marca
                        this.sistema = this.listaOPME[0].sistema
                        this.furos = this.listaOPME[0].qtdFuros
                        this.espessura = this.listaOPME[0].espessura
                        this.diametro = this.listaOPME[0].diametro
                        this.comprimento = this.listaOPME[0].comprimento
                        this.tamanho = this.listaOPME[0].tamanho
                        this.volume = this.listaOPME[0].volume
                        this.avancoRecuo = this.listaOPME[0].avancoRecuo
                        this.nomencla = this.listaOPME[0].nomenclatura
                        this.obs = this.listaOPME[0].obs
                        this.codigoOpme = this.listaOPME[0].codigoOpme

                    }).catch((e) => {})
                    .finally(() => {
                        this.sistemaTotal = [];
                        this.marcasTotal = [];
                        this.arrsistema = []
                        this.qtdFurosTotal = [];
                        this.arrqtdFuros = []
                        this.espessuraTotal = [];
                        this.arrespessura = []
                        this.diametroTotal = [];
                        this.arrdiametro = []
                        this.comprimentoTotal = [];
                        this.arrcomprimento = []
                        this.tamanhoTotal = [];
                        this.arrtamanho = []
                        this.volumeTotal = [];
                        this.arrvolume = []
                        this.avancoRecuo = [];
                        this.arrvavancoRecuo = []
                        this.nomenclatura = [];
                        this.arrvnomenclatura = []
                        this.categoriaTotal = []
                        this.materialTotal = []
                        this.tipoTotal = []
                        this.arrcategoriaTotal = []
                        this.arrmaterialTotal = []
                        this.arrtipoTotal = []
                        this.listaOPME.forEach(element => {
                            if (element.sistema != '' && element.sistema != null)
                                this.arrsistema.push(element.sistema)
                            if (element.qtdFuros != '' && element.qtdFuros != 0)
                                this.arrqtdFuros.push(element.qtdFuros)
                            if (element.espessura != '' && element.espessura != null)
                                this.arrespessura.push(element.espessura)
                            if (element.diametro != '' && element.diametro != null)
                                this.arrdiametro.push(element.diametro)
                            if (element.comprimento != '' && element.comprimento != null)
                                this.arrcomprimento.push(element.comprimento)
                            if (element.tamanho != '' && element.tamanho != null)
                                this.arrtamanho.push(element.tamanho)
                            if (element.volume != '' && element.volume != null)
                                this.arrvolume.push(element.volume)
                            if (element.avancoRecuo != '' && element.avancoRecuo != null)
                                this.arrvavancoRecuo.push(element.avancoRecuo)
                            if (element.categoria != '' && element.categoria != null)
                                this.arrcategoriaTotal.push(element.categoria)
                            if (element.material != '' && element.material != null)
                                this.arrmaterialTotal.push(element.material)
                            if (element.tipo != '' && element.tipo != null)
                                this.arrtipoTotal.push(element.tipo)
                            if (element.nomenclatura != '' && element.nomenclatura != null) {

                                if (searchNomenclatura.indexOf(element.nomenclatura) === -1) {
                                    searchNomenclatura.push(element.nomenclatura);
                                } else {
                                    const key = searchNomenclatura.indexOf(element.nomenclatura);
                                    searchNomenclatura.pop(key);
                                    console.log(key)
                                }


                            }


                        });



                        this.sistemaTotal = this.arrsistema.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.qtdFurosTotal = this.arrqtdFuros.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.espessuraTotal = this.arrespessura.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.diametroTotal = this.arrdiametro.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.comprimentoTotal = this.arrcomprimento.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.tamanhoTotal = this.arrtamanho.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.volumeTotal = this.arrvolume.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.avancoRecuo = this.arrvavancoRecuo.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        // this.nomenclatura = this.arrvnomenclatura.reduce((acc, val) => {
                        //     acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                        //     return acc;
                        // }, {})


                        this.categoriaTotal = this.arrcategoriaTotal.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.materialTotal = this.arrmaterialTotal.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.tipoTotal = this.arrtipoTotal.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                    })
                $(".load").css("display", 'none')
                autocomplete(document.getElementById("myInput"), searchNomenclatura);
            },



            buscaPorNomenclatura() {
                this.filtrosMaterias.nomenclatura = document.getElementById("myInput").value;
            },
            enderecoURLImg(param) {

                var base = '<?= base_url('upload/') ?>' + param;
                // var base = 'https://ebuco.com.br/admin/upload/' + param;
                return base;
            },
            limparFiltros() {
                this.filtrosMaterias.nomenclatura = ''
                this.filtrosMaterias.marca = ''
                this.filtrosMaterias.sistema = ''
                this.filtrosMaterias.qtdFuros = ''
                this.filtrosMaterias.espessura = ''
                this.filtrosMaterias.diametro = ''
                this.filtrosMaterias.comprimento = ''
                this.filtrosMaterias.tamanho = ''
                this.filtrosMaterias.volume = ''
                this.filtrosMaterias.avancoRecuo = ''
                this.filtrosMaterias.categoria = ''
                this.filtrosMaterias.material = ''
                this.filtrosMaterias.tipo = ''
            },
            incluirMaterial() {
                if (this.materialEscolha.qtdMaterial != 0 || this.materialEscolha.qtdMaterial != '') {
                    this.materialEscolha.codigoMaterial = this.detalheMaterial[0].codigoOpme
                    this.inclueFinalidadeRascunho(this.detalheMaterial[0].codigoOpmeTipo, this.materialEscolha.aplicacao);
                    var formData = new FormData();
                    formData.append("dados", JSON.stringify(this.materialEscolha));
                    axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/incluirMaterial') ?>', formData)
                        .then((resp) => {
                            this.telaOpmeSelecao = false
                        })
                        .catch((e) => {})
                        .finally(() => {
                            this.materialEscolha = Object.assign({}, this._materialEscolha);
                            this.getMateriaisSalvos();
                        })
                    $("#qtdMat").css('border: 1px solid silver');
                } else {
                    $("#qtdMat").css('border: 1px solid red');
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro',
                        text: 'Informe a quantidade!',
                    });
                }

            },
            async inclueFinalidadeRascunho(tipo, aplicacao) {
                Swal.fire({
                    title: '',
                    text: "Deseja salvar essa aplicação anatômica desse material para reutilizá-la no futuro?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não'
                }).then((result) => {
                    if (result.value) {
                        try {
                            var data = $.param({
                                CodigoOpmeTipo: tipo,
                                Finalidade: aplicacao
                            });
                            axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/incluirRascunhoFinalidade') ?>', data)
                                .then((resp) => {
                                    Swal.fire({
                                        icon: 'success',
                                        title: 'Sucesso',
                                        text: 'Aplicação anatômica salva.',
                                    })
                                })
                        } catch (error) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Erro',
                                text: 'Erro ao salvar finalidade para uso futuro.',
                            })
                        }
                    }
                });
            },
            closeSelecMaterial() {
                this.telaOpmeSelecao = false;
                $("#opmestab").click()
            },

            async getMateriaisSalvos() {
                await axios.post('<?= base_url('EditorLaudo/Editor/getMateriaisSalvos') ?>').then((resp) => {
                    this.listMateriais = resp.data
                }).catch((e) => {}).finally(() => {})
            },
            deletarMaterial(codigo) {
                var formData = new FormData();
                formData.append("codigo", codigo);
                axios.post('<?= base_url('EditorLaudo/Editor/deletarMaterial') ?>', formData).then((resp) => {
                    this.getMateriaisSalvos()
                }).catch((e) => {}).finally(() => {

                })
            },
            async updateMaterial() {
                var param = {
                    table: 'SetMaterial',
                    data: {
                        aplicacao: this.editMaterial.aplicacao,
                        especificacaoFabricante: this.editMaterial.especificacaoFabricante,
                        qtdMaterial: this.editMaterial.qtdMaterial
                    },
                    where: {
                        codigoLaudoMaterial: this.editMaterial.codigoMaterial
                    }
                }
                await vmGlobal.updateFromAPI(param);
                $('#modalEditMaterial', this.$el).modal('hide');
                this.getMateriaisSalvos()
            },
            editarMaterial(codigo) {
                if (!this.hasOwnProperty('_edit')) {
                    this._edit = Object.assign({}, this.editMaterial);
                }

                let material = this.listMateriais.filter(el => {
                    return el.codigoLaudoMaterial == codigo;
                })[0];

                this.editMaterial = {
                    aplicacao: material.aplicacao,
                    especificacaoFabricante: material.especificacaoFabricante,
                    codigoMaterial: material.codigoLaudoMaterial,
                    qtdMaterial: material.qtdMaterial
                }
                $('#modalEditMaterial', this.$el).modal('show');
            },
            limitesImg() {
                this.limite += 50
            },
            montaDatatable(id,order = []) {
                $(id).DataTable().destroy();
                try {
                    this.$nextTick(function() {
                        $(id).DataTable({
                            "destroy": true,
                            "scrollX": true,
                            "order" : order,
                            dom: 'Bfrtip',
                            buttons: [
                                'excel'
                            ],
                            "bSort": true,
                            "bSortable": true,
                            'autoWidth': false,
                            "oLanguage": {
                                "sEmptyTable": "NENHUM REGISTRO ENCONTRADO!",
                                "sInfo": "Mostrar _START_ até _END_ do _TOTAL_ registros",
                                "sInfoEmpty": "Mostrar 0 até 0 de 0 Registros",
                                "sInfoFiltered": "(Filtrar de _MAX_ total registros)",
                                "sInfoPostFix": "",
                                "sInfoThousands": ".",
                                "sLengthMenu": "Mostrando _MENU_ registros por pagina",
                                "sLoadingRecords": "Carregando...",
                                "sProcessing": "Processando...",
                                "sZeroRecords": "Nenhum registro encontrado",
                                "sSearch": "Pesquisar: ",
                                "oPaginate": {
                                    "sNext": "<i class=\"fas fa-chevron-right\" ></i>",
                                    "sPrevious": "<i class=\"fas fa-chevron-left\" ></i>",
                                    "sFirst": "Primeiro",
                                    "sLast": "Ultimo"
                                },
                                "oAria": {
                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                }
                            },
                        });
                    });
                } catch (err) {
                    console.error(err);
                }
            },
            async readRascunhosFinalidade(codigoOpmeTipo) {
              
                try {
                    await axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/readRascunhoFinalidade') ?>', $.param({
                            CodigoOpmeTipo: codigoOpmeTipo
                        }))
                        .then(response => {
                            this.finalidades = [];
                            this.finalidades = response.data;
                            $('#tblFinalidadeMaterial').DataTable().destroy();
                            this.montaDatatable('#tblFinalidadeMaterial');
                        })
                } catch (error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro',
                        text: 'Erro ao carregar finalidades salvas.',
                    })
                }
            },
            copyFinalidade(fin) {
                this.finalidadesSelected.push(fin);
                this.materialEscolha.aplicacao = this.materialEscolha.aplicacao + ' ' + fin.Finalidade;
            },
            eraseFinalidade(fin) {
                this.finalidadesSelected = this.finalidadesSelected.filter(el => {
                    return el.CodigoFinAnatomica != fin.CodigoFinAnatomica;
                });

                this.materialEscolha.aplicacao = '';
                $.each(this.finalidadesSelected, (key, val) => {
                    this.materialEscolha.aplicacao = this.materialEscolha.aplicacao + ' ' + val.Finalidade;
                })

            },
            viewFinalidade(fin) {
                this.finalidadeModal.Finalidade = '';
                this.finalidadeModal = Object.assign({}, fin);
                $('#modalFinalidade', this.$el).modal('show');
            }
        },
        mounted() {},
    })

    function autocomplete(inp, arr) {
        /*the autocomplete function takes two arguments,
        the text field element and an array of possible autocompleted values:*/
        var currentFocus;
        /*execute a function when someone writes in the text field:*/
        inp.addEventListener("input", function(e) {
            var a, b, i, val = this.value;
            /*close any already open lists of autocompleted values*/
            closeAllLists();
            if (!val) {
                return false;
            }
            currentFocus = -1;
            /*create a DIV element that will contain the items (values):*/
            a = document.createElement("DIV");
            a.setAttribute("id", this.id + "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");
            /*append the DIV element as a child of the autocomplete container:*/
            this.parentNode.appendChild(a);
            /*for each item in the array...*/
            for (i = 0; i < arr.length; i++) {
                /*check if the item starts with the same letters as the text field value:*/
                if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                    /*create a DIV element for each matching element:*/
                    b = document.createElement("DIV");
                    /*make the matching letters bold:*/
                    b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].substr(val.length);
                    /*insert a input field that will hold the current array item's value:*/
                    b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                    /*execute a function when someone clicks on the item value (DIV element):*/
                    b.addEventListener("click", function(e) {
                        /*insert the value for the autocomplete text field:*/
                        inp.value = this.getElementsByTagName("input")[0].value;
                        /*close the list of autocompleted values,
                        (or any other open lists of autocompleted values:*/
                        closeAllLists();
                    });
                    a.appendChild(b);
                }
            }
        });
        /*execute a function presses a key on the keyboard:*/
        inp.addEventListener("keydown", function(e) {
            var x = document.getElementById(this.id + "autocomplete-list");
            if (x) x = x.getElementsByTagName("div");
            if (e.keyCode == 40) {
                /*If the arrow DOWN key is pressed,
                increase the currentFocus variable:*/
                currentFocus++;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 38) { //up
                /*If the arrow UP key is pressed,
                decrease the currentFocus variable:*/
                currentFocus--;
                /*and and make the current item more visible:*/
                addActive(x);
            } else if (e.keyCode == 13) {
                /*If the ENTER key is pressed, prevent the form from being submitted,*/
                e.preventDefault();
                if (currentFocus > -1) {
                    /*and simulate a click on the "active" item:*/
                    if (x) x[currentFocus].click();
                }
            }
        });

        function addActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeActive(x);
            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (var i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(elmnt) {
            /*close all autocomplete lists in the document,
            except the one passed as an argument:*/
            var x = document.getElementsByClassName("autocomplete-items");
            for (var i = 0; i < x.length; i++) {
                if (elmnt != x[i] && elmnt != inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }
        /*execute a function when someone clicks in the document:*/
        document.addEventListener("click", function(e) {
            closeAllLists(e.target);
        });
    }
</script>