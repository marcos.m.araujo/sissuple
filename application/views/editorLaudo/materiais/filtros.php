<div class="card">
    <div class="body">

        <button @click="limparFiltros" type="button" class="btn btn-danger mb-2"><i class="fa fa-trash-o"></i> <span>Limpar filtros</span></button>
        <ul v-if="arrcategoriaTotal.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Categoria</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.categoria = index" v-for="(item, index) in categoriaTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
   
        <ul v-if="arrmaterialTotal.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Material</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.material = index" v-for="(item, index) in materialTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrtipoTotal.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Tipo</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.tipo = index" v-for="(item, index) in tipoTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrvnomenclatura.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Nomenclatura</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.nomenclatura = index" v-for="(item, index) in nomenclatura" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrsistema.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Sistema</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.sistema = index" v-for="(item, index) in sistemaTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrqtdFuros.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Qtd Furos</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.qtdFuros = index" v-for="(item, index) in qtdFurosTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrespessura.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Espessura</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.espessura = index" v-for="(item, index) in espessuraTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrdiametro.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Diâmetro</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.diametro = index" v-for="(item, index) in diametroTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrcomprimento.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Comprimento</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.comprimento = index" v-for="(item, index) in comprimentoTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrtamanho.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Tamanho</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.tamanho = index" v-for="(item, index) in tamanhoTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrvolume.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Volume</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.volume = index" v-for="(item, index) in volumeTotal" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
        <ul v-if="arrvavancoRecuo.length != 0" class="nav nav-tabs">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);" role="button" aria-haspopup="true" aria-expanded="false">Avanço/Recuo</a>
                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 39px, 0px);">
                    <a @click="filtrosMaterias.avancoRecuo = index" v-for="(item, index) in avancoRecuo" :value="index" class="dropdown-item" href="javascript:void(0);">{{index}}</a>
                </div>
            </li>
        </ul>
    </div>
</div>