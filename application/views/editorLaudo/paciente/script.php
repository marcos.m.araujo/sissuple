<script>
    const dadospaciente = new Vue({
        el: "#paciente",
        data() {
            return {
                dadosPaciente: {},
                dadosPlano: {},
                notificaPaciente: false,
                notificaPlano: false,
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                planos: [],
                emails: []
            }
        },
        async mounted() {
            this.getPaciente()
        },
        methods: {
            async getPaciente() {
                var param = {
                    table: 'GetPaciente',
                    where: {
                        codigoPaciente: '<?= $this->session->codigoPaciente ?>'
                    }
                }
                var dados = await vmGlobal.getFromAPI(param)
                this.dadosPaciente = dados[0]
                await this.getPlanos()
            },
            async getPlanos() {
                var param = {
                    table: 'Plano',
                    orderBy: ['nomePlano ASC']
                }
                var dados = await vmGlobal.getFromAPI(param)
                this.planos = dados
            },
            async update() {
                var param = {
                    table: 'SetPaciente',
                    data: {
                        nomePaciente: this.dadosPaciente.nomePaciente,
                        email: this.dadosPaciente.email,
                        dataNascimento: this.dadosPaciente.dataNascimento,
                        numeroCarteirinha: this.dadosPaciente.numeroCarteirinha,
                        convenio: this.dadosPaciente.convenio,
                        telefone: this.dadosPaciente.telefone,
                        CPF: this.dadosPaciente.CPF_,
                        sexo: this.dadosPaciente.sexo
                    },
                    where: {
                        codigoPaciente: '<?= $this->session->codigoPaciente ?>'
                    }
                }
                await vmGlobal.updateFromAPI(param);
                sidemenu.tela = 2;
            },
            modalPlano() {
                $("#modalCadastroPlano").modal()
            },
            async salvarPlano() {
                var param = {
                    table: 'Plano',
                    data: this.dadosPlano
                }
                await vmGlobal.insertFromAPI(param)
                await $("#modalCadastroPlano").modal('hide')
                await this.getPlanos()
            },
            async proximaTela(){
                let button = this.$refs.btnUpdatePaciente;
                $(button).trigger('click');
            },
            checkPaciente(){
                var chaves = [
                    'CPF_',
                    'dataNascimento',
                    'nomePaciente',
                    'email',
                    'telefone',
                    'sexo',
                    'numeroCarteirinha',
                    'convenio',
                    'idade'
                ];

                $.each(this.dadosPaciente,(key,val) => {
                    if(chaves.includes(key) && val.length == 0){
                        sidemenu.tela = 1;
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Dados obrigatórios do paciente não foram preenchidos',
                            showConfirmButton: false,
                            timer: 3000
                        })
                        return false;
                    }
                });
            }
        }
    })
</script>