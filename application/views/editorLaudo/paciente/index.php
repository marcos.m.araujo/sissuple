<style>
    .tags {
        color: red !important
    }

    .tagscard input {
        color: #fff !important;

    }
</style>
<div id="paciente">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Dados do paciente </h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">               
                <a href="javascript:void(0);" @click="proximaTela()" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">

        <div class="col-md-2"></div>
        <div class="col-xl-8 col-lg-8 col-md-7">
            <div class="card">
                <div class="header">
                    <h2>Editar dados do paciente </h2>
                    <ul class="header-dropdown dropdown">
                        <li><a href="javascript:void(0);" class="full-screen"><i class="icon-frame"></i></a></li>
                    </ul>
                </div>
                <div class="body">
                    <form action="" @submit.prevent="update">
                        <div class="row clearfix">
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label class="text-info">Nome</label>
                                    <input required v-model="dadosPaciente.nomePaciente" type="text" class="form-control" placeholder="Nome" title="Nome">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="form-group">
                                    <label class="text-info">E-mail</label>
                                    <input required v-model="dadosPaciente.email" type="email" class="form-control" placeholder="E-mail" title="E-mail">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <label class="text-info">Sexo</label>
                                    <select required v-model="dadosPaciente.sexo" class="form-control" title="Sexo">
                                        <option value="">-- Sexo --</option>
                                        <option value="M">M</option>
                                        <option value="F">F</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <label class="text-info">Data de nascimento</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="icon-calendar"></i></span>
                                        </div>
                                        <input required title="Data de nascimento" v-model="dadosPaciente.dataNascimento" type="date" class="form-control" placeholder="Data nascimento">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <label class="text-info">Núumero da Carteirinha</label>
                                    <input required title="Número da carteirinha" v-model="dadosPaciente.numeroCarteirinha" type="text" class="form-control" placeholder="Número da Carteirinha">
                                </div>
                            </div>
                            <label style="margin-left: 15px" class="text-info">Plano de saúde</label>
                            <div class="col-md-12 input-group">

                                <br>
                                <select @change="notificaPlano = false" v-model="dadosPaciente.convenio" class="custom-select" id="inputGroupSelect04" required>
                                    <option v-for="i in planos" :value="i.CodigoPlano">{{i.nomePlano}} - {{i.cidade}} / {{i.UF}}</option>
                                </select>
                                <div class="input-group-append">
                                    <button @click="modalPlano" class="btn btn-outline-info" type="button">Cadastrar novo plano de saúde</button>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <br>
                                <div class="form-group">
                                    <label class="text-info">Telefone</label>
                                    <input required title="Telefone" maxlength="12"  onkeypress="formatar(this, '00-000000000')" v-model="dadosPaciente.telefone" type="text" class="form-control" placeholder="Telefone">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <br>
                                <label class="text-info">CPF</label>

                                <div class="form-group">
                                    <input required title="CPF" maxlength="14" onkeypress="formatar(this, '000.000.000-00')" v-model="dadosPaciente.CPF_" type="text" class="form-control" placeholder="CPF">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <br>
                                <div class="form-group">
                                    <label class="text-info">Idade</label>
                                    <input required title="Idade" style="cursor: no-drop; background-color: #22252A;" disabled v-model="dadosPaciente.idade" type="text" class="form-control" placeholder="Idade">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 text-right" style="padding: 10px">
                                <button type="submit" class="btn  btn-success" ref="btnUpdatePaciente">Atualizar dados do paciente</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modalCadastroPlano" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title">Cadastro de Plano de Saúde</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select class="form-control" v-model="dadosPlano.UF">
                                <option value="NA">Nacional</option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AM">AM</option>
                                <option value="AP">AP</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MG">MG</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="PR">PR</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RS">RS</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="SC">SC</option>
                                <option value="SE">SE</option>
                                <option value="SP">SP</option>
                                <option value="TO">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input title="Digite o cidade" v-model="dadosPlano.cidade" type="text" class="form-control" placeholder="Cidade *">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input title="Digite o Nome do Plano" v-model="dadosPlano.nomePlano" type="text" class="form-control" placeholder="Nome do Plano *">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input title="Digite o Telefone" maxlength="12" onkeypress="formatar(this, '00-0000-0000')" v-model="dadosPlano.telefoneFixo" type="text" class="form-control" placeholder="Telefone">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button @click="salvarPlano" type="button" class="btn btn-primary">Salvar mudanças</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('editorLaudo/paciente/script') ?>