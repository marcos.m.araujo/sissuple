<script>
    editor = new Vue({
        el: "#editorLaudo",
        data() {
            return {
                tela: 0,
                closeMenu: false,
                novoItem: '',
                UF: '',
                searchInput: '',
                contatoHospitalTelefone: '',
                contatoHospitalEmail: '',
                isLoading: false,
                baseGetURL: '',
                basePostURL: '',
                params: [],
                dadosPaciente: null,
                listaHospitais: null,
                listaEmailHospital: null,
                listaTelefoneHospital: null,
                listaTratamentos: null,
                codigoTratamento: '',
                listaSintomas: null,
                listaDiagnosticos: null,
                listaCids: null,
                listaIntervencoes: null,
                listaProcedimentos: null,
                listaObjetivos: null,
                listaSequelas: null,
                listaFornecedores: [],
                estado: '',
                municipio: '',
                ativaFormCadFornecedor: false,
                municioFornecedor: null,
                utilizacaoOPME: '',
                contatoFornecedor: {},
                preferencialFornecedor: 0,
                enderecoOPME: '',
                listaOPME: null,
                dadosHospital: {
                    diariasInternacao: '',
                    diariasUTI: '',
                    tempoProcedimento: '',
                    tipoAnestesia: '',
                    hospital: '',
                    codigoHospital: '',
                    codigoEquipeCirurgica: '',
                    tipoHonorario: '',
                    dataCirurgia: '',
                },
                sistemaTotal: [],
                qtdFurosTotal: [],
                espessuraTotal: [],
                diametroTotal: [],
                comprimentoTotal: [],
                tamanhoTotal: [],
                nomenclatura: [],
                volumeTotal: [],
                avancoRecuo: [],
                arrsistema: [],
                arrqtdFuros: [],
                arrespessura: [],
                arrdiametro: [],
                arrcomprimento: [],
                arrtamanho: [],
                arrvolume: [],
                arrvavancoRecuo: [],
                arrvnomenclatura: [],
                categoriaTotal: [],
                materialTotal: [],
                tipoTotal: [],
                arrcategoriaTotal: [],
                arrmaterialTotal: [],
                arrtipoTotal: [],
                categoria: '',
                material: '',
                tipo: '',
                material: '',
                marca: '',
                sistema: '',
                furos: '',
                espessura: '',
                diametro: '',
                comprimento: '',
                tamanho: '',
                volume: '',
                avanco: '',
                nomencla: '',
                obs: '',
                filtrosMaterias: {
                    nomenclatura: '',
                    sistema: '',
                    qtdFuros: '',
                    espessura: '',
                    diametro: '',
                    comprimento: '',
                    tamanho: '',
                    volume: '',
                    avancoRecuo: '',
                    categoria: '',
                    material: '',
                    tipo: '',
                },
                codigoOpme: '',
                materialEscolha: {
                    aplicacao: '',
                    especificacaoFabricante: 'Sim',
                    codigoMaterial: '',
                    qtdMaterial: 1
                },
                classificacaoDocumento: '',
                listClassificacaoDocumento: [],
                listTipoDocumentos: [],
                tipoDocumento: '',
                documento: '',
                listDocumentos: [],
                listMateriais: [],
                arrayContato: [],
                email: '',
                emailPaciente: ''

            }
        },
        watch: {
            tela() {
                this.searchInput = ''
                this.params = []
                this.params.push({
                    tela: this.tela
                })
                if (this.tela < 4)
                    this.getAxios(this.params);
                else
                    this.pullEscolhasPosTratamento()
                if (this.tela == 11 && this.estado != '') {
                    this.consultaForneceores()
                }
                if (this.tela == 12) {
                    this.selecionaMateriais()
                    this.getMateriaisSalvos()
                }
                if (this.tela == 13) {
                    this.getClassificacaoDocumento()
                    this.getDocumentos()
                }
                if (this.tela == 14) {
                    for (i = 0; i < this.listaEmailHospital.length; i++) {

                        var index = $.inArray(this.listaEmailHospital[i].contato, this.arrayContato);
                        if (index != -1) {                      
                            return false
                        } {
                            this.arrayContato.push(this.listaEmailHospital[i].contato)
                        }

                    }
                }


            },
            filtrosMaterias: {
                deep: true,
                handler() {
                    this.selecionaMateriais()
                }
            },
            isLoading(val) {
                (val) ? $('#spinner').show(): $('#spinner').hide();
            },
            dadosHospital: {
                deep: true,
                handler(a) {
                    this.pushDadosCirurgicos(a);
                    this.pullContadosHospital(a.codigoHospital)

                },
            },

            searchInput(a) {
                this.search(a)
            },
            utilizacaoOPME(a) {
                if (a == 'Não') {
                    this.listaFornecedores = []
                    this.municioFornecedor = []
                    this.estado = ''
                    this.municipio = ''
                }
            },
            preferencialFornecedor() {
                this.preferenciaFornecedor()
            },
            classificacaoDocumento() {
                this.getClassificacaoDocumentoTipoDocumento()
            }
        },
        updated() {

        },
        mounted() {
            // $('#spinner').show()
            this.tela = 1
            this.baseGetURL = "<?= base_url('EditorLaudo/Editor/pull/') ?>"
            this.basePostURL = "<?= base_url('EditorLaudo/Editor/push/') ?>"
        },
        methods: {
            getHospitais(UF) {
                this.isLoading = true;
                axios.get("<?= base_url('EditorLaudo/Editor/getHospitais?UF=') ?>" + UF).then((res) => {
                    this.listaHospitais = res.data
                }).catch((e) => {
                    console.log(e)
                }).finally(() => {
                    this.dataTableInit('#example1', 5)
                    this.isLoading = false;
                })
            },

            selecionaHospital(nome, codigo) {
                this.dadosHospital.hospital = nome
                this.dadosHospital.codigoHospital = codigo
                $("#modalLitaHospital").modal('hide')
                this.listaEmailHospital = []
                this.listaTelefoneHospital = []
                this.pullContadosHospital(codigo)

            },

            addContatoHospital(table, contato) {
                if (contato == 'email')
                    var param = [{
                        table: table,
                        dados: {
                            contato: this.contatoHospitalEmail,
                            codigoHospital: this.dadosHospital.codigoHospital
                        },
                    }]
                if (contato == 'telefone')
                    var param = [{
                        table: table,
                        dados: {
                            contato: this.contatoHospitalTelefone,
                            codigoHospital: this.dadosHospital.codigoHospital
                        },
                    }]
                this.pushAxios(param)
                this.contatoHospitalTelefone = ''
                this.contatoHospitalEmail = ''
                setTimeout(() => {
                    this.pullContadosHospital(this.dadosHospital.codigoHosital)
                }, 300);
            },

            removeContato(table, coluna, codigo) {
                var get = [{
                    table: table,
                    codigo: codigo,
                    coluna: coluna
                }]
                var formData = new FormData();
                formData.append("Params", JSON.stringify(get));
                axios.post('<?= base_url("EditorLaudo/Editor/rmContato") ?>', formData).then((resp) => {
                    if (table == 'tblContatosEmailHospital' && this.listaEmailHospital.length == 1)
                        this.listaEmailHospital = []
                    if (table == 'tblContatosTelefoneHospital' && this.listaTelefoneHospital.length == 1)
                        this.listaTelefoneHospital = []
                }).catch((e) => {
                    console.log(e)
                }).finally(() => {
                    this.params = []
                    this.params.push({
                        tela: this.tela
                    })
                    this.getAxios(this.params);
                    console.log("aqui")
                })
            },

            selecionaTratamento(codigoTratamento) {
                this.codigoTratamento = codigoTratamento
                this.tela = 4
            },

            set(codigoSet, emUso) {
                var params = [{
                    codigoTratamento: this.codigoTratamento,
                    tela: this.tela,
                    codigoSet: codigoSet,
                    codigo: emUso
                }]
                this.pushAxios(params)
                this.param.push({
                    tela: this.tela
                })
                setTimeout(() => {
                    alert("ok")
                    this.getAxios(this.param)
                }, 200);

            },

            addItem() {
                var params = [{
                    codigoTratamento: this.codigoTratamento,
                    tela: this.tela,
                    novoItem: this.novoItem
                }]
                console.log(params)
                var formData = new FormData();
                formData.append("Params", JSON.stringify(params));
                axios.post('<?= base_url("EditorLaudo/Editor/novoItem") ?>', formData).then((resp) => {
                    this.pullEscolhasPosTratamento()
                }).catch((e) => {
                    console.log(e)
                })
            },

            search(param) {
                var get = [{
                    tela: this.tela,
                    param: param,
                    codigoTratamento: this.codigoTratamento
                }]

                this.isLoading = true;
                var formData = new FormData();
                formData.append("Params", JSON.stringify(get));
                axios.post('<?= base_url("EditorLaudo/Editor/like") ?>', formData).then((resp) => {
                    switch (this.tela) {
                        case 1:
                            this.dadosPaciente = resp.data
                            break;
                        case 4:
                            this.listaSintomas = resp.data
                            break;
                        case 5:
                            this.listaDiagnosticos = resp.data
                            break;
                        case 6:
                            this.listaCids = resp.data
                            break;
                        case 7:
                            this.listaIntervencoes = resp.data
                            break;
                        case 8:
                            this.listaProcedimentos = resp.data
                            break;
                        case 9:
                            this.listaObjetivos = resp.data
                            break;
                        case 10:
                            this.listaSequelas = resp.data
                            break;
                    }
                }).catch((e) => {
                    console.log(e)
                }).finally(() => {
                    this.isLoading = false;
                })
            },

            pullEscolhasPosTratamento() {
                var get = [{
                    tela: this.tela,
                    codigoTratamento: this.codigoTratamento
                }]
                this.getAxios(get)
            },

            getAxios(tela, codigoTratamento) {
              
                var param = [{
                    tela: tela,
                    codigoTratamento: codigoTratamento
                }]
                var formData = new FormData();
                formData.append("Params", JSON.stringify(param));
                axios.post(this.baseGetURL, formData).then((resp) => {
                  

                }).catch((e) => {

                }).finally(() => {
                    
                })
            },

            pushAxios(param) {
                this.isLoading = true;
                var formData = new FormData();
                formData.append("Params", JSON.stringify(param));
                axios.post(this.basePostURL, formData).then((resp) => {
                    switch (this.tela) {
                        case 1:
                            this.dadosPaciente = resp.data
                            break;
                        case 3:
                            this.pullEscolhasPosTratamento()
                            break;
                        case 4:
                            this.pullEscolhasPosTratamento()
                            break;
                        case 5:
                            this.pullEscolhasPosTratamento()
                            break;
                        case 6:
                            this.pullEscolhasPosTratamento()
                            break;
                        case 7:
                            this.pullEscolhasPosTratamento()
                            break;
                        case 8:
                            this.pullEscolhasPosTratamento()
                            break;
                        case 9:
                            this.pullEscolhasPosTratamento()
                            break;
                        case 10:
                            this.pullEscolhasPosTratamento()
                            break;
                    }
                }).catch((e) => {
                    console.log(e)
                }).finally(() => {
                    this.isLoading = false;
                })
            },

            dataTableInit(tbl, pageLength) {
                $(tbl).DataTable({
                    "searching": true,
                    "pageLength": pageLength,
                    "destroy": true,
                    "bLengthChange": false,
                    "bSort": false,
                    "autoWidth": false,
                    "oLanguage": {
                        "sEmptyTable": "Nenhum registro encontrado na tabela",
                        "sInfo": "Mostrar _START_ até _END_ do _TOTAL_ registros",
                        "sInfoEmpty": "Mostrar 0 até 0 de 0 Registros",
                        "sInfoFiltered": "(Filtrar de _MAX_ total registros)",
                        "sInfoPostFix": "",
                        "sInfoThousands": ".",
                        "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                        "sLoadingRecords": "Carregando...",
                        "sProcessing": "Processando...",
                        "sZeroRecords": "Nenhum registro encontrado",
                        "sSearch": "Pesquisar: ",
                        "oPaginate": {
                            "sNext": "Proximo",
                            "sPrevious": "Anterior",
                            "sFirst": "Primeiro",
                            "sLast": "Ultimo"
                        },
                        "oAria": {
                            "sSortAscending": ": Ordenar colunas de forma ascendente",
                            "sSortDescending": ": Ordenar colunas de forma descendente"
                        }
                    }
                });
            },

            pullContadosHospital(param) {
                axios.get('<?= base_url('EditorLaudo/Editor/contatosHospital?codigo=') ?>' + param).then((resp) => {
                    this.listaEmailHospital = resp.data
                }).catch((e) => {}).finally(() => {

                })
            },

            pushDadosCirurgicos(dados) {
                var get = [{
                    dados: dados,
                    tela: this.tela,
                }]
                this.pushAxios(get)
            },

            consultaForneceores(param) {
                var formData = new FormData();
                formData.append("estado", this.estado);
                formData.append("municipio", this.municipio);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/consultaFornecedores') ?>', formData).then((resp) => {
                    this.listaFornecedores = resp.data
                }).catch((e) => {

                }).finally(() => {
                    this.isLoading = false
                    this.consultaMunicipioFornecedor()
                    if (this.listaFornecedores.length == 0)
                        this.ativaFormCadFornecedor = true
                    else
                        this.ativaFormCadFornecedor = false
                })
            },

            consultaMunicipioFornecedor() {
                var formData = new FormData();
                formData.append("estado", this.estado);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/consultaMunicipioFornecedores') ?>', formData).then((resp) => {
                    this.municioFornecedor = resp.data
                }).catch((e) => {}).finally(() => {
                    this.isLoading = false;
                })
            },

            enviarDadosFornecedor() {
                console.log(this.contatoFornecedor)
            },

            removeFornecedor(codigoFornecedor) {
                var formData = new FormData();
                formData.append("codigoFornecedor", codigoFornecedor);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/removeFornecedor') ?>', formData).then((resp) => {}).catch((e) => {}).finally(() => {
                    this.consultaForneceores()
                })
            },

            preferenciaFornecedor() {
                var formData = new FormData();
                formData.append("fornecedorPreferencia", this.preferencialFornecedor);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/preferenciaFornecedor') ?>', formData).then((resp) => {}).catch((e) => {}).finally(() => {
                    this.consultaForneceores()
                })
            },

            selecionarFornecedor(codigoFornecedor, codigoMarcaPrimeiraMarca) {
                var formData = new FormData();
                formData.append("codigoMarcaPrimeiraMarca", codigoMarcaPrimeiraMarca);
                formData.append("codigoFornecedor", codigoFornecedor);
                formData.append("utilizacaoOpme", this.utilizacaoOPME);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/selecionaFornecedor') ?>', formData).then((resp) => {}).catch((e) => {}).finally(() => {
                    this.consultaForneceores()
                })
            },

            selecionaMateriais() {
                var formData = new FormData();
                formData.append("filtros", JSON.stringify(this.filtrosMaterias));
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/selecionaMateriais') ?>', formData).then((resp) => {
                        this.listaOPME = resp.data
                        this.enderecoOPME = this.listaOPME[0].imagem
                        this.material = this.listaOPME[0].material
                        this.marca = this.listaOPME[0].marca
                        this.sistema = this.listaOPME[0].sistema
                        this.furos = this.listaOPME[0].qtdFuros
                        this.espessura = this.listaOPME[0].espessura
                        this.diametro = this.listaOPME[0].diametro
                        this.comprimento = this.listaOPME[0].comprimento
                        this.tamanho = this.listaOPME[0].tamanho
                        this.volume = this.listaOPME[0].volume
                        this.avancoRecuo = this.listaOPME[0].avancoRecuo
                        this.nomencla = this.listaOPME[0].nomenclatura
                        this.comprimento = this.listaOPME[0].comprimento
                        this.espessura = this.listaOPME[0].espessura
                        this.obs = this.listaOPME[0].obs
                        this.codigoOpme = this.listaOPME[0].codigoOpme

                    }).catch((e) => {})
                    .finally(() => {
                        this.sistemaTotal = [];
                        this.arrsistema = []
                        this.qtdFurosTotal = [];
                        this.arrqtdFuros = []
                        this.espessuraTotal = [];
                        this.arrespessura = []
                        this.diametroTotal = [];
                        this.arrdiametro = []
                        this.comprimentoTotal = [];
                        this.arrcomprimento = []
                        this.tamanhoTotal = [];
                        this.arrtamanho = []
                        this.volumeTotal = [];
                        this.arrvolume = []
                        this.avancoRecuo = [];
                        this.arrvavancoRecuo = []
                        this.nomenclatura = [];
                        this.arrvnomenclatura = []
                        this.categoriaTotal = []
                        this.materialTotal = []
                        this.tipoTotal = []
                        this.arrcategoriaTotal = []
                        this.arrmaterialTotal = []
                        this.arrtipoTotal = []
                        this.listaOPME.forEach(element => {
                            if (element.sistema != '' && element.sistema != null)
                                this.arrsistema.push(element.sistema)
                            if (element.qtdFuros != '' && element.qtdFuros != 0)
                                this.arrqtdFuros.push(element.qtdFuros)
                            if (element.espessura != '' && element.espessura != null)
                                this.arrespessura.push(element.espessura)
                            if (element.diametro != '' && element.diametro != null)
                                this.arrdiametro.push(element.diametro)
                            if (element.comprimento != '' && element.comprimento != null)
                                this.arrcomprimento.push(element.comprimento)
                            if (element.tamanho != '' && element.tamanho != null)
                                this.arrtamanho.push(element.tamanho)
                            if (element.volume != '' && element.volume != null)
                                this.arrvolume.push(element.volume)
                            if (element.avancoRecuo != '' && element.avancoRecuo != null)
                                this.arrvavancoRecuo.push(element.avancoRecuo)
                            if (element.categoria != '' && element.categoria != null)
                                this.arrcategoriaTotal.push(element.categoria)
                            if (element.material != '' && element.material != null)
                                this.arrmaterialTotal.push(element.material)
                            if (element.tipo != '' && element.tipo != null)
                                this.arrtipoTotal.push(element.tipo)
                        });

                        this.sistemaTotal = this.arrsistema.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.qtdFurosTotal = this.arrqtdFuros.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.espessuraTotal = this.arrespessura.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.diametroTotal = this.arrdiametro.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.comprimentoTotal = this.arrcomprimento.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.tamanhoTotal = this.arrtamanho.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.volumeTotal = this.arrvolume.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.avancoRecuo = this.arrvavancoRecuo.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.nomenclatura = this.arrvnomenclatura.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.categoriaTotal = this.arrcategoriaTotal.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.materialTotal = this.arrmaterialTotal.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                        this.tipoTotal = this.arrtipoTotal.reduce((acc, val) => {
                            acc[val] = acc[val] === undefined ? 1 : acc[val] += 1;
                            return acc;
                        }, {})
                    })
            },

            enderecoURLImg(param) {
                var base = '<?= base_url("upload/") ?>' + param;
                return base;
            },

            limparFiltros() {
                this.filtrosMaterias.nomenclatura = ''
                this.filtrosMaterias.sistema = ''
                this.filtrosMaterias.qtdFuros = ''
                this.filtrosMaterias.espessura = ''
                this.filtrosMaterias.diametro = ''
                this.filtrosMaterias.comprimento = ''
                this.filtrosMaterias.tamanho = ''
                this.filtrosMaterias.volume = ''
                this.filtrosMaterias.avancoRecuo = ''
                this.filtrosMaterias.categoria = ''
                this.filtrosMaterias.material = ''
                this.filtrosMaterias.tipo = ''
            },

            modalEscolhaMaterial() {
                $("#modalEscolhaMaterial").modal()
            },

            incluirMaterial() {
                this.materialEscolha.codigoMaterial = this.codigoOpme
                var formData = new FormData();
                formData.append("dados", JSON.stringify(this.materialEscolha));

                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/incluirMaterial') ?>', formData)
                    .then((resp) => {
                        this.getMateriaisSalvos()
                        $("#modalEscolhaMaterial").modal('hide')
                    })
                    .catch((e) => {}).finally(() => {

                    })
            },

            getClassificacaoDocumento() {
                axios.post('<?= base_url('EditorLaudo/Editor/getClassificacaoDocumento') ?>').then((resp) => {
                    this.listClassificacaoDocumento = resp.data
                }).catch((e) => {}).finally(() => {

                })
            },

            getClassificacaoDocumentoTipoDocumento() {
                var formData = new FormData();
                formData.append("classificacao", this.classificacaoDocumento);
                axios.post('<?= base_url('EditorLaudo/Editor/getClassificacaoDocumentoTipoDocumento') ?>', formData).then((resp) => {
                    this.listTipoDocumentos = resp.data
                }).catch((e) => {}).finally(() => {

                })
            },

            salvarDocumentos() {
                var doc = document.getElementById("FileArquivo").files[0];
                if (doc != '') {
                    var formData = new FormData();
                    formData.append('Arquivo', doc);
                    formData.append('classificacao', this.classificacaoDocumento);
                    formData.append('tipoDocumento', this.tipoDocumento);
                    $.ajax({
                        url: '<?= base_url('EditorLaudo/Editor/salvarDocumentos') ?>',
                        method: 'POST',
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            editor.listTipoDocumentos = []
                            editor.listClassificacaoDocumento = []
                            editor.classificacaoDocumento = ''
                            editor.tipoDocumento = ''
                            editor.documento = ''
                            editor.getDocumentos()

                        },
                        error: function(error) {
                            console.log(error);
                        }
                    })
                }
            },

            getDocumentos() {
                axios.post('<?= base_url('EditorLaudo/Editor/getDocumentos') ?>').then((resp) => {
                    this.listDocumentos = resp.data
                }).catch((e) => {}).finally(() => {})
            },

            getMateriaisSalvos() {
                axios.post('<?= base_url('EditorLaudo/Editor/getMateriaisSalvos') ?>').then((resp) => {
                    this.listMateriais = resp.data
                }).catch((e) => {}).finally(() => {})
            },

            alterarDocumento(codigo, tipo) {
                var formData = new FormData();
                formData.append("codigoLaudoElaboracaoExame", codigo);
                formData.append("tipoAlteracao", tipo);
                axios.post('<?= base_url('EditorLaudo/Editor/alterarDocumento') ?>', formData).then((resp) => {
                    this.getDocumentos()
                }).catch((e) => {}).finally(() => {

                })
            },

            deletarMaterial(codigo) {
                var formData = new FormData();
                formData.append("codigo", codigo);
                axios.post('<?= base_url('EditorLaudo/Editor/deletarMaterial') ?>', formData).then((resp) => {
                    this.getMateriaisSalvos()
                }).catch((e) => {}).finally(() => {

                })
            },

            addRomoveArrayContato(array) {


                if (this.email == "" ||
                    this.email.indexOf('@') == -1 ||
                    this.email.indexOf('.') == -1) {
                    alert("Por favor, informe um E-MAIL válido!");
                    return false;
                }
                var index = $.inArray(this.email, this.arrayContato);
                if (index != -1) {
                    alert("e-mail já encontra-se na lista")
                    return false
                } {
                    this.arrayContato.push(this.email)
                    this.email = ''
                }
            },

            removeArray(item) {
                var index = $.inArray(item, this.arrayContato);
                if (index != -1) {
                    this.arrayContato.splice(index, 1);
                }
            },

        }
    })
</script>