<script>
    var marcas = new Vue({
        el: "#marcas",
        data() {
            return {
                codigoLaudoElaboracao: <?= $this->session->codigoLaudoElaboracao ?>,
                cadastroCirurgiao: <?= $this->session->codigoCirurgiao ?>,
                listaFornecedores: [],
                estado: '',
                municipio: '',
                utilizacaoOpme: '',
                municioFornecedor: null,
                ativaFormCadFornecedor: false,
                preferencialFornecedor: 0,
                codigoFornecedor: 0,
                fornecedor: {},
                notificarFornecedor: false,
                limite: 1
            }
        },
        watch: {
            notificarFornecedor() {
                this.updateNotificacao()
            },
         
            utilizacaoOpme() {
                toastr.options = {
                    "timeOut": "10000",
                    "closeButton": true,
                    "position": 'top-full-width',
                    "onclick": function() {
                        alert('onclick callback');
                    },
                };

                toastr['info'](' Nos termos do Art.7, inciso II da RN RN 428/2017 e do Art.5 da RN 115/2012 do CFO, o profissional assistente deve justificar clinicamente a sua indicação e oferecer, pelo menos, 3 (três) marcas de produtos de fabricantes diferentes, quando disponíveis, dentre aquelas regularizadas junto à ANVISA, que atendam às características especificada.');
            }
        },
        methods: {
            async updateNotificacao() {
                var param = {
                    table: 'Laudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao
                    },
                    data: {
                        notificarFornecedor: this.notificarFornecedor,

                    },
                }
                await vmGlobal.updateFromAPI(param)
             
            },
            async getNofificacoes() {
                var param = {
                    table: 'Laudo',
                    where: {
                        codigoLaudoElaboracao: this.codigoLaudoElaboracao
                    }
                }
                var dados = await vmGlobal.getFromAPI(param)
                if (dados[0].notificarFornecedor == 1) {
                    this.notificarFornecedor = true                  
                } else {
                    this.notificarFornecedor = false
                }

            },

            consultaForneceores() {
                var formData = new FormData();
                formData.append("estado", this.estado);
                formData.append("municipio", this.municipio);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/consultaFornecedores') ?>', formData).then((resp) => {
                    this.listaFornecedores = resp.data
                   
                }).catch((e) => {

                }).finally(() => {
                    this.getNofificacoes()
                })
            },

            consultaMunicipioFornecedor() {
                var formData = new FormData();
                formData.append("estado", this.estado);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/consultaMunicipioFornecedores') ?>', formData).then((resp) => {
                    this.municioFornecedor = resp.data
                }).catch((e) => {}).finally(() => {
                    this.isLoading = false;
                })
            },
            selecionarFornecedor(codigoFornecedor, codigoMarcaPrimeiraMarca) {          
                var formData = new FormData();
                formData.append("codigoMarcaPrimeiraMarca", codigoMarcaPrimeiraMarca);
                formData.append("codigoFornecedor", codigoFornecedor);            
                formData.append("utilizacaoOpme", this.utilizacaoOpme);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/selecionaFornecedor') ?>', formData).then((resp) => {}).catch((e) => {})
                .finally(() => {
                    this.consultaForneceores()
                    
                })
            },
            removeFornecedor(codigoFornecedor) {
                var formData = new FormData();
                formData.append("codigoFornecedor", codigoFornecedor);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/removeFornecedor') ?>', formData).then((resp) => {}).catch((e) => {}).finally(() => {
                    this.consultaForneceores()
                })
            },

            preferenciaFornecedor(codigoFornecedor) {
                var formData = new FormData();
                formData.append("fornecedorPreferencia", this.preferencialFornecedor);
                formData.append("codigoFornecedor", codigoFornecedor);
                axios.post('<?= base_url('EditorLaudo/MarcasEFornecedores/preferenciaFornecedor') ?>', formData).then((resp) => {
                    this.consultaForneceores()
                }).catch((e) => {}).finally(() => {
                  
                })
            },

            async enviarFornecedor() {
                var param = {
                    table: 'Fornecedores',
                    data: {
                        fornecedor: this.fornecedor.fornecedor,
                        estado: this.estado,
                        codigoCirurgiao: this.cadastroCirurgiao,
                        nomeRepresentante: this.fornecedor.nomeRepresentante,
                        municipio: this.fornecedor.municipio,
                        contato: this.fornecedor.contato
                    }
                }
                await vmGlobal.insertFromAPI(param)

            }


        }
    })
</script>