<div id="marcas">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Marcas de material</h2>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="deletarLaudo()" class="btn btn-danger  " title="Exlcuir laudo"> <i class="fas fa-trash-alt"></i></button>
                <button style="height: 30px;width: 30px; padding: 2px;" type="button" onclick="previewPDF()" class="btn btn-info  search_toggle" title="Preview PDF"><i class="far fa-file-pdf"></i></button>

            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" @click="sidemenu.tela = 10" class="btn btn-sm btn-info btn-round" title="">Anterior</a>
                <a href="javascript:void(0);" @click="sidemenu.tela = 12" class="btn btn-sm btn-info btn-round" title="">Próximo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">

        <div class="col-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label>Será necessário utilizar OPME's ? </label>
                                <select v-model="utilizacaoOpme" class="form-control">
                                    <option value='Sim'>Sim</option>
                                    <option value='Não'>Não</option>
                                </select>
                            </div>
                        </div>

                        <div v-if="utilizacaoOpme == 'Sim'" class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label>Estado do fornecedor</label>
                                <select @change="consultaForneceores" v-model="estado" class="form-control">
                                    <option value="">Selecione um estado</option>
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AP">AP</option>
                                    <option value="AM">AM</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MS">MS</option>
                                    <option value="MT">MT</option>
                                    <option value="MG">MG</option>
                                    <option value="PA">PR</option>
                                    <option value="PB">PB</option>
                                    <option value="PR">PR</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SP">SP</option>
                                    <option value="SE">SE</option>
                                    <option value="TO">TO</option>
                                </select>
                            </div>
                        </div>
                        <div v-if="utilizacaoOpme == 'Sim'" class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label>Município do fornecedor</label>
                                <select @change="consultaForneceores" v-model="municipio" class="custom-select">
                                    <option value="">Todos</option>
                                    <option v-for="i in municioFornecedor" :value="i.municipio">{{i.municipio}}</option>
                                </select>
                            </div>
                        </div>
                        <div v-if="false" class="col-12">
                            <ul class="list-group mb-3 tp-setting">
                                <li class="list-group-item text-warning">
                                    Notificar os fornecedores dos andamentos do laudo
                                    <div class="float-right">
                                        <label class="switch">
                                            <input v-model="notificarFornecedor" type="checkbox">
                                            <span class="slider round"></span>
                                        </label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div v-if="listaFornecedores.length > 0" class="col-12 row">
            <div v-for="i in listaFornecedores" class="col-lg-4 col-md-12 card">
                <div class="body">
                    <div class="media">
                        <div style="cursor: pointer" title="Selecionar fornecedor" v-if="i.check == 'checked'" @click="removeFornecedor(i.codigoFornecedor)" :class="[i.check == 'checked'? 'bg-success': 'bg-danger']" class="stamp mr-3 stamp-md ">
                            <i class="fas fa-check-double"></i>
                        </div>
                        <div style="cursor: pointer" title="Desmarcar fornecedor" v-else @click="selecionarFornecedor(i.codigoFornecedor, i.codigoMarcaPrimeiraMarca)" :class="[i.check == 'checked'? 'bg-success': 'bg-danger']" class="stamp mr-3 stamp-md ">
                            <i class="fas fa-check"></i>
                        </div>
                        <div class="media-body">
                            <h5 class="m-0">{{i.fornecedor}}</h5>
                            <p class="text-muted mb-0">{{i.primeiraMarca}}</p>
                        </div>


                    </div>
                    <div v-if="i.check == 'checked'" class="fancy-checkbox">
                        <label><input @click="preferenciaFornecedor(i.codigoFornecedor); preferencialFornecedor = i.codigoFornecedor" type="checkbox"><span>Marcar como esse fornecedor preferêncial</span></label>
                    </div>
                    <table class="table card-table mb-0">
                        <tbody>
                            <tr v-for="(j, index) in i.marcas.slice(0, limite) " :key="index">
                                <td class="font-weight-bold">{{j}}</td>
                                <td v-if="index == 0" class="text-right">
                                    <span v-if="limite != i.marcas.length && i.marcas.length > 1" style="cursor: pointer; " @click="limite = i.marcas.length " class="text-success">ver mais marcas <i class="fa fa-caret-down"></i></span>
                                    <span v-if="limite == i.marcas.length && i.marcas.length > 1" style="cursor: pointer; " @click="limite = 1 " class="text-danger">ver menos marcas <i class="fa fa-caret-up"></i></span>
                                </td>
                                <td v-else></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>

        <div class="col-md-12">
            <div v-if="listaFornecedores == '' && estado != ''" class="card">
                <div class="body">
                    <div class="row">
                        <div class="card border-danger">
                            <div class="body text-danger">
                                <h4 class="card-title">Não encontramos fornecedores para o estado {{estado}}.</h4>
                                <p class="card-text">Informe os dados do fornecedor que entraremos em contato para inclusão do mesmo na plataforma.</p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="text-input3">Nome do fornecedor</label>
                                <input v-model="fornecedor.fornecedor" type="text" id="text-input3" class="form-control" required="" data-parsley-min="5">
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="text-input3">Município</label>
                                <input v-model="fornecedor.municipio" type="text" id="text-input3" class="form-control" required="" data-parsley-min="5">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="text-input3">Nome do representante</label>
                                <input v-model="fornecedor.nomeRepresentante" type="text" id="text-input3" class="form-control" required="" data-parsley-min="5">
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="form-group">
                                <label for="text-input3">Contato comercial</label>
                                <input v-model="fornecedor.contato" type="text" onkeypress="formatar(this, '00-00000000')" id="text-input3" class="form-control" required="" data-parsley-min="5">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer col-md-12">
                        <button type="submit" @click="enviarFornecedor" class="btn btn-round btn-success">Enviar <i class="fas fa-chevron-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('editorLaudo/marcasEFornecedores/script') ?>