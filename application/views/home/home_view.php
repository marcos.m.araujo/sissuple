<style>
#dl {
    margin-top: -11px;
    cursor: pointer;
    border-bottom: 1px solid #17a2b8;
}

#dl dt {
    font-size: 10pt;
}

#dl dd {
    font-size: 9pt;
    margin-bottom: 2px;
}

#dl:hover {
    border-bottom: 1px solid red;  
}

#d1 dt *:hover {
    text-decoration: underline;
     
     
}
</style>

<section class="content">
    <div class="container-fluid">
        <?php if ($this->session->codigoPerfil == 1) {?>
        <div class="row center">
            <div class="col-md-3 "> </div>
            <!--                <a href="<?php echo base_url() ?>Paciente/Home" class="bloco col-md-3  ">
                    <i id="inconBloco" class="iconBloco fas fa-users"></i>
                    <img class="imgHome" id="inconBloco" src="<?=base_url('newtheme/')?>/img/user-circle2.png">
                    <p  class="textBloco">Meus Pacientes</p>
                </a>-->
            <!--                <a href="<?php echo base_url() ?>Laudos/Laudos/listaLaudos" id="laudos" class="bloco col-md-3  ">
                    <i id="inconBloco" class="iconBloco fas fas fa-file-medical-alt"></i>
                    <img class="imgHome" id="inconBloco" src="<?=base_url('newtheme/')?>/img/laudo.png">
                    <p class="textBloco">Minhas cirurgias</p>
                </a> -->

            <div class="col-md-12">
                <br>
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Processos em análise (10)</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="d-md-flex">
                            <div class="p-1 flex-1" style="overflow: hidden">
                                <div id="listaMeusPacientes" class="card-body">
                                    <?php if (!empty($pacientes)) {?>
                                    <?php foreach ($pacientes as $pc) {?>
                                    <dl id="dl" onclick="situacaoAnalise()">
                                        <dt><?=strtoupper($pc['nomePaciente'])?></dt>
                                        <dd><?=strtoupper($pc['convenio'])?></dd>
                                        <?php if (!empty($pc['tratamentos'])) {?>
                                        <dd><?=strtoupper($pc['tratamentos'])?></dd>
                                        <?php } else {?>
                                        <dd style="color: #DF0101">Nenhum tratamento selecionado</dd>
                                        <?php }?>
                                        <dd style="font-size: 12pt"><span class="badge badge-success">Em análise</span>
                                        </dd>
                                        <dd style="font-size: 12pt"><span class="badge badge-danger">Aguardando Parecer
                                                EBuco <i class="fas fa-user-clock"></i></span></dd>

                                        <div class="progress-group">
                                            <span style="margin-top: -10px" class="float-right"><b>11</b>/21</span>
                                            <div class="progress progress-sm">
                                                <div class="progress-bar bg-warning" style="width: 50%"
                                                    title="Dias para vencimento"></div>
                                            </div>
                                        </div>
                                    </dl>
                                    <?php }?>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Analises concluídas (10)</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                <i class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-widget="remove">
                                <i class="fa fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                        <div class="d-md-flex">
                            <div class="p-1 flex-1" style="overflow: hidden">
                                <div id="listaMeusPacientes" class="card-body">
                                    <?php if (!empty($pacientes)) {?>
                                    <?php foreach ($pacientes as $pc) {?>
                                    <dl id="dl" onclick="situacaoAnalise(<?=strtoupper($pc['codigoPaciente'])?>)"
                                        title="Definir situação">
                                        <dt><?=strtoupper($pc['nomePaciente'])?></dt>
                                        <dd><?=strtoupper($pc['convenio'])?></dd>
                                        <?php if (!empty($pc['tratamentos'])) {?>
                                        <dd><?=strtoupper($pc['tratamentos'])?></dd>
                                        <?php } else {?>
                                        <dd style="color: #DF0101">Nenhum tratamento selecionado</dd>
                                        <?php }?>
                                        <dd style="color: #DF0101"><span class="badge badge-danger">Negado</span></dd>

                                    </dl>
                                    <?php }?>
                                    <?php }?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>

        </div>
        <?php }?>
    </div>
</section>



<script>

</script>
<script src="<?php print base_url('newtheme/')?>jquery-3.3.1.min.js"></script>
<?php if ($this->session->codigoPerfil != 1) {?>

<script>
$(document).ready(function() {
    var windowWidth = window.innerWidth;
    if (windowWidth > 400) {
        $('#sidebar').removeClass("sidebar-collapse").addClass("sidebar-open");
    } else {
        $('#sidebar').removeClass("sidebar-open").addClass("sidebar-collapse");
    }
})
</script>

<?php }?>