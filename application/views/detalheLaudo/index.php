<?php $this->load->view('app_template/script_interno') ?>
<style>
    .btnSigilo {
        width: 100%;
        margin: 2px;
    }

    .timeCard {
        height: 900px !important;
        overflow: auto
    }

    .timeline-item {
        border-bottom: 1px solid #535559 !important;
        margin-top: 15px;
        padding-bottom: 20px;       
      
    }
  
    #tableTime td {
        background-color: #F1F4F6;
        cursor: pointer;
    }
  
</style>
<div style="margin-top: -100px!important" id="timeline" class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-12 col-sm-12">
                <h2 style="font-weight: 600">Paciente: {{detalhes[0].nomePaciente }}</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item text-info"><a> {{detalhes[0].tratamentos }}</a></li>
                        <li class="breadcrumb-item text-info"><a> {{detalhes[0].intervencao }}</a></li>
                        
                    </ol>
                </nav>
                Situação: <strong>{{detalhes[0].ultimoStatusTime }}</strong> <br>
                Código do Laudo: <strong>E-{{detalhes[0].codigoLaudoElaboracao }}</strong> <br>
                Data da elaboração: <strong>{{detalhes[0].dataLaudo }}</strong> <br>
                Hospital: <strong>{{detalhes[0].hospital }} - {{detalhes[0].estado }}</strong> <br>
                Data prevista para cirurgia: <strong>{{detalhes[0].dataCirurgiaF }}</strong>

            </div>

            <div style="margin-top:-50px" class="col-md-12 col-sm-12 text-right hidden-xs">
                <div class="float-right" style="margin-top: -10px" class="sidebar-scroll">
                    <nav id="left-sidebar-nav" class="sidebar-nav">
                        <ul id="main-menu" class="metismenu">
                            <li title="Ir para meus laudos"><a href="<?= base_url('HomeCirurgiao/home') ?>"><i class="fas fa-home bg-danger text-light"></i></a></li>
              
                            <li title="ARQUIVAR PROCESSO"><a href="javascript:void(0)" @click="timeline.arquivarLaudo()"><i class="fas fa-archive text-light bg-danger"></i></a></li>
                        </ul>
                    </nav>
                </div>
                <div class="btn-group" role="group">

                    <button @click="juntarDocumentos()" type="button" class="btn btn-info">
                        MOVIMENTAR PROCESSO
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-info m-b-5" style="height: 1px; width: 100%"></div>
    <div class="row clearfix">
        <div class="col-lg-4 timeCard">
            <div class="card">
                <div class="body">
                    <ul class="timeline">
                        <li v-for="i in listaTime" class="timeline-item">
                            <div v-if="i.codigoPerfil == '1'" class="timeline-info text-info"><span>{{vmGlobal.formatDataHora(i.data)}}<br><strong>{{i.nomeUsuario}}</strong></span></div>
                            <div v-if="i.codigoPerfil == '2'" class="timeline-info text-danger"><span>{{vmGlobal.formatDataHora(i.data)}}<br><strong>{{i.nomeUsuario}}</strong></span></div>
                            <div v-if="i.codigoPerfil == '3'" class="timeline-info text-success"><span>{{vmGlobal.formatDataHora(i.data)}}<br><strong>{{i.nomeUsuario}}</strong></span></div>
                            <div class="timeline-marker"></div>
                            <div class="timeline-content">
                                <h4 style="font-size: 12pt!important" class="timeline-title">{{i.TipoTime}}</h4>
                                <br>
                                <table id="tableTime" class="table table-hover table-custom spacing5 m-t--10 mb-2">
                                    <tbody>
                                        <tr  @click="abrirArquivo(thisLaudo.arquivo)" v-if="i.codigoPerfil == '1' && i.TipoTime == thisCabecaTimeline">
                                            <td class="font-weight-bold">1 - Laudo</td>
                                            <td style="padding-left: -20px" class="text-right"><i class="fas fa-paperclip"></i>
                                            </td>
                                        </tr>
                                        <tr @click="abrirArquivo(thisGuia.arquivo)" v-if="i.codigoPerfil == '1' && i.TipoTime == thisCabecaTimeline">
                                            <td class="font-weight-bold">2 - Guia Cirúrgica</td>
                                            <td class="text-right"><i class="fas fa-paperclip"></i>
                                            </td>
                                        </tr>
                                        <tr  @click="abrirArquivo(d.arquivo)" v-for="d in i.documentos">
                                            <td class="font-weight-bold">{{d.codigoLaudoElaboracaoExame}} - {{showDescricaoTextoLivre(d)}}</td>
                                            <td class="text-right"><i class="fas fa-paperclip"></i>
                                            </td>
                                        </tr>
                                    
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="row">
                <iframe style="width: 750px; height: 1000px; border:none" id="preview" class="col-md-12" src=""></iframe>
            </div>
        </div>
    </div>
    <?php $this->load->view('detalheLaudo/modalJuntada') ?>
    <?php $this->load->view('detalheLaudo/modalNotificar') ?>
    <?php $this->load->view('detalheLaudo/modalSituacao') ?>
</div>
<?php $this->load->view('detalheLaudo/script') ?>