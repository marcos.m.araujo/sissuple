<div class="modal fade bd-example-modal-xl" id="modalSituacao" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Mudar Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-4 col-md-4">
                    <div class="form-group">
                        <label class="form-label">Filtro por situação</label>
                        <select style="border:1px solid #17C2D7" v-model="Situacao" class="custom-select">
                            <option value='11'>Aceite pelo Hospital</option>
                            <option value='6'>Análise concluída - Autorizada parcial</option>
                            <option value='7'>Análise concluída - Cirurgia apta a realização</option>
                            <option value='5'>Análise concluída - Negada</option>
                            <option value='8'>Cirurgia Realizada</option>
                            <option value='4'>Divergência / junta médica</option>
                            <option value='10'>Juntada de Documentação</option>
                            <option value='2'>Laudo confeccionado sem entrada</option>
                            <option value='3'>Laudo dado entrada / Em análise</option>
                            <option value='9'>Processo encerrado</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" @click="alterarStatus" class="btn btn-success">Alterar</button>
            </div>
        </div>
    </div>
</div>