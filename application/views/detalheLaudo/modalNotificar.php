<div class="modal fade bd-example-modal-xl" id="modalNotificar" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Noficações</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-12">
                    <h6 class="text-info">Notificar os sequientes usuários sobre o andamento do laudo</h6>
                    <ul class="list-group mb-3 tp-setting">
                        <li class="list-group-item">
                            Paciente
                            <div class="float-right"><label class="switch"><input v-model="notificarPaciente" type="checkbox"> <span class="slider round"></span></label></div>
                        </li>
                    </ul>
                    <ul class="list-group mb-3 tp-setting">
                        <li class="list-group-item">
                            Hospital
                            <div class="float-right"><label class="switch"><input v-model="notificarHospital" type="checkbox"> <span class="slider round"></span></label></div>
                        </li>
                    </ul>
       
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" @click="notificarNow" class="btn btn-success" ><i class="fas fa-bell"></i> Notificar</button>
            </div>
        </div>
    </div>
</div>