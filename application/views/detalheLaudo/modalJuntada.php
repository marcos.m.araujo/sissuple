<div class="modal fade bd-example-modal-xl" id="modalJuntadaDocumentos" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Juntada de Documentos</h5>
                <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button> -->
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Anexar</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label>Classificação do documento</label>
                                        <select required v-model="classificacaoDocumento" class="form-control">
                                            <option :value="i.cabeca" v-for="i in listClassificacaoDocumento">{{i.cabeca}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-12">
                                    <div v-show="classificacaoDocumento != ''" class="form-group">
                                        <label>Tipo Documento</label>
                                        <select required v-model="tipoDocumento" class="form-control">
                                            <option :value="i.tipoDocumento" v-for="i in listTipoDocumentos">{{i.tipoDocumento}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div v-show="tipoDocumento != '' && !tipoDocumento.toLowerCase().includes('texto livre')" class="form-group" > 
                                        <label for="customFile">Arquivo</label>
                                        <div required class="custom-file">
                                            <input id="FileArquivo" v-model="documento" type="file" class="custom-file-input">
                                            <label class="custom-file-label" for="customFile">{{documento}}</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-md-6 col-sm-12">
                                    <div v-show="documento != ''" class="form-group">
                                        <button @click='salvarDocumentos' style="margin-top: 32px;" type="button" class="btn btn-primary">Anexar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h2>Documentos salvos</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-12" v-show="tipoDocumento != '' && tipoDocumento.toLowerCase().includes('texto livre')">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <div class="header">
                                                    <h2>Texto Livre</h2>
                                                </div>
                                                <div class="body">
                                                    <div class="row mb-2">
                                                        <div class="col-md-6">
                                                            <div class="project-actions">
                                                                <span>Sigiloso para: </span>
                                                                <div class="mt-2">
                                                                    <a href="javascript:void(0)" class="btn btn-default btn-sm" v-for="(val,name) in observacoesSigilo" @click="observacoesSigilo[name] = (val == 0)? 1 : 0">
                                                                        <i :style="{color: (observacoesSigilo[name] == 0)? 'green' : 'red'}" class="fas" :class="[(observacoesSigilo[name] == 0)? 'fa-lock-open' : 'fa-lock']"></i>
                                                                        {{name.replace('privado','')}}
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 mb-2">
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label for="" class="">Título do Documento:</label>
                                                                        <input type="text" class="form-control form-control-sm" v-model="descricaoTextoLivre">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 d-flex align-self-center text-center">
                                                                    <button @click='geneatePDF()' type="button" class="btn btn-primary">Salvar</button>
                                                                </div>                                    
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <textarea id="observacoesFinais" cols="30" rows="10" class="form-control">

                                                    </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-sm" style="width: 100%;">
                                        <thead>
                                            <tr class="text-center">
                                                <td>Documeto</td>
                                                <td colspan="4">
                                                    Sigiloso para :
                                                </td>
                                                <td>-</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr v-for="i in listDocumentos">
                                                <td style="word-wrap: break-word" :title="i.tipoDocumento">
                                                    <span>(Nº {{i.codigoLaudoElaboracaoExame}})<br> {{i.classificacaoDocumento}}</span><br>
                                                    <strong>{{i.tipoDocumento}}</strong>
                                                </td>
                                                <td>
                                                    <a v-if="i.privadoHospital == 0" class="btn btn-sm btnSigilo btn-outline-info" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoHospital')">
                                                        <i style="color: green" class="fas fa-lock-open"></i>
                                                        Hospital
                                                    </a>
                                                    <a v-if="i.privadoHospital == 1" class="btn btn-sm btnSigilo btn-outline-info" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoHospital')">
                                                        <i style="color: red" class="fas fa-lock"></i>
                                                        Hospital
                                                    </a>
                                                </td>
                                                <td>
                                                    <a v-if="i.privadoPaciente == 0" class="btn btn-sm btnSigilo btn-outline-info" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoPaciente')">
                                                        <i style="color: green" class="fas fa-lock-open"></i>
                                                        Paciente
                                                    </a>
                                                    <a v-if="i.privadoPaciente == 1" class="btn btn-sm btnSigilo btn-outline-info" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoPaciente')">
                                                        <i style="color: red" class="fas fa-lock"></i>
                                                        Paciente
                                                    </a>                                                
                                                </td>
                                                <td>
                                                    <a v-if="i.privadoPlano == 0" class="btn btn-sm btnSigilo btn-outline-info" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoPlano')">
                                                        <i style="color: green" class="fas fa-lock-open"></i>
                                                        Plano
                                                    </a>
                                                    <a v-if="i.privadoPlano == 1" class="btn btn-sm btnSigilo btn-outline-info" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoPlano')">
                                                        <i style="color: red" class="fas fa-lock"></i>
                                                        Plano
                                                    </a>
                                                </td>
                                                <td>
                                                    <a v-if="i.privadoFornecedor == 0" class="btn btn-sm btnSigilo btn-outline-info" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoFornecedor')">
                                                        <i style="color: green" class="fas fa-lock-open"></i>
                                                        Fornecedor
                                                    </a>
                                                    <a v-if="i.privadoFornecedor == 1" class="btn btn-sm btnSigilo btn-outline-info" href="#" @click="alterarDocumento(i.codigoLaudoElaboracaoExame, 'privadoFornecedor')">
                                                        <i style="color: red" class="fas fa-lock"></i>
                                                        Fornecedor
                                                    </a>
                                                </td>
                                                <td class="text-center">
                                                    <a class="btn btn-danger btn-sm" href="javascript:void(0);" @click="removerFile(i.codigoLaudoElaboracaoExame)" title="Remover">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" @click="sendFiles()">Finalizar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" @click="fecharTimeLine()">Cancelar</button>
            </div>
        </div>
    </div>
</div>