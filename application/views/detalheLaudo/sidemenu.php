<?php $this->load->view('app_template/script_interno') ?>
<div   id="sidemenubox" class="container ">
    <div class="row">
        <div class="col-12">
            <div style="margin-left: -380px;" class="navbar-brand">
                <a  href="index.html"><span>Detalhes do Laudo</span></a>
                <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right "><i class="lnr lnr-menu icon-close"></i></button>
            </div>
            <div class="sidebar-scroll">
                <nav id="left-sidebar-nav" class="sidebar-nav">
                    <ul id="main-menu" class="metismenu">
                        <li title="Ir para meus laudos"><a href="<?= base_url('HomeCirurgiao/home') ?>"><i class="fas fa-home bg-info"></i></a></li>                      
                        <li title="MOVIMENTAR PROCESSO"><a href="#" @click="timeline.juntarDocumentos()"><i class="fas fa-upload bg-info"></i></a></li>                       
                        <li title="ARQUIVAR PROCESSO"><a href="javascript:void(0)" @click="timeline.arquivarLaudo()"><i class="fas fa-archive bg-info"></i></a></li>                       
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<script>
    sidemenubox = new Vue({
        el: "#sidemenubox",
        data() {
            return {

            }
        },
        mounted() {

        },
        methods: {

        }


    })
</script>