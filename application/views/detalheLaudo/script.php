<script src="<?= base_url('newtheme/ckeditor/ckeditor.js') ?>"></script>
<script>
    $(document).ready(function() {
        $.fn.modal.Constructor.prototype._enforceFocus = function _enforceFocus() {
            var _this4 = this;
            $(document).off(Event.FOCUSIN).on(Event.FOCUSIN, function (event) {
                if (
                    document !== event.target
                    && _this4._element !== event.target
                    && $(_this4._element).has(event.target).length === 0
                    && !$(event.target.parentNode).hasClass('cke_dialog_ui_input_select')
                    && !$(event.target.parentNode).hasClass('cke_dialog_ui_input_text')
                ) {
                    _this4._element.focus();
                }
            });
        };
    });

    baseurl = '<?= base_url(); ?>';
    timeline = new Vue({
        el: "#timeline",
        data() {
            return {
                thisLaudo: {
                    arquivo: '',
                },
                thisGuia:{
                    arquivo: ''
                },
                thisCabecaTimeline: 'Pedido de Autorização de Cirurgia',
                newTimeLine: '',
                listaTime: [],
                codigoLaudo: '<?= $codigoLaudo ?>',
                classificacaoDocumento: '',
                listClassificacaoDocumento: [],
                tipoDocumento: '',
                listTipoDocumentos: [],
                documento: '',
                listDocumentos: [],
                notificarPaciente: false,
                notificarHospital: false,
                Situacao: '',
                detalhes: [],
                observacoesSigilo: {
                    privadoHospital: 0,
                    privadoPaciente: 0,
                    privadoPlano: 0,
                    privadoFornecedor: 1
                },
                descricaoTextoLivre: ''
            }
        },
        watch: {
            classificacaoDocumento() {
                this.getClassificacaoDocumentoTipoDocumento()
            }

        },
        created() {
            if(!this.hasOwnProperty('_observacoesSigilo')) {
                this._observacoesSigilo = Object.assign({}, this.observacoesSigilo);
            }
        },
        async mounted() {            
            await this.get();
            await this.getDados();
            // document.getElementById('preview').src = '<?= base_url('EditorLaudo/Laudo_Pedido_Autorizacao_PDF?codigoLaudoElaboracao=') ?>' + this.codigoLaudo;
            CKEDITOR.replace('observacoesFinais', {
                htmlEncodeOutput: true,
                removeButtons: 'Image,Source',
                extraPlugins: 'base64image',
                width: '100%',
                height: 500
            });
            
			$('#modalJuntadaDocumentos', this.$el).on('hide.bs.modal', () => {
				this.fecharTimeLine();
			});

            this.abrirArquivo(this.thisLaudo.arquivo);
        },
        methods: {
            async get() {
                await axios.get('getTimeLine?codigoLaudo=' + this.codigoLaudo)
                    .then(response => {
                        this.listaTime = response.data;

                        var timeline = response.data.filter(function(el) {
                            return el.TipoTime == 'Pedido de Autorização de Cirurgia';
                        })[0];

                        this.thisLaudo = timeline.documentos.filter(function(el){
                            return el.tipoDocumento.includes('1 - Laudo');
                        })[0];

                        this.thisGuia = timeline.documentos.filter(function(el){
                            return el.tipoDocumento.includes('2 - Guia Cirurgíca');
                        })[0];

                        this.listaTime = this.listaTime.map(function(el){
                            if(el.TipoTime == 'Pedido de Autorização de Cirurgia'){
                                el.documentos = el.documentos.filter(function(doc){
                                    return !doc.tipoDocumento.includes('1 - Laudo') && !doc.tipoDocumento.includes('2 - Guia Cirurgíca');
                                });
                            }
                            return el;
                        });
                    })
                    .catch((error) => {
                        console.log(error);
                    });
            },
            async getDados() {
                var param = {
                    table: "Laudos",
                    where: {
                        codigoLaudoElaboracaoCRIP: this.codigoLaudo
                    }
                }
                this.detalhes = await vmGlobal.getFromAPI(param);
            },

            abrirArquivoLaudo(param) {
                if(param === 'laudo')
                document.getElementById('preview').src = '<?= base_url('EditorLaudo/Laudo_Pedido_Autorizacao_PDF?codigoLaudoElaboracao=') ?>' + this.codigoLaudo;
                if(param === 'guia')
                document.getElementById('preview').src = '<?= base_url('EditorLaudo/Laudo_Pedido_Autorizacao_PDFGuia?codigoLaudoElaboracao=') ?>' + this.codigoLaudo;

            },
            abrirArquivo(param) {
                $('#preview',this.$el).attr('src',param);
                // this.$el.querySelector('#preview').src = param;
            },

            // INÍCIO JUNTADA
            async juntarDocumentos() {
                $("#modalJuntadaDocumentos",this.$el).modal({backdrop: 'static', keyboard: false});
                await this.getClassificacaoDocumento()

            },
            async salvarDocumentos() {
                var doc = document.getElementById("FileArquivo").files[0];
                if (doc != '') {
                    doc = {
                        codigoLaudoElaboracaoExame: this.listDocumentos.length + 1,
                        Arquivo: doc,
                        classificacaoDocumento: this.classificacaoDocumento,
                        tipoDocumento:  this.tipoDocumento,
                        codigoLaudoElaboracao: this.codigoLaudo
                    };

                    doc = Object.assign(doc,this._observacoesSigilo);

                    this.listDocumentos.push(doc);

                    timeline.classificacaoDocumento = '';
                    timeline.tipoDocumento = '';
                    timeline.documento = '';
                }
            },
            async finalizarJuntada() {

            },
            async getClassificacaoDocumento() {
                await axios.post('<?= base_url('EditorLaudo/Editor/getClassificacaoDocumento') ?>').then((resp) => {
                    this.listClassificacaoDocumento = resp.data
                }).catch((e) => {}).finally(() => {

                })
            },
            async getDocumentos() {
                await axios.get('<?= base_url('EditorLaudo/Editor/getDocumentosDetalhesLaudo?codigoTimeLine=') ?>' + this.newTimeLine).then((resp) => {
                    this.listDocumentos = resp.data
                }).catch((e) => {}).finally(() => {
                    //console.log(this.listDocumentos)
                })
            },
            getClassificacaoDocumentoTipoDocumento() {
                var formData = new FormData();
                formData.append("classificacao", this.classificacaoDocumento);
                axios.post('<?= base_url('EditorLaudo/Editor/getClassificacaoDocumentoTipoDocumento') ?>', formData).then((resp) => {
                    this.listTipoDocumentos = resp.data
                }).catch((e) => {}).finally(() => {

                })
            },
            alterarDocumento(codigo, tipo) {
               this.listDocumentos = this.listDocumentos.map((el) => {
                    if(el.codigoLaudoElaboracaoExame == codigo) {
                        el[tipo] = (el[tipo] == 1)? 0 : 1;
                    }
                    return el;
                });
            },
            async sendFiles(){
                if(this.listDocumentos.length != 0){
                    var qtdArquivos = this.listDocumentos.length - 1;
                    var thisFile = this.listDocumentos[qtdArquivos];
                    this.listDocumentos = this.listDocumentos.filter((el) => {
                        return el.codigoLaudoElaboracaoExame != thisFile.codigoLaudoElaboracaoExame
                    });

                    if(this.newTimeLine.length == 0){
                        this.newTimeLine = await axios.post(baseurl + 'HomeCirurgiao/Home/newTimeLine',$.param({
                            codigoLaudoElaboracao: this.codigoLaudo,
                            TipoTime: thisFile.classificacaoDocumento
                        }))
                        .then(response => {
                            return response.data;
                        });
                    }
                    
                    Swal.fire('Aguarde enviando arquivos...')
                    Swal.showLoading()

                    if(thisFile.tipoDocumento.toLowerCase().includes('texto livre')){
                        this.sendTextoLivre(thisFile);
                        return;
                    }

                    var formData = new FormData();
                    $.each(thisFile, (key,val) => {
                        if(key == 'codigoLaudoElaboracaoExame') {
                            return;
                        }
                        formData.append(key, val)
                    })

                    await axios.post(baseurl + 'EditorLaudo/Editor/salvarDocumentos', formData, {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    }).then(async response => {
                        Swal.hideLoading()
                        await this.sendFiles();
                    });
                }else{
                    $("#modalJuntadaDocumentos",this.$el).modal('hide');
                    this.get();
                    this.getDocumentos();
                    Swal.close();
                }
            },
            // FIM JUNTADA

            notificarNow() {
                var formData = new FormData();
                formData.append("codigoLaudo", this.codigoLaudo);
                formData.append("notificarPaciente", this.notificarPaciente);
                formData.append("notificarHospital", this.notificarHospital);
                axios.post('<?= base_url('Notificacoes/prepararNotificacoes') ?>', formData).then((resp) => {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Pronto!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $("#modalNotificar").modal('hide')
                }).catch((e) => {}).finally(() => {

                })
            },

            async alterarStatus() {

                var formData = new FormData();
                formData.append("codigoLaudo", this.codigoLaudo);
                formData.append("situacao", this.Situacao);
                axios.post('<?= base_url('EditorLaudo/Editor/atualizarLaudo') ?>', formData).then((resp) => {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'Pronto!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $("#modalSituacao").modal('hide')
                }).catch((e) => {}).finally(() => {

                })
            },
            arquivarLaudo(codigo){
                if(this.detalhes[0].codigoSituacaoLaudo == '3'){
                    Swal.fire({
                        title: 'Processo já está arquivado.',
                        icon: 'warning'
                    })
                    return;
                }
                Swal.fire({
                    title: 'Arquivar?',
                    text: "Tem certeza que deseja arquivar o laudo?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não',
                }).then(async (result) => {
                    if (result.value) {
                        const { value: text } = await Swal.fire({
                            title: 'Justificativa',
                            input: 'textarea',
                            inputPlaceholder: 'Digite sua justificativa...',
                            inputAttributes: {
                                'aria-label': 'Digite sua justificativa'
                            },
                            showCancelButton: true
                        });

                        if (text) {
                            var data = $.param({
                                codigoLaudoElaboracao: codigo,
                                Justificativa: text
                            });
                            await axios.post('<?= base_url('EditorLaudo/Editor/arquivarLaudo') ?>', data)
                            .then((resp) => {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Laudo arquivado com sucesso.',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                this.get();
                                this.getDados();
                            })
                            .catch((e) => {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Erro ao arquivar o laudo!',
                                })
                            });
                        }
                    }
                })
            },
            async geneatePDF() {
                if (CKEDITOR.instances.observacoesFinais.getData().length != 0) {

                    var pdfContent = CKEDITOR.instances.observacoesFinais.getData();
                    var dados = {
                        codigoLaudoElaboracaoExame: this.listDocumentos.length + 1,
                        codigoLaudoElaboracao: this.codigoLaudo,
                        descricaoTextoLivre: this.descricaoTextoLivre,
                        tipoDocumento:  this.tipoDocumento,
                        classificacaoDocumento: this.classificacaoDocumento
                    };

                    dados = Object.assign(dados,{pdfContent: pdfContent},this.observacoesSigilo);

                    this.observacoesSigilo = Object.assign({},this._observacoesSigilo);

                    this.listDocumentos.push(dados);
                    CKEDITOR.instances.observacoesFinais.setData('');

                    timeline.classificacaoDocumento = '';
                    timeline.tipoDocumento = '';
                    timeline.documento = '';
                }
            },
            async sendTextoLivre(doc){
                var dados = {
                    codigoLaudoElaboracao: this.codigoLaudo,
                    descricaoTextoLivre: this.descricaoTextoLivre,
                    privadoHospital: doc.privadoHospital,
                    privadoPaciente: doc.privadoPaciente,
                    privadoPlano: doc.privadoPlano,
                    privadoFornecedor: doc.privadoFornecedor
                }

                await axios.post(baseurl + 'EditorLaudo/Editor/generatePDF', $.param({
                    pdfContent: doc.pdfContent,
                    dados: dados
                }))
                .then(async response => {
                    await this.sendFiles();
                })
                .catch(error => {
                    if(error.response.data.hasOwnProperty(message)){
                        Swal.fire({
                            icon: 'error',
                            text: error.response.data.message
                        });
                    }
                    this.sendFiles();
                });
            },
            async notificar() {
                $("#modalNotificar").modal()
            },

            modificarSituacaoLaudo() {
                $("#modalSituacao").modal()
            },
            async fecharTimeLine(){
                this.listDocumentos = [];
                await axios.post(baseurl + 'HomeCirurgiao/Home/endTimeLine',$.param({endTime: true}))
                .then(response => {
                    this.newTimeLine = '';
                    this.listDocumentos = [];
                });
            },
            showDescricaoTextoLivre(doc){
                if(doc.descricaoTextoLivre != null){
                    return doc.descricaoTextoLivre;
                }

                return doc.tipoDocumento;
            },
            removerFile(codigo) {
                this.listDocumentos = this.listDocumentos.filter((el) => {
                    return el.codigoLaudoElaboracaoExame != codigo;
                });
            },
        }
    })
</script>
