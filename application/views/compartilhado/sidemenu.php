<div class="navbar-brand">
    <a href="<?= base_url('HomeCirurgiao/Home') ?>">
        <img width="100" src="<?= base_url('newtheme/img/logo.png') ?>">

    </a>
    <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
</div>
<div id="sid" class="sidebar-scroll">
    <div class="user-account">
        <div class="user_div">
            <img src="" class="user-photo">
        </div>
        <div class="dropdown">
            <span>Bem vindo,</span>
            <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown"><strong><?= $this->session->username ?></strong></a>
            <ul style="right: -70px!important" class="dropdown-menu dropdown-menu-right account vivify flipInY">
                <li><a href="page-profile.html"><i class="icon-user"></i>Meu perfil</a></li>
                <li class="divider"></li>
                <li><a href="<?= base_url('Login/destroy') ?>"><i class="icon-power"></i>Sair</a></li>
            </ul>
        </div>
    </div>
    <nav id="left-sidebar-nav" class="sidebar-nav ">
        <ul id="main-menu" class="metismenu">
            <li class="header"></li>
            <li><a href="<?= base_url('HomeCirurgiao/home') ?>" class="nav-link active show"><i class="fas fa-clipboard-list"></i><span>Meus Laudos</span></a></li>
            <li class="active"><a href="<?= base_url('ShareReport/Share') ?>" class="nav-link active show "><i class="fas fa-share-alt-square"></i><span>Laudos Compartilhados</span></a></li>
            <li><a class="nav-link" href="<?= base_url('Pacientes/home') ?>"><i class="icon-user"></i><span>Meus Pacientes </span></a></li>
        </ul>
    </nav>
</div>