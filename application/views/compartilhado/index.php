<?php $this->load->view('app_template/script_interno') ?>

<script src="<?php print base_url('theme/moment.min.js') ?>"></script>
<style>
    .btn-sm {
        cursor: pointer;
    }
</style>
<div id="laudosCompartilhado" class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>Laudos compartilhados comigo</h1>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="<?= base_url('Pacientes/home') ?>" class="btn btn-sm btn-round btn-success" title="Novo laudo"><i class="icon-create"></i> Novo Laudo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-12">


        </div>
        <div class="col-lg-12 b-cyan">
            <div class="card ">
                <div class="tab-content">
                    <div id="e_add">
                        <div class="table-responsive">
                            <table id="tableLaudos" class="table table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>Data compatilhamento</th>
                                        <th>Quem compatilhou</th>                                  
                                        <th>Tratamentos</th>                                  
                                        <th>Usar como modelo</th> 
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr v-for="i in listaCompatilhamento">
                                        <td>
                                            <span>{{formatData(i.dataCompartilhamento)}}</span>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="avtar-pic w35 bg-red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Avatar Name"><span>{{i.emailCirurgiao.substring(0,2).toUpperCase()}}</span></div>
                                                <div class="ml-3">
                                                    <a href="page-invoices-detail.html" title="">{{i.nomeCirurgiao}} </a>
                                                 
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex align-items-center">                                                
                                                <div class="ml-3">
                                                    <a href="page-invoices-detail.html" title="">{{i.tratamentos}} </a>
                                                    <p class="mb-0">{{i.intervencao}}</p>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="text-align: center;">
                                            <button type="button" class="btn btn-sm btn-success m-1" @click="modalClone(i.codigoLaudoElaboracao)" title="Usar este Laudo" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fas fa-recycle"></i> Usar modelo</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->load->view('compartilhado/modalClone') ?>

    </div>
</div>


<?php $this->load->view('compartilhado/script') ?>