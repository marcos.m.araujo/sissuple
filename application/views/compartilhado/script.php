<script>
    laudosCompartilhado = new Vue({
        el: "#laudosCompartilhado",
        data() {
            return {
                codigoCirurgiao: '',
                listPacientes: [],
                codigoLaudoElaboracao: '',
                listaCirurgioes: [],
                listaCompatilhamento: [],

            }
        },
        watch: {

        },
        async mounted() {
            await this.getCompartilhamento()
        },
        methods: {

            async getCompartilhamento() {     
                await  axios.get('<?= base_url('ShareReport/Share/getShereReportModel') ?>').then((resp) => {
                    this.listaCompatilhamento = resp.data;
                    vmGlobal.montaDatatable("#tableLaudos");
                }).catch((e) => {}).finally(() => {                  
                })           
            },

            async modalClone(codigoLaudo) {
                this.codigoLaudoElaboracao = codigoLaudo
                var param = {
                    table: "Cirurgiao",
                    where: {
                        codigoUsuario: '<?= $this->session->codigoUsuario ?>'
                    }
                }
                var valor = await vmGlobal.getFromAPI(param);
                $("#modalClone").modal()
                this.codigoCirurgiao = valor[0].codigoCirurgiao
                this.getPacientes(this.codigoCirurgiao)
            },

            async getPacientes(param) {
                var param = {
                    table: "vPaciente",
                    where: {
                        codigoCirurgiao: param
                    }
                }
                this.listPacientes = await vmGlobal.getFromAPI(param);
            },

            clonar(codigo) {
                window.location = '<?= base_url("EditorLaudo/Editor/usarLaudoModelo?q=") ?>' + this.codigoLaudoElaboracao + '&p=' + codigo;
            },
            formatData(param) {
                let val = moment(param)
                return val.format('DD/MM/YYYY H:mm:ss')
            },


        }
    })
</script>