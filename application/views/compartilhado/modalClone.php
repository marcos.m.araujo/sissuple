<div class="modal fade bd-example-modal-lg" tabindex="-1" id="modalClone" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Selecione um paciente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table style="text-align: center;" class="table table-hover table-custom spacing8">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>Editar Laudo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in listPacientes">
                                <td class="w60">
                                    <img  v-if="i.sexo == 'F'"src="<?= base_url('newtheme/img/paciente_f.png') ?>" data-toggle="tooltip" data-placement="top" title="" alt="Avatar" class="w35 rounded" data-original-title="Avatar Name">
                                    <img  v-else src="<?= base_url('newtheme/img/paciente_m.png') ?>" data-toggle="tooltip" data-placement="top" title="" alt="Avatar" class="w35 rounded" data-original-title="Avatar Name">
                                </td>
                                <td>
                                    <a href="javascript:void(0);" title="">{{i.nomePaciente}}</a>
                                    <div>{{i.email}}</div>
                                </td>
                                <td>
                                    <button @click="clonar(i.codigoPaciente)" type="button" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-check"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>