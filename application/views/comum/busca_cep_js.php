<script>

    function limpa_formulário_cep(campoLogradouro, campoMunicipio, campoBairro, campoCodigoBaseUF) {
        // Limpa valores do formulário de cep.
        campoLogradouro.val("");
        campoMunicipio.val("");
        campoBairro.val("");
        campoCodigoBaseUF.val("");
    }

    function buscaCEPCorreios(campoCEP, campoLogradouro, campoMunicipio, campoBairro, campoCodigoBaseUF) {
        //Nova variável "cep" somente com dígitos.
        var cep = campoCEP.val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
//    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if (validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            campoLogradouro.val("...");
            campoMunicipio.val("...");
            campoBairro.val("...");
            campoBairro.val("...");


            //Consulta o webservice viacep.com.br/
            $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    campoLogradouro.val(dados.logradouro);
                    campoBairro.val(dados.bairro);
                    campoMunicipio.val(dados.localidade);


                    if (campoCodigoBaseUF[0].tagName == "INPUT") {
                        campoCodigoBaseUF.val(dados.uf);
                    } else {
                        campoCodigoBaseUF.each(function () {
                            if ($(this).text() == dados.uf) {
                                $(this).attr("selected", "selected");
                                return;
                            }
                        });
                    }
                    
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    limpa_formulário_cep(campoLogradouro, campoMunicipio, campoBairro, campoCodigoBaseUF);

                    alert("CEP não encontrado.");
                }
            });
        } //end if.
        else {
            //cep é inválido.
            limpa_formulário_cep(campoLogradouro, campoMunicipio, campoBairro, campoCodigoBaseUF);
            if(cep.length > 8){
                alert("Formato de CEP inválido.");
            }
        }

    }

    const swap = function(json){
        var ret = {};
        for(var key in json){
            ret[json[key]] = key;
        }
        
        return ret;
    }
    
    const ufs = {
        "1": "AC",
        "2": "AL",
        "3": "AP",
        "4": "AM",
        "5": "BA",
        "6": "CE",
        "7": "DF",
        "8": "ES",
        "9": "GO",
        "10": "MA",
        "11": "MT",
        "12": "MS",
        "13": "MG",
        "14": "PA",
        "15": "PB",
        "16": "PR",
        "17": "PE",
        "18": "PI",
        "19": "RJ",
        "20": "RN",
        "21": "RS",
        "22": "RO",
        "23": "RR",
        "24": "SC",
        "25": "SP",
        "26": "SE",
        "27": "TO"
    }
    const validarCNPJ = function(cnpj) {
        
        cnpj = cnpj.replace(/[^\d]+/g,'');

        if(cnpj == '') return false;
        
        if (cnpj.length != 14)
            return false;

        // Elimina CNPJs invalidos conhecidos
        if (cnpj == "00000000000000" || 
            cnpj == "11111111111111" || 
            cnpj == "22222222222222" || 
            cnpj == "33333333333333" || 
            cnpj == "44444444444444" || 
            cnpj == "55555555555555" || 
            cnpj == "66666666666666" || 
            cnpj == "77777777777777" || 
            cnpj == "88888888888888" || 
            cnpj == "99999999999999")
            return false;
            
        // Valida DVs
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0,tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
            
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
                
        return true; 
    }
    


</script>