<html>
<head>
    <style>
        /* @page {
            size: auto;
            even-header-name: html_MyHeader1;
        } */
        .tablePDF {
            padding: 0 0 0 0;
            margin: 0 auto;
        }
        .tablePDF > td{
            text-align: left;                      
            padding-left: 3px; 
        }
        .td{
            width: 150px;
        }
        .td1{
            width: 100px;
        }
        .nomeCirurgiao{
            color:#084B8A;
            font-size: 14pt;
            font-weight: bold;
        }
        .dadosCabecalho{
            color:#084B8A;  
            font-size: 10pt;
        }
    </style>
</head>
<body>
    <htmlpageheader name="firstpage" style="display: none;">
        <h5 style="color:#084B8A; margin: 0 0 0 0; padding: 0 0 0 0;">LAUDO BUCOMAXILOFACIAL N.º: <?= $dadosElaboracaoLaudo['codigoLaudoElaboracao'] ?> - Paciente: <?= $dadosElaboracaoLaudo['nomePaciente'] ?> - <?= date("d/m/Y") ?> - Página: {PAGENO} </h5>
        <hr style="margin: 0 0 0 0; padding: 0 0 0 0;">
        <table style='width:100%;' class="tablePDF" cellspacing="0" cellpadding="0">
            <tr>
                <td style='width:100%'>
                    <p class='nomeCirurgiao'><?= $dadosElaboracaoLaudo['nomePaciente']; ?></p>
                    <p class='dadosCabecalho'>Plano de Saúde: <?= $dadosElaboracaoLaudo['nomePlano']; ?></p>
                    <p class='dadosCabecalho'>Carteirinha: <?= $dadosElaboracaoLaudo['numeroCarteirinha']; ?></p>
                    <p class='dadosCabecalho'>Cirurgião: <?= $dadosElaboracaoLaudo['sexo'] ?> <?= $dadosElaboracaoLaudo['nomeCirurgiao'] ?> <?= $dadosElaboracaoLaudo['sobrenomeCirurgiao']?></p>
                </td>
                <td style='width:0%; text-align: right;'>
                    <div style='width:100%; text-align: right; margin: 0 auto;' >
                        <img style='height: 100px; width: 100px; margin: 0 auto;' src="data:image/jpeg;base64, <?= $qrCode ?>" />
                    </div>
                </td>
            </tr>
        </table>
        <hr style="margin: 0 0 0 0; padding: 0 0 0 0;">
    </htmlpageheader>
    <htmlpageheader name="outraspaginas" style="display:none">
        <h5 style="color:#084B8A">LAUDO BUCOMAXILOFACIAL N.º: <?= $dadosElaboracaoLaudo['codigoLaudoElaboracao'] ?> - Paciente: <?= $dadosElaboracaoLaudo['nomePaciente'] ?> - <?= date("d/m/Y") ?> - Página: {PAGENO} </h5>
    </htmlpageheader>
    <htmlpagefooter name="myFooter1" style="display:none; width:100%">
        <div style="text-align: center; width:100%;">
            <p style="padding: 0 0 0 0; margin: 0 0 0 0;"><?= date('d/m/Y')?></p><br><br>
            <p style="padding: 0 0 0 0; margin: 0 0 0 0;"><?= $dadosElaboracaoLaudo['nomeCirurgiao'] ?> <?= $dadosElaboracaoLaudo['sobrenomeCirurgiao']?></p>
            <p style="padding: 0 0 0 0; margin: 0 0 0 0;"><?= $dadosElaboracaoLaudo['conselhoProfissional']?></p>
        </div>

    </htmlpagefooter>


    <sethtmlpageheader name="firstpage" value="on" show-this-page="1" />
    <sethtmlpageheader name="firstpage" value="on" />
    <!-- <sethtmlpageheader name="outraspaginas" value="on"/> -->
    <sethtmlpagefooter name="myFooter1" value="on" />

    <!-- <hr style="margin: 0 auto; padding: 0 0 0 0;"> -->
    <main>
        <?= $pdfContent?>
    </main>
</body>
</html>