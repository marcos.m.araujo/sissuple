<script>
    var pacientes = new Vue({
        el: "#pacientes",
        data() {
            return {
                dadosPlano: {},
                codigoCirurgiao: '',
                listPacientes: [],
                paciente: {},
                codigoPaciente: '',
                nomePacienteGet: '',
                planos: []

            }
        },
        async mounted() {
            await this.getCodigoCirurgiao();
            $('#novoPlano', this.$el).on('click', () => {
                $('#modalCadastroPlano', this.$el).modal('show');
            })
        },
        methods: {
            async getCodigoCirurgiao() {
                var param = {
                    table: "Cirurgiao",
                    where: {
                        codigoUsuario: '<?= $this->session->codigoUsuario ?>'
                    }
                }
                var valor = await vmGlobal.getFromAPI(param);
                this.codigoCirurgiao = valor[0].codigoCirurgiao
                this.getPacientes(this.codigoCirurgiao)
            },
            async getPlanos() {
                var param = {
                    table: 'Plano',
                    orderBy: ['nomePlano ASC']
                }
                this.planos = await vmGlobal.getFromAPI(param)
            },
            async getPacientes(param) {
                this.listPacientes = [];
                await vmGlobal.montaDatatable("#tblMeusPacientes");                   
                var param = {
                    table: "vPaciente",
                    where: {
                        codigoCirurgiao: param,
                        pacienteAtivo: 1,
                    }
                }
                this.listPacientes = await vmGlobal.getFromAPI(param);
                await vmGlobal.montaDatatable("#tblMeusPacientes");                   
            },
            async getPaciente(param) {
                this.codigoPaciente = param
                var param = {
                    table: "vPaciente",
                    where: {
                        codigoPaciente: param
                    }
                }
                var dados = await vmGlobal.getFromAPI(param);
                this.paciente = dados[0]
                $("#novoPaciente").modal()
                await this.getPlanos()
            },
            async cadastrar() {
                if (this.codigoPaciente == '') {
                    this.paciente.codigoCirurgiao = this.codigoCirurgiao
                    var param = {
                        table: "Paciente",
                        data: {
                            nomePaciente: this.paciente.nomePaciente,
                            sexo: this.paciente.sexo,
                            email: this.paciente.email,
                            telefone: this.paciente.telefone,
                            nomeMaePaciente: this.paciente.nomeMaePaciente,
                            dataNascimento: this.paciente.dataNascimento,
                            CPF: this.paciente.CPF_,
                            codigoCirurgiao: this.paciente.codigoCirurgiao,
                            pacienteAtivo: 1,
                            convenio: this.paciente.convenio,
                            numeroCarteirinha: this.paciente.numeroCarteirinha,
                            dataInicioContratoConvenio: this.paciente.dataInicioContratoConvenio
                        },
                    }
                    await vmGlobal.insertFromAPI(param)
                    await this.getPacientes(this.codigoCirurgiao)
                    $("#novoPaciente").modal('hide')
                    this.limparFormModal()
                } else {
                    this.update()
                }
            },

            limparFormModal() {
                this.paciente.nomePaciente = ''
                this.paciente.sexo = ''
                this.paciente.email = ''
                this.paciente.telefone = ''
                this.paciente.nomeMaePaciente = ''
                this.paciente.dataNascimento = ''
                this.paciente.CPF_ = ''
                this.paciente.codigoCirurgiao = ''
                this.paciente.convenio = ''
                this.paciente.numeroCarteirinha = ''
                this.paciente.dataInicioContratoConvenio
            },
            async buscaPaciente() {
                var param = {
                    table: "vPaciente",
                    where: {
                        codigoCirurgiao: this.codigoCirurgiao
                    },
                    like: {
                        nomePaciente: this.nomePacienteGet
                    }
                }
                this.listPacientes = await vmGlobal.getFromAPI(param);
            },
            async update() {
                var param = {
                    table: "Paciente",
                    data: {
                        nomePaciente: this.paciente.nomePaciente,
                        sexo: this.paciente.sexo,
                        email: this.paciente.email,
                        telefone: this.paciente.telefone,
                        nomeMaePaciente: this.paciente.nomeMaePaciente,
                        dataNascimento: this.paciente.dataNascimento,
                        CPF: this.paciente.CPF_,
                        convenio: this.paciente.convenio,
                        numeroCarteirinha: this.paciente.numeroCarteirinha,
                        dataInicioContratoConvenio: this.paciente.dataInicioContratoConvenio
                    },
                    where: {
                        codigoPaciente: this.codigoPaciente
                    }
                }
                await vmGlobal.updateFromAPI(param)
                await this.getPacientes(this.codigoCirurgiao)
                $("#novoPaciente").modal('hide')
                this.codigoPaciente = ''
            },
            async disablePaciente(codigo) {
                var param = {
                    table: "Paciente",
                    data: {
                        pacienteAtivo: 0
                    },
                    where: {
                        codigoPaciente: codigo
                    }
                }
                await vmGlobal.updateFromAPI(param)
                await this.getPacientes(this.codigoCirurgiao)
            },
            novoLaudo(codigo) {
                window.location = '<?= base_url("EditorLaudo/Editor/NovoLaudo?codigo=") ?>' + codigo;
            },
            async salvarPlano() {
                var param = {
                    table: 'Plano',
                    data: this.dadosPlano
                }
                await vmGlobal.insertFromAPI(param)
                await $("#modalCadastroPlano", this.$el).modal('hide')
                this.dadosPlano = {}
                await this.getPlanos()
            }
        },
    })
</script>