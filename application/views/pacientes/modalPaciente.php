<div class="modal fade" id="novoPaciente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Novo Paciente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nome</label>
                        <input v-model="paciente.nomePaciente" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Sexo</label>
                        <select v-model="paciente.sexo" class="form-control">
                            <option value="M">M</option>
                            <option value="F">F</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">E-email</label>
                        <input v-model="paciente.email" type="email" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Telefone</label>
                        <input v-model="paciente.telefone" onkeypress="formatar(this, '00-00000000')" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nome da mãe</label>
                        <input v-model="paciente.nomeMaePaciente" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Data nascimento</label>
                        <input v-model="paciente.dataNascimento" type="date" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">CPF</label>
                        <input v-model="paciente.CPF_" maxlength="14" onkeypress="formatar(this, '000.000.000-00')" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Plano de saúde: <a href="javascript:void(0);" class="btn btn-sm btn-success" id="novoPlano" title="Cadastrar Novo Plano."><i class="fa fa-plus"></i> </a></label>
                        <select v-model="paciente.convenio" class="form-control">
                            <option v-for="p in planos" :value="p.CodigoPlano">{{p.nomePlano}}</option>
                       
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Número da carteirinha</label>
                        <input v-model="paciente.numeroCarteirinha" type="text" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Data início vigência do plano</label>
                        <input v-model="paciente.dataInicioContratoConvenio" type="date" class="form-control" id="recipient-name">
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button" @click="cadastrar" class="btn btn-success">Salvar</button>
            </div>
        </div>
    </div>
</div>