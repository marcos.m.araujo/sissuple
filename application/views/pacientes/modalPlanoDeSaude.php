<div class="modal" id="modalCadastroPlano" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-info">
                    <h5 class="modal-title">Cadastro de Plano de Saúde</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <select class="form-control" v-model="dadosPlano.UF">
                                <option value="NA">Nacional</option>
                                <option value="AC">AC</option>
                                <option value="AL">AL</option>
                                <option value="AM">AM</option>
                                <option value="AP">AP</option>
                                <option value="BA">BA</option>
                                <option value="CE">CE</option>
                                <option value="DF">DF</option>
                                <option value="ES">ES</option>
                                <option value="GO">GO</option>
                                <option value="MA">MA</option>
                                <option value="MG">MG</option>
                                <option value="MS">MS</option>
                                <option value="MT">MT</option>
                                <option value="PA">PA</option>
                                <option value="PB">PB</option>
                                <option value="PE">PE</option>
                                <option value="PI">PI</option>
                                <option value="PR">PR</option>
                                <option value="RJ">RJ</option>
                                <option value="RN">RN</option>
                                <option value="RS">RS</option>
                                <option value="RO">RO</option>
                                <option value="RR">RR</option>
                                <option value="SC">SC</option>
                                <option value="SE">SE</option>
                                <option value="SP">SP</option>
                                <option value="TO">TO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input title="Digite o cidade" v-model="dadosPlano.cidade" type="text" class="form-control" placeholder="Cidade ">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input title="Digite o Nome do Plano" v-model="dadosPlano.nomePlano" type="text" class="form-control" placeholder="Nome do Plano *">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <input title="Digite o Telefone" maxlength="12" onkeypress="formatar(this, '00-0000-0000')" v-model="dadosPlano.telefoneFixo" type="text" class="form-control" placeholder="Telefone">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                    <button @click="salvarPlano" type="button" class="btn btn-primary">Salvar mudanças</button>
                </div>
            </div>
        </div>
    </div>