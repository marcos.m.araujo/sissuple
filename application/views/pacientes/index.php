<?php $this->load->view('app_template/script_interno') ?>
<style>
    #novoLaudo:hover{
        font-weight: bold;
        color: red
    }
</style>
<div id="pacientes" class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h2>Pacientes</h2>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Selecione um paciente para iniciar a confecção do laudo</a></li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="javascript:void(0);" data-toggle="modal" data-target="#novoPaciente" @click="getPlanos()" class="btn btn-sm btn-success btn-round" title="Themeforest">Novo Paciente</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <!-- <div class="col-12">
            <div class="card">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-112 col-md-12 col-sm-12">
                            <div class="input-group">
                                <input type="text" @keydown="buscaPaciente" v-model="nomePacienteGet" class="form-control" placeholder="Pesquisa por nome">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div v-for="i in listPacientes" class="col-lg-3 col-md-4 col-sm-6"> -->
            <!-- <div class="card c_grid c_yellow">
                <div class="body text-center ribbon">
                <a href="#" id="novoLaudo" title="Elaborar novo laudo" @click="novoLaudo(i.codigoPacienteCRIP)" class="ribbon-box green">Novo laudo</a>
                    <div class="circle">
                        <img v-if="i.sexo == 'F'" class="rounded-circle" src="<?= base_url('newtheme/img/paciente_f.png') ?>" alt="">
                        <img v-else class="rounded-circle" src="<?= base_url('newtheme/img/paciente_m.png') ?>" alt="">
                    </div>
                    <h6 class="mt-3 mb-0">{{i.nomePaciente}}</h6>
                    <span>{{i.email}}</span>
                    <br>
                    <span>{{i.nomePlano}}</span>
                    <br>
                    <br>
                    <button @click="getPaciente(i.codigoPaciente)" class="btn  btn-outline-info btn-sm">Ver detalhes</button>
                    <button  class="btn  btn-outline-danger btn-sm" @click="disablePaciente(i.codigoPaciente)">Deletar</button>

                </div>
            </div> -->
        <!-- </div> -->
        <div class="col-md-12 table-responsive">
            <table class="table text-center" id="tblMeusPacientes">
                <thead>
                    <tr>
                        <th>
                            Paciente
                        </th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Plano</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="i in listPacientes">
                        <td>
                            <div class="circle mr-2 align-middle" style="float: left;">
                                <img v-if="i.sexo === 'F'" class="rounded-circle" src="<?= base_url('newtheme/img/paciente_f.png') ?>" alt="" width="25px" height="25px">
                                <img v-else class="rounded-circle" src="<?= base_url('newtheme/img/paciente_m.png') ?>" alt="" width="25px" height="25px">
                            </div>
                            <div class="d-flex justify-content-start align-middle ml-3">
                                {{i.nomePaciente}}
                            </div>
                        </td>
                        <td>{{i.email}}</td>
                        <td>{{i.telefone}}</td>
                        <td>{{i.nomePlano}}</td>
                        <td>
                            <button  @click="novoLaudo(i.codigoPacienteCRIP)" class="btn btn-sm btn-default m-1" title="Novo Laudo" data-toggle="tooltip" data-placement="top" data-original-title="Novo Laudo"><i class="fas fa-notes-medical text-success"></i></button>
                            <button  @click="getPaciente(i.codigoPaciente)" class="btn btn-sm btn-default m-1" title="Ver Detalhes" data-toggle="tooltip" data-placement="top" data-original-title="Detalhes"><i class="fas fa-user-edit text-info"></i></button>
                            <button class="btn btn-sm btn-default m-1" @click="disablePaciente(i.codigoPaciente)" title="Remover Paciente" data-toggle="tooltip" data-placement="top" data-original-title="Remover"><i class="text-danger icon-trash"></i></button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <?php $this->load->view('pacientes/modalPaciente') ?>
    <?php $this->load->view('pacientes/modalPlanoDeSaude') ?>
  
</div>

<?php $this->load->view('pacientes/script') ?>