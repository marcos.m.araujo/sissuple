<!doctype html>
<html lang="en">

<head>
    <title>eBuco | Login</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta name="description" content="Oculux Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
    <meta name="author" content="GetBootstrap, design by: puffintheme.com">

    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="<?= base_url('app_theme/vendor/bootstrap/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('app_theme/vendor/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('app_theme/vendor/animate-css/vivify.min.css') ?>">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="<?= base_url('app_theme/assets/css/site.min.css') ?>">

</head>

<body class="theme-cyan font-montserrat">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="bar1"></div>
            <div class="bar2"></div>
            <div class="bar3"></div>
            <div class="bar4"></div>
            <div class="bar5"></div>
        </div>
    </div>

    <div class="auth-main2 particles_js light">
        <div class="auth_div vivify fadeInTop">
            <div class="card">
                <div class="body">
                    <div id="logo" class="login-img m-t-80" style="text-align: center; margin-left: 30px">
                        <img class="img-fluid" src="<?= base_url('newtheme/img/logo.png') ?>" />
                        <br>
                        <p style="color: black; font-size: 16pt">Plataforma de auxílio a cirurgiões<br> na confecção e controle de laudos.</p>
                    </div>
                    <div id="disabl" class="bg-warning " style="height: 400px; width: 2px"></div>
                    <form class="form-auth-small" id="loginForm">
                        <div class="mb-3">
                            <p id="login" class="lead text-dark">Login</p>
                            <?= $this->session->cadastroRealizado . '<br><br>' ?>
                        </div>
                        <div class="form-group">
                            <label for="signin-email" class="control-label sr-only">Email</label>
                            <input type="email" class="form-control round" id="email" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="signin-password" class="control-label sr-only">Password</label>
                            <input type="password" class="form-control round" id="senha" name="senha" value="thisisthepassword" placeholder="senha">
                        </div>

                        <button type="submit" class="btn btn-warning btn-round btn-block">ENTRAR</button>
                        <div class="mt-4">
                            <span><a href="javascript:void(0)" id="recovery">Esqueci minha senha?</a></span><br>
                            <!-- <span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="page-forgot-password.html">Forgot password?</a></span> -->
                            <span>Não tem conta? <a href="<?= base_url() ?>Site/cadastroCirurgiao">Cadastra-se</a></span>
                        </div>
                    </form>
                    <div class="pattern">
                        <span class="red"></span>
                        <span class="indigo"></span>
                        <span class="blue"></span>
                        <span class="green"></span>
                        <span class="orange"></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="particles-js"></div>
    </div>
    <!-- END WRAPPER -->

    <div class="modal fade bg-info" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form method="POST" action="<?= base_url() ?>Login/recuperaSenha">
                <div class="modal-content">
                    <div style="text-align: center; align-items: center;" class="modal-body">
                        <span>Solicitação de recuperação de senha</span><br>

                        <div style="margin-top: 100px;" class="wrap-input100 validate-input" data-validate="Valid email is: a@b.c">
                            <input required id="email" class="input100" type="text" name="email" placeholder="Informe seu e-mail.">
                            <label class="focus-input100" data-placeholder="Email"></label>
                        </div>
                        <br>
                        <button style="margin-top: 100px;" type="submit" class="btn btn-outline-warning">Recuperar senha</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <?php $this->load->view('login/login_view_js.php'); ?>
    <script src="<?= base_url('app_theme') ?>/bundles/libscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme') ?>/bundles/vendorscripts.bundle.js"></script>
    <script src="<?= base_url('app_theme') ?>/bundles/mainscripts.bundle.js"></script>
    <script>
        $('#recovery').on('click', function() {
            $('.modal').modal('show');
        });

        $(window).bind("resize", function() {
            if ($(this).width() < 900) {
                $("#disabl").css('display', 'none');
                $("#login").css('display', 'none');
                $("#logo").css('margin-left', '0px');
            }else{
                $("#disabl").css('display', 'block');
                $("#login").css('display', 'block');
                $("#logo").css('margin-left', '30px');
            }
        })
    </script>
</body>

</html>