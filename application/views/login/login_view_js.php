<script src="<?php print base_url('newtheme/') ?>jquery-3.3.1.min.js"></script>
<script>
    var baseURL = "<?php print base_url() ?>"
    $('#loginForm').submit(function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            url: baseURL + 'Login/login',
            method: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                console.log(data.ativo)
                if (data.ativo == null) {
                    alert("usuario ou senha incorreto");
                } else if (data.ativo == 0) {
                    window.location = baseURL + "Login/bemVindo?nome=" + data.nome + "&email=" + data.email + "&codigoUsuario=" + data.codigoUsuario;
                } else if (data.ativo == 1) {
                    window.location = baseURL + "HomeCirurgiao/Home";
                }
            },
            error: function (data) {
            }
        })
    })

    $(document).ready(function () {
        var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        var isFirefox = typeof InstallTrigger !== 'undefined'; // Firefox 1.0+
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        var isChrome = !!window.chrome && !isOpera; // Chrome 1+
        var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6
        if (isOpera) {
            alert('Algumas funcionalidades do eBuco podem não funcionar corretamente neste navegador. Sugestões (Chrome, Firefox ou IE')
        }
        if (isSafari) {
            alert('Algumas funcionalidades do eBuco podem não funcionar corretamente neste navegador. Sugestões (Chrome, Firefox ou IE')
        }
    })

</script>

