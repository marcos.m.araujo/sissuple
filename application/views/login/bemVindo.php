<!DOCTYPE html>
<html lang="en">
    <head>
        <title>ebuco | Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="<?= base_url('newtheme/') ?>login/images/icons/ebuco.ico"/>     
        <link rel="stylesheet" type="text/css" href="<?= base_url('newtheme/') ?>login/css/main.css">
        <style>
            #centro {
                width: 100%;
                height: 50px;
                line-height: 50px;
                text-align:center;               
                color: white;
                position: absolute;
                top: 40%; 
                margin-top: -25px; 
            }
            #centro span{
                font-size: 10pt;
                margin-top: -80px;
                color: #000000;
            }
            #token{
                height: 40px;
                border-radius: 50px;
                text-align: center;
            }
            #btn{
                background-color: green;
                height: 40px;
                border-radius: 50px;
                width: 120px;
            }
        </style>
    </head>
    <body>
        <div id="centro">
            <form method="POST" action="<?= base_url() ?>Cirurgiao/Cirurgiao/ativarToken">
                Bem vindo Dr. <?= $this->session->nome?>! 
                <br>
                <span>Confirme o código de ativação enviado para o email cadastrado 
                    <?php
                    if (!empty($this->session->email)) {
                        echo $this->session->email ;
                    }
                    ?>

                </span><br>
                <span style="color: red; font-size: 14pt;">
                    <?php if (!empty($confirmacao)) { ?>
                        <?= $confirmacao ?><br>
                    <?php } ?>

                </span>
                <input type="hidden" name="codigoUsuario" value="<?php
                if (!empty($this->session->codigoUsuario)) {
                    echo $this->session->codigoUsuario;
                }
                ?>">
                <input required="true" id="token" type="text" name="token"><br>
                <button id="btn" type="submit">Confirmar</button>
            </form>


            <form method="POST" action="<?= base_url() ?>Login/reenviarEmail/">
                <span style="color: red; font-size: 14pt;">
                    <?php if (!empty($confirmacaoReenvio)) { ?>
                        <?= $confirmacaoReenvio ?><br>
                    <?php } ?>

                </span>
                <input type="hidden" name="codigoUsuario" value="<?php
                if (!empty($this->session->codigoUsuario)) {
                    echo $this->session->codigoUsuario;
                }
                ?>">   
                <input type="hidden" name="email" value="<?php
                if (!empty($this->session->email)) {
                    echo $this->session->email;
                }
                ?>">
                <button style="color:#fff" type="submit">Reenviar</button>
            </form>

        </div>
    </body>
</html>

