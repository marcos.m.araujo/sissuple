
<html>
    <head>
        <meta charset="utf-8">
        <title>ebuco | Registro</title>
        <link rel="icon" type="image/png" href="<?= base_url('newtheme/') ?>login/images/icons/ebuco.ico"/>
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- Font-->
        <link rel="stylesheet" type="text/css" href="<?= base_url('newtheme/') ?>registro/css/opensans-font.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('newtheme/') ?>registro/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
        <!-- Main Style Css -->
        <link rel="stylesheet" href="<?= base_url('newtheme/') ?>registro/css/style.css"/>
        <style>
            .termoServico{
                background-color: #EFF5FB;
                height: 500px;
                padding: 10px;
                overflow-y: scroll; 
                width: 100%;
            }
            .dvm-button {
                margin-top: 10px;
                color: #FFF;
                background-color: green;
                border: none;
                border-radius: 60px;
                height: 50px;
                line-height: 30px;
                padding: 0 20px;
                text-transform: uppercase;
                font-weight: bold;
                cursor: pointer;
                margin-left: 90%;
            }
        </style>
    </head>
    <body>
        <div class="page-content">
            <div class="form-v1-content">
                <div class="form-register">
                    <div class="wizard-form">
                        <form id="formtotal"  enctype="multipart/form-data" method="POST" action="<?= base_url() ?>Cirurgiao/Cirurgiao/cadastrarCirurgiao" >
                            <!-- SECTION 1 -->
                            <h2>
                                <p class="step-icon"><span>01</span></p>
                                <span class="step-text">Informações Pessoais</span>
                            </h2>
                            <section>
                                <div class="inner">
                                    <div class="wizard-header">
                                        <h3 class="heading">Informações Pessoais</h3>
                                        <p>Por favor, insira suas informações e prossiga para a próxima etapa para que possamos criar sua conta.</p>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder">
                                            <fieldset>
                                                <legend>Primeiro Nome</legend>
                                                <input type="text" class="form-control" id="nomeCirurgiao" name="nomeCirurgiao" placeholder="Primeiro nome" required>
                                            </fieldset>
                                        </div>
                                        <div class="form-holder">
                                            <fieldset>
                                                <legend>Sobrenome</legend>
                                                <input type="text" class="form-control" id="sobrenomeCirurgiao" name="sobrenomeCirurgiao" placeholder="Sobrenome" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <fieldset>
                                                <legend>CPF</legend>
                                                <input type="text" class="form-control" id="cpf" name="cpf" placeholder="CPF" required onchange="validaCPF()" maxlength="14" onkeypress="formatar(this, '000.000.000-00')" >
                                            </fieldset>
                                        </div>
                                        <div class="form-holder form-holder-2">
                                            <fieldset>
                                                <legend>Email</legend>
                                                <input type="email" id="email" onblur="emailDuplicado()" name="email" class="form-control" pattern="[^@]+@[^@]+.[a-zA-Z]{2,6}" placeholder="example@email.com" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div style="text-align: center" class="form-row">
                                        <span style="color: red">
                                            <?= $validacaoSenha ?>
                                        </span>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <fieldset id="fildSenha">                                                 
                                                <legend >Senha</legend>
                                                <input type="password" class="form-control" id="senha" name="senha" placeholder="*****" required  >
                                            </fieldset>
                                        </div>
                                        <div class="form-holder form-holder-2">
                                            <fieldset id="fildSenhaC">
                                                <legend>Confirma senha</legend>
                                                <input type="password" id="confirmaSenha" onblur="validaSenha()" name="confirmaSenha" class="form-control" placeholder="*****" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <fieldset>
                                                <legend>Telefone celular</legend>
                                                <input type="text" onkeypress="formatar(this, '00 00000-0000')" class="form-control" id="telefoneCirurgiao" name="telefoneCirurgiao" placeholder="61 9999-7777" required>
                                            </fieldset>
                                        </div>
                                        <div class="form-holder form-holder-2">
                                            <select style="margin-top:8px" id="sexo" name="sexo" class="form-control select2 inpt" >
                                                <option value="">Sexo</option>
                                                <option value="F">F</option>
                                                <option value="M">M</option>
                                            </select>
                                        </div>
                                    </div>                                    
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">                                             
                                            <select  id="codigoConselhoProfissional" name="codigoConselhoProfissional" class="form-control select2 inpt" >
                                                <?php if (!empty($dadosCirurgiao[0]['codigoConselhoProfissional'])) { ?>
                                                    <option value="<?= $dadosCirurgiao[0]['codigoConselhoProfissional'] ?>"><?= $dadosCirurgiao[0]['sigla'] ?></option>
                                                <?php } ?>

                                                <?php if (isset($conselhoProfissional) && !empty($conselhoProfissional)): ?>
                                                    <?php foreach ($conselhoProfissional as $value): ?>
                                                        <option value="<?= $value->codigoConselhoProfissional ?>"><?= $value->conselhoProfissional; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>                                       
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <fieldset>
                                                <legend>N. Conselho Profissional</legend>
                                                <input  type="number" class="form-control" id="numeroConselhoProfissional" name="numeroConselhoProfissional" placeholder="123456789" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">  
                                            <select id="codigoEstado" name="codigoEstado" class="" style="width: 100%;" title="UF Conselho Profissional" >
                                                <?php if (!empty($dadosCirurgiao[0]['codigoEstado'])) { ?>
                                                    <option value="<?= $dadosCirurgiao[0]['codigoEstado'] ?>"><?= $dadosCirurgiao[0]['estado'] ?></option>
                                                <?php } ?>
                                                <?php if (isset($estados) && !empty($estados)): ?>
                                                    <?php foreach ($estados as $value): ?>
                                                        <option value="<?= $value->codigoEstado ?>"><?= $value->estado; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>                                        
                                        </div>
                                    </div>
                    
                                </div>
                            </section>
                            <!-- SECTION 2 -->
                            <h2>
                                <p class="step-icon"><span>02</span></p>
                                <span class="step-text">Minha Clínica</span>
                            </h2>
                            <section>
                                <div class="inner">
                                    <div class="wizard-header">
                                        <h3 class="heading">Minha Clínica</h3>
                                        <p>Por favor, insira informações sobre a sua clínica e prossiga para a próxima etapa.</p>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2 ">
                                            <fieldset>
                                                <legend>Nome da clínica</legend>
                                                <input type="text" class="form-control" name="nomeClinica" id="nomeClinica" placeholder="Nome da clínica" required>
                                            </fieldset>
                                        </div>                                       
                                    </div>
                                    <div class="form-row">                                      
                                        <div class="form-holder form-holder-2">
                                            <fieldset>
                                                <legend>CNPJ</legend>
                                                <input type="text" onkeypress="formatar(this, '00.000.000/0000-00')" class="form-control" id="cnpj" name="cnpj" max="18" name="last-name" placeholder="CNPJ" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder">
                                            <fieldset>
                                                <legend>CEP</legend>
                                                <input type="text" onblur="buscaCEP()" class="form-control" name="cep" id="cepClinica" placeholder="" required>
                                            </fieldset>
                                        </div>
                                        <div class="form-holder">
                                            <fieldset>
                                                <legend>UF</legend>
                                                <input type="text" class="form-control"  name="uf" id="uf" placeholder="" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-row">                                       
                                        <div class="form-holder form-holder-2">
                                            <fieldset>
                                                <legend>Município</legend>
                                                <input type="text" class="form-control" id="municipio" name="municipio" id="last-name" name="last-name" placeholder="Município" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder form-holder-2">
                                            <fieldset>
                                                <legend>Endereço</legend>
                                                <input type="text" class="form-control" id="endereco"  name="endereco" placeholder="Endereço" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder">
                                            <fieldset>
                                                <legend>Complemento</legend>
                                                <input type="text" class="form-control" name="complemento" placeholder="Complemento" required>
                                            </fieldset>
                                        </div>
                                        <div class="form-holder form-holder-2">
                                            <fieldset>
                                                <legend>Bairro</legend>
                                                <input type="text" class="form-control" id="bairro" name="bairro" placeholder="Bairro" required>
                                            </fieldset>
                                        </div>                             
                                    </div>
                                    <div class="form-row">
                                        <div class="form-holder  form-holder-2">
                                            <fieldset>
                                                <legend>Telefone</legend>
                                                <input type="text" class="form-control" name="telefoneComercial" id="telefoneComercial"  placeholder="Telefone" required onkeypress="formatar(this, '00 00000-0000')">
                                            </fieldset>
                                        </div>
                                        <div class="form-holder  form-holder-2">
                                            <fieldset>
                                                <legend>Email</legend>
                                                <input type="email" class="form-control" id="emailClinica" name="emailClinica" placeholder="Email" required>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- SECTION 3 -->
                            <h2>
                                <p class="step-icon"><span>03</span></p>
                                <span class="step-text">Termos de Uso</span>
                            </h2>
                            <section>
                                <div class="inner">
                                    <div class="wizard-header">
                                        <h3 class="heading">Termos de Uso</h3>
                                        <p>Por favor leia o termo de servico para a conclusão do cadastro.</p>
                                    </div>
                                    <div class="termoServico">
                                        <?php if (!empty($termo[0]['termo'])) { ?>
                                            <?= $termo[0]['termo'] ?>
                                        <?php } ?>
                                    </div>
                                    <br>
                                    <label> Aceito os termos 
                                        <input onclick="aceite()" type="checkbox" value="1" >
                                    </label>     
                                </div>
                            </section>
                            <button title="salvar" id="btnSubmit" style="display: none; margin-right: 40px" class="dvm-button" type="submit" ><i style="font-size: 14pt" class="zmdi zmdi-check"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<script>
   function aceite(){
        $("#btnSubmit").toggle("slow");
    }
    function validaSenha() {
        if ($('#senha').val() != $('#confirmaSenha').val()) {
            $.notify('senhas não conferem!', 'error');
            $("#fildSenhaC").css('border-color', 'red');
            $("#fildSenha").css('border-color', 'red');
        } else {
            $("#fildSenhaC").css('border-color', '#61b045');
            $("#fildSenha").css('border-color', '#61b045');
        }
    }
</script>
<script src="<?= base_url('newtheme/') ?>/registro/js/jquery-3.3.1.min.js"></script>
<script src="<?= base_url('newtheme/') ?>/registro/js/jquery.steps.js"></script>
<script src="<?= base_url('newtheme/') ?>/registro/js/main.js"></script>
<?php $this->load->view('comum/comum_js'); ?>
<?php $this->load->view('comum/busca_cep_js'); ?>
<?php $this->load->view('html/scripts_view'); ?>
<?php $this->load->view('cirurgiao_old/home_js.php'); ?>
<?php $this->load->view('cirurgiao_old/clinica_js.php'); ?>
