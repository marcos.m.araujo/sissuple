<?php $this->load->view('app_template/script_interno') ?>
<style>
    .btn-sm {
        cursor: pointer;
    }

    ::-webkit-scrollbar-track {
        background: #fff !important;
    }

    ::-webkit-scrollbar {
        width: 2px;
        height: 6px !important;
        background: #17c2d7 !important;
    }

    ::-webkit-scrollbar-thumb {
        background: #17c2d7 !important;
    }

    #tableLaudos td {
        cursor: pointer;
        border: 1px solid #F1F4F6;
    }
    .heightTR {
       height: 160px;
    }
</style>
<div id="laudos" class="container-fluid">
    <div class="block-header">
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                <h1>Meus Laudos</h1>
            </div>
            <div class="col-md-6 col-sm-12 text-right hidden-xs">
                <a href="<?= base_url('Pacientes/home') ?>" class="btn btn-sm btn-round btn-success" title="Novo laudo"><i class="icon-create"></i> Novo Laudo</a>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-12">

            <ul id="configuracao" class="nav nav-tabs2">
                <li class="nav-item "><a class="nav-link active show " @click="Situacao = '2'" data-toggle="tab">Cirurgias em análise</a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" @click="Situacao = '3'">Cirurgias arquivadas</a></li>
            </ul>
        </div>
        <div class="col-lg-12 b-cyan">
            <div class="card ">

                <div class="tab-content">
                    <div class="tab-pane show active" id="e_add">
                        <div id="table-responsive" class="table-responsive">
                            <table id="tableLaudos" class="table table-custom spacing5 mb-0">
                                <thead style="width: 100%">
                                    <tr>
                                        <th>Data</th>
                                        <th>Paciente / Plano</th>
                                        <th>Tratamentos / Intervenções</th>
                                        <th>Última Movimetanção</th>
                                        <th>Ações</th>
                                    </tr>
                                </thead>

                                <tbody id="tBody">
                                    <tr :class="[lista.length == 1 ? 'heightTR': '']" v-for="i in lista">
                                     
                                        <td @click="detalheLaudo(i.codigoLaudoElaboracaoCRIP)">
                                            {{i.dataLaudo}}
                                        </td>
                                        <td @click="detalheLaudo(i.codigoLaudoElaboracaoCRIP)">
                                            <div class="d-flex align-items-center">
                                                <div class="ml-3">
                                                    <a style="cursor: pointer;font-size: 12pt;" title="">{{i.nomePaciente}}</a>
                                                    <p class="mb-0 text-info">{{i.nomePlano}}</p>

                                                </div>
                                            </div>
                                        </td>
                                        <td @click="detalheLaudo(i.codigoLaudoElaboracaoCRIP)" style="width: 300px">
                                            <div style="width: 600px!important;overflow:auto;word-wrap: break-word " class="ml-3">
                                                <a style="cursor: pointer; font-size: 12pt; font-weight: 600" title="">{{i.tratamentos}}</a>
                                                <p class="mb-0 ">{{i.intervencao}}</p>
                                            </div>
                                        </td>
                                        <td @click="detalheLaudo(i.codigoLaudoElaboracaoCRIP)">
                                            <b><span class=" text-info ml-0 p-2 mr-0">{{i.ultimoStatusTime}}</span><b>
                                        </td>
                                        <td class="text-center">

                                            <div  class="btn-group" role="group">
                                                <button id="btnGroupDrop1" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-list-ul"></i>
                                                </button>
                                                <div id="btnGroup" class="dropdown-menu">
                                                    <a class="dropdown-item" @click="abrirLaudoPDF(i.codigoLaudoElaboracaoCRIP)" href="javascript:void(0);"><i class="text-warning far fa-file-pdf"></i> Abrir Laudo PDF</a>
                                                    <a class="dropdown-item" @click="getListCirurgioes(i.codigoCirurgiao, i.codigoLaudoElaboracao)" href="javascript:void(0);"><i class="text-success far fa-share-square"></i> Compartilhar Laudo</a>
                                                    <a class="dropdown-item" @click="modalClone(i.codigoLaudoElaboracao)" href="javascript:void(0);"><i class="fas fa-recycle"></i> Clonar Laudo</a>
                                                    <a class="dropdown-item" v-if="i.codigoSituacaoLaudo != '3'" @click="arquivarLaudo(i.codigoLaudoElaboracaoCRIP)" href="javascript:void(0);"><i class="fas fa-archive"></i> Arquivar Laudo</a>
                                                    <a class="dropdown-item" @click="deletarLaudo(i.codigoLaudoElaboracaoCRIP)" href="javascript:void(0);"><i class="text-danger icon-trash"></i> Excluir Laudo</a>

                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane show" id="e_leave">

                    </div>

                </div>
            </div>
        </div>
        <?php $this->load->view('homeCirurgiao/modalClone') ?>
        <?php $this->load->view('homeCirurgiao/modalCirurgioes') ?>
    </div>
</div>
<?php $this->load->view('homeCirurgiao/script') ?>