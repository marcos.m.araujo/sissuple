<script>
    laudos = new Vue({
        el: "#laudos",
        data() {
            return {
                lista: [],
                Situacao: '2',
                codigoCirurgiao: '',
                listPacientes: [],
                codigoLaudoElaboracao: '',
                listaCirurgioes: [],
                listaCompatilhamento: [],
                ativaTelaCompatilhamento: false
            }
        },
        watch: {
            Situacao(a) {     
                this.get()
            }
        },
        async mounted() {
            await this.get()
     
      
        },
        methods: {
            async get() {                    
                var param = {
                    table: 'Laudos',
                    where: {
                        codigoSituacaoLaudo: this.Situacao,
                        codigoCirurgiao: '<?= $this->session->codigoCirurgiao ?>'
                    }
                }
                this.lista = await vmGlobal.getFromAPI(param)             
                await vmGlobal.montaDatatable("#tableLaudos")                    
               console.log(this.lista);
            },
            detalheLaudo(codigo) {
                window.location.href = 'Home/detalheLaudo?codigoLaudo=' + codigo
            },
            async deletarLaudo(codigo) {
                Swal.fire({
                    title: 'Deletar?',
                    text: "Será excluído tudo referente ao laudo!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não',
                }).then((result) => {
                    if (result.value) {
                        this.deletar(codigo)
                    }
                })
            },
            async deletar(codigo) {
                var formData = new FormData();
                formData.append("codigoElaboracao", codigo);
                axios.post('<?= base_url('EditorLaudo/Editor/excluirLaudo') ?>', formData).then((resp) => {

                }).catch((e) => {}).finally(() => {
                    document.location.reload(true);
                })
            },
            arquivarLaudo(codigo){
                Swal.fire({
                    title: 'Arquivar?',
                    text: "Tem certeza que deseja arquivar o laudo?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não',
                }).then(async (result) => {
                    if (result.value) {
                        const { value: text } = await Swal.fire({
                            title: 'Justificativa',
                            input: 'textarea',
                            inputPlaceholder: 'Digite sua justificativa...',
                            inputAttributes: {
                                'aria-label': 'Digite sua justificativa'
                            },
                            showCancelButton: true
                        });

                        if (text) {
                            var data = $.param({
                                codigoLaudoElaboracao: codigo,
                                Justificativa: text
                            });
                            await axios.post('<?= base_url('EditorLaudo/Editor/arquivarLaudo') ?>', data)
                            .then((resp) => {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Laudo arquivado com sucesso.',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                                this.get();
                            })
                            .catch((e) => {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Erro ao arquivar o laudo!',
                                })
                            });
                        }
                    }
                })
            },
            abrirLaudoPDF(param) {
                window.open("<?= base_url('HomeCirurgiao/Home/openLaudo?codigoLaudoElaboracao=') ?>" + param);
            },
            async modalClone(codigoLaudo) {
                this.codigoLaudoElaboracao = codigoLaudo
                var param = {
                    table: "Cirurgiao",
                    where: {
                        codigoUsuario: '<?= $this->session->codigoUsuario ?>'
                    }
                }
                var valor = await vmGlobal.getFromAPI(param);
                $("#modalClone").modal()
                this.codigoCirurgiao = valor[0].codigoCirurgiao
                this.getPacientes(this.codigoCirurgiao)
            },
            async getPacientes(param) {
                var param = {
                    table: "vPaciente",
                    where: {
                        codigoCirurgiao: param
                    }
                }
                this.listPacientes = await vmGlobal.getFromAPI(param);
            },

            async getCompartilhamento() {
                this.ativaTelaCompatilhamento = true
                var param = {
                    table: "Compartilhamento",
                    where: {
                        email: '<?= $this->session->email ?>'
                    }
                }
                this.listaCompatilhamento = await vmGlobal.getFromAPI(param);        
                vmGlobal.montaDatatable("#tableLaudosComaptilhamento")
            },

            clonar(codigo) {
                window.location = '<?= base_url("EditorLaudo/Editor/usarLaudoModelo?q=") ?>' + this.codigoLaudoElaboracao + '&p=' + codigo;
            },

            async getListCirurgioes(codigoCirurgiao, codigoLaudo) {
                this.codigoCirurgiao = codigoCirurgiao
                this.codigoLaudoElaboracao = codigoLaudo
                $("#modalListaCirurgioes").modal()
                var param = {
                    table: "Cirurgioes",
                }
                this.listaCirurgioes = await vmGlobal.getFromAPI(param);
            },
            compartilharLaudo(email) {

                var formData = new FormData();
                formData.append("emails", email);
                formData.append("codigo", this.codigoLaudoElaboracao);
                formData.append("codigoCirurgiao", this.codigoCirurgiao);
                axios.post('<?= base_url("ShareReport/Share/sendEmailModeloLaudo") ?>', formData).then((resp) => {
                    Swal.fire({
                        position: 'top-end',
                        type: 'success',
                        title: 'Compartilhado!',
                        showConfirmButton: false,
                        timer: 1500
                    })
                    $("#modalListaCirurgioes").modal('hide');
                }).catch((e) => {}).finally(() => {

                })

            }
        }
    })
</script>