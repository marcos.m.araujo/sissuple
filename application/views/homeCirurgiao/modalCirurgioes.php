<div class="modal fade bd-example-modal-lg" tabindex="-1" id="modalListaCirurgioes" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Selecione um cirurgião para compartilhar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table style="text-align: center;" class="table table-hover table-custom spacing8">
                        <thead>
                            <tr>                              
                                <th>Nome</th>
                                <th>Selecione</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="i in listaCirurgioes">                              
                                <td>
                                    <a href="javascript:void(0);" title="">{{i.nomeCirurgiao}} {{i.sobrenomeCirurgiao}}</a>
                                    
                                </td>
                                <td>
                                    <button @click="compartilharLaudo(i.email)" type="button" class="btn btn-success btn-sm" title="Edit"><i class="fa fa-check"></i></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>