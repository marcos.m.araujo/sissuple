<a href="javascript:void(0);" class="megamenu_toggle btn btn-danger"><i class="icon-close"></i></a>
<div class="container">
    <div class="row clearfix">
        <div class="col-12">
            <ul class="q_links">
                <li><a href="app-inbox.html"><i class="icon-envelope"></i><span>Inbox</span></a></li>
                <li><a href="app-chat.html"><i class="icon-bubbles"></i><span>Messenger</span></a></li>
                <li><a href="app-calendar.html"><i class="icon-calendar"></i><span>Event</span></a></li>
                <li><a href="page-profile.html"><i class="icon-user"></i><span>Profile</span></a></li>
                <li><a href="page-invoices.html"><i class="icon-printer"></i><span>Invoice</span></a></li>
                <li><a href="page-timeline.html"><i class="icon-list"></i><span>Timeline</span></a></li>
            </ul>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card">
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-7">
                            <h5 class="mb-0">1,092</h5>
                            <small class="info">Total emails</small>
                        </div>
                        <div class="col-12">
                            <div class="progress progress-xxs mb-0 mt-3">
                                <div class="progress-bar bg-info" data-transitiongoal="87" aria-valuenow="87" style="width: 87%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card">
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-7">
                            <h5 class="mb-0">68</h5>
                            <small class="info">Unread emails</small>
                        </div>
                        <div class="col-12">
                            <div class="progress progress-xxs mb-0 mt-3">
                                <div class="progress-bar bg-danger" data-transitiongoal="22" aria-valuenow="22" style="width: 14%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card">
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-7">
                            <h5 class="mb-0">301</h5>
                            <small class="info">Social emails</small>
                        </div>
                        <div class="col-12">
                            <div class="progress progress-xxs mb-0 mt-3">
                                <div class="progress-bar bg-warning" data-transitiongoal="30" aria-valuenow="30" style="width: 54%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
            <div class="card">
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-7">
                            <h5 class="mb-0">518</h5>
                            <small class="info">Update</small>
                        </div>
                        <div class="col-12">
                            <div class="progress progress-xxs mb-0 mt-3">
                                <div class="progress-bar bg-green" data-transitiongoal="73" aria-valuenow="73" style="width: 54%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-12">
            <ul class="list-group">
                <li class="list-group-item">
                    Anyone send me a message
                    <div class="float-right">
                        <label class="switch">
                            <input type="checkbox" checked="">
                            <span class="slider round"></span>
                        </label>
                    </div>
                </li>
                <li class="list-group-item">
                    Anyone seeing my profile page
                    <div class="float-right">
                        <label class="switch">
                            <input type="checkbox" checked="">
                            <span class="slider round"></span>
                        </label>
                    </div>
                </li>
                <li class="list-group-item">
                    Anyone posts a comment on my post
                    <div class="float-right">
                        <label class="switch">
                            <input type="checkbox">
                            <span class="slider round"></span>
                        </label>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<div id="particles-js"></div>