
<script src="<?= base_url('app_theme/vendor/jquery-3.4.1.min.js') ?>"></script>
<script src="<?= base_url('app_theme/vendor/jquery.dataTables.min.js') ?>"></script>
<script>
    var vmGlobal = new Vue({
        methods: {

            async getFromAPI(params) {
                var output = '';
                await axios.post('<?= base_url('API/get'); ?>', $.param(params))
                    .then((response) => {
                        if (response.data.status == false)
                            throw new Error(response.data.result);

                        output = response.data.result;
                    })
                    .catch((e) => {
                        console.log(e)
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao consultar dados. " + e,
                            showConfirmButton: true,

                        });
                    });
                return output;
            },
            async updateFromAPI(params, file = null, notificar = true) {

                var formData = new FormData();
                formData.append('Dados', JSON.stringify(params));

                if (file !== null) {
                    formData.append(file.name, file.value);
                }

                await axios.post('<?= base_url('API/update'); ?>', formData)
                    .then((response) => {

                        if (response.data.status == false) {
                            throw new Error(response.data.result);
                        }

                        if(notificar){
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                // title: 'Alteração executada com sucesso',
                                showConfirmButton: false,
                                timer: 400
                            })                            
                        }
                        
                        return true;
                    })
                    .catch((e) => {
                        console.log(e)
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao consultar dados. " + e,
                            showConfirmButton: true,

                        });
                    });
            },
            async deleteFromAPI(params, file = null, notificar = true) {

                var formData = new FormData();
                formData.append('Dados', JSON.stringify(params));

                if (file !== null) {
                    formData.append(file.name, file.value);
                }

                await axios.post('<?= base_url('API/delete'); ?>', formData)
                    .then((response) => {

                        if (response.data.status == false) {
                            throw new Error(response.data.result);
                        }

                        if(notificar){
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: 'Delatado',
                                showConfirmButton: false,
                                timer: 400
                            })
                        }

                        return true;
                    })
                    .catch((e) => {
                        console.log(e)
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao consultar dados. " + e,
                            showConfirmButton: true,
                        });
                    });
            },
            async insertFromAPI(params, file = null, notificar = true) {

                var formData = new FormData();
                formData.append('Dados', JSON.stringify(params));

                if (file !== null) {
                    formData.append(file.name, file.value);
                }

                await axios.post('<?= base_url('API/insert'); ?>', formData)
                    .then((response) => {

                        if (response.data.status == false) {
                            throw new Error(response.data.result);
                        }

                        if(notificar){
                            Swal.fire({
                                position: 'center',
                                icon: 'success',
                                // title: 'Alteração executada com sucesso',
                                showConfirmButton: false,
                                timer: 550
                            });
                        }

                        return true;
                    })
                    .catch((e) => {
                        console.log(e)
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar inserir os dados. " + e,
                            showConfirmButton: true,

                        });
                    });
            },
            async deleteFile(path) {
                var params = {
                    path: path
                };
                await axios.post('<?= base_url('API/deleteFile'); ?>', $.param(params))
                    .then((response) => {

                        if (response == false)
                            throw new Error("Arquivo não encontrado");

                        return true;
                    })
                    .catch((e) => {
                        console.log(e)
                        Swal.fire({
                            position: 'center',
                            type: 'error',
                            title: 'Erro!',
                            text: "Erro ao tentar deletar arquivo. " + e,
                            showConfirmButton: true,

                        });
                        return false;
                    });
            },
            // usado apra buscar caracteristicas clincas do laudo
            async getAxios(tela, codigoTratamento) {
                var baseGetURL = "<?= base_url('EditorLaudo/Editor/pull/') ?>"
                var output = '';
                var param = [{
                    tela: tela,
                    codigoTratamento: codigoTratamento
                }]
                var formData = new FormData();
                formData.append("Params", JSON.stringify(param));
                await axios.post(baseGetURL, formData).then((resp) => {
                    output = resp.data
                }).catch((e) => {

                }).finally(() => {

                })
                return output;
            },
            montaDatatable(id, order = []) {
                // $(id).DataTable().clear().draw();
                $(id).DataTable().destroy();
                try {
                    this.$nextTick(function() {
                        $(id).DataTable({
                            "destroy": true,
                            "scrollX": true,
                            "order" : order,
                            dom: 'Bfrtip',
                            buttons: [
                                'excel'
                            ],
                            "bSort": true,
                            "bSortable": true,
                            'autoWidth': false,
                            "oLanguage": {
                                "sEmptyTable": "NENHUM REGISTRO ENCONTRADO!",
                                "sInfo": "Mostrar _START_ até _END_ do _TOTAL_ registros",
                                "sInfoEmpty": "Mostrar 0 até 0 de 0 Registros",
                                "sInfoFiltered": "(Filtrar de _MAX_ total registros)",
                                "sInfoPostFix": "",
                                "sInfoThousands": ".",
                                "sLengthMenu": "Mostrando _MENU_ registros por pagina",
                                "sLoadingRecords": "Carregando...",
                                "sProcessing": "Processando...",
                                "sZeroRecords": "Nenhum registro encontrado",
                                "sSearch": "Pesquisar: ",
                                "oPaginate": {
                                    "sNext": "<i class=\"fas fa-chevron-right\" ></i>",
                                    "sPrevious": "<i class=\"fas fa-chevron-left\" ></i>",
                                    "sFirst": "Primeiro",
                                    "sLast": "Ultimo"
                                },
                                "oAria": {
                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                }
                            },
                        });
                    });
                } catch (err) {
                    console.error(err);
                }
            },
            formatDataHora(param) {
                let val = moment(param)
                return val.format('DD/MM/YYYY H:mm:ss')
            },
        }
    });
</script>