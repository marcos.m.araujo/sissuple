<!DOCTYPE HTML>
<html lang='pt-br'>

<title>eBuco</title>
<meta charset="utf-8">
<meta http-equiv="Content-Language" content="pt-br">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!-- VENDOR CSS -->
<!-- <link rel="stylesheet" href="<?= base_url('app_theme/vendor/bootstrap/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('app_theme/vendor/font-awesome/css/font-awesome.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('app_theme/vendor/animate-css/vivify.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('app_theme/vendor/toastr/toastr.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('app_theme/vendor/jquery-datatable/dataTables.bootstrap4.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('app_theme/vendor/sweetalert/sweetalert.css') ?>">
<link rel="stylesheet" href="<?= base_url('app_theme/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('app_theme/vendor/dropify/css/dropify.min.css') ?>">
<link rel="stylesheet" href="<?= base_url('app_theme/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css') ?>"> -->
<!-- MAIN CSS -->
<!-- <link rel="stylesheet" href="<?= base_url('app_theme/css/site.min.css') ?>"> -->
<link rel="stylesheet" href="<?= base_url('app_theme/build_css/css.global_min.css') ?>">

<!-- fontawesome https://fontawesome.com/icons?d=gallery -->
<!-- <link data-prerender="keep" rel="stylesheet" href="<?php print base_url('newtheme/fontawesome/css/all.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('newtheme/fontawesome/css/brands.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('newtheme/fontawesome/css/fontawesome.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('newtheme/fontawesome/css/regular.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('newtheme/fontawesome/css/solid.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('newtheme/fontawesome/css/svg-with-js.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('newtheme/fontawesome/css/v4-shims.css') ?>">
<link data-prerender="keep" rel="stylesheet" href="<?php print base_url('newtheme/fontawesome/css/v4-shims.min.css') ?>"> -->
<script src="<?= base_url('vue/vue.js') ?>"></script>
<!-- axios -->
<script src="<?= base_url('vue/axios.min.js') ?>"></script>