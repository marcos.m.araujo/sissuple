<!DOCTYPE HTML>
<html lang='pt-br'>
<title>eBuco</title>
<meta charset="utf-8">
<meta http-equiv="Content-Language" content="pt-br">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="<?= base_url('app_theme/build_css/css.global_min.css') ?>">
<script src="<?= base_url('vue/vue.js') ?>"></script>
<script src="<?= base_url('vue/axios.min.js') ?>"></script>