<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('reordenarArrayFiles'))
{
    /**
     * Reordena um array de de varios FILES em arrays associativos
     * @param $file_post
     * @return array
     */
    function reordenarArrayFiles(&$file_post){
        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);

        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }
}

if ( ! function_exists('uploadFile'))
{
    /**
     * @param $file O arquivo
     * @param $newName Novo nome do Aquivo
     * @param $caminho Caminho de Destino do arquivo
     */
    function uploadFile($file, $newName, $caminho){
        $CI =& get_instance();

        $_FILES['file'] = $file;
        $config['upload_path']          = $caminho;
        $config['allowed_types']        = 'pdf|jpg|png';
        $config['file_name'] = $newName;
        $CI->load->library('upload');
        $CI->upload->initialize($config);
        if (!$CI->upload->do_upload('file')){
            error(500, 'Erro! Não foi possivel enviar os anexos');
        }
    }
}

if ( ! function_exists('error'))
{
    /**
     * Disapara uma mensagem de erro
     * @param $code codigo
     * @param $msg messagem
     */
    function error($code, $msg){
        http_response_code($code);
        die(json_encode(array('error' => $msg )));
    }
}

if(!function_exists('mask')){
    function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++)
        {
            if($mask[$i] == '#')
            {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else
            {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
}

if(!function_exists('generateQrCode')){
    function generateQrCode($qr)
    {
        $aux = base_url('newtheme/') . 'qr_img0.50j/php/qr_img.php?';
        $aux .= 'd=http://192.168.0.18:8080/sissuple//Laudos/Laudo_Pedido_Autorizacao_PDF?codigoLaudoElaboracao=' . $qr . '&';
        $aux .= 'e=H&';
        $aux .= 's=10&';
        $aux .= 't=J';
        return $aux;
    }
}

if(!function_exists('isValidMd5')){
    function isValidMd5($md5 ='')
    {
        return preg_match('/^[a-f0-9]{32}$/', $md5);
    }
}

if(!function_exists('ifHasFileRenameFile')){
    function ifHasFileRenameFile($field)
    {
        if(isset($_FILES[$field]) && !empty($_FILES[$field]['name'])) {
            $ext = substr($_FILES['logo']['name'], -4);
            $_FILES[$field]['name'] = hash('md5', getdate()[0]) . $ext;
            
            return $_FILES[$field]['name'];
        }

        return '';
    }
}

if(!function_exists('ifHasFileSendFileToUpload')){
    function ifHasFileSendFileToUpload($field)
    {
        if(isset($_FILES[$field]) && !empty($_FILES[$field]['tmp_name'])) {
            uploadFile($_FILES['logo'], $_FILES['logo']['name'], "./uploads/logoClinica/");
        }
    }
}

